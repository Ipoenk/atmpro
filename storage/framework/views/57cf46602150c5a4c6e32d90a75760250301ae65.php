
<?php $__env->startSection('content'); ?>    
  
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Nasabah</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
  <form action="/nasabah.edit/<?php echo e($nasabahs->id); ?>" method="POST" class="form-horizontal">
    <?php echo csrf_field(); ?>
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Pribadi</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="niknsb">Nik NSB</label>
              <input type="text" name ="niknsb" id="niknsb" class="form-control" value="<?php echo e($nasabahs->niknsb); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="namansb">Nama Nasabah</label>
              <input type="text" name ="namansb" id="namansb" class="form-control" value="<?php echo e(strtoupper($nasabahs->namansb)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tmplahirnsb">Tempat Lahir</label>
              <input type="text" name ="tmplahirnsb" id="tmplahirnsb" class="form-control" value="<?php echo e(strtoupper($nasabahs->tmplahirnsb)); ?>" disabled>
            </div>
            
            <div class="form-group">
              <label class="col-sm-6 control-label" for="tgllahirnsb">Tanggal Lahir</label>
              <div>
                  <input type="date" class="form-control <?php $__errorArgs = ['tgllahirnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "tgllahirnsb" id="tgllahirnsb" placeholder="Isikan Tanggal Lahir" value="<?php echo e(date('Y-m-d',strtotime($nasabahs->tgllahirnsb))); ?>" disabled/>
                  
                  <?php $__errorArgs = ['tgllahirnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  
            <div class="form-group">
              <label class="col-sm-6 control-label" for="kelaminnsb">Jenis Kelamin</label>
              <div>
                  <select class="form-control <?php $__errorArgs = ['kelaminnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "kelaminnsb" = id="kelaminnsb" disabled>
                      <option value="<?php echo e(strtoupper($nasabahs->kelaminnsb)); ?>"><?php echo e(strtoupper($nasabahs->kelaminnsb)); ?></option>
                      <option value="LAKI-LAKI">LAKI-LAKI</option>
                      <option value="PEREMPUAN">PEREMPUAN</option>
                  </select>
                  <?php $__errorArgs = ['kelaminnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-6 control-label" for="agamansb">Agama</label>
              <div>
              <select class="form-control <?php $__errorArgs = ['agamansb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "agamansb" id="agamansb" disabled>
                  <option value ="<?php echo e(strtoupper($nasabahs->agamansb)); ?>"><?php echo e(strtoupper($nasabahs->agamansb)); ?></option>
                  <option value ="ISLAM">ISLAM</option>
                  <option value ="KATOLIK">KATOLIK</option>
                  <option value ="PROTESTAN">PROTESTAN</option>
                  <option value ="HINDU">HINDU</option>
                  <option value ="BUDHA">BUDHA</option>
                  <option value ="KONGHUCU">KONGHUCU</option>
              </select>
              <?php $__errorArgs = ['agamansb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                  <div class="invalid-feedback"><?php echo e($message); ?></div>
              <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  
            <div class="form-group">
              <label for="alamatnsbktp">Alamat Sesuai KTP</label>
              <input type="text" name ="alamatnsbktp" id="alamatnsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->alamatnsbktp)); ?>" disabled>
            </div>
            <div class="form-group ">
              <label class="col-sm-4 control-label" for="propinsinsbktp">Propinsi Sesuai KTP</label>
              <div >
                  <select name="propinsinsbktp" id="propinsinsbktp" class="form-control <?php $__errorArgs = ['propinsinsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                  <option value="<?php echo e(strtoupper($nasabahs->propinsinsbktp)); ?>"><?php echo e(strtoupper($nasabahs->namaprop)); ?></option>
                  <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                  </select>
                  <?php $__errorArgs = ['propinsinsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
          <div class="form-group ">
            <label class="col-sm-6 control-label" for="kotansbktp">Kota/Kabupaten Sesuai KTP</label>
            <div >
                <select name="kotansbktp" id="kotansbktp" class="form-control <?php $__errorArgs = ['kotansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="<?php echo e(strtoupper($nasabahs->kotansbktp)); ?>"><?php echo e(strtoupper($nasabahs->kotanama)); ?></option>
                </select>
                <?php $__errorArgs = ['kotansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
          <label class="col-sm-6 control-label" for="kecamatannsbktp">Kecamatan Sesuai KTP</label>
          <div >
              <select name="kecamatannsbktp" id="kecamatannsbktp" class="form-control <?php $__errorArgs = ['kecamatannsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
              <option value="<?php echo e(strtoupper($nasabahs->kecamatannsbktp)); ?>"><?php echo e(strtoupper($nasabahs->camatnama)); ?></option>
              </select>
              <?php $__errorArgs = ['kecamatannsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                  <div class="invalid-feedback"><?php echo e($message); ?></div>
              <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
          </div>
      </div>            
      <div class="form-group ">
        <label class="col-sm-6 control-label" for="desansbktp">Desa/Kelurahan Sesuai KTP</label>
        <div >
            <select name="desansbktp" id="desansbktp" class="form-control <?php $__errorArgs = ['desansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
            <option value="<?php echo e(strtoupper($nasabahs->desansbktp)); ?>"><?php echo e(strtoupper($nasabahs->desanama)); ?></option>
            </select>
            <?php $__errorArgs = ['desansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        </div>
      </div>  

      <div class="form-group">
          <label for="deposnsbktp">Kodepos Sesuai KTP</label>
          <input type="text" name ="kodeposnsbktp"  id="kodeposnsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->kodeposnsbktp)); ?>" disabled>
      </div>
      <div class="form-group">
        <label for="notelpnsb">No Telp</label>
        <input type="text" name ="notelpnsb" id="notelpnsb" class="form-control" value="<?php echo e(strtoupper($nasabahs->notelpnsb)); ?>" disabled>
      </div>
      <div class="form-group">
        <label for="emailnsb">Alamat Email</label>
        <input type="text" name ="emailnsb"  id="emailnsb" class="form-control" value="<?php echo e(strtoupper($nasabahs->email)); ?>" disabled>
      </div>
      <div class="form-group">
        <label for="alamatnsbdom">Alamat Domisili</label>
        <input type="text" name ="alamatnsbdom" id="alamatnsbdom" class="form-control" value="<?php echo e(strtoupper($nasabahs->alamatdomisili)); ?>" disabled>
      </div>
      <div class="form-group ">
        <label class="col-sm-6 control-label" for="propinsidomisili">Propinsi Domisili</label>
        <div >
            <select name="propinsidomisili" id="propinsidomisili" class="form-control <?php $__errorArgs = ['propinsidomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
            <option value="<?php echo e(strtoupper($nasabahs->propinsidomisili)); ?>"><?php echo e(strtoupper($nasabahs->propdom)); ?></option>
            <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
            </select>
            <?php $__errorArgs = ['propinsidomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        </div>
    </div>

    <div class="form-group ">
      <label class="col-sm-6 control-label" for="kotadomisili">Kota/Kabupaten Domisili</label>
      <div>
          <select name="kotadomisili" id="kotadomisili" class="form-control <?php $__errorArgs = ['kotadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
          <option value="<?php echo e(strtoupper($nasabahs->kotadomisili)); ?>"><?php echo e(strtoupper($nasabahs->kotadom)); ?></option>
          </select>
      </div>
        <?php $__errorArgs = ['kotadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
          <div class="invalid-feedback"><?php echo e($message); ?></div>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
    </div>

    <div class="form-group ">
      <label class="col-sm-6 control-label" for="kecamatandomisili">Kecamatan Domisili</label>
      <div>
          <select name="kecamatandomisili" id="kecamatandomisili" class="form-control <?php $__errorArgs = ['kecamatandomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
          <option value="<?php echo e(strtoupper($nasabahs->kecamatandomisili)); ?>"><?php echo e(strtoupper($nasabahs->camatdom)); ?></option>
          </select>
          <?php $__errorArgs = ['kecamatandomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
              <div class="invalid-feedback"><?php echo e($message); ?></div>
          <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
      </div>
    </div>
    <div class="form-group ">
      <label class="col-sm-6 control-label" for="desadomisili">Desa/Kelurahan Domisili</label>
      <div>
          <select name="desadomisili" id="desadomisili" class="form-control <?php $__errorArgs = ['desadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
          <option value="<?php echo e(strtoupper($nasabahs->desadomisili)); ?>"><?php echo e(strtoupper($nasabahs->desadom)); ?></option>
          </select>
          <?php $__errorArgs = ['desadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
              <div class="invalid-feedback"><?php echo e($message); ?></div>
          <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
      </div>
    </div>  
      <div class="form-group">
        <label for="notelp">No HP</label>
        <input type="text" name ="notelp" id="notelp" class="form-control" value="<?php echo e(strtoupper($nasabahs->notelpnsb)); ?>" disabled>
      </div>
      <div class="form-group">
        <label class="col-sm-6 control-label" for="statusperkawinan">Status Perkawinan</label>
        <div>
            <select class="form-control <?php $__errorArgs = ['statusperkawinan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "statusperkawinan" id="statusperkawinan" disabled>
                <option value ="<?php echo e(strtoupper($nasabahs->statusperkawinan)); ?>"><?php echo e(strtoupper($nasabahs->statusperkawinan)); ?></option>
                <option value ="BELUM MENIKAH">BELUM MENIKAH</option>
                <option value ="MENIKAH">MENIKAH</option>
                <option value ="CERAI HIDUP">CERAI HIDUP</option>
                <option value ="CERAI MATI">CERAI MATI</option>
            </select>
            <?php $__errorArgs = ['statusperkawinan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        </div>
    </div>

      <div class="form-group">
        <label for="namaibukandung">Nama Ibu Kandung</label>
        <input type="text" name ="namaibukandung" id="namaibukandung" class="form-control" value="<?php echo e(strtoupper($nasabahs->namaibukandung)); ?>" disabled>
      </div>
      
    </div>
    <!-- /.card-body -->
  </div>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Penghasilan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="sumberdana">Sumber Dana</label>
              <input type="text" name ="sumberdana" id="sumberdana" class="form-control" value="<?php echo e(strtoupper($nasabahs->sumberdana)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="pendapatan">Pendapatan</label>
              <input type="text" name ="pendapatan" id="pendapatan" class="form-control" value="<?php echo e($nasabahs->pendapatan); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="pengeluaran">Pengeluaran</label>
              <input type="text" name ="pengeluaran" id="pengeluaran" class="form-control" value="<?php echo e($nasabahs->pengeluaran); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tanggungan">Tanggungan</label>
              <input type="text" name ="tanggungan" id="tanggungan" class="form-control" value="<?php echo e($nasabahs->tanggungan); ?>" disabled>
            </div>
            <div class="form-group">
              <label class="col-sm-6 control-label" for="ststempattinggal">Status Tempat Tinggal</label>
              <div>
                  <select class="form-control <?php $__errorArgs = ['ststempattinggal'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "ststempattinggal" id="ststempattinggal" disabled>
                      <option value = "<?php echo e(strtoupper($nasabahs->ststempattinggal)); ?>"><?php echo e(strtoupper($nasabahs->ststempattinggal)); ?></option>
                      <option value = "MILIK SENDIRI">MILIK SENDIRI</option>
                      <option value = "MILIK ORANG TUA">MILIK ORANG TUA</option>
                      <option value = "RUMAH DINAS">RUMAH DINAS</option>
                      <option value = "SEWA">SEWA</option>
                      <option value = "LAINNYA">LAINNYA</option>
                  </select>
                  <?php $__errorArgs = ['ststempattinggal'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  
          </div>
        </div>
        <!-- /.card -->
      </div>
      <div class="col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Data Pasangan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="nikps">NIK Pasangan</label>
              <input type="text" name ="nikps"  id="nikps" class="form-control" value="<?php echo e(strtoupper($nasabahs->nikps)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="namaps">Nama Pasangan</label>
              <input type="text" name ="namaps" id="namaps" class="form-control" value="<?php echo e(strtoupper($nasabahs->namaps)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tmplahirps">Tempat Lahir Pasangan</label>
              <input type="text" name ="tmplahirps" id="tmplahirps" class="form-control" value="<?php echo e(strtoupper($nasabahs->tmplahirps)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tgllahirps">Tanggal Lahir Pasangan</label>
              <input type="date" name ="tgllahirps" id="tgllahirps" class="form-control" value="<?php echo e(date('Y-m-d',strtotime($nasabahs->tgllahirps))); ?>" disabled>
            </div>
            
            <div class="form-group">
              <label class="col-sm-6 control-label" for="agamaps">Agama</label>
              <div>
              <select class="form-control <?php $__errorArgs = ['agamaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "agamaps" id="agamaps" disabled>
                  <option value ="<?php echo e(strtoupper($nasabahs->agamaps)); ?>"><?php echo e(strtoupper($nasabahs->agamaps)); ?></option>
                  <option value ="ISLAM">ISLAM</option>
                  <option value ="KATOLIK">KATOLIK</option>
                  <option value ="PROTESTAN">PROTESTAN</option>
                  <option value ="HINDU">HINDU</option>
                  <option value ="BUDHA">BUDHA</option>
                  <option value ="KONGHUCU">KONGHUCU</option>
              </select>
              <?php $__errorArgs = ['agamaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                  <div class="invalid-feedback"><?php echo e($message); ?></div>
              <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
          </div>
        </div>            
            <div class="form-group">
              <label for="alamatps">Alamat Pasangan</label>
              <input type="text" name ="alamatps" id="alamatps" class="form-control" value="<?php echo e(strtoupper($nasabahs->alamatps)); ?>" disabled>
            </div>
            <div class="form-group ">
              <label class="col-sm-6 control-label" for="propinsips">Propinsi</label>
              <div>
                  <select name="propinsips" id="propinsips" class="form-control <?php $__errorArgs = ['propinsips'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                  <option value="<?php echo e(strtoupper($nasabahs->propinsips)); ?>"><?php echo e(strtoupper($nasabahs->propps)); ?></option>
                  <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                  </select>
                  <?php $__errorArgs = ['propinsips'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  
            <div class="form-group ">
              <label class="col-sm-6 control-label" for="kotaps">Kota/Kabupaten</label>
              <div>
                  <select name="kotaps" id="kotaps" class="form-control <?php $__errorArgs = ['kotaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                  <option value="<?php echo e(strtoupper($nasabahs->kotaps)); ?>"><?php echo e(strtoupper($nasabahs->namakotaps)); ?></option>
                  </select>
                  <?php $__errorArgs = ['kotaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  
          <div class="form-group ">
              <label class="col-sm-6 control-label" for="kecamatanps">Kecamatan</label>
              <div>
                  <select name="kecamatanps" id="kecamatanps" class="form-control <?php $__errorArgs = ['kecamatanps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                  <option value="<?php echo e(strtoupper($nasabahs->kecamatanps)); ?>"><?php echo e(strtoupper($nasabahs->namacamatps)); ?></option>
                  </select>
                  <?php $__errorArgs = ['kecamatanps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  
            <div class="form-group ">
              <label class="col-sm-6 control-label" for="desaps">Desa/Kelurahan</label>
              <div>
                  <select name="desaps" id="desaps" class="form-control <?php $__errorArgs = ['desaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                  <option value="<?php echo e(strtoupper($nasabahs->desaps)); ?>"><?php echo e(strtoupper($nasabahs->namadesaps)); ?></option>
                  </select>
                  <?php $__errorArgs = ['desaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
            </div>

            <div class="form-group">
              <label for="kodeposps">Kodepos </label>
              <input type="text" name ="kodeposps"  id="kodeposps" class="form-control <?php $__errorArgs = ['kodeposps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(strtoupper($nasabahs->kodeposps)); ?>" disabled>
              <?php $__errorArgs = ['kodeposps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
              <div class="invalid-feedback"><?php echo e($message); ?></div>
              <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
    
            <div class="form-group">
              <label for="notelpps">No HP Pasangan</label>
              <input type="text" name ="notelpps"  id="notelpps" class="form-control" value="<?php echo e(strtoupper($nasabahs->notelpps)); ?>" disabled>
            </div>
            <div class="form-group">
              <label class="col-sm-6 control-label" for="kelaminps">Jenis Kelamin</label>
              <div>
                  <select name="kelaminps" id="kelaminps" class="form-control  <?php $__errorArgs = ['kelaminps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                      <option value ="<?php echo e(strtoupper($nasabahs->kelaminps)); ?>"><?php echo e(strtoupper($nasabahs->kelaminps)); ?></option>
                      <option value ="LAKI-LAKI">LAKI-LAKI</option>
                      <option value ="PEREMPUAN">PEREMPUAN</option>
                  </select>
                  <?php $__errorArgs = ['kelaminps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
              </div>
          </div>
  

    </div>
              <!-- /.card-body -->
  </div>
        <!-- /.card -->
        
        <!-- /.card -->
      
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Data Pekerjaan</h3>
    
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                
                <div class="form-group">
                  <label class="col-sm-6 control-label" for="jenispsh">Jenis Perusahaan</label>
                  <div>
                      <select class="form-control <?php $__errorArgs = ['jenispsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "jenispsh" id="jenispsh" disabled>
                          <option value ="<?php echo e(strtoupper($nasabahs->jenispsh)); ?>"><?php echo e(strtoupper($nasabahs->jenispsh)); ?></option>
                          <option value ="PNS">PNS</option>
                          <option value ="TNI-POLRI">TNI-POLRI</option>
                          <option value ="PERSERO">PERSERO</option>
                          <option value ="PERUSAHAAN DAERAH">PERUSAHAAN DAERAH</option>
                          <option value ="PT">PT</option>
                          <option value ="CV">CV</option>
                          <option value ="FIRMA">FIRMA</option>
                          <option value ="KOPERASI">KOPERASI</option>
                          <option value ="YAYASAN">YAYASAN</option>
                          <option value ="LAIN-LAIN">LAIN-LAIN</option>
                      </select>
                      <?php $__errorArgs = ['jenispsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                       <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
              </div>
      
                <div class="form-group">
                  <label for="namapsh">Nama Perusahaan</label>
                  <input type="text" name ="namapsh" id="namapsh" class="form-control" value="<?php echo e(strtoupper($nasabahs->namapsh)); ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="alamatpsh">Alamat Perusahaan</label>
                  <input type="text" name ="alamatpsh" id="alamatpsh" class="form-control" value="<?php echo e(strtoupper($nasabahs->alamatpsh)); ?>" disabled>
                </div>
                
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="propinsipsh">Propinsi</label>
                  <div>
                      <select name="propinsipsh" id="propinsipsh" class="form-control <?php $__errorArgs = ['propinsipsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                      <option value="<?php echo e(strtoupper($nasabahs->propinsipsh)); ?>"><?php echo e(strtoupper($nasabahs->proppsh)); ?></option>
                      <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                      </select>
                      <?php $__errorArgs = ['propinsipsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                       <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
              </div>
      
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="kotapsh">Kota/Kabupaten</label>
                  <div>
                      <select name="kotapsh" id="kotapsh" class="form-control <?php $__errorArgs = ['kotapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                      <option value="<?php echo e(strtoupper($nasabahs->kotapsh)); ?>"><?php echo e(strtoupper($nasabahs->namakotapsh)); ?></option>
                      </select>
                      <?php $__errorArgs = ['kotapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                       <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
              </div>
      
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="kecamatanpsh">Kecamatan</label>
                  <div>
                      <select name="kecamatanpsh" id="kecamatanpsh" class="form-control <?php $__errorArgs = ['kecamatanpsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                      <option value="<?php echo e(strtoupper($nasabahs->kecamatanpsh)); ?>"><?php echo e(strtoupper($nasabahs->namacamatpsh)); ?></option>
                      </select>
                      <?php $__errorArgs = ['kecamatanpsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                       <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
              </div>
      
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="desapsh">Desa/Kelurahan</label>
                  <div>
                      <select name="desapsh" id="desapsh" class="form-control <?php $__errorArgs = ['desapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                      <option value="<?php echo e(strtoupper($nasabahs->desapsh)); ?>"><?php echo e(strtoupper($nasabahs->namadesapsh)); ?></option>
                      </select>
                      <?php $__errorArgs = ['desapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                       <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
              </div>
      
                <div class="form-group">
                  <label for="kodepospsh">Kodepos Perusahaan</label>
                  <input type="text" name ="kodepospsh" id="kodepospsh" class="form-control" value="<?php echo e(strtoupper($nasabahs->kodepospsh)); ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="notelppsh">No Telp Perusahaan</label>
                  <input type="text" name ="notelppsh" id="notelppsh" class="form-control" value="<?php echo e(strtoupper($nasabahs->notelppsh)); ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="bidangusaha">Bidang Usaha</label>
                  <input type="text" name ="bidangusaha" id="bidangusaha" class="form-control" value="<?php echo e(strtoupper($nasabahs->bidangusaha)); ?>" disabled>
                </div>
              </div>
    
          <!-- /.card-body -->
            </div>
        </div>
      </div>   
    <div class="row">
      <div class="col-12">
        <a type="button" href="<?php echo e(route('nasabah')); ?>" name = "kembali" id ="kembali" class="btn btn-primary col-1">Kembali</a>
        <a type="button" name = "edit" id ="edit" class="btn btn-secondary col-1" onclick="hidup()">Edit</a>
        <button type="submit" name = "update" id ="update" class="btn btn-success col-1" hidden>Simpan</button>
        <a type="button" name = "batal" id ="batal" class="btn btn-warning col-1" onclick="mati()" hidden>Batal</a>

      </div>
    </div>
  </form>
  </section>
  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <script>
    function hidup(){
      $('#niknsb').attr('disabled',false);
      $('#namansb').attr('disabled',false);
      $('#tmplahirnsb').attr('disabled',false);
      $('#tgllahirnsb').attr('disabled',false);
      $('#kelaminnsb').attr('disabled',false);
      $('#agamansb').attr('disabled',false);
      $('#alamatnsbktp').attr('disabled',false);
      $('#propinsinsbktp').attr('disabled',false);
      $('#kotansbktp').attr('disabled',false);
      $('#kecamatannsbktp').attr('disabled',false);
      $('#desansbktp').attr('disabled',false);
      $('#kodeposnsbktp').attr('disabled',false);
      $('#notelpnsb').attr('disabled',false);
      $('#emailnsb').attr('disabled',false);
      $('#alamatnsbdom').attr('disabled',false);
      $('#propinsidomisili').attr('disabled',false);
      $('#kotadomisili').attr('disabled',false);
      $('#kecamatandomisili').attr('disabled',false);
      $('#desadomisili').attr('disabled',false);
      $('#notelp').attr('disabled',false);
      $('#email').attr('disabled',false);
      $('#statusperkawinan').attr('disabled',false);
      $('#namaibukandung').attr('disabled',false);
      $('#sumberdana').attr('disabled',false);
      $('#pendapatan').attr('disabled',false);
      $('#pengeluaran').attr('disabled',false);
      $('#tanggungan').attr('disabled',false);
      $('#ststempattinggal').attr('disabled',false);
      $('#nikps').attr('disabled',false);
      $('#namaps').attr('disabled',false);
      $('#tmplahirps').attr('disabled',false);
      $('#tgllahirps').attr('disabled',false);
      $('#agamaps').attr('disabled',false);
      $('#alamatps').attr('disabled',false);
      $('#propinsips').attr('disabled',false);
      $('#kotaps').attr('disabled',false);
      $('#kecamatanps').attr('disabled',false);
      $('#desaps').attr('disabled',false);
      $('#kodeposps').attr('disabled',false);
      $('#notelpps').attr('disabled',false);
      $('#kelaminps').attr('disabled',false);
      $('#jenispsh').attr('disabled',false);
      $('#namapsh').attr('disabled',false);
      $('#alamatpsh').attr('disabled',false);
      $('#propinsipsh').attr('disabled',false);
      $('#kotapsh').attr('disabled',false);
      $('#kecamatanpsh').attr('disabled',false);
      $('#desapsh').attr('disabled',false);
      $('#kodepospsh').attr('disabled',false);
      $('#notelppsh').attr('disabled',false);
      $('#bidangusaha').attr('disabled',false);
      $('#update').attr('hidden',false);
      $('#batal').attr('hidden',false);
      $('#edit').attr('hidden',true);
    }
    function mati(){
      $('#namansb').attr('disabled',true);
      $('#tmplahirnsb').attr('disabled',true);
      $('#tgllahirnsb').attr('disabled',true);
      $('#kelaminnsb').attr('disabled',true);
      $('#agamansb').attr('disabled',true);
      $('#alamatnsbktp').attr('disabled',true);
      $('#propinsinsbktp').attr('disabled',true);
      $('#kotansbktp').attr('disabled',true);
      $('#kecamatannsbktp').attr('disabled',true);
      $('#desansbktp').attr('disabled',true);
      $('#kodeposnsbktp').attr('disabled',true);
      $('#notelpnsb').attr('disabled',true);
      $('#emailnsb').attr('disabled',true);
      $('#alamatnsbdom').attr('disabled',true);
      $('#propinsidomisili').attr('disabled',true);
      $('#kotadomisili').attr('disabled',true);
      $('#kecamatandomisili').attr('disabled',true);
      $('#desadomisili').attr('disabled',true);
      $('#notelp').attr('disabled',true);
      $('#email').attr('disabled',true);
      $('#statusperkawinan').attr('disabled',true);
      $('#namaibukandung').attr('disabled',true);
      $('#sumberdana').attr('disabled',true);
      $('#pendapatan').attr('disabled',true);
      $('#pengeluaran').attr('disabled',true);
      $('#tanggungan').attr('disabled',true);
      $('#ststempattinggal').attr('disabled',true);
      $('#nikps').attr('disabled',true);
      $('#namaps').attr('disabled',true);
      $('#tmplahirps').attr('disabled',true);
      $('#tgllahirps').attr('disabled',true);
      $('#agamaps').attr('disabled',true);
      $('#alamatps').attr('disabled',true);
      $('#propinsips').attr('disabled',true);
      $('#kotaps').attr('disabled',true);
      $('#kecamatanps').attr('disabled',true);
      $('#desaps').attr('disabled',true);
      $('#notelpps').attr('disabled',true);
      $('#kelaminps').attr('disabled',true);
      $('#jenispsh').attr('disabled',true);
      $('#namapsh').attr('disabled',true);
      $('#alamatpsh').attr('disabled',true);
      $('#propinsipsh').attr('disabled',true);
      $('#kotapsh').attr('disabled',true);
      $('#kecamatanpsh').attr('disabled',true);
      $('#desapsh').attr('disabled',true);
      $('#kodepospsh').attr('disabled',true);
      $('#notelppsh').attr('disabled',true);
      $('#bidangusaha').attr('disabled',true);
      $('#update').attr('hidden',true);
      $('#edit').attr('hidden',false);
      $('#batal').attr('hidden',true);
    }

    $("#propinsinsbktp").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotansbktp').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotansbktp').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kotansbktp").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatannsbktp').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatannsbktp').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatannsbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatannsbktp").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    console.log(data);
                    $('#desansbktp').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desansbktp').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#propinsidomisili").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotadomisili').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotadomisili').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kotadomisili").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatandomisili').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatandomisili').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatandomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatandomisili").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desadomisili').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desadomisili').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#propinsips").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotaps').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotaps').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotaps').attr('disabled',false);
                }
            });
        });
        
        $("#kotaps").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanps').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanps').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanps').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanps").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desaps').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desaps').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desaps').attr('disabled',false);
                }
            });
        });
        
        $("#propinsipsh").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotapsh').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotapsh').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotapsh').attr('disabled',false);
                }
            });
        });
        
        $("#kotapsh").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanpsh').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanpsh').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanpsh').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanpsh").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desapsh').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desapsh').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desapsh').attr('disabled',false);
                }
            });
        });

  </script>
  <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/nasabah/detail.blade.php ENDPATH**/ ?>