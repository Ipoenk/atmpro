
<?php $__env->startSection('content'); ?>  
<div class="container">
   

	<a name="top" id="top"></a>
	<div class="fw-background">
		<div></div>
	</div>
		
		
		<table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%"> 
			<thead>
				<tr>
					<th>No</th>
					<th>No. KTP</th>
					<th>Nama</th>
					<th>Alamat</th>
					<th>Tmp.Lahir</th>
					<th>Tgl.Lahir</th>
					<th>Pekerjaan</th>
					<th>Nama Ibu</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $datanasabahs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datanasabahs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				 

				<tr>
					<td style="width:10%" name=<?php echo e($datanasabahs->id); ?> id="nomor"><?php echo e($loop->iteration); ?></td>
					<td style="width:10%"><?php echo e($datanasabahs->niknsb); ?></td>
					<td style="width:15%"><?php echo e(strtoupper($datanasabahs->namansb)); ?></td>
					<td ><?php echo e(strtoupper($datanasabahs->alamatnsbktp)); ?></td>
					<td style="width:8%"><?php echo e(strtoupper($datanasabahs->tmplahirnsb)); ?></td>
					<td style="width:8%"><?php echo e(date('d-m-Y',strtotime($datanasabahs->tgllahirnsb))); ?></td>
					<td style="width:8%"><?php echo e(strtoupper($datanasabahs->jenispsh)); ?></td>
					<td style="width:8%"><?php echo e(strtoupper($datanasabahs->namaibukandung)); ?></td>
					
				</tr>                               
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

			</tbody>
			<tfoot>
				<th>No</th>
				<th>No. KTP</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tmp.Lahir</th>
				<th>Tgl.Lahir</th>
				<th>Pekerjaan</th>
				<th>Nama Ibu</th>
			</tfoot>
		</table>
		<a href=""></a>
		<a type="button" href="<?php echo e(route('nasabah.create')); ?>" class="btn btn-primary col-1">+ Add</a>
		<a type="button" href="/nasabah.detail/"onclick = "nomor()" class="btn btn-secondary col-1">Detail</a>
		<a type="button" href="/kredit.create/<?php echo e($datanasabahs->id); ?>" class="btn btn-success col-1">Kredit</a>
		<a type="button" class="btn btn-danger col-1">Tabungan</a>
		<a type="button" class="btn btn-warning col-1">Deposito</a>
		
</div>	


<?php $__env->stopSection(); ?>
<script type="text/javascript">
				  var _gaq = _gaq || [];
				  _gaq.push(['_setAccount', 'UA-365466-5']);
				  _gaq.push(['_trackPageview']);

				  (function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				  })();

	function nomor(){
		$('#name').attr('value');	
	} 
    	  
    </script>




<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Project_ATM\atmpro\resources\views/nasabah/daftarnasabah.blade.php ENDPATH**/ ?>