
<?php $__env->startSection('content'); ?>
<section class="content">
    <div>
        <h2 align="center">DAFTAR USER</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    Log::info($users); 
                ?>    
                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td><?php echo e(strtoupper($users->name)); ?></td>
                    <td><?php echo e(strtoupper($users->level)); ?></td>
                    <td><?php echo e(strtoupper($users->email)); ?></td>
                    <td><?php echo e(($users->password)); ?></td>
                    <td>
                        <div class="timeline-footer">
                        <a href="/register.detail/<?php echo e($users->id); ?>" class="btn btn-info btn-sm">Lihat Detail</a>
                    </div>
                    </td>
                </tr>                               
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

            </tbody>
        </table>
		<a type="button" href="<?php echo e(route('register')); ?>" class="btn btn-primary col-1">+ Add</a>
    </div>	
</section>
<?php $__env->stopSection(); ?>    
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Project_ATM\proatm\resources\views/auth/daftaruser.blade.php ENDPATH**/ ?>