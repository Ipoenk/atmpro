
<?php $__env->startSection('content'); ?>    
  
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Nasabah</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Nasabah Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form action="<?php echo e(route('kredit.save')); ?>" method="post" class="form-horizontal">
      <?php echo e(csrf_field()); ?>

        <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Pribadi</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="niknsb">Nik NSB</label>
              <input type="text" name ="niknsb" id="niknsb" class="form-control" value="<?php echo e($nasabahs->niknsb); ?>" >
            </div>
            <div class="form-group">
              <label for="namansb">Nama Nasabah</label>
              <input type="text" name ="namansb" id="namansb" class="form-control" value="<?php echo e(strtoupper($nasabahs->namansb)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tmplahirnsb">Tempat Lahir</label>
              <input type="text" name="tmplahirnsb" id="tmplahirnsb" class="form-control" value="<?php echo e(strtoupper($nasabahs->tmplahirnsb)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tgllahirnsb">Tanggal Lahir</label>
              <input type="text" name ="tgllahirnsb" id="tgllahirnsb" class="form-control" value="<?php echo e(date('d-m-Y',strtotime($nasabahs->tgllahirnsb))); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kelaminnsb">Kelamin</label>
              <input type="text" name ="kelaminnsb" id="kelaminnsb" class="form-control" value="<?php echo e(strtoupper($nasabahs->kelaminnsb)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="agamansb">Agama</label>
              <input type="text" name ="agamansb" id="agamansb" class="form-control" value="<?php echo e(strtoupper($nasabahs->agamansb)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="alamatnsbktp">Alamat Sesuai KTP</label>
              <input type="text" name ="alamatnsbktp" id="alamatnsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->alamatnsbktp)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="provinsinsbktp">Provinsi Sesuai KTP</label>
              <input type="text" name ="provinsinsbktp" id="provinsinsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->namaprop)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kabupatennsbktp">Kabupaten Sesuai KTP</label>
              <input type="text" name ="kabupatennsbktp" id="kabupatennsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->kotanama)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kecamatannsbktp">Kecamatan Sesuai KTP</label>
              <input type="text" name ="kecamatannsbktp" id="kecamatannsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->camatnama)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kelurahannsbktp">Kelurahan Sesuai KTP</label>
              <input type="text" name ="kelurahannsbktp" id="kelurahannsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->desanama)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="deposnsbktp">Kodepos Sesuai KTP</label>
              <input type="text" name ="kodeposnsbktp" id="kodeposnsbktp" class="form-control" value="<?php echo e(strtoupper($nasabahs->kodeposnsbktp)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="alamatnsbdom">Alamat Domisili</label>
              <input type="text" name ="alamatnsbdom" id="alamatnsbdom" class="form-control" value="<?php echo e(strtoupper($nasabahs->alamatdomisili)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="provinsidomisili">Provinsi Domisili</label>
              <input type="text" name ="provinsidomisili" id="provinsidomisili" class="form-control" value="<?php echo e(strtoupper($nasabahs->propdom)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kabupatendomisili">Kabupaten Domisili</label>
              <input type="text" name ="kabupatendomisili" id="kabupatendomisili" class="form-control" value="<?php echo e(strtoupper($nasabahs->kotadom)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kecamatandomisili">Kecamatan Domisili</label>
              <input type="text" name ="kecamatandomisili" id="kecamatandomisili" class="form-control" value="<?php echo e(strtoupper($nasabahs->camatdom)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="kelurahandomisili">Kelurahan Domisili</label>
              <input type="text" name ="kelurahandomisili" id="kelurahandomisili" class="form-control" value="<?php echo e(strtoupper($nasabahs->desadom)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="notelp">No HP</label>
              <input type="text" name ="notelp" id="notelp" class="form-control" value="<?php echo e(strtoupper($nasabahs->notelpnsb)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="email">Alamat Email</label>
              <input type="text" name ="email" id="email" class="form-control" value="<?php echo e(strtoupper($nasabahs->email)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="statusperkawinan">Status Pernikahan</label>
              <input type="text" name ="statusperkawinan" id="statusperkawinan" class="form-control" value="<?php echo e(strtoupper($nasabahs->statusperkawinan)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="namaibukandung">Nama Ibu Kandung</label>
              <input type="text" name = "namaibukandung" id="namaibukandung" class="form-control" value="<?php echo e(strtoupper($nasabahs->namaibukandung)); ?>" disabled>
            </div>
            
          </div>
          <!-- /.card-body -->
        </div>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Penghasilan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="sumberdana">Sumber Dana</label>
              <input type="text" name ="sumberdana" id="sumberdana" class="form-control" value="<?php echo e(strtoupper($nasabahs->sumberdana)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="pendapatan">Pendapatan</label>
              <input type="text" name = "pendapatan" id="pendapatan" class="form-control" value="<?php echo e(number_format($nasabahs->pendapatan)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="pengeluaran">Pengeluaran</label>
              <input type="text" name ="pengeluaran" id="pengeluaran" class="form-control" value="<?php echo e(number_format($nasabahs->pengeluaran)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="tanggungan">Tanggungan</label>
              <input type="text" name ="tanggungan" id="tanggungan" class="form-control" value="<?php echo e(number_format($nasabahs->tanggungan)); ?>" disabled>
            </div>
            <div class="form-group">
              <label for="stsrumah">Status Tempat Tinggal</label>
              <input type="text" name ="stsrumah" id="stsrumah" class="form-control" value="<?php echo e(strtoupper($nasabahs->ststempattinggal)); ?>" disabled>
            </div>

          </div>
        </div>
        <!-- /.card -->
      </div>
      <div class="form-group col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Data Pengajuan Kredit</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group col-sm-8 control-label">
              <label for="no_kredit">Nomor Kredit</label>
              <input type="text" name = "no_kredit" id="no_kredit" style="text-transform:uppercase" class="form-control <?php $__errorArgs = ['no_kredit'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e($no_kredit); ?>" >
              <?php $__errorArgs = ['no_kredit'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
              <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
             <small id="no_kredit" class="form-text text-muted">Maksimum 16 karakter</small>

            </div>
            <div class="form-group col-sm-6 control-label">
              <label for="tglkredit">Tanggal Pengajuan</label>
              <input type="date" name = "tglkredit" id="tglkredit"  value="<?php echo date('Y-m-d'); ?>" class="form-control"  >
            </div>
            <div class="form-group col-sm-5 control-label">
              <label for="pinj_pokok">Pokok Pengajuan</label>
              <input type="number" name ="pinj_pokok" id="pinj_pokok" class="form-control" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type = "currency" value="<?php echo e(old('pinj_pokok')); ?>">
            </div>
            <div class="form-group col-sm-5">
              <label for="skbunga">Suku Bunga</label>
              <input type="text" name = "skbunga" id="skbunga" class="form-control" value="<?php echo e(old('skbunga')); ?>">
            </div>
            
              <div class="form-group col-sm-4 control-label">
                <label for="sistem">Sistem</label>
                <select class="form-control" name = "sistem" id="sistem" value="<?php echo e(old('sistem')); ?>">
                    <option value = "FLAT">FLAT</option>
                    <option value = "BUNGA-BUNGA">BUNGA-BUNGA</option>
                    <option value = "MUSIMAN">MUSIMAN</option>
                </select>
              </div>
            
            <div class="form-group col-sm-5 control-label">
              <label for="lama">Lama Kredit</label>
              <input type="number" name = "lama" id="lama" class="form-control" onblur="tampilangsuran()" value="<?php echo e(old('lama')); ?>">
            </div>
            <div class="form-group col-sm-6">
              <label for="jatuhtempo">Jatuh Tempo</label>
              <input type="date" name = "jatuhtempo" id="jatuhtempo" class="form-control" value="<?php echo e(old('jatuhtempo')); ?>">
            </div>
            <div class="form-group col-sm-6">
              <label for="tglmulai">Angsuran Awal</label>
              <input type="date" name = "tglmulai" id="tglmulai" class="form-control" value="<?php echo e(old('tglmulai')); ?>">
            </div>
            <div class="form-group col-sm-6">
              <label for="tglakhir">Angsuran Akhir</label>
              <input type="date" name ="tglakhir"  id="tglakhir" class="form-control" value="<?php echo e(old('tglakhir')); ?>">
            </div>
            
              <div class="form-group col-sm-4 control-label">
                <label for="namaao">Nama AO</label>
                <select class="form-control" name = "namaao" id="namaao" value="<?php echo e(old('namaao')); ?>">
                    <option value = "KANTOR">KANTOR</option>
                    <option value = "WINDO">WINDO</option>
                    <option value = "ALIP">ALIP</option>
                </select>
              </div>
            
            <div class="form-group control-label">
              <label for="perantara">Nama Perantara</label>
              <input type="text" name ="perantara"  id="perantara" class="form-control" value="<?php echo e(old('perantara')); ?>">
            </div>

          </div>
                    <!-- /.card-body -->
        </div>
        <!-- /.card -->
        
        <!-- /.card -->
      
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Perhitungan Kredit</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group col-sm-5">
              <label for="saldopiutang">Saldo Piutang</label>
              <input type="number" name ="saldopiutang" id="saldopiutang" class="form-control" value="<?php echo e(0); ?>" >
            </div>
            <div class="form-group col-sm-5">
              <label for="bbt">Saldo BBT</label>
              <input type="number" name ="bbt" id="bbt" class="form-control" value="<?php echo e(0); ?>" >
            </div>
            <div class="form-group col-sm-5">
              <label for="totangsuran">Angsuran</label>
              <input type="number" name = "totangsuran" id="totangsuran" class="form-control" value="<?php echo e(0); ?>" >
            </div>
            <div class="form-group col-sm-5">
              <label for="angs_pokok">Angsuran Pokok</label>
              <input type="number" name ="angs_pokok" id="angs_pokok" class="form-control" value="<?php echo e(0); ?>" >
            </div>
            <div class="form-group col-sm-5">
              <label for="angs_bunga">Angsuran Bunga</label>
              <input type="number" name ="angs_bunga" id="angs_bunga" class="form-control" value="<?php echo e(0); ?>" >
            </div>
          </div>
    
          <!-- /.card-body -->
          </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Biaya-Biaya Kredit</h3>
    
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group col-sm-5">
                  <label for="nom_provisi">Provisi</label>
                  <input type="number" name ="nom_provisi"  id="nom_provisi" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="nom_admin">Administrasi</label>
                  <input type="number" name ="nom_admin" id="nom_admin" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="notaris">Biaya Notaris</label>
                  <input type="number" name =  "notaris" id="notaris" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="materai">Materai</label>
                  <input type="number" name ="materai" id="materai" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="assjiwa">Asuransi Jiwa</label>
                  <input type="number" name ="assjiwa" id="assjiwa" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="asuransi">Asuransi Jaminan</label>
                  <input type="number" name ="asuransi" id="asuransi" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="sim_ang">Simpanan Pokok</label>
                  <input type="number" name ="sim_ang" id="sim_ang" class="form-control" value="<?php echo e(0); ?>">
                </div>
                <div class="form-group col-sm-5">
                  <label for="bitrans">Biaya Lain-Lain</label>
                  <input type="number" name ="bitrans" id="bitrans" class="form-control" value="<?php echo e(0); ?>">
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            
        </div>
      </div>   
      <h2>Tabel Angsuran</h2>
      
    <div id = "viewangsuran" name = "viewangsuran">
        <table class="table table-stripped" id = "tabelAngsuran" name = "tabelAngsuran" border="2">
          <tr>
            <th>Tanggal Angsur</th>
            <th>Ke</th>
            <th>Bayar Pokok</th>
            <th>Bayar Bunga</th>
            <th>Bakidebet</th>  
            <th>BBT</th>  
            <th>Saldo Piutang</th>  
          </tr> 
          
        </table>
      </div>
    <div class="row">
        <div class="col-12">
          <a href="<?php echo e(route('nasabah')); ?>" onClick="return confirm('Apakah Anda benar-benar akan kembali?')"class="btn btn-secondary">Kembali</a>
          <button class="btn btn-success float-right" onClick="return confirm('Apakah data pengajuan sudah benar Kredit?')" type="submit">Simpan</button>
          
        </div>
      </div>
    </form>
  </section>
  <!-- jQuery -->
  <script src="<?php echo e(asset('/adminlte')); ?>/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo e(asset('/adminlte')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo e(asset('/adminlte')); ?>/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo e(asset('/adminlte')); ?>/js/demo.js"></script>
   <script src="<?php echo e(asset('/js/currency.js')); ?>"></script> 
  <script scr="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script type="text/javascript">
       $(document).ready(function(){
        $(".form-group").focusout(function(){
        // import * as moment from 'moment'
        var apinpokok = $("#pinj_pokok").val()
        var abbt = parseFloat($("#bbt").val())
        var xpinjpokok = parseFloat($("#pinj_pokok").val())
        spinjpokok = parseInt(apinpokok.replace(',','',apinpokok))
        var xprsbunga  = parseFloat($("#skbunga").val())
        var xlama = parseInt($("#lama").val())

        var xangspokok = (spinjpokok) / xlama
        xangpokokfix = xangspokok.toFixed(0)
        $('#angs_pokok').attr("value",parseInt(xangpokokfix))

        var xangbunga = (spinjpokok) * (xprsbunga/100)
        xangsbungafix = xangbunga.toFixed(0) 
        $('#angs_bunga').attr("value",parseInt(xangsbungafix))
        
        var xtotangsur = parseInt(xangpokokfix) + parseInt(xangsbungafix)
        $('#totangsuran').attr("value",xtotangsur)

        var xbbt = (spinjpokok) * (xprsbunga/100) * xlama
        xbbtfix = xbbt.toFixed(0)  
        $('#bbt').attr("value",xbbtfix)
        
        var xsaldopiutang = (spinjpokok) + parseInt(xbbtfix)
        $('#saldopiutang').attr("value",xsaldopiutang)

        // var currentDate = moment(#tglkredit).format('DD-MM-YYYY');
        function add_months(dt, n) 
        {
            return new Date(dt.setMonth(dt.getMonth() + n))      
        }
        var xtglaju = $("#tglkredit").val()
        // console.log(xtglaju)
        //  var  xday = moment().add(7, 'months');
        //  console.log(xday)
        // var d = new Date(xtglaju);
        // var n = d.getMonth();
        // console.log(n)
        // var v = new Date(xtglaju, date('d-m-Y'));
        // ztanggal = add_months(xtglaju,xlama)
        // console.log(xtglaju)

        // var futureMonth = moment(xtglaju).add(xlama, 'M');
        // $('#jatuhtempo').attr("value",futureMonth)
        // console.log(futureMonth )


        // var xtglkredit = $("#tglkredit").val()
        // var xhariini = xtglkredit.getDate();
        // var xlama = parseInt($("#lama").val())
        
        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }        
        // function tampilangsuran() {
        //   console.log(xlama)
          // document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
          $('#tabelAngsuran').empty();
          // document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
          //   <tr>
          //         <th>`+'Tanggal Angsuran' +`</th>
          //         <th>`+'Ke+'`</th>
          //         <th>`+'Bayar Pokok'+`</th>
          //         <th>`+'Bayar Bunga'+`</th>
          //         <th>`+'Bakidebet'+`</th>
          //         <th>`+'Saldo BBT'+`</th>
          //         <th>`+'Saldo Piutang'+`</th>
          //   </tr>`

          if(xlama > 0 && xpinjpokok > 0 && xprsbunga > 0 && abbt > 0){
            document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
            <tr>
                  <td>`+ $("#tglkredit").val() +`</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>`+ formatNumber($("#pinj_pokok").val()) +`</td>
                  <td>`+ formatNumber($("#bbt").val()) +`</td>
                  <td>`+ formatNumber($("#saldopiutang").val()) +`</td>
            </tr>`
            vbaki = xpinjpokok
            vbbt = abbt
            vsaldopiut = xsaldopiutang
            for( i = 1; i < xlama; i++){
                vbaki = vbaki - parseFloat($("#angs_pokok").val())
                vbbt = vbbt - parseFloat($("#angs_bunga").val())
                vsaldopiut = vsaldopiut - (parseFloat($("#angs_pokok").val()) + parseFloat($("#angs_bunga").val()))
                document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
                <tr>
                    <td>`+ $("#tglkredit").val() +`</td>
                    <td>`+i+`</td>
                    <td>`+ formatNumber($("#angs_pokok").val()) +`</td>
                    <td>`+ formatNumber($("#angs_bunga").val()) +`</td>
                    <td>`+ formatNumber(vbaki) +`</td>
                    <td>`+ formatNumber(vbbt) +`</td>
                    <td>`+ formatNumber(vsaldopiut) +`</td>                
                </tr>`
              }
            xangspokokakhir = xpinjpokok -  (parseFloat($("#angs_pokok").val()) * (xlama - 1))
            xangsbungaakhir = abbt - (parseFloat($("#angs_bunga").val()) * (xlama - 1))
            document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
            <tr>
                  <td>`+ $("#tglkredit").val() +`</td>
                  <td>`+xlama+`</td>
                  <td>`+formatNumber(xangspokokakhir)+`</td>
                  <td>`+formatNumber(xangsbungaakhir)+`</td>
                  <td>`+ 0 +`</td>
                  <td>`+ 0 +`</td>
                  <td>`+ 0 +`</td>
            </tr>`
          // }
        };  

 
        
        // var ztanggal = moment().add(2, 'months')
        // console.log(ztanggal)


        // var xjatuhtempo = add_months(xtglaju,xlama)
        // console.log(xjatuhtempo)
      //   $("input[data-type='currency']").on({keyup: function() {
      //         formatCurrency($(this));
      //     },
      //         blur: function() { 
      //       formatCurrency($(this), "blur");
      // }
      //   });


      //   function formatNumber(n) {
      //     // format number 1000000 to 1,234,567
      //     return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      //   }


      // function formatCurrency(input, blur) {
      //   // appends $ to value, validates decimal side
      //   // and puts cursor back in right position.
        
      //   // get input value
      //   var input_val = input.val();
        
      //   // don't validate empty input
      //   if (input_val === "") { return; }
        
      //   // original length
      //   var original_len = input_val.length;

      //   // initial caret position 
      //   var caret_pos = input.prop("selectionStart");
          
      //   // check for decimal
      //   if (input_val.indexOf(".") >= 0) {

      //     // get position of first decimal
      //     // this prevents multiple decimals from
      //     // being entered
      //     var decimal_pos = input_val.indexOf(".");

      //     // split number by decimal point
      //     var left_side = input_val.substring(0, decimal_pos);
      //     var right_side = input_val.substring(decimal_pos);

      //     // add commas to left side of number
      //     left_side = formatNumber(left_side);

      //     // validate right side
      //     right_side = formatNumber(right_side);
          
      //     // On blur make sure 2 numbers after decimal
      //     if (blur === "blur") {
      //       right_side += "00";
      //     }
          
      //     // Limit decimal to only 2 digits
      //     right_side = right_side.substring(0, 0);

      //     // join number by .
      //     input_val = left_side;

      //   } else {
      //     // no decimal entered
      //     // add commas to number
      //     // remove all non-digits
      //     input_val = formatNumber(input_val);
      //     input_val = input_val;
          
      //     // final formatting
      //     if (blur === "blur") {
      //       input_val += "";
      //     }
      //   }
        
      //   // send updated string to input
      //   input.val(input_val);

      //   // put caret back in the right position
      //   var updated_len = input_val.length;
      //   caret_pos = updated_len - original_len + caret_pos;
      //   input[0].setSelectionRange(caret_pos, caret_pos);
      // }

      })
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/kredit/form.blade.php ENDPATH**/ ?>