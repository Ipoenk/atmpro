   <a href="/" class="brand-link navbar-purple">
    <img src="/adminlte/img/atm4.png"
         class="brand-image img-circle elevation-4"
         style="opacity: .8">
    <span class="brand-text font-weight-dark"><b>SIM - ATM</b></span>
  </a>

  <!-- Sidebar --> 
  <div class="sidebar sidebar-dark-primary">
    <!-- Sidebar user (optional) -->
    <br/>
    <br/>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item"> 
          <a href="<?php echo e(route('logout')); ?>" class="nav-link"> 
            <i class="fas fa-power-off fa-1x"></i>
            <p>
              Logout
            </p>
          </a>
           
        </li>
        <li class="nav-item"> 
          <a href="<?php echo e(route('home')); ?>" class="nav-link"> 
            <i class="nav-icon fas fa-home"></i>
            <p>
              Home
               
            </p>
          </a>
           
        </li>

        <li class="nav-item"> 
          <a href="<?php echo e(route('nasabah')); ?>" class="nav-link">
            <i class="nav-icon fas fa-portrait"></i>
            <p>
              Nasabah
              <span class="right badge fas fa-search badge-danger">New</span>
            </p> 
          </a>
        </li>
        <br/>
        
        <?php if(Auth()->user()->role_id == 10): ?>
        <li class="nav-item">
          <a href="<?php echo e(route('simpanan')); ?>" class="nav-link">
            <i class="nav-icon fas fa-file-invoice-dollar"></i>
            <p>
              Simpanan
              <span class="badge badge-info right">6</span>
            </p>
          </a>
        </li>
               
        <li class="nav-item">
          <a href="<?php echo e(route('deposito')); ?>" class="nav-link">
            <i class="nav-icon fas fa-money-check-alt"></i>
            <p>
              Deposito
              <span class="badge badge-info right">6</span>
            </p>
          </a>
           
        </li>
        <?php endif; ?>
        <li class="nav-item">
          <a href="<?php echo e(route('kredit.index')); ?>" class="nav-link">
            <i class="nav-icon fas fa-hand-holding-usd"></i>
            <p>
              Pinjaman
              <span class="badge badge-info right">6</span>
            </p>
          </a>

          
        </li>
        <br/>
        
        <li class="nav-item">
          <a href="<?php echo e(route('kasir')); ?>" class="nav-link">
            <i class="nav-icon fas fa-cash-register"></i>
            <p>
              Kasir
              
            </p>
          </a>
           
        </li>
        

        <li class="nav-item">
          <a href="<?php echo e(route('accounting')); ?>" class="nav-link">
            <i class="nav-icon fas fa-file-invoice"></i>
            <p>
              Accounting
               
            </p>
          </a>
           
        </li>

        <li class="nav-item">
          <a href="<?php echo e(route('setting')); ?>" class="nav-link">
            <i class="nav-icon fas fa-users-cog"></i>
            <p>
              SETTING
               
            </p>
          </a>
           
        </li>

        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->

  </div>
<?php /**PATH D:\Projectemg\AtmPro\resources\views/template/sidebar.blade.php ENDPATH**/ ?>