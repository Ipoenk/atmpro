<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIM | ATM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo e(URL::asset('/adminlte/img/atm3.png')); ?>" type="image/png" sizes="96x96">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('/adminlte')); ?>/plugins/fontawesome-free/css/all.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(asset('/adminlte')); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo e(asset('/adminlte')); ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('/adminlte')); ?>/css/adminlte.min.css">
   
  
  <style>
span.glyphicon{
    float:left;
    display:inline-block; 
    font-size: initial;

  /* IE 7 hack */
    *zoom:1;
    *display: inline;
    margin-top: 2px;
    margin-left: 2%;
    height: 25px;    
    
}
 

.table-condensed{
  font-size: 14px;
}

.body2, .html2 {
  height: 90%;
  margin: 0; 
}

#bg-size1 {
 

  /* Full height */
  height: 100%; 
  border: 1px solid black; 
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: 300px 300px; 
} 
div.transbox {
  width: 600px;
  height: 500px;
  margin: 26px; 
  margin-left: auto;
  margin-right: auto;   
  background-color: #ffffff;
  opacity: 0.6;
  text-align: center;
}
div.transbox p {
  margin: 5%;
  font-weight: bold;
  color: #000000;
}

.centered {
  display: block;  
  margin-left: 15%;
  margin-right: auto; 
}

.serif {
  font-family: "Times New Roman", Times, serif;
}

* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-15 {
  float: left;
  width: 15%;
  margin-top: 6px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

.col-50 {
  float: left;
  width: 50%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
} 
 

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media  screen and (max-width: 600px) {
  .col-15,.col-25,.col-50, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
} 
</style> 
</head>

<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
       
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <a><strong><?php echo e(Auth::user()->name); ?></strong></a>
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/adminlte/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  <p id = "opr" name = "opr">
                    <?php echo e(Auth::user()->name); ?>

                    
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/adminlte/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/adminlte/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown" > 
        <div class="image" data-toggle="dropdown" href="#">
          <img src="<?php echo e(asset('/adminlte')); ?>/img/user2-160x160.jpg" class="img-circle img-size-32" alt="User Image">
        </div>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
         <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/adminlte/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>  
        </div>
      </li>
       
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
   <?php echo $__env->make('template.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php echo $__env->yieldContent('content'); ?>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer"> 
    <marquee behavior="scroll" direction="left" scrollamount="7">
      <strong>Kepada semua karyawan karyawati agar selalu menjaga disiplin dalam bekerja dan taat pada aturan yang berlaku, Trimakasih.</strong> 
      
      <b><a href="http://www.atm28.net">&copy; 2020 Team - Andalan Tata Manajemen</a></b>
    
    </marquee>
  </footer>
    
</div>


<script src="<?php echo e(asset('/adminlte')); ?>/plugins/jquery/jquery.min.js"></script>

<script src="<?php echo e(asset('/adminlte')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo e(asset('/adminlte')); ?>/js/adminlte.min.js"></script>
<script src="<?php echo e(asset('/adminlte')); ?>/js/demo.js"></script> 
<script src="<?php echo e(asset('/adminlte')); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo e(asset('/adminlte')); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo e(asset('/adminlte')); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo e(asset('/adminlte')); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script> 
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
 


<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
</script>
<script type="text/javascript">
    //nasabah
    var pendapatan = document.getElementById('pendapatan');
    var pengeluaran = document.getElementById('pengeluaran');
    //kredit realisasi
    var pinj_pokok = document.getElementById('pinj_pokok');
    var saldopiutang = document.getElementById('saldopiutang');
    var bbt = document.getElementById('bbt');
    var totangsuran = document.getElementById('totangsuran');
    var angs_pokok = document.getElementById('angs_pokok');
    var angs_bunga = document.getElementById('angs_bunga');
    var nom_provisi = document.getElementById('nom_provisi');
    var nom_admin = document.getElementById('nom_admin');
    var notaris = document.getElementById('notaris');
    var materai = document.getElementById('materai');
    var assjiwa = document.getElementById('assjiwa');
    var asuransi = document.getElementById('asuransi');
    var sim_ang = document.getElementById('sim_ang');
    var bitrans = document.getElementById('bitrans');
  
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendapatan.addEventListener('keyup', function(e){
      pendapatan.value = formatRupiah(this.value);
    });
    pengeluaran.addEventListener('keyup', function(e){
      pengeluaran.value = formatRupiah(this.value);
    }); 
    pinj_pokok.addEventListener('keyup', function(e){
      pinj_pokok.value = formatRupiah(this.value);
    }); 
    saldopiutang.addEventListener('keyup', function(e){
      saldopiutang.value = formatRupiah(this.value);
    }); 
    bbt.addEventListener('keyup', function(e){
      bbt.value = formatRupiah(this.value);
    }); 
    totangsuran.addEventListener('keyup', function(e){
      totangsuran.value = formatRupiah(this.value);
    }); 
    angs_pokok.addEventListener('keyup', function(e){
      angs_pokok.value = formatRupiah(this.value);
    }); 
    angs_bunga.addEventListener('keyup', function(e){
      angs_bunga.value = formatRupiah(this.value);
    }); 

    nom_provisi.addEventListener('keyup', function(e){
      nom_provisi.value = formatRupiah(this.value);
    });
    nom_admin.addEventListener('keyup', function(e){
      nom_admin.value = formatRupiah(this.value);
    });
    notaris.addEventListener('keyup', function(e){
      notaris.value = formatRupiah(this.value);
    });
    materai.addEventListener('keyup', function(e){
      materai.value = formatRupiah(this.value);
    });
    assjiwa.addEventListener('keyup', function(e){
      assjiwa.value = formatRupiah(this.value);
    });
    asuransi.addEventListener('keyup', function(e){
      asuransi.value = formatRupiah(this.value);
    });
    sim_ang.addEventListener('keyup', function(e){
      sim_ang.value = formatRupiah(this.value);
    });
    bitrans.addEventListener('keyup', function(e){
      bitrans.value = formatRupiah(this.value);
    });


    /* Fungsi formatRupiah */
    
    
    function formatRupiah(angka, prefix){
      var number_string = angka.replace(/[^.\d]/g, '').toString(),
      split       = number_string.split('.'),
      sisa        = split[0].length % 3, 
      rupiah        = split[0].substr(0, sisa),
      ribuan        = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
      }

    rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }

</script>  
<?php echo $__env->yieldPushContent('scripts'); ?>
</body>
</html>
<?php /**PATH D:\Project_ATM\proatm\resources\views/layouts/master.blade.php ENDPATH**/ ?>