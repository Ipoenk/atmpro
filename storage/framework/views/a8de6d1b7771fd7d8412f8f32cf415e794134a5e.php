

<?php $__env->startSection('title', 'Entri Data'); ?> 
<?php $__env->startSection('subtitle', 'Page Title'); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div>
    <form action="<?php echo e(route('nasabah.store')); ?>" method="POST" class="form-horizontal">
        <?php echo e(csrf_field()); ?>

        
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PRIBADI</b>
            </h4>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="jenisnsb">Kelompok Anggota</label>
            <div class="col-sm-4 control-label">
                <select class="form-control" name = "jenisnsb" id="jenisnsb" >
                    <option value = "PERORANGAN">PERORANGAN</option>
                    <option value = "KELOMPOK">KELOMPOK</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="niknsb">Kode ID Pengenal</label>
            <div class="col-sm-4">
                <input type="text"  name="niknsb" id="niknsb" minlength="1" maxlength="16" class="form-control numberonly <?php $__errorArgs = ['niknsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> " value="<?php echo e(old('niknsb')); ?>"  />
                <?php $__errorArgs = ['niknsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                 <div class="invalid-feedback"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                <small id="niknsb" class="form-text text-muted">Maksimum 16 karakter</small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="namansb">Nama</label>
            <div class="col-sm-6">
                <input type="text" style="text-transform:uppercase"  name="namansb" id="namansb" class="form-control <?php $__errorArgs = ['niknsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('namansb')); ?>"  />
                <?php $__errorArgs = ['namansb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
               <small id="namansb" class="form-text text-muted">Maksimum 100 karakter</small>                
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="alamatnsbktp">Alamat Sesuai KTP</label>
            <div class="col-sm-8">
                <textarea  name="alamatnsbktp" style="text-transform:uppercase"  id="alamatnsbktp" class="form-control <?php $__errorArgs = ['alamatnsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" ><?php echo e(old('alamatnsbktp')); ?></textarea>
                <?php $__errorArgs = ['alamatnsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>

        <div class="form-group ">
            <label class="col-sm-4 control-label" for="propinsinsbktp">Propinsi Sesuai KTP</label>
            <div class="col-sm-4">
                <select name="propinsinsbktp" id="propinsinsbktp" class="form-control <?php $__errorArgs = ['propinsinsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" >
                <option value="">Pilih Propinsi</option>
                <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                </select>
                <?php $__errorArgs = ['propinsinsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kotansbktp">Kota/Kabupaten Sesuai KTP</label>
            <div class="col-sm-4">
                <select name="kotansbktp" id="kotansbktp" class="form-control <?php $__errorArgs = ['kotansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
                <?php $__errorArgs = ['kotansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kecamatannsbktp">Kecamatan Sesuai KTP</label>
            <div class="col-sm-4">
                <select name="kecamatannsbktp" id="kecamatannsbktp" class="form-control <?php $__errorArgs = ['kecamatannsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                <?php $__errorArgs = ['kecamatannsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="desansbktp">Desa/Kelurahan Sesuai KTP</label>
            <div class="col-sm-4">
                <select name="desansbktp" id="desansbktp" class="form-control <?php $__errorArgs = ['desansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                <?php $__errorArgs = ['desansbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="kodeposnsbktp">KodePos Sesuai KTP</label>
            <div class="col-sm-3">

                <input type="text" name="kodeposnsbktp" id="kodeposnsbktp" minlength="1" maxlength="5" class="form-control <?php $__errorArgs = ['kodeposnsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('kodeposnsbktp')); ?>"  />
                    <?php $__errorArgs = ['kodeposnsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="invalid-feedback"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="notelpnsb">Telepon</label>
            <div class="col-sm-4">
                <input type="text" name="notelpnsb" id="notelpnsb" class="form-control numbersOnly <?php $__errorArgs = ['kodeposnsbktp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="+62" value="<?php echo e(old('notelpnsb')); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="emailnsb">Email</label>
            <div class="col-sm-6">
                <input type="text" name="emailnsb" id="emailnsb" class="form-control <?php $__errorArgs = ['emailnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="atm@gmail.com" value="<?php echo e(old('emailnsb')); ?>" />
                <?php $__errorArgs = ['emailnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="alamatdomisili">Alamat Domisili</label>
            <div class="col-sm-8">
                <textarea  name="alamatdomisili" style="text-transform:uppercase"  id="alamatdomisili" class="form-control <?php $__errorArgs = ['emailnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" ><?php echo e(old('alamatdomisili')); ?></textarea>
                <?php $__errorArgs = ['alamatdomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>

        <div class="form-group ">
            <label class="col-sm-4 control-label" for="propinsidomisili">Propinsi Domisili</label>
            <div class="col-sm-4">
                <select name="propinsidomisili" id="propinsidomisili" class="form-control <?php $__errorArgs = ['propinsidomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" >
                <option value="">Pilih Propinsi</option>
                <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                </select>
                <?php $__errorArgs = ['propinsidomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kotadomisili">Kota/Kabupaten Domisili</label>
            <div class="col-sm-4">
                <select name="kotadomisili" id="kotadomisili" class="form-control <?php $__errorArgs = ['kotadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
            </div>
            <?php $__errorArgs = ['kotadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kecamatandomisili">Kecamatan Domisili</label>
            <div class="col-sm-4">
                <select name="kecamatandomisili" id="kecamatandomisili" class="form-control <?php $__errorArgs = ['kecamatandomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                <?php $__errorArgs = ['kecamatandomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="desadomisili">Desa/Kelurahan Domisili</label>
            <div class="col-sm-4">
                <select name="desadomisili" id="desadomisili" class="form-control <?php $__errorArgs = ['desadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                <?php $__errorArgs = ['desadomisili'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="kelaminnsb">Jenis Kelamin</label>
            <div class="col-sm-3 control-label">
                <select class="form-control <?php $__errorArgs = ['kelaminnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "kelaminnsb" = id="kelaminnsb" >
                    <option value="LAKI-LAKI">LAKI-LAKI</option>
                    <option value="PEREMPUAN">PEREMPUAN</option>
                </select>
                <?php $__errorArgs = ['kelaminnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="tmplahirnsb">Tempat Lahir</label>
            <div class="col-sm-3">
                <input type="text" style="text-transform:uppercase"  name="tmplahirnsb" id="tmplahirnsb" class="form-control <?php $__errorArgs = ['tmplahirnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('tmplahirnsb')); ?>"  />
                <?php $__errorArgs = ['tmplahirnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="tgllahirnsb">Tanggal Lahir</label>
            <div class="col-sm-3">
                <input type="date" class="form-control <?php $__errorArgs = ['tgllahirnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "tgllahirnsb" id="tgllahirnsb" placeholder="Isikan Tanggal Lahir" value="<?php echo e(old('tgllahirnsb')); ?>" >
                <?php $__errorArgs = ['tgllahirnsb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="agamansb">Agama</label>
            <div class="col-sm-3">
            <select class="form-control <?php $__errorArgs = ['agamansb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "agamansb" id="agamansb" >
                <option value ="ISLAM">ISLAM</option>
                <option value ="KATOLIK">KATOLIK</option>
                <option value ="PROTESTAN">PROTESTAN</option>
                <option value ="HINDU">HINDU</option>
                <option value ="BUDHA">BUDHA</option>
                <option value ="KONGHUCU">KONGHUCU</option>
            </select>
            <?php $__errorArgs = ['agamansb'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="statusperkawinan">Status Perkawinan</label>
            <div class="col-sm-3 control-label">
                <select class="form-control <?php $__errorArgs = ['statusperkawinan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "statusperkawinan" id="statusperkawinan" >
                    <option value ="BELUM MENIKAH">BELUM MENIKAH</option>
                    <option value ="MENIKAH">MENIKAH</option>
                    <option value ="CERAI HIDUP">CERAI HIDUP</option>
                    <option value ="CERAI MATI">CERAI MATI</option>
                </select>
                <?php $__errorArgs = ['statusperkawinan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="namaibukandung">Nama Ibu Kandung</label>
            <div class="col-sm-4">
                <input type="text" style="text-transform:uppercase"  name="namaibukandung" id="namaibukandung" class="form-control <?php $__errorArgs = ['namaibukandung'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('namaibukandung')); ?>"  />
                <?php $__errorArgs = ['namaibukandung'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PASANGAN</b>
            </h4>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="nikps">Kode ID Pengenal</label>
            <div class="col-sm-4">

                <input type="text" name="nikps" id="nikps" minlength="1" maxlength="16" class="form-control <?php $__errorArgs = ['nikps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('Nikps')); ?>"  />
                <?php $__errorArgs = ['nikps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="namaps">Nama</label>
            <div class="col-sm-8">
                <input type="text" style="text-transform:uppercase"  name="namaps" id="namaps" class="form-control <?php $__errorArgs = ['namaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('namaps')); ?>"  />
                <?php $__errorArgs = ['namaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="alamatps">Alamat</label>
            <div class="col-sm-8">
                <textarea  name="alamatps" style="text-transform:uppercase"  id="alamatps" class="form-control <?php $__errorArgs = ['alamatps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" ><?php echo e(old('alamatps')); ?></textarea>
                <?php $__errorArgs = ['alamatps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>

        <div class="form-group ">
            <label class="col-sm-4 control-label" for="propinsips">Propinsi</label>
            <div class="col-sm-4">
                <select name="propinsips" id="propinsips" class="form-control <?php $__errorArgs = ['propinsips'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                <option value="">Pilih Propinsi</option>
                <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                </select>
                <?php $__errorArgs = ['propinsips'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kotaps">Kota/Kabupaten</label>
            <div class="col-sm-4">
                <select name="kotaps" id="kotaps" class="form-control <?php $__errorArgs = ['kotaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
                <?php $__errorArgs = ['kotaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kecamatanps">Kecamatan</label>
            <div class="col-sm-4">
                <select name="kecamatanps" id="kecamatanps" class="form-control <?php $__errorArgs = ['kecamatanps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                <?php $__errorArgs = ['kecamatanps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="desaps">Desa/Kelurahan</label>
            <div class="col-sm-4">
                <select name="desaps" id="desaps" class="form-control <?php $__errorArgs = ['desaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                <?php $__errorArgs = ['desaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="kodeposps">KodePos</label>
            <div class="col-sm-3">

                <input type="text" name="kodeposps" id="kodeposps" minlength="1" maxlength="5" class="form-control <?php $__errorArgs = ['kodeposps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('kodeposps')); ?>" />
                <?php $__errorArgs = ['kodeposps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="notelpps">Telepon</label>
            <div class="col-sm-4">
                <input type="text" name="notelpps" id="notelpps" class="form-control <?php $__errorArgs = ['notelpps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> numbersOnly" placeholder="" value="<?php echo e(old('notelpps')); ?>" />
                <?php $__errorArgs = ['notelpps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="kelaminps">Jenis Kelamin</label>
            <div class="col-sm-3 control-label">
                <select name="kelaminps" id="kelaminps" class="form-control  <?php $__errorArgs = ['kelaminps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('kelaminps')); ?>">
                    <option value ="LAKI-LAKI">LAKI-LAKI</option>
                    <option value ="PEREMPUAN">PEREMPUAN</option>
                </select>
                <?php $__errorArgs = ['kelaminps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="tmplahirps">Tempat Lahir</label>
            <div class="col-sm-3">
                <input type="text" style="text-transform:uppercase" name="tmplahirps" id="tmplahirps" class="form-control <?php $__errorArgs = ['tmplahirps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('tmplahirps')); ?>" />
                <?php $__errorArgs = ['tmplahirps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="tgllahirps">Tanggal Lahir</label>
            <div class="col-sm-3">
                <input type="date" class="form-control <?php $__errorArgs = ['tgllahirps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "tgllahirps" id="tgllahirps" placeholder="Isikan Tanggal Lahir" value="<?php echo e(old('tgllahirps')); ?>" />
                <?php $__errorArgs = ['tgllahirps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="agamaps">Agama</label>
            <div class="col-sm-3">
            <select class="form-control <?php $__errorArgs = ['agamaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "agamaps" id="agamaps" value="<?php echo e(old('agamaps')); ?>">
                <option value ="ISLAM">ISLAM</option>
                <option value ="KATOLIK">KATOLIK</option>
                <option value ="PROTESTAN">PROTESTAN</option>
                <option value ="HINDU">HINDU</option>
                <option value ="BUDHA">BUDHA</option>
                <option value ="KONGHUCU">KONGHUCU</option>
            </select>
            <?php $__errorArgs = ['agamaps'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        </div>
            </div>
                <div class="alert alert-info">
                <h4 class="align-center">
                    <b>DATA PEKERJAAN</b>
                </h4>
                </div>
            <div class="form-group">
            <label class="col-sm-4 control-label" for="jenispsh">Jenis Perusahaan</label>
            <div class="col-sm-4 control-label">
                <select class="form-control <?php $__errorArgs = ['jenispsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "jenispsh" id="jenispsh" value="<?php echo e(old('jenispsh')); ?>">
                    <option value ="PNS">PNS</option>
                    <option value ="TNI-POLRI">TNI-POLRI</option>
                    <option value ="PERSERO">PERSERO</option>
                    <option value ="PERUSAHAAN DAERAH">PERUSAHAAN DAERAH</option>
                    <option value ="PT">PT</option>
                    <option value ="CV">CV</option>
                    <option value ="FIRMA">FIRMA</option>
                    <option value ="KOPERASI">KOPERASI</option>
                    <option value ="YAYASAN">YAYASAN</option>
                    <option value ="LAIN-LAIN">LAIN-LAIN</option>
                </select>
                <?php $__errorArgs = ['jenispsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="namapsh">Nama Perusahaan</label>
            <div class="col-sm-8">
                <input type="text" style="text-transform:uppercase" name="namapsh" id="namapsh" class="form-control <?php $__errorArgs = ['namapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('namapsh')); ?>" />
                <?php $__errorArgs = ['namapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="alamatpsh">Alamat Perusahaan</label>
            <div class="col-sm-8">
                <textarea  name="alamatpsh" style="text-transform:uppercase"  id="alamatpsh" class="form-control <?php $__errorArgs = ['alamatpsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" ><?php echo e(old('alamatpsh')); ?></textarea>
                <?php $__errorArgs = ['alamatpsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="propinsipsh">Propinsi</label>
            <div class="col-sm-4">
                <select name="propinsipsh" id="propinsipsh" class="form-control <?php $__errorArgs = ['propinsipsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                <option value="">Pilih Propinsi</option>
                <?php $__currentLoopData = $allProvinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsis => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($provinsis); ?>"> <?php echo e($value); ?> </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                </select>
                <?php $__errorArgs = ['propinsipsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kotapsh">Kota/Kabupaten</label>
            <div class="col-sm-4">
                <select name="kotapsh" id="kotapsh" class="form-control <?php $__errorArgs = ['kotapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
                <?php $__errorArgs = ['kotapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="kecamatanpsh">Kecamatan</label>
            <div class="col-sm-4">
                <select name="kecamatanpsh" id="kecamatanpsh" class="form-control <?php $__errorArgs = ['kecamatanpsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                <?php $__errorArgs = ['kecamatanpsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-4 control-label" for="desapsh">Desa/Kelurahan</label>
            <div class="col-sm-4">
                <select name="desapsh" id="desapsh" class="form-control <?php $__errorArgs = ['desapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                <?php $__errorArgs = ['desapsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="kodepospsh">KodePos</label>
            <div class="col-sm-3">

                <input type="text" name="kodepospsh" id="kodepospsh" minlength = "1" maxlength = "5" class="form-control <?php $__errorArgs = ['kodepospsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('kodepospsh')); ?>" />
                <?php $__errorArgs = ['kodepospsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            </div>
        </div>        
        <div class="form-group">
            <label class="col-sm-4 control-label" for="notelppsh">No. Telp</label>
            <div class="col-sm-4">
                <input type="text" name="notelppsh" id="notelppsh" class="form-control <?php $__errorArgs = ['notelppsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('notelppsh')); ?>" />
                <?php $__errorArgs = ['notelppsh'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="bidangusaha">Bidang Usaha</label>
            <div class="col-sm-4">
                <input type="text" style="text-transform:uppercase" name="bidangusaha" id="bidangusaha" class="form-control <?php $__errorArgs = ['bidangusaha'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('Bidangusaha')); ?>" />
                <?php $__errorArgs = ['bidangusaha'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PENDAPATAN</b>
            </h4>
            </div>
            <div class="form-group">
            <label class="col-sm-4 control-label" for="sumberdana">Sumber Dana</label>
            <div class="col-sm-3 control-label">
                <select class="form-control <?php $__errorArgs = ['sumberdana'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "sumberdana" id="sumberdana" value="<?php echo e(old('sumberdana')); ?>">
                    <option value = "GAJI" >GAJI</option>
                    <option value = "USAHA">USAHA</option>
                    <option value = "LAINNYA">LAINNYA</option>
                </select>
                <?php $__errorArgs = ['sumberdana'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="pendapatan">Pendapatan Kotor</label>
            <div class="col-sm-3 "> 
                <input type="number" min="0" name = "pendapatan" id="pendapatan" class="form-control <?php $__errorArgs = ['pendapatan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(number_format(old('pendapatan'))); ?>" >
                <small  class="text-muted">
                    Pertahun
                </small>
                <?php $__errorArgs = ['pendapatan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                 <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>   
        </div>        
        <div class="form-group">
            <label class="col-sm-4 control-label" for="pengeluaran">Pengeluaran Kotor</label>
            <div class="col-sm-3"> 
                <input type="number" min="0" name = "pengeluaran" id="pengeluaran" class=" form-control <?php $__errorArgs = ['pengeluaran'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(number_format(old('pengeluaran'))); ?>" />
                <small  class="text-muted">
                    Pertahun
                </small>
                <?php $__errorArgs = ['pengeluaran'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>        
        </div>   
        <div class="form-group">
            <label class="col-sm-4 control-label" for="tanggungan">Jumlah Tanggungan</label>
            <div class="col-sm-3" > 
                <input type="number" min="0" name = "tanggungan" id="tanggungan" class="form-control <?php $__errorArgs = ['tanggungan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(number_format(old('tanggungan'))); ?>" />
                <small class="text-muted">
                    Jiwa (Anak + Istri + Famili)
                </small>
                <?php $__errorArgs = ['tanggungan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>        
        <div class="form-group">
            <label class="col-sm-4 control-label" for="ststempattinggal">Status Tempat Tinggal</label>
            <div class="col-sm-4 control-label">
                <select class="form-control <?php $__errorArgs = ['ststempattinggal'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "ststempattinggal" id="ststempattinggal" value="<?php echo e(old('ststempattinggal')); ?>">
                    <option value = "MILIK SENDIRI">MILIK SENDIRI</option>
                    <option value = "MILIK ORANG TUA">MILIK ORANG TUA</option>
                    <option value = "RUMAH DINAS">RUMAH DINAS</option>
                    <option value = "SEWA">SEWA</option>
                    <option value = "LAINNYA">LAINNYA</option>
                </select>
                <?php $__errorArgs = ['ststempattinggal'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
        </div>
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PENGAJUAN</b>
            </h4>
            </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="pengajuan">Jenis Pengajuan</label>
            <select class="form-control col-sm-6 <?php $__errorArgs = ['pengajuan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name = "pengajuan" id="pengajuan" value="<?php echo e(old('pengajuan')); ?>">
                <option value = "KREDIT">PENGAJUAN KREDIT</option>
                <option value = "TABUNGAN">BUKA REKENING TABUNGAN</option>
                <option value = "DEPOSITO">BUKA DEPOSITO</option>
            </select>
    </div>

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button class="btn btn-primary mr-1" type="submit">Next</button>
                
                <a href="<?php echo e(Route('nasabah')); ?>" class="btn btn-danger">Kembali</a>
            </div>
        </div>
    </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<script src="<?php echo e(asset('js/jquery-2.2.3.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.dataTables.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/admin-lte.js')); ?>"></script>
<?php $__env->startPush('scripts'); ?>
    <script>
        $("#propinsinsbktp").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotansbktp').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotansbktp').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kotansbktp").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatannsbktp').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatannsbktp').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatannsbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatannsbktp").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    console.log(data);
                    $('#desansbktp').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desansbktp').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#propinsidomisili").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotadomisili').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotadomisili').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kotadomisili").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatandomisili').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatandomisili').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatandomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatandomisili").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desadomisili').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desadomisili').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#propinsips").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotaps').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotaps').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotaps').attr('disabled',false);
                }
            });
        });
        
        $("#kotaps").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanps').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanps').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanps').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanps").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desaps').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desaps').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desaps').attr('disabled',false);
                }
            });
        });
        
        $("#propinsipsh").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotapsh').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotapsh').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotapsh').attr('disabled',false);
                }
            });
        });
        
        $("#kotapsh").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanpsh').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanpsh').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanpsh').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanpsh").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desapsh').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desapsh').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desapsh').attr('disabled',false);
                }
            });
        });
               
    </script>
    <?php echo $__env->yieldPushContent('scripts'); ?>
    <?php $__env->stopPush(); ?>    

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/nasabah/form.blade.php ENDPATH**/ ?>