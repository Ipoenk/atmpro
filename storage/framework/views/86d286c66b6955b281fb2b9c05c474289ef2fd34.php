
<?php $__env->startSection('content'); ?>    
<section class="content">
    <div>
        <h2 align="center">DAFTAR REALISASI</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:70%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Kredit</th>
                    <th>Nama Debitur</th>
                    <th>Plafon Akad</th>
                    <th>BBT Awal</th>
                    <th>Suku Bunga</th>
                    <th>Lama</th>
                    <th>Sistem</th>
                    <th>Saldo Piutang</th>
                    <th>Tgl Kredit</th>
                    <th>Tgl Awal Angsur</th>
                    <th>Tgl Akhir Angsur</th>
                    <th>Tgl Jatuh Tempo</th>
                    <th>Provisi</th>
                    <th>Administrasi</th>
                    <th>Materai</th>
                    <th>Notaris</th>
                    <th>Asuransi Jiwa</th>
                    <th>Asuransi Jaminan</th>
                    <th>Simpanan Anggota</th>
                    <th>Biaya Lain</th>
                    <th>Nama Ao</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
 
                <?php $__currentLoopData = $realisasi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $realisasi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td><?php echo e($realisasi->no_kredit); ?></td>
                    <td><?php echo e(strtoupper($realisasi->namansb)); ?></td>
                    <td><?php echo e(number_format($realisasi->pinj_pokok)); ?></td>
                    <td><?php echo e(number_format($realisasi->bbt)); ?></td>
                    <td><?php echo e(number_format($realisasi->pinj_prsbunga)); ?></td>
                    <td><?php echo e(number_format($realisasi->lama)); ?></td>
                    <td><?php echo e(strtoupper($realisasi->sistem)); ?></td>
                    <td><?php echo e(number_format(($realisasi->pinj_pokok + $realisasi->bbt))); ?></td>
                    <td style="width:8%"><?php echo e(date('d-m-Y',strtotime($realisasi->tgl_kredit))); ?></td>
                    <td><?php echo e(date('d-m-Y',strtotime($realisasi->tgl_mulai))); ?></td>
                    <td><?php echo e(date('d-m-Y',strtotime($realisasi->tgl_akhir))); ?></td>
                    <td><?php echo e(date('d-m-Y',strtotime($realisasi->jatuhtempo))); ?></td>
                    <td><?php echo e(number_format($realisasi->nom_provisi)); ?></td>
                    <td><?php echo e(number_format($realisasi->nom_adm)); ?></td>
                    <td><?php echo e(number_format($realisasi->nom_meterai)); ?></td>
                    <td><?php echo e(number_format($realisasi->nom_notaris)); ?></td>
                    <td><?php echo e(number_format($realisasi->assjiwa)); ?></td>
                    <td><?php echo e(number_format($realisasi->nom_asuransi)); ?></td>
                    <td><?php echo e(number_format($realisasi->sim_ang)); ?></td>
                    <td><?php echo e(number_format($realisasi->nom_trans)); ?></td>
                    <td><?php echo e($realisasi->namaao); ?></td>
                    <td>
                        <div class="timeline-footer">
                            <a href="/kredit.viewangsuran/<?php echo e($realisasi->id); ?>" class="btn btn-info btn-sm">Lihat Kartu</a>
                            </div>
                    </td>
                </tr>                               
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

            </tbody>
            
        </table>
    </div>	
</section>
<?php $__env->stopSection(); ?>  
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/kredit/daftar_realisasi.blade.php ENDPATH**/ ?>