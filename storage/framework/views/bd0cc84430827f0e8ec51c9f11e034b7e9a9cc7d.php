
<?php $__env->startSection('content'); ?>
<section class="content">
    <div>
        <h2 align="center">DAFTAR SUB KANTOR</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Kantor</th>
                    <th>Nama Pimpinan</th>
                    <th>Nama Wakil Pimpinan</th>
                    <th>Nama Komisaris</th>
                    <th>Pengelola</th>
                    <th>Status</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>No Telp</th>
                    <th>No Fax</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
 
                <?php
                Log::info($kantors); 
                ?>
                <?php $__currentLoopData = $kantors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantors): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td><?php echo e(strtoupper($kantors->namakantor)); ?></td>
                    <td><?php echo e(strtoupper($kantors->dirut)); ?></td>
                    <td><?php echo e(strtoupper($kantors->direktur)); ?></td>
                    <td><?php echo e(strtoupper($kantors->koma)); ?></td>
                    <td><?php echo e(strtoupper($kantors->pengelola)); ?></td>
                    <td><?php echo e(strtoupper($kantors->status)); ?></td>
                    <td><?php echo e(strtoupper($kantors->alamat)); ?></td>
                    <td><?php echo e(strtoupper($kantors->kota)); ?></td>
                    <td><?php echo e(strtoupper($kantors->telp)); ?></td>
                    <td><?php echo e(strtoupper($kantors->fax)); ?></td>
                    <td>
                        <div class="timeline-footer">
                            <a href="#" class="btn btn-info btn-sm">Lihat Detail</a>
                            
                    </td>
                </tr>                               
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

            </tbody>
        </table>
    <a type="button" href="/kantorsub.create/<?php echo e($id); ?>" class="btn btn-primary col-1">+ Add</a>
    </div>	
</section>
<?php $__env->stopSection(); ?>    
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/setting/daftar_kantor_detail.blade.php ENDPATH**/ ?>