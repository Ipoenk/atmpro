
<?php $__env->startSection('content'); ?>
    <section class="content">
        <div>
			<h2 align="center">DAFTAR NOMINATIF</h2>
			
        </div>
        <div class="container">
            <table id="example1" class="table table-bordered table-striped table-condensed" style="width:80%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Kredit</th>
                        <th>Nama Debitur</th>
                        <th>Plafon Akad</th>
                        <th>BBT Awal</th>
                        <th>Suku Bunga</th>
                        <th>Lama</th>
                        <th>Sistem</th>
                        <th>Plafon</th>
                        <th>Bakidebet</th>
                        <th>Saldo BBT</th>
                        <th>Saldo Piutang</th>
                        <th>Tgl Kredit</th>
                        <th>Tgl Awal Angsur</th>
                        <th>Tgl Akhir Angsur</th>
                        <th>Tgl Jatuh Tempo</th>
                        <th>Angsuran Pokok</th>
                        <th>Angsuran Bunga</th>
                        <th>Tgk Pokok</th>
                        <th>Tgk Bunga</th>
                        <th>Lama Tgk Pokok</th>
                        <th>Lama Tgk Bunga</th>
                        <th>Provisi</th>
                        <th>Administrasi</th>
                        <th>Materai</th>
                        <th>Notaris</th>
                        <th>Asuransi Jiwa</th>
                        <th>Asuransi Jaminan</th>
                        <th>Simpanan Anggota</th>
                        <th>Biaya Lain</th>
                        <th>Kolek</th>
                        <th>Nama Ao</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $__currentLoopData = $nominatif; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nominatif): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($loop->iteration); ?></td>
                        <td><?php echo e($nominatif->no_kredit); ?></td>
                        <td><?php echo e(strtoupper($nominatif->namansb)); ?></td>
                        <td><?php echo e(number_format($nominatif->pinj_pokok)); ?></td>
                        <td><?php echo e(number_format($nominatif->bbt)); ?></td>
                        <td><?php echo e(number_format($nominatif->pinj_prsbunga)); ?></td>
                        <td><?php echo e(number_format($nominatif->lama)); ?></td>
                        <td><?php echo e(strtoupper($nominatif->sistem)); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format($nominatif->pinj_pokok-$nominatif->pokokdibayar)); ?></td>
                        <td><?php echo e(number_format($nominatif->bbt-$nominatif->bungadibayar )); ?></td>
                        <td><?php echo e(number_format(($nominatif->pinj_pokok + $nominatif->bbt))-($nominatif->pokokdibayar+$nominatif->bungadibayar)); ?></td>
                        <td><?php echo e(date('d-m-Y',strtotime($nominatif->tgl_nominatif))); ?></td>
                        <td><?php echo e(date('d-m-Y',strtotime($nominatif->tgl_mulai))); ?></td>
                        <td><?php echo e(date('d-m-Y',strtotime($nominatif->tgl_akhir))); ?></td>
                        <td><?php echo e(date('d-m-Y',strtotime($nominatif->jatuhtempo))); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format(0)); ?></td>
                        <td><?php echo e(number_format($nominatif->nom_provisi)); ?></td>
                        <td><?php echo e(number_format($nominatif->nom_adm)); ?></td>
                        <td><?php echo e(number_format($nominatif->nom_meterai)); ?></td>
                        <td><?php echo e(number_format($nominatif->nom_notaris)); ?></td>
                        <td><?php echo e(number_format($nominatif->assjiwa)); ?></td>
                        <td><?php echo e(number_format($nominatif->nom_asuransi)); ?></td>
                        <td><?php echo e(number_format($nominatif->sim_ang)); ?></td>
                        <td><?php echo e(number_format($nominatif->nom_trans)); ?></td>
                        <td>Lancar  </td>
                        <td><?php echo e(strtoupper($nominatif->namaao)); ?></td>
                        
                    </tr>                               
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                </tbody>
                
            </table>
        </div>	
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/kredit/daftar_nominatif.blade.php ENDPATH**/ ?>