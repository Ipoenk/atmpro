<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIM | ATM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo e(URL::asset('/adminlte/img/atm3.png')); ?>" type="image/png" sizes="96x96">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('/adminlte')); ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons --> 
  <link rel="stylesheet" href="<?php echo e(asset('/adminlte')); ?>/css/adminlte.min.css">  
  <style>

.table-condensed{
  font-size: 10px;
}

.body2, .html2 {
  height: 90%;
  margin: 0;
}

#bg-size {
  /* The image used */
  background-image: url("<?php echo e(asset('/adminlte/img/koperasiatm.png')); ?>");

  /* Full height */
  height: 100%; 
  border: 1px solid black; 
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: 550px 450px;
} 
div.transbox {
  width: 550px;
  height: 480px;
  margin: 26px; 
  margin-left: auto;
  margin-right: auto;   
  background-color: #ffffff;
  opacity: 0.6;
  text-align: center;
}
div.transbox p {
  margin: 5%;
  font-weight: bold;
  color: #000000;
}
 
</style> 
</head>

<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
       
       
    </ul>

     

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto"> 
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown" > 
        <div class="image" data-toggle="dropdown" href="#">
          <img src="<?php echo e(asset('/adminlte')); ?>/img/user2-160x160.jpg" class="img-circle img-size-32" alt="User Image">
        </div>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
         <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/adminlte/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>  
        </div>
      </li>
       
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
   <?php echo $__env->make('template.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="bg-size">
    
    <!-- Content Header (Page header) -->
    <?php echo $__env->yieldContent('content'); ?> 
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer"> 
    <marquee behavior="scroll" direction="left" scrollamount="7">
      <strong>Kepada semua karyawan karyawati agar selalu menjaga disiplin dalam bekerja dan taat pada aturan yang berlaku, Trimakasih.</strong> 
      
      <b><a href="http://www.atm28.net">&copy; 2020 Team - Andalan Tata Manajemen</a></b>
    
    </marquee>
  </footer>

    
</div>
<!-- ./wrapper -->
<script>
  $.simpleTicker($("#breakingNewsTicker"), {
        speed: 1250,
        delay: 3500,
        easing: 'swing',
        effectType: 'roll'
    });
</script>
<!-- jQuery --> 
<script src="<?php echo e(asset('/adminlte')); ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(asset('/adminlte')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('/adminlte')); ?>/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('/adminlte')); ?>/js/demo.js"></script>  
</body>
</html>
<?php /**PATH D:\Project_ATM\atmpro\resources\views/template/master.blade.php ENDPATH**/ ?>