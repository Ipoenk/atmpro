
<?php $__env->startSection('content'); ?>
<section class="content">
    <!-- Info boxes -->
<div>
    </br> </br> 
    <div class="row">
          
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>ENTRY DATA</h3>

                </div>
                <div class="icon"> 
                    <i class="fas fa-keyboard"></i>
                </div> 
                <div class="card card-primary collapsed-card">
                  <div class="card-header"> 
                    <div class="card-tools"> 
                        <button type="button" class="small-box-footer btn btn-tool" data-card-widget="collapse">  
                            <b>Menu Entry <i class="fas fa-arrow-circle-down"></i></b>
                        </button> 
                    </div>
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('nasabah')); ?>">
                        <span class="glyphicon fas fa-arrow-circle-right"> 
                            <b style="color:black">Entry Realisasi</b>
                        </span>
                    </a>

                </div>
                <div class="card-body small-box">
                    <a href="#">
                        <span class="glyphicon fas fa-arrow-circle-right"> 
                            <b style="color:black">Entry Angsuran</b>
                        </span>
                    </a>

                </div>
            </div>
        </div>   
           
        
    </div>
    <div class="col-lg-3 col-6 centered">
            <!-- small card -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>LAPORAN</h3>

                </div>
                <div class="icon">
                    <i class="fas fa-book-reader"></i>
                </div>
                <div class="card card-primary collapsed-card">
                  <div class="card-header">
                    <div class="card-tools">
                        <button type="button" class="small-box-footer btn btn-tool" data-card-widget="collapse">  
                            <b>Menu Laporan <i class="fas fa-arrow-circle-down"></i></b>
                        </button>
                    </div>
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Realisasi</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="#">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Pembayaran Angsuran</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.nominatif')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Nominatif</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="#">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Tunggakan</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan PPAP</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="#">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Provisi</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan PBYAD</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="#">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Per Kategori</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan JthTmp Angsuran</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan JthTmp Kredit</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Pelunasan Kredit</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="<?php echo e(route('kredit.realisasi')); ?>">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Saldo Piutang</b>
                    </span></a>
                
                </div>
            </div>
    </div> 
     
</div>


     
  
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/kredit/menukredit.blade.php ENDPATH**/ ?>