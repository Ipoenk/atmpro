
<?php $__env->startSection('content'); ?>
<section class="content">

    
    <form action="/register.update/<?php echo e($user->id); ?>" method="post" class="form-horizontal">
      <?php echo csrf_field(); ?>
        <div class="container">
          <h1>Edit Register</h1>
          <p>Please fill in this form to create an account.</p>
          <hr>
      
          <label for="Nama"><b>Nama User</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nama" name="nama" id="nama" value="<?php echo e($user->name); ?>" required>

          <label for="email"><b>Email</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Email" name="email" id="email" value="<?php echo e($user->email); ?>" required>

          <label for="level">Level User</label>
            <select class="form-control col-5" name = "level" id="level" required>
            <option value="<?php echo e($user->role_id); ?>"><?php echo e($user->level); ?></option>
              <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $roles => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($roles); ?>"> <?php echo e($value); ?> </option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
          </select>
  
          <label for="psw"><b>Password</b></label>
          <input class="form-control col-5" type="password" placeholder="Enter Password" name="psw" id="psw" required>
      
          <label for="psw-repeat"><b>Repeat Password</b></label>
          <input class="form-control col-5" type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat" required>
          <hr>
      
          <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
          <button type="submit" onClick="return confirm('Apakah data sudah benar?')" class="registerbtn">Register</button>
        </div>
      
        <div class="container signin">
          <p>Already have an account? <a href="<?php echo e(route('login')); ?>">Sign in</a>.</p>
        </div>
      </form>    
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/auth/edit_register.blade.php ENDPATH**/ ?>