
<?php $__env->startSection('content'); ?>
<section class="content">

    
    <form action="<?php echo e(route('kantor.save')); ?>" method="post" class="form-horizontal">
      <?php echo csrf_field(); ?>
        <div class="container">
          <h1>Setting Kantor</h1>
          <p>Please fill in this form to create an office.</p>
          <hr>
      
          <label for="kodekantor"><b>Kode Kantor</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Kode Kantor" name="kodekantor" id="kodekantor" value="<?php echo e(old('kodekantor')); ?>" required>

          <label for="namakantor"><b>Nama Kantor</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nama Kantor" name="namakantor" id="namakantor" value="<?php echo e(old('namakantor')); ?>" required>

          <label for="namadirut"><b>Pimpinan</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nama Pimpinan" name="namadirut" id="namadirut" value="<?php echo e(old('namadirut')); ?>" required>

          <label for="namadirektur"><b>Wakil Pimpinan</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nama Wakil Pimpinan" name="namadirektur" id="namadirektur" value="<?php echo e(old('namadirektur')); ?>" required>

          <label for="namakomisaris"><b>Komisaris</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nama Komisaris Aktif" name="namakomisaris" id="namakomisaris" value="<?php echo e(old('email')); ?>" required>

          <label for="notelp"><b>No Telp</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nomor Telephon" name="notelp" id="notelp" value="<?php echo e(old('notelp')); ?>" required>

          <label for="email"><b>Email</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Email" name="email" id="email" value="<?php echo e(old('email')); ?>" required>

 
     
          
          <button type="submit" onClick="return confirm('Apakah data sudah benar?')" class="registerbtn">Simpan</button>
        </div>
      
        
      </form>    
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/setting/form_kantor.blade.php ENDPATH**/ ?>