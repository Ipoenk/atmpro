
<?php $__env->startSection('content'); ?>
<section class="content">

    
    <form action="/kantorsub.save/<?php echo e($id); ?>" method="post" class="form-horizontal">
      <?php echo csrf_field(); ?>
        <div class="container">
          <h1>Setting Sub Kantor</h1>
          <p>Please fill in this form to create an office.</p>
          <hr>

          <input class="form-control col-5" type="hidden"  name="kantor_id" id="kantor_id" value="<?php echo e($id); ?>" required>

          <label for="kodekantor"><b>Kode Kantor</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Kode Kantor" name="kodekantor" id="kodekantor" value="<?php echo e(old('kodekantor')); ?>" required>

          <label for="koordinator"><b>Nama Koordinator</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Koordinator" name="koordinator" id="koordinator" value="<?php echo e(old('koordinator')); ?>" required>

          <label for="cabang"><b>Cabang</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Cabang" name="cabang" id="cabang" value="<?php echo e(old('cabang')); ?>" required>

          <label for="alamatktr"><b>Alamat</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Alamat" name="alamatktr" id="alamatktr" value="<?php echo e(old('alamatktr')); ?>" required>

          <label for="kotaktr"><b>Kota</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Kota" name="kotaktr" id="kotaktr" value="<?php echo e(old('kotaktr')); ?>" required>

          <label for="notelp"><b>No Telp</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nomor Telephon" name="notelp" id="notelp" value="<?php echo e(old('notelp')); ?>" required>

          <label for="nofax"><b>No. Fax</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan No. Faximile" name="nofax" id="nofax" value="<?php echo e(old('email')); ?>" required>

        </div>
 
     
          
          <div>
             <p>
                <button type="submit" onClick="return confirm('Apakah data sudah benar?')" class="registerbtn">Simpan</button>
            </p>   
          </div>
      
        
      </form>    
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projectemg\AtmPro\resources\views/setting/form_detail_sub_kantor.blade.php ENDPATH**/ ?>