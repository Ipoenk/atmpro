<?php

// use Illuminate\Support\Facades\Route;

// namespace Illuminate\Container;
// namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Container\BindingResolutionException;

use App\Http\Controllers;
use Illuminate\Http\Request;
if(version_compare(PHP_VERSION, '7.2.0','>=')){
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    }

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/','Auth\LoginController@index')->name('login');
Route::post('/otentikasi','Auth\LoginController@otentikasi')->name('otentikasi');
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::get('/home','HomeController@home')->name('home'); 
Route::get('/kredit', 'KreditController@jmlkrd')->name('jmlkrd');
Route::get('nasabah', 'NasabahController@jmlnsb')->name('jmlnsb');

Route::group(['middleware' => ['auth']], function()
{
	Route::get('nasabah', 'NasabahController@index')->name('nasabah')->middleware('can:view.nasabah');
	Route::get('nasabah.create', 'NasabahController@create')->name('nasabah.create')->middleware('can:tambah.nasabah');
	Route::get('nasabah.detail/{id}', 'NasabahController@show')->name('nasabah.detail')->middleware('can:detail.nasabah');
	Route::post('nasabah.edit/{id}', 'NasabahController@update')->name('nasabah.edit')->middleware('can:edit.nasabah');
	Route::post('nasabahs', 'NasabahController@store')->name('nasabah.store')->middleware('can:tambah.nasabah');
});


Route::middleware(['auth'])->group(function(){
	Route::get('/kredit', 'KreditController@index')->name('kredit.index')->middleware('can:view.kredit'); 
	Route::get('/kredit.create/{id}', 'KreditController@create')->name('kredit.create')->middleware('can:tambah.kredit'); 
	Route::post('/kredit.save', 'KreditController@store')->name('kredit.save')->middleware('can:tambah.kredit'); 
	Route::get('/kredit.realisasi', 'KreditController@realisasi')->name('kredit.realisasi')->middleware('can:view.kredit'); 
	Route::get('/kredit.lapangsuran', 'KreditController@lapangsuran')->name('kredit.lapangsuran')->middleware('can:view.kredit'); 
	Route::get('/kredit.pelunasan', 'KreditController@pelunasan')->name('kredit.pelunasan')->middleware('can:view.kredit'); 
	Route::get('/kredit.nominatif', 'KreditController@nominatif')->name('kredit.nominatif')->middleware('can:view.kredit'); 
	Route::get('/kredit.viewangsuran/{id}', 'KreditController@viewangsuran')->name('kredit.viewangsuran')->middleware('can:view.kredit');
	Route::post('/kredit.jaminansimpan/{id}', 'JaminanController@store')->name('kredit.jaminansimpan')->middleware('can:tambah.kredit'); 
	Route::get('/kredit.daftarjaminan/{id}', 'JaminanController@show')->name('kredit.daftarjaminan')->middleware('can:view.kredit');

	Route::get('/kredit.jaminan/{id}', 'JaminanController@index')->name('kredit.jaminan')->middleware('can:tambah.kredit');
});

// Route::middleware(['auth,super admin,user kredit'])->group(function(){
// 	Route::get('/kredit', 'KreditController@index')->name('kredit.index')->middleware('can:view.kredit'); 
// 	Route::get('/kredit.create/{id}', 'KreditController@create')->name('kredit.create')->middleware('can:tambah.kredit'); 
// 	Route::post('/kredit.save', 'KreditController@store')->name('kredit.save')->middleware('can:tambah.kredit'); 
// 	Route::get('/kredit.realisasi', 'KreditController@realisasi')->name('kredit.realisasi')->middleware('can:tambah.kredit'); 
// 	Route::get('/kredit.lapangsuran', 'KreditController@lapangsuran')->name('kredit.lapangsuran')->middleware('can:view.kredit'); 
// 	Route::get('/kredit.pelunasan', 'KreditController@pelunasan')->name('kredit.pelunasan')->middleware('can:view.kredit'); 
// 	Route::get('/kredit.nominatif', 'KreditController@nominatif')->name('kredit.nominatif')->middleware('can:view.kredit'); 
	Route::get('/kasir.transangsuran', 'KasirController@transangsuran')->name('kasir.transangsuran'); 

// });



Route::get('/simpanan', 'SimpananController@index')->name('simpanan'); 
Route::get('/deposito', 'DepositoController@index')->name('deposito'); 

Route::get('/doc.cetakdocpk', 'CetakController@index')->name('doc.cetakdocpk'); 
Route::get('/doc.cetakperjanjian/{id}', 'CetakController@cetak')->name('doc.cetakperjanjian'); 

// Route::get('/kredit', 'KreditController@index')->name('kredit.index'); 
// Route::get('/kredit.create/{id}', 'KreditController@create')->name('kredit.create'); 
// Route::post('/kredit.save', 'KreditController@store')->name('kredit.save'); 
// Route::get('/kredit.realisasi', 'KreditController@realisasi')->name('kredit.realisasi'); 
// Route::get('/kredit.lapangsuran', 'KreditController@lapangsuran')->name('kredit.lapangsuran'); 

// Route::get('/kredit.pelunasan', 'KreditController@pelunasan')->name('kredit.pelunasan'); 
// Route::get('/kredit.nominatif', 'KreditController@nominatif')->name('kredit.nominatif'); 

// Route::get('/kredit', 'KreditController@index')->name('kredit.index'); 
// Route::get('/kredit.create/{id}', 'KreditController@create')->name('kredit.create'); 
// Route::post('/kredit.save', 'KreditController@store')->name('kredit.save'); 
// Route::get('/kredit.realisasi', 'KreditController@realisasi')->name('kredit.realisasi'); 
// Route::get('/kredit.lapangsuran', 'KreditController@lapangsuran')->name('kredit.lapangsuran'); 
// Route::get('/kredit.viewangsuran/{id}', 'KreditController@viewangsuran')->name('kredit.viewangsuran'); 
// Route::get('/kredit.pelunasan', 'KreditController@pelunasan')->name('kredit.pelunasan'); 
// Route::post('/kredit.nominatif', 'KreditController@nominatif')->name('kredit.nominatif'); 

Route::get('/kasir', 'KasirController@index')->name('kasir'); 
Route::get('/accounting', 'AccountingController@index')->name('accounting');  
Route::get('getKabupaten/provinsi/{id}', 'NasabahController@getKabupaten')->name('getKabupaten');
Route::get('getKecamatan/kabupaten/{id}', 'NasabahController@getKecamatan')->name('getKecamatan');
Route::get('getkelurahan/kecamatan/{id}', 'NasabahController@getkelurahan')->name('getkelurahan');


// Route::get('/home','HomeController@home');
// Route::resource('nasabah','NasabahController');
Route::get('/simpanan', 'SimpananController@index')->name('simpanan'); 
Route::get('/deposito', 'DepositoController@index')->name('deposito'); 

Route::get('/setting', 'SettingController@index')->name('setting'); 
Route::get('/setting.register', 'Auth\LoginController@create')->name('register'); 
Route::post('/register.save', 'Auth\LoginController@store')->name('register.save'); 
Route::get('/register.show', 'Auth\LoginController@show')->name('register.show'); 
Route::post('/register.update/{id}', 'Auth\LoginController@update')->name('register.update'); 
Route::get('/register.detail/{id}', 'Auth\LoginController@detail')->name('register.detail'); 
Route::get('/kantor', 'KantorController@index')->name('kantor'); 
Route::get('/kantor.create', 'KantorController@create')->name('kantor.create'); 
Route::post('/kantor.update/{id}', 'KantorController@update')->name('kantor.update'); 
Route::post('/kantor.save', 'KantorController@store')->name('kantor.save'); 
Route::get('/kantor.show', 'KantorController@show')->name('kantor.show'); 
Route::get('/kantor.detail/{id}', 'KantorController@detail')->name('kantor.detail'); 
Route::get('/kantor.sub/{id}', 'KantordetailController@index')->name('kantor.sub'); 
Route::get('/kantorsub.create/{id}', 'KantordetailController@create')->name('kantorsub.create'); 
Route::post('/kantorsub.save/{id}', 'KantordetailController@store')->name('kantorsub.save'); 
Route::get('/kantorsub.show/{id}', 'KantordetailController@show')->name('kantorsub.show'); 
Route::post('/kantorsub.update/{id}', 'KantordetailController@update')->name('kantorsub.update'); 

