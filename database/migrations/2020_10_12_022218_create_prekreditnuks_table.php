<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrekreditnuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prekreditnuks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nasabah_id'); 
            $table->unsignedBigInteger('kreditnuk_id');
            $table->string('no_mohon',10)->nullable();
            $table->string('niknsb',16);
            $table->string('namansb',100);
            $table->string('tmplahirnsb',30);
            $table->date('tgllahirnsb');
            $table->string('kelaminnsb',15);
            $table->string('agamansb',15);
            $table->string('propinsinsbktp',2);
            $table->string('kotansbktp',4);
            $table->string('kecamatannsbktp',7);
            $table->string('desansbktp',10);
            $table->string('kodeposnsbktp',5);
            $table->string('alamatdomisili');
            $table->string('propinsidomisili',2);
            $table->string('kotadomisili',4);
            $table->string('kecamatandomisili',7);
            $table->string('desadomisili',10);
            $table->string('notelp',20);
            $table->string('statusperkawinan',15);
            $table->string('namaibukandung',50);
            $table->string('nikps',16)->nullable();
            $table->string('namaps',50)->nullable();
            $table->string('alamatps')->nullable();
            $table->string('propinsips',2)->nullable();
            $table->string('kotaps',4)->nullable();
            $table->string('kecamatanps',7)->nullable();
            $table->string('desaps',10)->nullable();
            $table->string('kodeposps',5)->nullable();
            $table->string('notelpps',20)->nullable();
            $table->string('kelaminps',15)->nullable();
            $table->string('tmplahirps',30)->nullable();
            $table->date('tgllahirps')->nullable();
            $table->string('agamaps',15)->nullable();
            $table->string('jenispsh',20);
            $table->string('namapsh',50);
            $table->string('alamatpsh');
            $table->string('propinsipsh',2);
            $table->string('kotapsh',4);
            $table->string('kecamatanpsh',7);
            $table->string('desapsh',10);
            $table->string('kodepospsh',5);
            $table->string('notelppsh',20);
            $table->string('bidangusaha',30);
            $table->string('sumberdana',15);
            $table->decimal('pendapatan',15,0);
            $table->decimal('pengeluaran',15,0);
            $table->decimal('tanggungan',4);
            $table->string('ststempattinggal',20);
            $table->string('jenisnsb');
            $table->date('tgl_mohon');
            $table->string('catatan')->nullable();
            $table->string('hsltani',50)->nullable();
            $table->string('hslpegawai',50)->nullable();
            $table->string('hsljasa',50)->nullable();
            $table->string('kegunaan',50)->nullable();
            $table->string('usaha',50)->nullable();
            $table->string('hsldagang',50)->nullable();
            $table->string('hubdagang',50)->nullable();
            $table->string('kreditlalu',50)->nullable();
            $table->string('andalan',50)->nullable();
            $table->string('pjm_nama',25)->nullable();
            $table->string('pjm_alamat',30)->nullable();
            $table->string('keperluan',50)->nullable();
            $table->string('status',20)->nullable();
            $table->string('pengajuan')->nullable();
            $table->string('survey')->nullable();
            $table->string('analisa')->nullable();
            $table->string('no_srtsetuju',20)->nullable();
            $table->date('tgl_srtsetuju')->nullable();
            $table->string('tujuan',200)->nullable();
            $table->date('tglubah')->nullable();
            $table->string('email',150)->nullable();
            $table->string('terkait',4)->nullable();
            $table->string('langgarbmpk',1)->nullable();
            $table->string('lampauibmpk',1)->nullable();
            $table->string('gopublic',1)->nullable();
            $table->string('peringkat',6)->nullable();
            $table->string('lembaga_peringkat',2)->nullable();
            $table->date('tglperingkat')->nullable();
            $table->string('namagrup',150)->nullable();
            $table->string('kodekantor',3)->nullable();
            $table->string('golpihak3',4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prekreditnuks');
    }
}
