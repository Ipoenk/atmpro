<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kredits', function (Blueprint $table) {
            $table->id();
            $table->string('no_kredit',25);
            $table->string('no_ref',25);
            $table->string('kode_kantor',10);
            $table->string('no_mohon',10);
            $table->date('tgl_kredit');
            $table->decimal('pinj_pokok',15,0);
            $table->decimal('plafon',15,0);
            $table->decimal('pinj_prsbunga',8,2);
            $table->string('sistem',15);
            $table->integer('lama');
            $table->date('tgl_mulai');
            $table->date('tgl_akhir');
            $table->boolean('advance')->nullable();
            $table->integer('nbulan')->nullable();
            $table->decimal('angsur_pokok',15,0);
            $table->decimal('angsur_bunga',15,0);
            $table->decimal('pinj_nombunga',15,0);
            $table->decimal('bbt',15,0);
            $table->integer('npokok')->nullable();
            $table->decimal('prs_provisi',8,2);
            $table->decimal('prs_adm',8,2);
            $table->decimal('nom_provisi',15,0);
            $table->decimal('nom_notaris',15,0);
            $table->decimal('nom_adm',15,0);
            $table->decimal('nom_meterai',15,0);
            $table->decimal('nom_asuransi',15,0);
            $table->decimal('assjiwa',15,0);
            $table->decimal('sim_ang',15,0);
            $table->decimal('bakidebet',15,0);
            $table->decimal('saldo_bbt',15,0);
            $table->decimal('saldo_piutang',15,0);
            $table->date('jatuhtempo');
            $table->integer('jml_jaminan')->nullable();
            $table->date('tgl_lunas')->default('1900-01-01')->nullable();
            $table->string('status',10)->nullable();
            $table->decimal('totangspokok',15,0)->nullable();
            $table->decimal('totangsbunga',15,0)->nullable();
            $table->string('norektab',20)->nullable();
            $table->string('nobg',20)->nullable();
            $table->string('kode_pk',2)->nullable();
            $table->string('tt_nama',50)->nullable();
            $table->string('tt_kerja',50)->nullable();
            $table->string('tt_alamat',100)->nullable();
            $table->string('tt_noktp',30)->nullable();
            $table->string('penggunaan',2)->nullable();
            $table->string('golongan',8)->nullable();
            $table->string('organisasi',30)->nullable();
            $table->string('sektor',8)->nullable();
            $table->string('goljamin',50)->nullable();
            $table->string('programkredit',2)->nullable();
            $table->string('namaao',10);
            $table->string('perantara',30);
            $table->date('tgl_kurang')->default('1900-01-01')->nullable();
            $table->date('tgl_ragu')->default('1900-01-01')->nullable();
            $table->date('tgl_macet')->default('1900-01-01')->nullable();
            $table->date('tgl_cadang')->default('1900-01-01')->nullable();
            $table->date('tgl_hapusint')->default('1900-01-01')->nullable();
            $table->string('no_hapusint',20)->nullable();
            $table->date('tgl_hapusbi')->default('1900-01-01')->nullable();
            $table->string('no_hapusbi',20)->nullable();
            $table->date('tgl_lunaslalu')->default('1900-01-01')->nullable();
            $table->string('cek',10)->nullable();
            $table->string('sts_kirim',10)->nullable();
            $table->boolean('panjang')->nullable();
            $table->boolean('mnl_angs')->nullable();
            $table->boolean('mnl_tabl')->nullable();
            $table->string('upload',20)->nullable();
            $table->integer('bagjamin')->nullable();
            $table->string('fasilitas',4)->nullable();
            $table->string('sifatkrd',2)->nullable();
            $table->string('golkredit',8)->nullable();
            $table->string('cdg_nosrt',10)->nullable();
            $table->string('cdg_ok',20)->nullable();
            $table->string('jnsbunga',10)->nullable();
            $table->string('usaha',5)->nullable();
            $table->string('jnsusaha',1)->nullable();
            $table->decimal('nom_trans',15,0);
            $table->string('prdbayar',1)->nullable();
            $table->string('smbdana',2)->nullable();
            $table->string('terkait',1)->nullable();
            $table->string('dampak',20)->nullable();
            $table->string('tt_skdir',20)->nullable();
            $table->date('tt_tglsk')->default('1900-01-01')->nullable();
            $table->string('opr',30)->nullable();
            $table->boolean('restrukturisasi')->nullable();
            $table->string('tgl_restruk',8)->default('19000101')->nullable();
            $table->string('tgl_restruk_awal',8)->default('19000101')->nullable();
            $table->string('tgl_restruk_akhir',8)->default('19000101')->nullable();
            $table->integer('restruk_ke')->nullable();
            $table->string('kode_restruk',2)->nullable();
            $table->string('ket_restruk',50)->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kredits');
    }
}
