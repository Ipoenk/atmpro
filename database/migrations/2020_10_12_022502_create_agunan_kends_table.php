<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgunanKendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agunan_kends', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kredit_id');
            $table->string('no_agunan',20);
            $table->string('no_mohon',10);
            $table->string('merktype',100);
            $table->string('plat',10);
            $table->string('nopolisi',15);
            $table->string('norangka',30);
            $table->string('nomesin',30);
            $table->string('nobpkb',30);
            $table->string('kodya',15);
            $table->string('tahun',4);
            $table->string('warna',100);
            $table->string('jenis',15);
            $table->string('pemilik',25);
            $table->string('alamat',100);
            $table->decimal('niltaksasi',15,0);
            $table->decimal('nilai',15,0);
            $table->decimal('taksasi',15,0);
            $table->decimal('nilpasar',15,0);
            $table->string('ikatan');
            $table->string('ikatanpk');
            $table->boolean('terkait');
            $table->string('status',10);
            $table->string('no_kait',25);
            $table->string('perjanjian',30);
            $table->string('opr',20); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agunan_kends');
    }
}
