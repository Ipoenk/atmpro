<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgunanSertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agunan_serts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kredit_id');
            $table->string('no_agunan',20);
            $table->string('no_mohon',10);
            $table->string('nosertif',20);
            $table->string('jenis',15);
            $table->string('sertstatus',10);
            $table->string('pemilik',25);
            $table->string('alamat',100);
            $table->string('kodya',15);
            $table->string('lokkodya',20);
            $table->decimal('luastanah',15,0);
            $table->decimal('luasbangunan',15,0);
            $table->string('lokasi',100);
            $table->decimal('taksasi',15,0);
            $table->decimal('nilpasar',15,0);
            $table->decimal('niltaksasi',15,0);
            $table->decimal('nilai',15,0);
            $table->string('ikatanpk');
            $table->decimal('nilnjop',15,0);
            $table->decimal('nilhaktg',15,0);
            $table->string('perjanjian',30);
            $table->string('ikatan',15);
            $table->string('status',10);
            $table->boolean('terkait');
            $table->string('no_kait',25);
            $table->string('opr',20);    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agunan_serts');
    }
}
