<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngsuranJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angsuran_jadwals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kredit_id');
            $table->date('tgl_angsur');
            $table->decimal('bayar_pokok',15,0);
            $table->decimal('bayar_bunga',15,0);
            $table->decimal('angs_pokok',15,0);
            $table->decimal('angs_bunga',15,0);
            $table->integer('bayar_ke');
            $table->decimal('provisi',15,0);
            $table->decimal('bitrans',15,0);
            $table->date('akhbulan');
            $table->decimal('ab_pdbunga',15,0);
            $table->decimal('ab_provisi',15,0);
            $table->decimal('ab_bitrans',15,0);
            $table->decimal('jta_pdbunga',15,0);
            $table->decimal('jta_provisi',15,0);
            $table->decimal('jta_bitrans',15,0);
            $table->decimal('jta_adm',15,0);
            $table->decimal('ab_adm',15,0);
            $table->decimal('adm',15,0);
            $table->string('opr',30); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angsuran_jadwals');
    }
}
