<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgunanDeptabnuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agunan_deptabnuks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kreditnuk_id');
            $table->string('no_agunan',20);
            $table->string('no_mohon',10);
            $table->boolean('terkait');
            $table->string('kodya',15);
            $table->string('nodep',20);
            $table->string('namabank',20);
            $table->string('jenis',15);
            $table->string('pemilik',25);
            $table->string('alamat',100);
            $table->date('tempo');
            $table->decimal('nilpasar',15,0);
            $table->decimal('niltaksasi',15,0);
            $table->decimal('nominal',15,0);
            $table->decimal('nilai',15,0);
            $table->decimal('taksasi',15,0);
            $table->integer('jangka');
            $table->string('ikatanpk',25);
            $table->string('perjanjian',25);
            $table->string('ikatan',15);
            $table->string('status',10);
            $table->string('no_kait',25);
            $table->string('opr',20);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agunan_deptabnuks');
    }
}
