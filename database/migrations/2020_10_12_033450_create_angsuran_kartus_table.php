<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngsuranKartusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angsuran_kartus', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kredit_id');
            $table->string('angs_nobukti',10);
            $table->boolean('denda_simp');
            $table->string('kantorkas',20);
            $table->date('angs_tgl');
            $table->decimal('bunga_sldbbt',15,0);
            $table->decimal('plafon',15,0);
            $table->decimal('bakidebet',15,0);
            $table->decimal('potongan',15,0);
            $table->decimal('ongkos',15,0);
            $table->decimal('bunga_hasil',15,0);
            $table->decimal('angs_titippokok',15,0);
            $table->decimal('angs_titipbunga',15,0);
            $table->decimal('denda',15,0);
            $table->decimal('sld_piutang',15,0);
            $table->decimal('angs_pokok',15,0);
            $table->decimal('angs_bunga',15,0);
            $table->decimal('angs_ke',15,0);
            $table->string('cara',15);
            $table->decimal('selisih',15,0);
            $table->string('opr',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angsuran_kartus');
    }
}
