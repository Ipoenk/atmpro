<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKantordetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kantordetails', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kantor_id');
            $table->string('idcabang',5);
            $table->string('cabang',50);
            $table->string('alamat');
            $table->string('telp',20);
            $table->string('fax',20);
            $table->string('kota',50);         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kantordetails');
    }
}
