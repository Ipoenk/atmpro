<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNasabahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nasabahs', function (Blueprint $table) {
            $table->id();
            $table->string('niknsb',16);
            $table->string('namansb',100);
            $table->string('tmplahirnsb',30);
            $table->date('tgllahirnsb');
            $table->string('kelaminnsb',15);
            $table->string('agamansb',15);
            $table->string('alamatnsbktp');
            $table->string('propinsinsbktp',2);
            $table->string('kotansbktp',4);
            $table->string('kecamatannsbktp',7);
            $table->string('desansbktp',10);
            $table->string('kodeposnsbktp',5);
            $table->string('alamatdomisili');
            $table->string('propinsidomisili',2);
            $table->string('kotadomisili',4);
            $table->string('kecamatandomisili',7);
            $table->string('desadomisili',10);
            $table->string('notelpnsb',20);
            $table->string('email',150);
            $table->string('statusperkawinan',15);
            $table->string('namaibukandung',50);
            $table->string('nikps',16)->nullable();
            $table->string('namaps',50)->nullable();
            $table->string('alamatps')->nullable();
            $table->string('propinsips',2)->nullable();
            $table->string('kotaps',4)->nullable();
            $table->string('kecamatanps',7)->nullable();
            $table->string('desaps',10)->nullable();
            $table->string('kodeposps',5)->nullable();
            $table->string('notelpps',20)->nullable();
            $table->string('kelaminps',15)->nullable();
            $table->string('tmplahirps',30)->nullable();
            $table->date('tgllahirps')->nullable();
            $table->string('agamaps',15)->nullable();
            $table->string('jenispsh',20)->nullable();
            $table->string('namapsh',50);
            $table->string('alamatpsh');
            $table->string('propinsipsh',2);
            $table->string('kotapsh',4);
            $table->string('kecamatanpsh',7);
            $table->string('desapsh',10);
            $table->string('kodepospsh',5);
            $table->string('notelppsh',20);
            $table->string('bidangusaha',30);
            $table->string('sumberdana',15);
            $table->decimal('pendapatan',15,0);
            $table->decimal('pengeluaran',15,0);
            $table->integer('tanggungan');
            $table->string('ststempattinggal',20);
            $table->string('pengajuan',50)->nullable();
            $table->string('jenisnsb');
            $table->string('opr',30)->nullable();            
            $table->integer('keanggotaan_id')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nasabahs');
    }
}
