<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKreditnuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kreditnuks', function (Blueprint $table) {
            $table->id();
            $table->string('no_kredit',25);
            $table->string('no_ref',25);
            $table->string('kode_kantor',10);
            $table->string('no_mohon',10);
            $table->date('tgl_kredit');
            $table->float('pinj_pokok');
            $table->float('plafon');
            $table->decimal('pinj_prsbunga',8,2);
            $table->string('sistem',15);
            $table->integer('lama');
            $table->date('tgl_mulai');
            $table->date('tgl_akhir');
            $table->boolean('advance');
            $table->integer('nbulan');
            $table->float('angsur_pokok');
            $table->float('angsur_bunga');
            $table->float('pinj_nombunga');
            $table->float('bbt');
            $table->integer('npokok');
            $table->decimal('prs_provisi',8,2);
            $table->decimal('prs_adm',8,2);
            $table->float('nom_provisi');
            $table->float('nom_notaris');
            $table->float('nom_adm');
            $table->float('nom_meterai');
            $table->float('nom_asuransi');
            $table->float('assjiwa');
            $table->float('sim_ang');
            $table->float('bakidebet');
            $table->float('saldo_bbt');
            $table->float('saldo_piutang');
            $table->date('jatuhtempo');
            $table->string('status',20);
            $table->string('opr',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kreditnuks');
    }
}
