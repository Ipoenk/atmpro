<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngsuranBayarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angsuran_bayars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kredit_id');
            $table->string('nobukti',10);
            $table->string('kantorkas',20);
            $table->date('tanggal');
            $table->decimal('ongkos',15,0);
            $table->decimal('potongan',15,0);
            $table->decimal('pokok',15,0);
            $table->decimal('bunga',15,0);
            $table->decimal('denda',15,0);
            $table->decimal('denda_kena',15,0);
            $table->decimal('denda_lalu',15,0);
            $table->decimal('denda_utang',15,0);
            $table->string('cara',10);
            $table->decimal('selisih',15,0);
            $table->string('opr',10);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angsuran_bayars');
    }
}
