<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinsiSeeder extends Seeder
{
    protected $table;
 
    protected $filename;

    public function __construct()
    {
 
        $this->table = 'provinsis';
        $this->filename = base_path('database/csv/provinsi.csv');
     
    }
    public function run()
    {
        DB::table($this->table)->delete();
        $header = null;
        $seedData = $this->seedFromCSV($this->filename, $header);
        foreach ($seedData as $key => $provinsis) {
            $seedData[$key] = $provinsis;
            $seedData[$key]['created_at'] = \Carbon\Carbon::now();
            $seedData[$key]['updated_at'] = \Carbon\Carbon::now();
        }
    
        $collection = collect($seedData);
            foreach ($collection->chunk(50) as $chunk) {
                DB::table('provinsis')->insert($chunk->toArray());
            }
    }

    private function seedFromCSV($filename, $header)
    {
        $delimiter = ",";

        if(!file_exists($filename) || !is_readable($filename))
        {
            return FALSE;
        }
 
        $data = array();
 
        if(($handle = fopen($filename, 'r')) !== FALSE)
        {
            while(($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
 
        return $data;
    }
}
