<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

     
        $this->call(KantorSeeder::class);
        $this->call(KantordetailSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionsRoleTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProvinsiSeeder::class);
        $this->call(KabupatenSeeder::class);
        $this->call(KecamatanSeeder::class);
        $this->call(KelurahanSeeder::class);   
        $this->call(Master_kodepkSeeder::class);   
        $this->call(Master_penggunaanSeeder::class);   
        $this->call(Master_kerjaSeeder::class);   
        $this->call(Master_hubbankSeeder::class);   
        $this->call(Master_goljaminSeeder::class);   
        $this->call(Master_golonganSeeder::class);   
        $this->call(Master_sektorSeeder::class);   
        $this->call(Master_usahaSeeder::class);   
        $this->call(KreditSeeder::class);   
        $this->call(NasabahSeeder::class);   
        $this->call(PrekreditSeeder::class);   
        $this->call(Angsuran_jadwalSeeder::class);   
        $this->call(Angsuran_bayarSeeder::class);   
        $this->call(Angsuran_kartuSeeder::class);   
        $this->call(Agunan_kendSeeder::class);   
        $this->call(Agunan_sertSeeder::class);   

    }
}
