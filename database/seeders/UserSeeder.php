<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Super Admin',
            'role_id' => 10, // Super Admin
            'kantor_id' => 1, // Super Krdeit
            'email' => 'ipoenk@yahoo.com',
            'password' => Hash::make('12345678')
        ]);

        // DB::table('users')->insert([
        //     'name' => 'Yeni',
        //     'role_id' => 3, // Super Krdeit
        //     'kantor_id' => 1, // Super Krdeit
        //     'email' => 'yeni@yahoo.com',
        //     'password' => Hash::make('12345678')
        // ]);

        // DB::table('users')->insert([
        //     'name' => 'Yayuk',
        //     'role_id' => 4, // User Tabungan
        //     'kantor_id' => 1, // Super Krdeit
        //     'email' => 'yayuk@yahoo.com',
        //     'password' => Hash::make('12345678')
        // ]);

        // DB::table('users')->insert([
        //     'name' => 'Basri',
        //     'role_id' => 5, // Super Tabungan
        //     'kantor_id' => 1, // Super Krdeit
        //     'email' => 'basri@yahoo.com',
        //     'password' => Hash::make('12345678')
        // ]);
    }
}
