<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class PermissionsRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        //role
        $userkrd = Role::where('level','1')->first();

        $kacab = Role::where('level','2')->first();

        $kakrd = Role::where('level','3')->first();

        $usertab = Role::where('level','4')->first();

        $katab = Role::where('level','5')->first();

        $userksr = Role::where('level','6')->first();

        $ao = Role::where('level','8')->first();

        $spvao = Role::where('level','9')->first();

        $super = Role::where('level','10')->first();

        //permission nasabah
        $viewnasabah = Permission::where('kode','view.nasabah')->first()->id;
        $tambahnasabah = Permission::where('kode','tambah.nasabah')->first()->id;
        $detailnasabah = Permission::where('kode','detail.nasabah')->first()->id;
        $editnasabah = Permission::where('kode','edit.nasabah')->first()->id;
        $hapusnasabah = Permission::where('kode','delete.nasabah')->first()->id;

		//permission realisasi
        $viewkredit = Permission::where('kode','view.kredit')->first()->id;
        $tambahkredit = Permission::where('kode','tambah.kredit')->first()->id;
        $editkredit = Permission::where('kode','edit.kredit')->first()->id;
        $hapuskredit = Permission::where('kode','delete.kredit')->first()->id;

        //permission tabungan
        $viewtab = Permission::where('kode','view.tabungan')->first()->id;
        $tambahtab = Permission::where('kode','tambah.tabungan')->first()->id;
        $edittab = Permission::where('kode','edit.tabungan')->first()->id;
        $hapustab = Permission::where('kode','delete.tabungan')->first()->id;

        //permission deposito
        $viewdepo = Permission::where('kode','view.deposito')->first()->id;
        $tambahdepo = Permission::where('kode','tambah.deposito')->first()->id;
        $editdepo = Permission::where('kode','edit.deposito')->first()->id;
        $hapusdepo = Permission::where('kode','delete.deposito')->first()->id;

        //permission kasir
        $viewkas = Permission::where('kode','view.kasir')->first()->id;
        $tambahkas = Permission::where('kode','tambah.kasir')->first()->id;
        $editkas = Permission::where('kode','edit.kasir')->first()->id;
        $hapuskas = Permission::where('kode','delete.kasir')->first()->id;
        
        //mapping user

        $permissionUserKrd = [
        	$viewnasabah,
        	$tambahnasabah,
        	$detailnasabah,
        	$editnasabah,
        	$viewkredit,
        	$tambahkredit
        	
        	
        ];

        $permissionSuper = [
        	$viewnasabah,
        	$tambahnasabah,
        	$detailnasabah,
        	$editnasabah,
        	$hapusnasabah,
        	$viewkredit,
        	$tambahkredit,
	    	$editkredit,
        	$hapuskredit,
            $viewtab,
            $viewdepo,
            $tambahtab,
            $edittab,
            $hapustab,
            $tambahdepo,
            $editdepo,
            $hapusdepo,
            $viewkas,
            $editkas,
            $hapuskas,
            $tambahkas
        ];

        $permissionKakrd = [
            $viewnasabah,
            $tambahnasabah,
            $detailnasabah,
            $editnasabah,
            $hapusnasabah,
            $viewkredit,
            $tambahkredit,
            $editkredit,
            $hapuskredit
        ];

        $permissionAO = [
            $viewnasabah,
            $detailnasabah,
            $viewkredit            
        ];

        $permissionSpvAo = [
            $viewnasabah,
            $detailnasabah,
            $viewkredit,
            $viewtab,
            $viewdepo
        ];

        $permissionKacab = [
            $viewnasabah,
            $tambahnasabah,
            $detailnasabah,
            $editnasabah,
            $hapusnasabah,
            $viewkredit,
            $tambahkredit,
            $editkredit,
            $hapuskredit,
            $viewtab,
            $viewdepo,
            $tambahtab,
            $edittab,
            $hapustab,
            $tambahdepo,
            $editdepo,
            $hapusdepo,
            $viewkas,
            $editkas,
            $hapuskas

        ];

        $permissionUserKsr = [
            $viewnasabah,
            $detailnasabah,
            $viewkredit,
            $viewkas,
            $editkas,
            $tambahkas,
        ];

        $permissionUserTab = [
            $viewnasabah,
            $detailnasabah,
            $viewtab,
            $tambahtab,
            
        ];

        $permissionKaTab = [
            $viewnasabah,
            $detailnasabah,
            $viewtab,
            $tambahtab,
            $edittab,
            $hapustab,
            $viewdepo,
            $tambahdepo,
            $editdepo,
            $hapusdepo
            
        ];



        $userkrd->permissions()->sync($permissionUserKrd);
        $super->permissions()->sync($permissionSuper);
        $kakrd->permissions()->sync($permissionKakrd);
        $userksr->permissions()->sync($permissionUserKsr);
        $kacab->permissions()->sync($permissionKacab);
        $ao->permissions()->sync($permissionAO);
        $spvao->permissions()->sync($permissionSpvAo);
        $katab->permissions()->sync($permissionKatab);
        
    }
}
