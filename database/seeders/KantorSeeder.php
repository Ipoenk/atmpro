<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kantor;
use Illuminate\Support\Facades\DB;

class KantorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kantor::truncate();
        DB::table('kantors')->insert([
            'idkantor'      => 'ATM',
            'namakantor'    => 'ANDALAN TATA MANAJEMEN', // Super Admin
            'dirut'         => '    WINDO DARMAWAN',
            'direktur'      => 'ALIFIN DARMAWAN',
            'koma'          => 'INGE ENGELICA',
            'kontak'        => '0341-449922',
            'email'         => 'atm@sulusindo.net'
        ]);
    }
}
