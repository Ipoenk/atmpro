<?php

namespace Database\Seeders;

use App\Models\Master_penggunaan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Master_penggunaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        // Master_penggunaan::truncate();
        $this->table = 'Master_penggunaans';
        $this->filename = base_path('database/csv/master_penggunaan.csv');
    }
    public function run()
    {
        DB::table($this->table)->delete();
        $header =  null;
        $seedData = $this->seedFromCSV($this->filename, $header);
        foreach ($seedData as $key => $master_penggunaans) {
            $seedData[$key] = $master_penggunaans;
            $seedData[$key]['created_at'] = \Carbon\Carbon::now();
            $seedData[$key]['updated_at'] = \Carbon\Carbon::now();
        }
    
        $collection = collect($seedData);
            foreach ($collection->chunk(50) as $chunk) {
                DB::table('Master_penggunaans')->insert($chunk->toArray());
            }

    }

    private function seedFromCSV($filename, $header)
    {
        $delimiter = ",";

        if(!file_exists($filename) || !is_readable($filename))
        {
            return FALSE;
        }
 
        $data = array();
 
        if(($handle = fopen($filename, 'r')) !== FALSE)
        {
            while(($row = fgetcsv($handle, 2000, $delimiter)) !== FALSE)
            {
                if(!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
 
        return $data;
    }
}
