<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        DB::table('roles')->insert([
            'name' => 'User Kredit',
            'code' => 'user kredit',
            'level' => 1
        ]);

        DB::table('roles')->insert([
            'name' => 'Kepala Cabang',
            'code' => 'head-office',
            'level' => 2
        ]);

        DB::table('roles')->insert([
            'name' => 'Super Kredit',
            'code' => 'kabag kredit',
            'level' => 3
        ]);

        DB::table('roles')->insert([
            'name' => 'User Tabungan',
            'code' => 'user tabungan',
            'level' => 4
        ]);

        DB::table('roles')->insert([
            'name' => 'Super Tabungan',
            'code' => 'kabag tabungan',
            'level' => 5
        ]);

        DB::table('roles')->insert([
            'name' => 'User Kasir',
            'code' => 'user kasir',
            'level' => 6
        ]);

        DB::table('roles')->insert([
            'name' => 'Super Kasir',
            'code' => 'kabag kasir',
            'level' => 7
        ]);

        DB::table('roles')->insert([
            'name' => 'User AO',
            'code' => 'user ao',
            'level' => 8
        ]);

        DB::table('roles')->insert([
            'name' => 'Super AO',
            'code' => 'spv ao',
            'level' => 9
        ]);

        DB::table('roles')->insert([
            'name' => 'Super Admin',
            'code' => 'super-admin',
            'level' => 10
        ]);
    }
}
