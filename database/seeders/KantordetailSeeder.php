<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kantordetail;
use Illuminate\Support\Facades\DB;

class KantordetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kantordetail::truncate();
        DB::table('kantordetails')->insert([
            'kantor_id'     => 1,
            'kodekantor'    => 'ATP', // Super Admin
            'idcabang'      => '',
            'koordinator'   => 'PUTRI DAYU',
            'cabang'        => 'BENGAWANSOLO',
            'alamat'        => 'Jl. R. Tumenggung Suryo 32-34',
            'telp'          => '0341-449922',
            'fax'           => '0341-449922',
            'kota'          => 'MALANG',
            'status'        => 'PUSAT',
        ]);
    }
}
