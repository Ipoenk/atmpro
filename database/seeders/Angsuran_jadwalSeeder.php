<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Angsuran_jadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        // Master_goljamin::truncate();
        $this->table = 'angsuran_jadwals';
        $this->filename = base_path('database/csv/angsuran_jadwal.csv');
    }
    public function run()
    {
        DB::table($this->table)->delete();
        $header =  null;
        $seedData = $this->seedFromCSV($this->filename, $header);
        foreach ($seedData as $key => $angsuran_jadwals) {
            $seedData[$key] = $angsuran_jadwals;
            $seedData[$key]['created_at'] = \Carbon\Carbon::now();
            $seedData[$key]['updated_at'] = \Carbon\Carbon::now();
        }
    
        $collection = collect($seedData);
            foreach ($collection->chunk(50) as $chunk) {
                DB::table('angsuran_jadwals')->insert($chunk->toArray());
            }

    }

    private function seedFromCSV($filename, $header)
    {
        $delimiter = ";";

        if(!file_exists($filename) || !is_readable($filename))
        {
            return FALSE;
        }
 
        $data = array();
 
        if(($handle = fopen($filename, 'r')) !== FALSE)
        {
            while(($row = fgetcsv($handle, 2000, $delimiter)) !== FALSE)
            {
                if(!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
 
        return $data;
    }
}

