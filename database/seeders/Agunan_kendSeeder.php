<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class Agunan_kendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        // Master_goljamin::truncate();
        $this->table = 'agunan_kends';
        $this->filename = base_path('database/csv/agunan_kend.csv');
    }
    public function run()
    {
        DB::table($this->table)->delete();
        $header =  null;
        $seedData = $this->seedFromCSV($this->filename, $header);
        foreach ($seedData as $key => $agunan_kends) {
            $seedData[$key] = $agunan_kends;
            $seedData[$key]['created_at'] = \Carbon\Carbon::now();
            $seedData[$key]['updated_at'] = \Carbon\Carbon::now();
        }
    
        $collection = collect($seedData);
            foreach ($collection->chunk(50) as $chunk) {
                DB::table('agunan_kends')->insert($chunk->toArray());
            }

    }

    private function seedFromCSV($filename, $header)
    {
        $delimiter = ";";

        if(!file_exists($filename) || !is_readable($filename))
        {
            return FALSE;
        }
 
        $data = array();
 
        if(($handle = fopen($filename, 'r')) !== FALSE)
        {
            while(($row = fgetcsv($handle, 2000, $delimiter)) !== FALSE)
            {
                if(!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
 
        return $data;
    }
}
