<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();
        //nasabah
        Permission::create([
        	'kode' => 'view.nasabah',
        	'nama' => 'Melihat Nasabah'
        ]);
        Permission::create([
        	'kode' => 'tambah.nasabah',
        	'nama' => 'Menambah Nasabah'
        ]);
        Permission::create([
        	'kode' => 'detail.nasabah',
        	'nama' => 'Melihat Detil Nasabah'
        ]);
        Permission::create([
        	'kode' => 'edit.nasabah',
        	'nama' => 'Edit Nasabah'
        ]);
        Permission::create([
        	'kode' => 'delete.nasabah',
        	'nama' => 'Hapus Nasabah'
        ]);
		// kredit
        Permission::create([
        	'kode' => 'view.kredit',
        	'nama' => 'Melihat Data Kredit'
        ]);
        Permission::create([
        	'kode' => 'tambah.kredit',
        	'nama' => 'Menambah Data Realisasi'
        ]);
      
        Permission::create([
        	'kode' => 'edit.kredit',
        	'nama' => 'Koreksi Data Realisasi'
        ]);
        Permission::create([
        	'kode' => 'delete.kredit',
        	'nama' => 'Hapus Data Realisasi'
        ]);
    
        // tabungan
        Permission::create([
            'kode' => 'view.tabungan',
            'nama' => 'Melihat Data Tabungan'
        ]);
        Permission::create([
            'kode' => 'tambah.tabungan',
            'nama' => 'Menambah Data Tabungan'
        ]);
      
        Permission::create([
            'kode' => 'edit.tabungan',
            'nama' => 'Koreksi Data Tabungan'
        ]);
        Permission::create([
            'kode' => 'delete.tabungan',
            'nama' => 'Hapus Data Tabungan'
        ]);

        // deposito
        Permission::create([
            'kode' => 'view.deposito',
            'nama' => 'Melihat Data Deposito'
        ]);
        Permission::create([
            'kode' => 'tambah.deposito',
            'nama' => 'Menambah Data Deposito'
        ]);
      
        Permission::create([
            'kode' => 'edit.deposito',
            'nama' => 'Koreksi Data Deposito'
        ]);
        Permission::create([
            'kode' => 'delete.deposito',
            'nama' => 'Hapus Data Deposito'
        ]);

        // kasir
        Permission::create([
            'kode' => 'view.kasir',
            'nama' => 'Melihat Data Transaksi Kasir'
        ]);
        Permission::create([
            'kode' => 'tambah.kasir',
            'nama' => 'Menambah Data Transaksi Kasir'
        ]);
      
        Permission::create([
            'kode' => 'edit.kasir',
            'nama' => 'Koreksi Data Transaksi Kasir'
        ]);
        Permission::create([
            'kode' => 'delete.kasir',
            'nama' => 'Hapus Data Transaksi Kasir'
        ]);
    }
}
