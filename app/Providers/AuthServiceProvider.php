<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerProAtm();

        //
    }

    public function registerProAtm(){

        // Gate::define('akses',function($user){
        //     $role = User::find($user->name)->role;
        //     foreach ($role as $roles) {
        //         if ($roles->name == 'Super Admin') {
        //             return true;
        //         }
        //     }
        //     return null;
        // });
        
        Gate::define('view.nasabah',function($user){
            return $user->role->hasPermission('view.nasabah');
        });
        Gate::define('tambah.nasabah',function($user){
            return $user->role->hasPermission('tambah.nasabah');
        });
        Gate::define('detail.nasabah',function($user){
            return $user->role->hasPermission('detail.nasabah');
        });
        Gate::define('edit.nasabah',function($user){
            return $user->role->hasPermission('edit.nasabah');
        });
        Gate::define('delete.nasabah',function($user){
            return $user->role->hasPermission('delete.nasabah');
        });
        
        Gate::define('view.kredit',function($user){
            return $user->role->hasPermission('view.kredit');
        });
        Gate::define('tambah.kredit',function($user){
            return $user->role->hasPermission('tambah.kredit');
        });
        
        Gate::define('edit.kredit',function($user){
            return $user->role->hasPermission('edit.kredit');
        });
        Gate::define('delete.kredit',function($user){
            return $user->role->hasPermission('delete.kredit');
        });
    }
}
