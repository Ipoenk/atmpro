<?php

namespace App\Http\Controllers;
use App\Models\Kantor;
use App\Models\Kantordetail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class KantordetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $kantors = Kantordetail::join('kantors','kantors.id','kantordetails.kantor_id')
            ->select('kantors.id','kantors.namakantor','kantors.dirut','kantors.direktur','kantors.koma',
            'kantordetails.id as detail_id','kantordetails.kodekantor','kantordetails.idcabang',
            'kantordetails.koordinator','kantordetails.cabang','kantordetails.alamat',
            'kantordetails.kota','kantordetails.telp','kantordetails.fax','kantordetails.status')
            ->where('kantor_id',$id)
            ->get();
        return view('setting.daftar_kantor_detail',compact('kantors','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('setting.form_detail_sub_kantor',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kodekantor'    => 'required',
            'koordinator'   => 'required',
            'cabang'        => 'required',
            'alamatktr'     => 'required',
            'kotaktr'       => 'required',
            'notelp'        => 'required',
            'nofax'         => 'required',
            'status'        => 'required'
        ]);

        $kantor = new Kantordetail;
            $kantor->kantor_id = strtoupper($request->kantor_id);
            $kantor->idcabang = '';
            $kantor->kodekantor = strtoupper($request->kodekantor);
            $kantor->koordinator = strtoupper($request->koordinator);
            $kantor->cabang = strtoupper($request->cabang);
            $kantor->alamat = strtoupper($request->alamatktr);
            $kantor->kota = strtoupper($request->kotaktr);
            $kantor->telp = strtoupper($request->notelp);
            $kantor->fax = strtoupper($request->nofax);
            $kantor->status = strtoupper($request->status);
        $kantor->save();    

        return redirect()->route('kantor.sub', [$kantor->kantor_id])->with('Data Sudah sukses tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kantor = Kantordetail::where('id',$id)
            ->first();
         return view('setting.edit_form_detail_sub_kantor',compact('kantor'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'kodekantor'    => 'required',
            'koordinator'   => 'required',
            'cabang'        => 'required',
            'alamatktr'     => 'required',
            'kotaktr'       => 'required',
            'notelp'        => 'required',
            'nofax'         => 'required',
            'status'        => 'required'
        ]);

        Kantordetail::where('id',$id)
		->update([
            'kantor_id'     => strtoupper($request->kantor_id),
            'idcabang'      => '',
            'kodekantor'    => strtoupper($request->kodekantor),
            'koordinator'   => strtoupper($request->koordinator),
            'cabang'        => strtoupper($request->cabang),
            'alamat'        => strtoupper($request->alamatktr),
            'kota'          => strtoupper($request->kotaktr),
            'telp'          => strtoupper($request->notelp),
            'fax'           => strtoupper($request->nofax),
            'status'        => strtoupper($request->status),
        ]);

        return redirect()->route('kantor.sub', [$id])->with('Data Sudah sukses tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
