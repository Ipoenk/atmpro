<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\Kantor;
class HomeController extends Controller
{
    public function home()
    {
    //   $id_kantor = Auth()->User->name;  
      $id_kantor = Auth()->user()->kantor_id; 
      $kantor = Kantor::join('kantordetails','kantordetails.kantor_id','kantors.id')
            ->select('kantors.namakantor','kantordetails.cabang','kantordetails.alamat','kantordetails.kota','kantordetails.telp','kantordetails.status')
            ->where('kantordetails.id','=',$id_kantor)
            ->first();  
    return view('template.homecontent',compact('kantor'));
	}
}
