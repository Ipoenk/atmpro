<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Role; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Alert;


class LoginController extends Controller
{
    public function index(){
    	return view('auth.login');
    }

    public function otentikasi(Request $request){
    	$request->validate([
    		'post_email' => 'required',
    		'post_password' => 'required'
    	]);

    	if(Auth::attempt([
    		'email' => $request->post_email,
    		'password' => $request->post_password
    	],false)){
            toast('Anda Berhasil Login','success', 'top-right');
    		return redirect(route('home'));
    	}else{
            Alert::warning('Peringatan', 'Email Atau Password Anda Salah!!!');
    		return redirect(route('login'));
    	}

    	
    }

	public function create()
    {
		$kantors = DB::table('kantordetails')
		->join('kantors','kantors.id','kantordetails.kantor_id')
		->select('kantors.namakantor','kantordetails.kodekantor','kantordetails.id as id_cabang','kantordetails.cabang')
		->get();
		$roles = DB::table('roles')->pluck("name","level");
        return view('auth.register',compact('roles','kantors'));
	}
	
	public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Nama'          => 'required',
            'level'       	=> 'required',
            'kantor'       	=> 'required',
            'email'         => 'required|email|unique:users,email',
            'psw'      		=> 'required|min:8',
            'psw-repeat' 	=> 'same:psw'
        ]);

        $user = new User;
            $user->name = strtoupper($request->nama);
            $user->role_id = $request->level;
            $user->kantor_id = $request->kantor;
            $user->email = $request->email;
            $user->password =  Hash::make($request->psw);
		$user->save();    
		return redirect('register.show');

    }

	public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'Nama'          => 'required',
            'level'       	=> 'required',
            'kantor'       	=> 'required',
            'email'         => 'required|email|unique:users,email',
            'psw'      		=> 'required|min:8',
            'psw-repeat' 	=> 'same:psw'
        ]);

		User::where('id',$id)
		->update([
            'name' 		=> strtoupper($request->nama),
            'role_id' 	=> $request->level,
            'kantor_id' 	=> $request->kantor,
            'email' 	=> $request->email,
			'password' 	=>  Hash::make($request->psw)
		]);

		return redirect('register.show');

	}
	
	public function detail($id){
		$kantors = DB::table('kantordetails')
		->join('kantors','kantors.id','kantordetails.kantor_id')
		->select('kantors.namakantor','kantordetails.kodekantor','kantordetails.id as id_cabang','kantordetails.cabang')
		->get();

		$roles = DB::table('roles')->pluck("name","level");
		$user = User::join('roles','roles.id','users.role_id')
				->select('users.id','users.name','users.email','users.role_id','roles.name as level')
				->where('users.id',$id)->first();
		return view('auth.edit_register',compact('user','roles','kantors'));		

	}

	public function show(){
		$kantors = DB::table('kantordetails')
		->join('kantors','kantors.id','kantordetails.kantor_id')
		->select('kantors.namakantor','kantordetails.kodekantor','kantordetails.id as id_cabang','kantordetails.cabang')
		->get();

		$users = DB::table('users')
		->join('roles','roles.id','users.role_id')
		->select('users.id','users.name','users.email','users.password','roles.name as level')
		->get();
	return view('auth.daftaruser',compact('users','kantors'));
	}

    public function logout(){
    	Auth::logout();
        
    	return redirect(route('login'))->with('toast_success', 'Terima Kasih!!');
    }
}
