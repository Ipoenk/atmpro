<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nasabah;
use App\Models\Kredit;
use App\Models\Prekredit;
use App\Models\Angsuran_jadwal;
use App\Models\Angsuran_bayar;
use App\Models\Angsuran_kartu;
use App\Models\Master_kodepk;
use App\Models\Master_goljamin;
use App\Models\Master_golongan;
use App\Models\Master_hubunganbank;
use App\Models\Master_kerja;
use App\Models\Master_penggunaan;
use App\Models\Master_sektor;
use App\Models\Master_usaha;
use App\Models\Kantor;
use App\Models\Agunan_sert;
use App\Models\Agunan_kend;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;

class CetakController extends Controller
{
    public function index(Request $request)
    {

        $model = $request->metodelaporan;
        if($model == "PERTANGGAL"){
            $ketmodel = 'Pertanggal';    
            $stgl1 = date("Y-m-d", strtotime($request->tgl1));            
            $realisasi = DB::table('kredits')
                ->join('prekredits','kredits.id','prekredits.kredit_id')
                ->select('kredits.*','prekredits.niknsb','prekredits.namansb','prekredits.tmplahirnsb','prekredits.tgllahirnsb','prekredits.kelaminnsb','prekredits.agamansb')
                ->where('kredits.tgl_kredit','=',$stgl1)
                ->orderBy('kredits.no_kredit')
                ->get();
            // $realisasi = Kredit::join('prekredits','prekredits.kredit_id','=','kredits.id')
            // ->where('kredits.id','=',$vid)
            // ->orderBy('kredits.no_kredit')
            // ->get();

            Log::info($realisasi);
            // Log::info($stgl1);

            return view('kredit.cetak_dokumen',compact('realisasi','stgl1','model','ketmodel'));

        }
        else if($model == "PERBULAN"){
            $ketmodel = 'Perbulan';    
                
            $blnlap = $request->bulanlap;            
            $tahunlap = $request->tahunlap;   
            if($blnlap =1){
                $namabulan = 'JANUARI'; 
            } 
            else if($blnlap =2){
                $namabulan = 'PEBRUARI';
            } 
            else if($blnlap =3){
                $namabulan = 'MARET';
            } 
            else if($blnlap =4){
                    $namabulan = 'APRIL';
            } 
            else if($blnlap =5){
                $namabulan = 'MEI';
            } 
            else if($blnlap =6){
                $namabulan = 'JUNI';
            } 
            else if($blnlap =7){
                $namabulan = 'JULI';
            } 
            else if($blnlap =8){
                $namabulan = 'AGUSTUS';
            } 
            else if($blnlap =9){
                $namabulan = 'SEPTEMBER';
            } 
            else if($blnlap =10){
                $namabulan = 'OKTOBER';
            } 
            else if($blnlap =11){
                $namabulan = 'NOPEMBER';
            } 
            else {
                $namabulan = 'DESEMBER';
            }

            $realisasi = DB::table('kredits')
            ->join('prekredits','kredits.id','prekredits.kredit_id')
            ->select('kredits.*','prekredits.niknsb','prekredits.namansb','prekredits.tmplahirnsb','prekredits.tgllahirnsb','prekredits.kelaminnsb','prekredits.agamansb')
            ->whereMonth('kredits.tgl_kredit','=',$blnlap)
            ->whereYear('kredits.tgl_kredit','=',$tahunlap)
            ->orderBy('kredits.tgl_kredit','asc')
            ->orderBy('kredits.no_kredit','asc')
            ->get();
            return view('kredit.cetak_dokumen',compact('realisasi','blnlap','tahunlap','namabulan','model'));

        }
        else {
            $ketmodel = 'Antar Tanggal';    
            $stgl1 = date("Y-m-d", strtotime($request->attgl1));            
            $stgl2 = date("Y-m-d", strtotime($request->attgl2));            
            $realisasi = DB::table('kredits')
            ->join('prekredits','kredits.id','prekredits.kredit_id')
            ->select('kredits.*','prekredits.niknsb','prekredits.namansb','prekredits.tmplahirnsb','prekredits.tgllahirnsb','prekredits.kelaminnsb','prekredits.agamansb')
            ->where('kredits.tgl_kredit','>',$stgl1)
            ->where('kredits.tgl_kredit','<',$stgl2)
            ->orderBy('kredits.tgl_kredit','asc')
            ->orderBy('kredits.no_kredit','asc')
            ->get();
            return view('kredit.cetak_dokumen',compact('realisasi','stgl1','stgl2','model','ketmodel'));
    
        }
    }
    public function cetak($id)
    {
        $id_kantor = Auth()->user()->kantor_id; 
        $kantor = Kantor::join('kantordetails','kantordetails.kantor_id','kantors.id')
              ->select('kantors.namakantor','kantors.dirut','kantors.direktur','kantordetails.cabang','kantordetails.alamat','kantordetails.kota','kantordetails.telp','kantordetails.status')
              ->where('kantordetails.id','=',$id_kantor)
              ->first();  
        $datakredit = Prekredit::join('kredits', 'prekredits.kredit_id', '=', 'kredits.id')
              ->select('prekredits.kredit_id', 'prekredits.namansb','prekredits.tmplahirnsb','prekredits.tgllahirnsb','prekredits.statusperkawinan','prekredits.alamatnsbktp','prekredits.niknsb','prekredits.notelp',
              'kredits.tgl_kredit','kredits.no_kredit','kredits.lama','kredits.sistem','kredits.pinj_pokok','kredits.pinj_prsbunga',
              'kredits.tgl_mulai','kredits.tgl_akhir','kredits.jatuhtempo','kredits.bbt','kredits.angsur_pokok','kredits.angsur_bunga',
              'kredits.nom_provisi','kredits.nom_adm','kredits.nom_notaris','kredits.nom_meterai','kredits.nom_asuransi',
              'kredits.assjiwa','kredits.sim_ang','kredits.nom_trans','kredits.namaao','kredits.perantara')
              ->where('kredits.id',$id)
              ->first();
        $agunan_sert = Agunan_sert::where('kredit_id',$id)
        ->get();      
        $agunan_kend = Agunan_kend::where('kredit_id',$id)
        ->get();      
        return view('doc.modkerja_flat',compact('kantor','datakredit','agunan_sert','agunan_kend'));

    }
}
