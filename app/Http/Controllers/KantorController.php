<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kantor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KantorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $kantors = Kantor::all();
       return view('setting.daftar_kantor',compact('kantors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.form_kantor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idkantor'      => 'required',
            'namakantor'    => 'required',
            'dirut'         => 'required',
            'direktur'      => 'required',
            'koma'          => 'required',
            'notelp'        => 'required',
            'email'         => 'required|email|unique:users,email'
        ]);

        $kantor = new Kantor;
            $kantor->idkantor = strtoupper($request->kodekantor);
            $kantor->namakantor = strtoupper($request->namakantor);
            $kantor->dirut = strtoupper($request->namadirut);
            $kantor->direktur = strtoupper($request->namadirektur);
            $kantor->koma = strtoupper($request->namakomisaris);
            $kantor->kontak = strtoupper($request->notelp);
            $kantor->email = strtoupper($request->email);
        $kantor->save();    

        return redirect('kantor')->with('Data Sudah sukses tersimpan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'idkantor'      => 'required',
            'namakantor'    => 'required',
            'dirut'         => 'required',
            'direktur'      => 'required',
            'koma'          => 'required',
            'notelp'        => 'required',
            'email'         => 'required|email|unique:users,email'
        ]);

        Kantor::where('id',$id)
		->update([
            'idkantor'      => strtoupper($request->kodekantor),
            'namakantor'    => strtoupper($request->namakantor),
            'dirut'         => strtoupper($request->namadirut),
            'direktur'      => strtoupper($request->namadirektur),
            'koma'          => strtoupper($request->namakomisaris),
            'kontak'        => strtoupper($request->notelp),
            'email'         => strtoupper($request->email),
        ]);   
        return redirect('kantor')->with('Data Sudah sukses tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail($id)
    {
        $kantor = DB::table('kantors')
                ->where('id',$id)
                ->first();
        return view('setting.edit_kantor',compact('kantor'));    
    }
}
