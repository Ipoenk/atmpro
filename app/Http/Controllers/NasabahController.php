<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Nasabah;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datanasabahs = Nasabah::all();
        // return $datatables->make(true);
        // $allProvinsi = DB::table('provinsis')->pluck("nama","id");
        // $datanasabahs = DB::table('nasabahs')
        // ->join('kabupatens','kabupatens.id','nasabahs.kotansbktp')
        // ->join('kecamatans','kecamatans.id','nasabahs.kecamatannsbktp')
        // ->join('kelurahans','kelurahans.id','nasabahs.desansbktp')
        // ->select('nasabahs.*','kabupatens.nama as kotanama','kecamatans.nama as camatnama','kelurahans.nama as namadesa')
        // ->get();
        // dd($datanasabahs);
        return view('nasabah.daftarnasabah',compact('datanasabahs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $allnasabah = nasabah::all();
        // return view('nasabah.create',['nasabahs' => $allnasabah]);
        $datanasabahs = DB::table('nasabahs')
                    ->join('kabupatens','kabupatens.id','nasabahs.kotansbktp')
                    ->join('kecamatans','kecamatans.id','nasabahs.kecamatannsbktp')
                    ->join('kelurahans','kelurahans.id','nasabahs.desansbktp')
                    ->select('nasabahs.*','kabupatens.nama as kotanama','kecamatans.nama as camatnama','kelurahans.nama as namadesa')
                    ->get();
 
        // Log::info($datanasabahs);

        $allProvinsi = DB::table('provinsis')->pluck("nama","id");
        return view('nasabah.form', ['allProvinsi'=>$allProvinsi],['datanasabahs'=>$datanasabahs]);  
        // return view('nasabahs.create');
    }

    public function getKabupaten($id)
    {

        $kabupatens = DB::table('kabupatens')->where('provinsi_id',$id)->get();
        return response()->json(['kabupatens'=>$kabupatens]);
    }

    public function getKecamatan($id)
    {
        $kecamatans = DB::table('kecamatans')->where('kabupaten_id',$id)->get();
        return response()->json(['kecamatans'=>$kecamatans]);

    }

    public function getKelurahan($id)
    {

        $kelurahans = DB::table('kelurahans')->where('kecamatan_id',$id)->get();
        return response()->json(['kelurahans'=>$kelurahans]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'jenisnsb'          => 'required',
            'niknsb'            => 'required',
            'namansb'           => 'required',
            'alamatnsbktp'      => 'required',
            'propinsinsbktp'    => 'required',
            'kotansbktp'        => 'required',
            'kecamatannsbktp'   => 'required',
            'desansbktp'        => 'required',
            'kodeposnsbktp'     => 'required',
            'notelpnsb'         => 'required',
            'email'             => 'required|email|unique:users', 
            'alamatdomisili'    => 'required',
            'propinsidomisili'  => 'required',
            'kotadomisili'      => 'required',
            'kecamatandomisili' => 'required',
            'desadomisili'      => 'required',
            'kelaminnsb'        => 'required',
            'tmplahirnsb'       => 'required',
            'tgllahirnsb'       => 'required',
            'agamansb'          => 'required',
            'statusperkawinan'  => 'required',
            'pendidikan'        => 'required',
            'namaibukandung'    => 'required',
            'nikps'             => 'required_if:statusperkawinan,=, MENIKAH',
            'namaps'            => 'required_if:statusperkawinan,=, MENIKAH',
            'alamatps'          => 'required_if:statusperkawinan,=, MENIKAH',
            'propinsips'        => 'required_if:statusperkawinan,=, MENIKAH',
            'kotaps'            => 'required_if:statusperkawinan,=, MENIKAH',
            'kecamatanps'       => 'required_if:statusperkawinan,=, MENIKAH',
            'desaps'            => 'required_if:statusperkawinan,=, MENIKAH',
            'kodeposps'         => 'required_if:statusperkawinan,=, MENIKAH',
            'notelpps'          => 'required_if:statusperkawinan,=, MENIKAH',
            'kelaminps'         => 'required_if:statusperkawinan,=, MENIKAH',
            'tmplahirps'        => 'required_if:statusperkawinan,=, MENIKAH',
            'tgllahirps'        => 'required_if:statusperkawinan,=, MENIKAH',
            'agamaps'           => 'required_if:statusperkawinan,=, MENIKAH',
            'jenispsh'          => 'required',
            'namapsh'           => 'required',
            'alamatpsh'         => 'required',
            'propinsipsh'       => 'required',
            'kotapsh'           => 'required',
            'kecamatanpsh'      => 'required',
            'desapsh'           => 'required',
            'kodepospsh'        => 'required',
            'notelppsh'         => 'required',
            'Bidangusaha'       => 'required',
            'sumberdana'        => 'required',
            'pendapatan'        => 'required',
            'pengeluaran'       => 'required',
            'tanggungan'        => 'required',
            'ststempattinggal'  => 'required',            
        ]);
        

         
        if ($validator->fails()) {
            redirect()
                ->back()
                ->withErrors($validator->errors());
        }   
            $nasabah = new Nasabah;
                $nasabah->jenisnsb = strtoupper($request->jenisnsb);
                $nasabah->niknsb = $request->niknsb;
                $nasabah->namansb = strtoupper($request->namansb);
                $nasabah->alamatnsbktp = strtoupper($request->alamatnsbktp);
                $nasabah->propinsinsbktp = $request->propinsinsbktp;
                $nasabah->kotansbktp = $request->kotansbktp;
                $nasabah->kecamatannsbktp = $request->kecamatannsbktp;
                $nasabah->desansbktp = $request->desansbktp;
                $nasabah->kodeposnsbktp = $request->kodeposnsbktp;
                $nasabah->notelpnsb = $request->notelpnsb;
                $nasabah->email = strtoupper($request->emailnsb);
                $nasabah->alamatdomisili = strtoupper($request->alamatdomisili);
                $nasabah->propinsidomisili = $request->propinsidomisili;
                $nasabah->kotadomisili = $request->kotadomisili;
                $nasabah->kecamatandomisili = $request->kecamatandomisili;
                $nasabah->desadomisili = $request->desadomisili;
                $nasabah->kelaminnsb = strtoupper($request->kelaminnsb);
                $nasabah->tmplahirnsb = strtoupper($request->tmplahirnsb);
                $nasabah->tgllahirnsb = $request->tgllahirnsb;
                $nasabah->agamansb = strtoupper($request->agamansb);
                $nasabah->statusperkawinan = strtoupper($request->statusperkawinan);
                $nasabah->pendidikan = strtoupper($request->pendidikan);
                $nasabah->namaibukandung = strtoupper($request->namaibukandung);
                $nasabah->Nikps = $request->Nikps;
                $nasabah->namaps = strtoupper($request->namaps);
                $nasabah->alamatps = strtoupper($request->alamatps);
                $nasabah->propinsips = $request->propinsips;
                $nasabah->kotaps = $request->kotaps;
                $nasabah->kecamatanps = $request->kecamatanps;
                $nasabah->desaps = $request->desaps;
                $nasabah->kodeposps = $request->kodeposps;
                $nasabah->notelpps = $request->notelpps;
                $nasabah->kelaminps = strtoupper($request->kelaminps);
                $nasabah->tmplahirps = strtoupper($request->tmplahirps);
                $nasabah->tgllahirps = $request->tgllahirps;
                $nasabah->agamaps = strtoupper($request->agamaps);
                $nasabah->jenispsh = strtoupper($request->jenispsh);
                $nasabah->namapsh = strtoupper($request->namapsh);
                $nasabah->alamatpsh = strtoupper($request->alamatpsh);
                $nasabah->propinsipsh = $request->propinsipsh;
                $nasabah->kotapsh = $request->kotapsh;
                $nasabah->kecamatanpsh = $request->kecamatanpsh;
                $nasabah->desapsh = $request->desapsh;
                $nasabah->kodepospsh = $request->kodepospsh;
                $nasabah->notelppsh = $request->notelppsh;
                $nasabah->Bidangusaha = strtoupper($request->Bidangusaha);
                $nasabah->sumberdana = strtoupper($request->sumberdana);
                $nasabah->pendapatan = (int)str_replace(',','',$request->pendapatan);
                $nasabah->pengeluaran = (int)str_replace(',','',$request->pengeluaran);
                $nasabah->pengeluaran = (int)str_replace(',','',$request->pendapatan);
                $nasabah->tanggungan = $request->tanggungan;
                $nasabah->ststempattinggal = strtoupper($request->ststempattinggal);
                $nasabah->pengajuan = strtoupper($request->pengajuan);
                $nasabah->opr = '';
                $nasabah->save();

            return redirect()->route('kredit.create', [$nasabah->id])->with('toast_success', 'Lanjut Ke Realisasi Kredit!!');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        // $nasabahs = Nasabah::findOrFail($id);
        $allProvinsi = DB::table('provinsis')->pluck("nama","id");
 
        // // $nasabahs = DB::table('nasabahs')
        $nasabahs = DB::table('nasabahs')
        // ->join('provinsis','provinsis.id','nasabahs.propinsinsbktp')
        // ->join('kabupatens','kabupatens.id','nasabahs.kotansbktp')
        // ->join('kecamatans','kecamatans.id','nasabahs.kecamatannsbktp')
        // ->join('kelurahans','kelurahans.id','nasabahs.desansbktp')
        // ->join('provinsis as propdom','propdom.id','nasabahs.propinsidomisili')
        // ->join('kabupatens as kabdom','kabdom.id','nasabahs.kotadomisili')
        // ->join('kecamatans as camatdom','camatdom.id','nasabahs.kecamatandomisili')
        // ->join('kelurahans as desadom','desadom.id','nasabahs.desadomisili')
        // ->join('provinsis as propps','propps.id','nasabahs.propinsips')
        // ->join('kabupatens as kabps','kabps.id','nasabahs.kotaps')
        // ->join('kecamatans as camatps','camatps.id','nasabahs.kecamatanps')
        // ->join('kelurahans as desaps','desaps.id','nasabahs.desaps')
        // ->join('provinsis as proppsh','proppsh.id','nasabahs.propinsipsh')
        // ->join('kabupatens as kabpsh','kabpsh.id','nasabahs.kotapsh')
        // ->join('kecamatans as camatpsh','camatpsh.id','nasabahs.kecamatanpsh')
        // ->join('kelurahans as desapsh','desapsh.id','nasabahs.desapsh')
        // ->select('nasabahs.*','provinsis.nama as namaprop',
        // 'kabupatens.nama as kotanama','kecamatans.nama as camatnama',
        // 'kelurahans.nama as desanama','propdom.nama as propdom','kabdom.nama as kotadom',
        // 'camatdom.nama as camatdom','desadom.nama as desadom','propps.nama as propps',
        // 'kabps.nama as namakotaps','camatps.nama as namacamatps','desaps.nama as namadesaps',
        // 'proppsh.nama as proppsh','kabpsh.nama as namakotapsh','camatpsh.nama as namacamatpsh',
        // 'desapsh.nama as namadesapsh')
        ->where('nasabahs.id',$id)
        ->first();
        // dd($nasabah);
        return view('nasabah.detail',compact('nasabahs','allProvinsi'));  

        // Log::info($nasabah);
        // dd($nasabahs->jenisnsb);
        // $allProvinsi = DB::table('provinsis')->pluck("nama","id");
        // return view('nasabah.detail',['nasabahs'=>$nasabah]);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'jenisnsb'          => 'required',
            'niknsb'            => 'required',
            'namansb'           => 'required',
            'alamatnsbktp'      => 'required',
            'propinsinsbktp'    => 'required',
            'kotansbktp'        => 'required',
            'kecamatannsbktp'   => 'required',
            'desansbktp'        => 'required',
            'kodeposnsbktp'     => 'required',
            'notelpnsb'         => 'required',
            'email'             => 'unique:connection.nasabahs,email', 
            'alamatdomisili'    => 'required',
            'propinsidomisili'  => 'required',
            'kotadomisili'      => 'required',
            'kecamatandomisili' => 'required',
            'desadomisili'      => 'required',
            'kelaminnsb'        => 'required',
            'tmplahirnsb'       => 'required',
            'tgllahirnsb'       => 'required|date',
            'agamansb'          => 'required',
            'statusperkawinan'  => 'required',
            'pendidikan'        => 'required',
            'namaibukandung'    => 'required',
            'nikps'             => 'required_if:statusperkawinan,=, MENIKAH',
            'namaps'            => 'required_if:statusperkawinan,=, MENIKAH',
            'alamatps'          => 'required_if:statusperkawinan,=, MENIKAH',
            'propinsips'        => 'required_if:statusperkawinan,=, MENIKAH',
            'kotaps'            => 'required_if:statusperkawinan,=, MENIKAH',
            'kecamatanps'       => 'required_if:statusperkawinan,=, MENIKAH',
            'desaps'            => 'required_if:statusperkawinan,=, MENIKAH',
            'kodeposps'         => 'required_if:statusperkawinan,=, MENIKAH',
            'notelpps'          => 'required_if:statusperkawinan,=, MENIKAH',
            'kelaminps'         => 'required_if:statusperkawinan,=, MENIKAH',
            'tmplahirps'        => 'required_if:statusperkawinan,=, MENIKAH',
            'tgllahirps'        => 'required_if:statusperkawinan,=, MENIKAH|date',
            'agamaps'           => 'required_if:statusperkawinan,=, MENIKAH',
            'jenispsh'          => 'required',
            'namapsh'           => 'required',
            'alamatpsh'         => 'required',
            'propinsipsh'       => 'required',
            'kotapsh'           => 'required',
            'kecamatanpsh'      => 'required',
            'desapsh'           => 'required',
            'kodepospsh'        => 'required',
            'notelppsh'         => 'required',
            'Bidangusaha'       => 'required',
            'sumberdana'        => 'required',
            'pendapatan'        => 'required',
            'pengeluaran'       => 'required',
            'tanggungan'        => 'required',
            'ststempattinggal'  => 'required',            
        ]);
        if ($validator->fails()) {
            redirect()
                ->back()
                ->withErrors($validator->errors());
        }
        
          
        Nasabah::where('id',$id) 
            ->update([
            'jenisnsb' => strtoupper($request->jenisnsb),
            'niknsb' => $request->niknsb,
            'namansb' => strtoupper($request->namansb),
            'alamatnsbktp' => strtoupper($request->alamatnsbktp),
            'propinsinsbktp' => $request->propinsinsbktp,
            'kotansbktp' => $request->kotansbktp,
            'kecamatannsbktp' => $request->kecamatannsbktp,
            'desansbktp' => $request->desansbktp,
            'kodeposnsbktp' => $request->kodeposnsbktp,
            'notelpnsb' => $request->notelpnsb,
            'email' => strtoupper($request->emailnsb),
            'alamatdomisili' => strtoupper($request->alamatdomisili),
            'propinsidomisili' => $request->propinsidomisili,
            'kotadomisili' => $request->kotadomisili,
            'kecamatandomisili' => $request->kecamatandomisili,
            'desadomisili' => $request->desadomisili,
            'kelaminnsb' => strtoupper($request->kelaminnsb),
            'tmplahirnsb' => strtoupper($request->tmplahirnsb),
            'tgllahirnsb' => $request->tgllahirnsb,
            'agamansb' => strtoupper($request->agamansb),
            'statusperkawinan' => strtoupper($request->statusperkawinan),
            'pendidikan' => strtoupper($request->pendidikan),
            'namaibukandung' => strtoupper($request->namaibukandung),
            'nikps' => $request->nikps,
            'namaps' => strtoupper($request->namaps),
            'alamatps' => strtoupper($request->alamatps),
            'propinsips' => $request->propinsips,
            'kotaps' => $request->kotaps,
            'kecamatanps' => $request->kecamatanps,
            'desaps' => $request->desaps,
            'kodeposps' => $request->kodeposps,
            'notelpps' => $request->notelpps,
            'kelaminps' => strtoupper($request->kelaminps),
            'tmplahirps' => strtoupper($request->tmplahirps),
            'tgllahirps' => $request->tgllahirps,
            'agamaps' => strtoupper($request->agamaps),
            'jenispsh' => strtoupper($request->jenispsh),
            'namapsh' => strtoupper($request->namapsh),
            'alamatpsh' => strtoupper($request->alamatpsh),
            'propinsipsh' => $request->propinsipsh,
            'kotapsh' => $request->kotapsh,
            'kecamatanpsh' => $request->kecamatanpsh,
            'desapsh' => $request->desapsh,
            'kodepospsh' => $request->kodepospsh,
            'notelppsh' => $request->notelppsh,
            'Bidangusaha' => strtoupper($request->Bidangusaha),
            'sumberdana' => strtoupper($request->sumberdana),
            'pendapatan' => str_replace(',','',$request->pendapatan),  
            'pengeluaran' => str_replace(',','',$request->pengeluaran),
            'pendapatan' => (int)str_replace(',','',$request->pendapatan),
            'pengeluaran' => (int)str_replace(',','',$request->pengeluaran),
            'tanggungan' => $request->tanggungan,
            'ststempattinggal' => strtoupper($request->ststempattinggal),
            'pengajuan' => strtoupper($request->pengajuan),
            'opr' => '']);
        return redirect(route('nasabah'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     
     public function jmlnsb(Request $request)
    {
        $paginate = 10;
        $data['nasabahs'] = Nasabah::paginate($paginate);
        return view('nasabah.index', $data)->with('i', ($request->input('page', 1) - 1) * $paginate);
    }
}

