<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nasabah;
use App\Models\Kredit;
use App\Models\Prekredit;
use App\Models\Angsuran_jadwal;
use App\Models\Angsuran_bayar;
use App\Models\Angsuran_kartu;
use App\Models\Master_kodepk;
use App\Models\Master_goljamin;
use App\Models\Master_golongan;
use App\Models\Master_hubunganbank;
use App\Models\Master_kerja;
use App\Models\Master_penggunaan;
use App\Models\Master_sektor;
use App\Models\Master_usaha;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use Illuminate\Console\Scheduling\ManagesFrequencies;
use Illuminate\Support\Facades\Validator;

class KreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kredit.menukredit');
        // return view('kredit.test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        
        $kodekantor = 'AKM';
        $bulan = date('m');
        $tahun = date('y');
        $noref = $kodekantor.$tahun.$bulan;
        $no_kredit = '';

        // $allProvinsi = DB::table('provinsis')->pluck("nama","id");
        $sifatpks = DB::table('master_kodepks')->pluck("note","kodeslik");

        $jnsgunas = DB::table('master_penggunaans')->pluck('note','kode');
           
        $goldebs = DB::table('master_golongans')->pluck('note','kode');

        $sekekonomis =DB::table('master_sektors')->pluck('note','kode');

        $jnsusahas = DB::table('master_usahas')->pluck('note','kode');

        $goljamins = DB::table('master_goljamins')->pluck('note','kode');
            
        $kredits = Kredit::select('no_kredit','tgl_kredit','tgl_lunas','tgl_mulai')
        ->where('no_kredit','like',"\\".$noref."%")
        ->orderBy('tgl_kredit', 'desc')
        ->first();

        if(is_null($kredits)){
            $no_kredit = $noref.'00001';
        }
        else {
            if(count($kredits) > 0){
                //ambil selisih hari tunggakan
                $akhir = (int)substr($kredits->no_kredit,8,5)+1;
                $no_kredit = $noref.str_repeat('0',5-strlen($akhir)).$akhir;

            }
            else {
                $no_kredit = $noref.'00001';
            }
        }

        $nasabahs = DB::table('nasabahs')
            ->join('provinsis','provinsis.id','nasabahs.propinsinsbktp')
            ->join('kabupatens','kabupatens.id','nasabahs.kotansbktp')
            ->join('kecamatans','kecamatans.id','nasabahs.kecamatannsbktp')
            ->join('kelurahans','kelurahans.id','nasabahs.desansbktp')
            ->join('provinsis as propdom','propdom.id','nasabahs.propinsidomisili')
            ->join('kabupatens as kabdom','kabdom.id','nasabahs.kotadomisili')
            ->join('kecamatans as camatdom','camatdom.id','nasabahs.kecamatandomisili')
            ->join('kelurahans as desadom','desadom.id','nasabahs.desadomisili')
            ->join('provinsis as proppsh','proppsh.id','nasabahs.propinsipsh')
            ->join('kabupatens as kabpsh','kabpsh.id','nasabahs.kotapsh')
            ->join('kecamatans as camatpsh','camatpsh.id','nasabahs.kecamatanpsh')
            ->join('kelurahans as desapsh','desapsh.id','nasabahs.desapsh')
            ->select('nasabahs.*','provinsis.nama as namaprop',
            'kabupatens.nama as kotanama','kecamatans.nama as camatnama',
            'kelurahans.nama as desanama','propdom.nama as propdom','kabdom.nama as kotadom',
            'camatdom.nama as camatdom','desadom.nama as desadom',
            'proppsh.nama as proppsh','kabpsh.nama as kotapsh','camatpsh.nama as camatpsh',
            'desapsh.nama as desapsh')
        ->where('nasabahs.id',$id)
        ->first();
        return view('kredit.form',compact('nasabahs','sifatpks','jnsgunas','goldebs','sekekonomis','jnsusahas','goljamins','no_kredit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nasabahs = Nasabah::where('niknsb',$request->niknsb)
        ->first();

        $validator = Validator::make($request->all(), [
            'no_kredit'     => 'required',
            'tglkredit'     => 'required|date',
            'pinj_pokok'    => 'required|numeric',
            'skbunga'       => 'required|numeric',
            'sistem'        => 'required',
            'lama'          => 'required|numeric',
            'tgl_mulai'     => 'required|date',
            'tgl_akhir'     => 'required|date',
            'jatuhtempo'    => 'required|date',
            'namaao'        => 'required',
            'perantara'     => 'required',
            'saldopiutang'  => 'required|numeric',
            'pinj_pokok'    => 'required|numeric',
            'bbt'           => 'required|numeric',
            'angs_pokok'    => 'required|numeric',
            'angs_bunga'    => 'required|numeric',
            'nom_provisi'   => 'required|numeric',
            'nom_admin'     => 'required|numeric',
            'notaris'       => 'required|numeric',
            'materai'       => 'required|numeric',
            'assjiwa'       => 'required|numeric',
            'asuransi'      => 'required|numeric',
            'sim_ang'       => 'required|numeric',
            'bitrans'       => 'required|numeric',
            'sifatpk'       => 'required',
            'jnsguna'       => 'required',
            'terkait'       => 'required',
            'sumberdana'    => 'required',
            'prdbayar'      => 'required',
            'goldeb'        => 'required',
            'sekekonomi'    => 'required',
            'jnsusaha'      => 'required',
            'goljamin'      => 'required',
            'bidangusaha'   => 'required',
            'tujuan'        => 'required',
        ]);

        $prekredits = Prekredit::select('no_mohon')
        ->orderBy('no_mohon', 'desc')
        ->first();

        if(is_null($prekredits)){
            $nomohon = '0000000001';
        }
        else {
            if(count($prekredits) > 0){
                //ambil selisih hari tunggakan
                $akhir = (int)($prekredits->no_mohon)+1;
                $nomohon = str_repeat('0',10-strlen($akhir)).$akhir;

            }
            else {
                $nomohon = '0000000001';
            }
        }

        $kodekantor = 'AKM';
        $kredit = new Kredit;
            $kredit->no_kredit = $request->no_kredit;
            $kredit->no_ref = $request->no_kredit;
            $kredit->kode_kantor = $kodekantor;
            $kredit->no_mohon = $nomohon;
            $kredit->tgl_kredit = $request->tglkredit;
            $kredit->pinj_pokok = $request->pinj_pokok;
            $kredit->bakidebet = $request->pinj_pokok;
            $kredit->pinj_prsbunga = $request->skbunga;
            $kredit->sistem = $request->sistem;
            $kredit->lama = $request->lama;
            $kredit->tgl_mulai = $request->tglmulai;
            $kredit->tgl_akhir = $request->tglakhir;
            $kredit->jatuhtempo = $request->jatuhtempo;
            $kredit->namaao = strtoupper($request->namaao);
            $kredit->perantara = strtoupper($request->perantara);
            $kredit->saldo_piutang = (int)str_replace(',','',$request->saldopiutang);
            $kredit->plafon = (int)str_replace(',','',$request->pinj_pokok);
            $kredit->pinj_nombunga = (int)str_replace(',','',$request->bbt);  
            $kredit->bbt = (int)str_replace(',','',$request->bbt);
            $kredit->saldo_bbt = (int)str_replace(',','',$request->bbt);
            $kredit->angsur_pokok = (int)str_replace(',','',$request->angs_pokok);
            $kredit->angsur_bunga = (int)str_replace(',','',$request->angs_bunga);
            $kredit->nom_provisi = (int)str_replace(',','',$request->nom_provisi);
            $kredit->prs_provisi = ((int)str_replace(',','',$request->nom_provisi) / (int)str_replace(',','',$request->pinj_pokok)) * 100;
            $kredit->nom_adm = (int)str_replace(',','',$request->nom_admin);
            $kredit->prs_adm = ((int)str_replace(',','',$request->nom_admin)/(int)str_replace(',','',$request->pinj_pokok)) * 100;
            $kredit->nom_notaris = (int)str_replace(',','',$request->notaris);
            $kredit->nom_meterai = (int)str_replace(',','',$request->materai);
            $kredit->assjiwa = (int)str_replace(',','',$request->assjiwa);
            $kredit->nom_asuransi = (int)str_replace(',','',$request->asuransi);
            $kredit->sim_ang = (int)str_replace(',','',$request->sim_ang);
            $kredit->nom_trans = (int)str_replace(',','',$request->bitrans);
            $kredit->sifatkrd = $request->sifatpk;
            $kredit->usaha = $request->bidangusaha;
            $kredit->smbdana = $request->sumberdana;
            $kredit->prdbayar = $request->prdbayar;
            $kredit->terkait = $request->terkait;
            $kredit->goljamin = $request->goljamin;
            $kredit->penggunaan = $request->jnsguna;
            $kredit->golongan = $request->goldeb;
            $kredit->sektor = $request->sekekonomi;
            $kredit->tujuan = $request->tujuan;
            $kredit->tgl_lunas = '1900-01-01';
        $kredit->save();

        $prekredit = new Prekredit;
            $prekredit->nasabah_id = $nasabahs->id;
            $prekredit->kredit_id = $kredit->id;
            $prekredit->no_mohon = $nomohon;
            $prekredit->niknsb = $nasabahs->niknsb;
            $prekredit->namansb  = strtoupper($nasabahs->namansb);
            $prekredit->tmplahirnsb = strtoupper($nasabahs->tmplahirnsb);
            $prekredit->tgllahirnsb = $nasabahs->tgllahirnsb;
            $prekredit->kelaminnsb = strtoupper($nasabahs->kelaminnsb);
            $prekredit->agamansb   = strtoupper($nasabahs->agamansb);
            $prekredit->alamatnsbktp = strtoupper($nasabahs->alamatnsbktp);
            $prekredit->propinsinsbktp = $nasabahs->propinsinsbktp;
            $prekredit->kotansbktp = $nasabahs->kotansbktp;
            $prekredit->kecamatannsbktp = $nasabahs->kecamatannsbktp;
            $prekredit->desansbktp = $nasabahs->desansbktp;
            $prekredit->kodeposnsbktp = $nasabahs->kodeposnsbktp;
            $prekredit->alamatdomisili = strtoupper($nasabahs->alamatdomisili);
            $prekredit->propinsidomisili = $nasabahs->propinsidomisili;
            $prekredit->kotadomisili = $nasabahs->kotadomisili;
            $prekredit->kecamatandomisili = $nasabahs->kecamatandomisili;
            $prekredit->desadomisili = $nasabahs->desadomisili;
            $prekredit->notelp = $nasabahs->notelpnsb;
            $prekredit->statusperkawinan = strtoupper($nasabahs->statusperkawinan);
            $prekredit->namaibukandung = strtoupper($nasabahs->namaibukandung);
            $prekredit->nikps = $nasabahs->nikps;
            $prekredit->namaps = strtoupper($nasabahs->namaps);
            $prekredit->alamatps = strtoupper($nasabahs->alamatps);
            $prekredit->propinsips = $nasabahs->propinsips;
            $prekredit->kotaps = $nasabahs->kotaps;
            $prekredit->kecamatanps = $nasabahs->kecamatanps;
            $prekredit->desaps = $nasabahs->desaps;
            $prekredit->kodeposps = $nasabahs->kodeposps;
            $prekredit->notelpps = $nasabahs->notelpps;
            $prekredit->kelaminps = strtoupper($nasabahs->kelaminps);
            $prekredit->tmplahirps = strtoupper($nasabahs->tmplahirps);
            $prekredit->tgllahirps = $nasabahs->tgllahirps;
            $prekredit->agamaps = strtoupper($nasabahs->agamaps);
            $prekredit->jenispsh = strtoupper($nasabahs->jenispsh);
            $prekredit->namapsh = strtoupper($nasabahs->namapsh);
            $prekredit->alamatpsh = strtoupper($nasabahs->alamatpsh);
            $prekredit->propinsipsh = $nasabahs->propinsipsh;
            $prekredit->kotapsh = $nasabahs->kotapsh;
            $prekredit->kecamatanpsh = $nasabahs->kecamatanpsh;
            $prekredit->desapsh = $nasabahs->desapsh;
            $prekredit->kodepospsh = $nasabahs->kodepospsh;
            $prekredit->notelppsh = $nasabahs->notelppsh;
            $prekredit->Bidangusaha = strtoupper($nasabahs->Bidangusaha);
            $prekredit->sumberdana = strtoupper($nasabahs->sumberdana);
            $prekredit->pendapatan = (int)str_replace(',','',$nasabahs->pendapatan);
            $prekredit->pengeluaran = (int)str_replace(',','',$nasabahs->pengeluaran);
            $prekredit->tanggungan = $nasabahs->tanggungan;
            $prekredit->ststempattinggal = strtoupper($nasabahs->ststempattinggal);
            $prekredit->jenisnsb = strtoupper($nasabahs->jenisnsb);
            $prekredit->tgl_mohon = date('Y-m-d');
        $prekredit->save();

        // Buat Jadwal Angsuran
        $PokokPinjaman  = $request->pinj_pokok;
        $prsbunga       = $request->pinj_prsbunga;
        $lamakredit     = $request->lama;
        $tglawal        = $request->tgl_kredit;
        $saldopiutang   = $request->saldopiutang;
        $saldobbt       = $request->$request->bbt;

        for ($i = 1; $i <= $lamakredit - 1; $i++){
        $jadwalangsuran  = new Angsuran_jadwal;
            $jadwalangsuran->kredit_id = $kredit->id;
            $jadwalangsuran->tgl_angsur = Carbon::parse($request->tgl_kredit)->addmonths($i)->format('Y-m-d');
            $jadwalangsuran->bayar_pokok = 0;
            $jadwalangsuran->bayar_bunga = 0;
            $jadwalangsuran->angs_pokok = $request->angs_pokok;
            $jadwalangsuran->angs_bunga = $request->angs_bunga;
            $jadwalangsuran->bayar_ke = $i;
            $jadwalangsuran->provisi = 0;
            $jadwalangsuran->bitrans = 0;
            $jadwalangsuran->akhbulan = Carbon::parse($request->tgl_kredit)->addmonths($i)->endOfMonth()->format('Y-m-d');
            $jadwalangsuran->ab_pdbunga = 0;
            $jadwalangsuran->ab_provisi= 0;
            $jadwalangsuran->ab_bitrans= 0;
            $jadwalangsuran->jta_pdbunga= 0;
            $jadwalangsuran->jta_provisi= 0;
            $jadwalangsuran->jta_bitrans= 0;
            $jadwalangsuran->jta_adm = 0;
            $jadwalangsuran->ab_adm = 0;
            $jadwalangsuran->adm = 0;
            $jadwalangsuran->opr ='';
        $jadwalangsuran->save();
        }
      
            // Angsuran Terakhir

        $pokokakhir =  $PokokPinjaman - ($request->angs_pokok * ($lamakredit-1));  
        $jadwalangsuran  = new Angsuran_jadwal;
            $jadwalangsuran->kredit_id = $kredit->id;
            $jadwalangsuran->tgl_angsur = Carbon::parse($request->tgl_kredit)->addmonths($lamakredit)->format('Y-m-d');
            $jadwalangsuran->bayar_pokok = 0;
            $jadwalangsuran->bayar_bunga = 0;
            $jadwalangsuran->angs_pokok = $pokokakhir;
            $jadwalangsuran->angs_bunga = $request->angs_bunga;
            $jadwalangsuran->bayar_ke = $i;
            $jadwalangsuran->provisi = 0;
            $jadwalangsuran->bitrans = 0;
            $jadwalangsuran->akhbulan = Carbon::parse($request->tgl_kredit)->addmonths($lamakredit)->endOfMonth()->format('Y-m-d');
            $jadwalangsuran->ab_pdbunga = 0;
            $jadwalangsuran->ab_provisi= 0;
            $jadwalangsuran->ab_bitrans= 0;
            $jadwalangsuran->jta_pdbunga= 0;
            $jadwalangsuran->jta_provisi= 0;
            $jadwalangsuran->jta_bitrans= 0;
            $jadwalangsuran->jta_adm = 0;
            $jadwalangsuran->ab_adm = 0;
            $jadwalangsuran->adm = 0;
            $jadwalangsuran->opr ='';
        $jadwalangsuran->save();

        // return redirect(route('kredit.jaminan'));
        return redirect()->route('kredit.jaminan', [$kredit->id])>with('toast_success', 'Lanjut Ke Agunan!!');;

        // return redirect('kredit.jaminan')->with('status','Data Pengajuan Berhasil Tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function nominatif(Request $request)
    {

        //    Loading Nominatif
       
    if(is_null($request)){
       $tanggalLaporan = Carbon::now()->format('Y-m-d');
    }
    else{
        // $tanggalLaporan = $tgllap->date("Y-m-d");
        $tanggalLaporan = date("Y-m-d", strtotime($request->inputtgllap));
    }

    //    $nominatif = Kredit::->join('prekredits','prekredits.kredit_id','=','kredits.id')

       $nominatif = Kredit::where('kredits.tgl_lunas','=','1900-01-01')
            ->orWhere('kredits.tgl_lunas', '>', $tanggalLaporan)
            ->Where('kredits.tgl_kredit', '<=', $tanggalLaporan)
            ->orderBy('id','asc')
            ->orderBy('tgl_kredit','asc')
            ->get();

    foreach($nominatif as $nominatif){
           //pre kredit
           $prekr = Prekredit::select('namansb')
           ->where('kredit_id',$nominatif->id)
           ->first();

           //sum dari total angsuran pokok
           $pokokdibayar = Angsuran_bayar::where('kredit_id',$nominatif->id)
           ->where('tanggal','<=',$tanggalLaporan)
           ->sum('pokok');
           //sum dari total angsuran bunga
           $bungadibayar = Angsuran_bayar::where('kredit_id',$nominatif->id)
           ->where('tanggal','<=',$tanggalLaporan)
           ->sum('bunga');

           $checkerAngsuran = Angsuran_bayar::where('kredit_id',$nominatif->id)->get();

           $selisihHari = 0;
           $kolek = 0;

           $carbonTanggalLaporan = Carbon::createFromFormat('Y-m-d',$tanggalLaporan);
           
           if(count($checkerAngsuran) > 0){
                   //ambil selisih hari tunggakan
               $tanggalAkhirBayar = Angsuran_bayar::where('kredit_id',$nominatif->id)
               ->max('tanggal');


               $tanggalAkhirBayar = Carbon::createFromFormat('Y-m-d',$tanggalAkhirBayar);
            //    dd($tanggalAkhirBayar);

                $selisihHari = $carbonTanggalLaporan->diffInDays($tanggalAkhirBayar); 
            //    $selisihHari = $carbonTanggalLaporan->diffInDays($tanggalAkhirBayar); 
            //    dd($selisihHari);

                              
               // $this->info($kolek);
           }
           if($nominatif->tgl_mulai < $tanggalLaporan && count($checkerAngsuran) == 0){

               $tanggalSeharusnyaBayar = Carbon::createFromFormat('Y-m-d',$nominatif->tgl_mulai);

               $selisihHari = $carbonTanggalLaporan->diffInDays($tanggalSeharusnyaBayar);

           }
        //    jika kolek 1
           if($selisihHari < 31){
               $kolek = 1;
               $ketkolek = 'Lancar';
           }

        //    jika kolek 2
           else if($selisihHari < 91 ){
               $kolek = 2;
               $ketkolek = 'Dalam Perhatian';
           }
        //    jika kolek 3
           else if($selisihHari < 121){
               $kolek = 3;
               $ketkolek = 'Kurang Lancar';
           }
        //    jika kolek 4
        else if($selisihHari < 365){
               $kolek = 4;
               $ketkolek = 'Diragukan';
           }
           else {
               $kolek = 5;
               $ketkolek = 'Macet';
               }

        //    $nominatif->prekredit = $prekr;
           $nominatif->pokokdibayar = $pokokdibayar;
           $nominatif->bungadibayar = $bungadibayar;
           $nominatif->selisihHari = $selisihHari;
           $nominatif->kolektibilitas = $kolek;
           $nominatif->ketkolektibilitas = $ketkolek;

        }

        foreach($nominatif as $nominatif){
            
        }
        // $kredits = $nominatif;
        Log:info($nominatif);
        return view('kredit.daftar_nominatif',compact('nominatif','tanggalLaporan'));

    }

    public function realisasi(Request $request)
    {

        $model = $request->metodelaporan;
        if($model == "PERTANGGAL"){
            $ketmodel = 'Pertanggal';    
            $stgl1 = date("Y-m-d", strtotime($request->tgl1));            

            $realisasi = DB::table('kredits')
            ->join('prekredits','prekredits.kredit_id','kredits.id')
            ->where('kredits.tgl_kredit','=',$stgl1)
            ->orderBy('kredits.no_kredit')
            ->get();
            // Log::info($realisasi);
            // Log::info($stgl1);


            return view('kredit.daftar_realisasi',compact('realisasi','stgl1','model','ketmodel'));

        }
        else if($model == "PERBULAN"){
            $ketmodel = 'Perbulan';    
                
            $blnlap = $request->bulanlap;            
            $tahunlap = $request->tahunlap;   
            if($blnlap =1){
                $namabulan = 'JANUARI'; 
            } 
            else if($blnlap =2){
                $namabulan = 'PEBRUARI';
            } 
            else if($blnlap =3){
                $namabulan = 'MARET';
            } 
            else if($blnlap =4){
                    $namabulan = 'APRIL';
            } 
            else if($blnlap =5){
                $namabulan = 'MEI';
            } 
            else if($blnlap =6){
                $namabulan = 'JUNI';
            } 
            else if($blnlap =7){
                $namabulan = 'JULI';
            } 
            else if($blnlap =8){
                $namabulan = 'AGUSTUS';
            } 
            else if($blnlap =9){
                $namabulan = 'SEPTEMBER';
            } 
            else if($blnlap =10){
                $namabulan = 'OKTOBER';
            } 
            else if($blnlap =11){
                $namabulan = 'NOPEMBER';
            } 
            else {
                $namabulan = 'DESEMBER';
            }

            $realisasi = DB::table('kredits')
            ->join('prekredits','prekredits.kredit_id','kredits.id')
            ->whereMonth('kredits.tgl_kredit','=',$blnlap)
            ->whereYear('kredits.tgl_kredit','=',$tahunlap)
            ->orderBy('kredits.tgl_kredit','asc')
            ->orderBy('kredits.no_kredit','asc')
            ->get();
            return view('kredit.daftar_realisasi',compact('realisasi','blnlap','tahunlap','namabulan','model'));

        }
        else {
            $ketmodel = 'Antar Tanggal';    
            $stgl1 = date("Y-m-d", strtotime($request->attgl1));            
            $stgl2 = date("Y-m-d", strtotime($request->attgl2));            
            $realisasi = DB::table('kredits')
            ->join('prekredits','prekredits.kredit_id','kredits.id')
            ->where('kredits.tgl_kredit','>',$stgl1)
            ->where('kredits.tgl_kredit','<',$stgl2)
            ->orderBy('kredits.tgl_kredit','asc')
            ->orderBy('kredits.no_kredit','asc')
            ->get();
            return view('kredit.daftar_realisasi',compact('realisasi','stgl1','stgl2','model','ketmodel'));
    
        }
    }

    public function lapangsuran(Request $request)
    {

        $model = $request->metodelaporan;
        if($model == "PERTANGGAL"){
            $ketmodel = 'Pertanggal';    
            $stgl1 = date("Y-m-d", strtotime($request->tgl1));            

            $angsuran = Angsuran_bayar::join('kredits','angsuran_bayars.kredit_id','kredits.id')
                ->join('prekredits','prekredits.kredit_id','angsuran_bayars.kredit_id')
                ->select('angsuran_bayars.*','kredits.no_kredit','prekredits.namansb')                
                ->where('angsuran_bayars.tanggal','=',$stgl1)
                ->orderBy('angsuran_bayars.kredit_id')
                ->get();
            return view('kredit.daftar_angsuran',compact('angsuran','stgl1','model','ketmodel'));

        }
        else if($model == "PERBULAN"){
            $ketmodel = 'Perbulan';    
                
            $blnlap = $request->bulanlap;            
            $tahunlap = $request->tahunlap;   
            if($blnlap =1){
                $namabulan = 'JANUARI'; 
            } 
            else if($blnlap =2){
                $namabulan = 'PEBRUARI';
            } 
            else if($blnlap =3){
                $namabulan = 'MARET';
            } 
            else if($blnlap =4){
                    $namabulan = 'APRIL';
            } 
            else if($blnlap =5){
                $namabulan = 'MEI';
            } 
            else if($blnlap =6){
                $namabulan = 'JUNI';
            } 
            else if($blnlap =7){
                $namabulan = 'JULI';
            } 
            else if($blnlap =8){
                $namabulan = 'AGUSTUS';
            } 
            else if($blnlap =9){
                $namabulan = 'SEPTEMBER';
            } 
            else if($blnlap =10){
                $namabulan = 'OKTOBER';
            } 
            else if($blnlap =11){
                $namabulan = 'NOPEMBER';
            } 
            else {
                $namabulan = 'DESEMBER';
            }

            $angsuran = Angsuran_bayar::select('angsuran_bayars.*','kredits.no_kredit','prekredits.namansb')
                ->join('kredits','angsuran_bayars.kredit_id','kredits.id')
                ->join('prekredits','prekredits.kredit_id','angsuran_bayars.kredit_id')
                ->whereMonth('angsuran_bayars.tanggal','=',$blnlap)
                ->whereYear('angsuran_bayars.tanggal','=',$tahunlap)
                ->orderBy('angsuran_bayars.tanggal','asc')
                ->orderBy('angsuran_bayars.kredit_id','asc')
                ->get();
            return view('kredit.daftar_angsuran',compact('angsuran','blnlap','tahunlap','namabulan','namabulan'));

        }
        else {
            $ketmodel = 'Antar Tanggal';    
            $stgl1 = date("Y-m-d", strtotime($request->attgl1));            
            $stgl2 = date("Y-m-d", strtotime($request->attgl2));            
            $angsuran = Angsuran_bayar::select('angsuran_bayars.*','kredits.no_kredit','prekredits.namansb')
            ->join('kredits','angsuran_bayars.kredit_id','kredits.id')
            ->join('prekredits','prekredits.kredit_id','angsuran_bayars.kredit_id')
            ->where('angsuran_bayars.tanggal','>',$stgl1)
            ->where('angsuran_bayars.tanggal','<',$stgl2)
            ->orderBy('angsuran_bayars.tanggal','asc')
            ->orderBy('angsuran_bayars.kredit_id','asc')
            ->get();
            return view('kredit.daftar_angsuran',compact('angsuran','stgl1','stgl2','model','ketmodel'));

        }
    }

    // public function lapangsuran()
    // {
    //     return view('kredit.daftar_angsuran');
    // }
    public function viewangsuran($id)
    {
        $datakredit = Prekredit::join('kredits', 'prekredits.kredit_id', '=', 'kredits.id')
            ->select('prekredits.kredit_id', 'prekredits.namansb','prekredits.alamatnsbktp','prekredits.notelp',
            'kredits.tgl_kredit','kredits.no_kredit','kredits.lama','kredits.sistem','kredits.pinj_pokok','kredits.pinj_prsbunga',
            'kredits.tgl_mulai','kredits.tgl_akhir','kredits.jatuhtempo','kredits.bbt','kredits.angsur_pokok','kredits.angsur_bunga',
            'kredits.nom_provisi','kredits.nom_adm','kredits.nom_notaris','kredits.nom_meterai','kredits.nom_asuransi',
            'kredits.assjiwa','kredits.sim_ang','kredits.nom_trans','kredits.namaao','kredits.perantara')
            ->where('kredits.id',$id)
            ->first();
        // ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        $tabelangsuran = Angsuran_jadwal::where('kredit_id',$id)
        ->OrderBy('bayar_ke')
        ->get();

        return view('kredit.lihat_kartu',compact('datakredit','tabelangsuran'));
    }
    public function pelunasan()
    {
        return view('kredit.daftar_pelunasan');
    }

    public function jmlkrd(Request $request)
    {
        $paginate = 10;
        $data['kredits'] = Kredit::paginate($paginate);
        return view('kredit.index', $data)->with('i', ($request->input('page', 1) - 1) * $paginate);
    }
    // public static function jumlahfasilitas()
    // {
    //     $jumlahfasilitas = DB::table('kredits')
    //     ->where('tgl_lunas','=','1900-01-01')
    //     ->count();
         
    //     if($jumlahfasilitas > 0) {
    //         //more than one raw
    //     }else {
    //         //zero raw
    //     }
         

    //     return view('template.sidebar',compact('jumlahfasilitas'));
    // }
}
