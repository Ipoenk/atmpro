<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Agunan_kend;
use App\Models\Agunan_sert;
use App\Models\Agunan_emas;
use App\Models\Agunan_deptab;
use App\Models\Prekredit;
class JaminanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return view('kredit.form_jaminan',compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nomohon = Prekredit::select('no_mohon')
                ->where('kredit_id','=',$request->idkredit)
                ->first();

        if($request->jenisjam = 'KENDARAAN'){
            $validator = Validator::make($request->all(), [
                'jenisjam'          => 'required',
                'anjaminankend'     => 'required',
                'alamatkend'        => 'required',
                'kotajaminankend'   => 'required',
                'merktype'          => 'required',
                'tahun'             => 'required',
                'warna'             => 'required',
                'nopol'             => 'required',
                'nobpkb'            => 'required',
                'norangka'          => 'required',
                'nomesin'           => 'required',
                'nilpasar'          => 'required|numeric',
                'niltaksasi'        => 'required|numeric',            
                'jenispk'           => 'required',
                'ikatankenppk'      => 'required',
            ]);

                if ($validator->fails()) {
                    redirect()
                        ->back()
                        ->withErrors($validator->errors());
                }

                $noagunanakhir = Agunan_kend::max('no_agunan');

                    if(is_null($noagunanakhir)){
                        $noagunan = '0000000001';
                    }
                    else {
                        if(count($noagunanakhir) > 0){
                            //ambil selisih hari tunggakan
                            $akhir = (int)($noagunanakhir->no_agunan)+1;
                            $noagunan = str_repeat('0',10-strlen($akhir)).$akhir;
                        }
                        else {
                            $noagunan = '0000000001';
                        }
                    }

                $jaminankend = new Agunan_kend;
                    $jaminankend->kredit_id = $request->idkredit;
                    $jaminankend->no_agunan = $noagunan;
                    $jaminankend->no_mohon = $nomohon->no_mohon;
                    $jaminankend->merktype = strtoupper($request->merktype);
                    $jaminankend->plat = substr($request->nopol,1,1);
                    $jaminankend->nopolisi = strtoupper($request->nopol);
                    $jaminankend->norangka = strtoupper($request->norangka);
                    $jaminankend->nomesin = strtoupper($request->nomesin);
                    $jaminankend->nobpkb = strtoupper($request->nobpkb);
                    $jaminankend->kodya = strtoupper($request->kotajaminankend); 
                    $jaminankend->tahun= strtoupper($request->tahun);
                    $jaminankend->warna = strtoupper($request->warna);
                    $jaminankend->jenis = strtoupper($request->jenisjam);
                    $jaminankend->pemilik = strtoupper($request->anjaminankend);
                    $jaminankend->alamat = strtoupper($request->alamatkend);
                    $jaminankend->niltaksasi = $request->niltaksasi;
                    $jaminankend->nilai = $request->niltaksasi;
                    $jaminankend->taksasi = $request->niltaksasi;
                    $jaminankend->nilpasar = $request->nilpasar;
                    $jaminankend->ikatan = strtoupper($request->jenispk);
                    $jaminankend->ikatanpk = strtoupper($request->ikatankenppk);
                    $jaminankend->terkait = 0;
                    $jaminankend->status = '';
                    $jaminankend->no_kait = '';
                    $jaminankend->perjanjian = strtoupper($request->ikatankenppk);
                    $jaminankend->opr = '';
                $jaminankend->SAVE();
        }
        else{
            if($request->jenisjam = 'SERTIFIKAT'){
                $validator = Validator::make($request->all(), [
                    'jenisjam'          => 'required',
                    'anjaminansert'     => 'required',
                    'alamatsert'        => 'required',
                    'kotajaminansert'   => 'required',
                    'alamatlokasisert'  => 'required',
                    'kotalokasisert'    => 'required',
                    'luastanah'         => 'required|numeric',
                    'luasbangunan'      => 'required|numeric',
                    'status'            => 'required',
                    'nosert'            => 'required',
                    'gambar'            => 'required',
                    'nilpasarsert'      => 'required|numeric',
                    'niltaksasisert'    => 'required|numeric',
                    'nilnjop'           => 'required|numeric',
                    'nilht'             => 'required|numeric',
                    'jenispk'           => 'required',
                    'ikatansertpk'      => 'required',

                ]);
    
                    if ($validator->fails()) {
                        redirect()
                            ->back()
                            ->withErrors($validator->errors());
                    }
    
                    $noagunanakhir = Agunan_sert::max('no_agunan');

                    if(is_null($noagunanakhir)){
                        $noagunan = '0000000001';
                    }
                    else {
                        if(count($noagunanakhir) > 0){
                            //ambil selisih hari tunggakan
                            $akhir = (int)($noagunanakhir->no_agunan)+1;
                            $noagunan = str_repeat('0',10-strlen($akhir)).$akhir;
                        }
                        else {
                            $noagunan = '0000000001';
                        }
                    }
                    $jaminansert = new Agunan_sert;
                        $jaminansert->kredit_id     = $request->idkredit;
                        $jaminansert->no_agunan     = $noagunan;
                        $jaminansert->no_mohon      = $nomohon->no_mohon;
                        $jaminansert->nosertif      = $request->nosert;
                        $jaminansert->jenis         = $request->jenisjam;
                        $jaminansert->sertstatus    = $request->status;
                        $jaminansert->pemilik       = strtoupper($request->anjaminansert);
                        $jaminansert->alamat        = strtoupper($request->alamatsert);
                        $jaminansert->kodya         = strtoupper($request->kotajaminansert);
                        $jaminansert->lokkodya      = strtoupper($request->kotalokasisert);
                        $jaminansert->luastanah     = $request->luastanah;
                        $jaminansert->luasbangunan  = $request->luasbangunan;
                        $jaminansert->lokasi        = $request->alamatlokasisert;
                        $jaminansert->taksasi       = $request->niltaksasisert;
                        $jaminansert->nilpasar      = $request->nilpasarsert;
                        $jaminansert->niltaksasi    = $request->niltaksasisert;
                        $jaminansert->nilai         = $request->niltaksasisert;
                        $jaminansert->nilnjop       = $request->nilnjop;
                        $jaminansert->nilhaktg      = $request->nilht;
                        $jaminansert->ikatanpk      = $request->ikatansertpk;
                        $jaminansert->perjanjian    = $request->ikatansertpk;
                        $jaminansert->ikatan        = $request->jenispk;
                        $jaminansert->status        = '';
                        $jaminansert->terkait       = 0;
                        $jaminansert->no_kait       = '';
                        $jaminansert->opr           = '';
                    $jaminansert->SAVE();            
                }
            else{
                if($request->jenisjam = 'EMAS'){
                    $validator = Validator::make($request->all(), [
                        'jenisjam'          => 'required',
                        'anjaminanemas'     => 'required',
                        'alamatemas'        => 'required',
                        'kotajaminanemas'   => 'required',
                        'nosurat'           => 'required',
                        'namatoko'          => 'required',
                        'berat'             => 'required|numric',
                        'karat'             => 'required|numric',
                        'nilpasaremas'      => 'required|numric',
                        'niltaksasiemas'    => 'required|numric',
                        'jenispk'           => 'required',
                        'ikatanemaspk'      => 'required',
    
                    ]);
        
                        if ($validator->fails()) {
                            redirect()
                                ->back()
                                ->withErrors($validator->errors());
                        }
        
                        $noagunanakhir = Agunan_emas::max('no_agunan');

                        if(is_null($noagunanakhir)){
                            $noagunan = '0000000001';
                        }
                        else {
                            if(count($noagunanakhir) > 0){
                                //ambil selisih hari tunggakan
                                $akhir = (int)($noagunanakhir->no_agunan)+1;
                                $noagunan = str_repeat('0',10-strlen($akhir)).$akhir;
                            }
                            else {
                                $noagunan = '0000000001';
                            }
                        }

                        $jaminanemas = new Agunan_emas;
                        $jaminanemas->kredit_id = $request->idkredit;
                        $jaminanemas->no_agunan = $noagunan;
                        $jaminanemas->no_mohon = $nomohon->no_mohon;
                        $jaminanemas->pemilik = $request->anjaminanemas;
                        $jaminanemas->alamat = $request->alamatemas;
                        $jaminanemas->kodya = $request->kotajaminanemas;
                        $jaminanemas->nosurat = $request->nosurat;
                        $jaminanemas->namatoko = $request->namatoko;
                        $jaminanemas->jenis = $request->jenisjam;
                        $jaminanemas->berat = $request->berat;
                        $jaminanemas->karat = $request->karat;
                        $jaminanemas->nilpasar = $request->nilpasaremas;
                        $jaminanemas->niltaksasi = $request->niltaksasiemas;
                        $jaminanemas->nilai = $request->niltaksasiemas;
                        $jaminanemas->taksasi = $request->niltaksasiemas;
                        $jaminanemas->ikatanpk = $request->ikatanemaspk;
                        $jaminanemas->perjanjian = $request->ikatanemaspk;
                        $jaminanemas->terkait = 0;
                        $jaminanemas->ikatan = $request->jenispk;
                        $jaminanemas->status = '';
                        $jaminanemas->no_kait = '';
                        $jaminanemas->opr = '';
                        $jaminanemas->SAVE();   
                }
                else{
                    if($request->jenisjam = 'TABUNGAN'){
                        // return redirect()->route('kredit.jaminantabdep', compact($jaminan));
                    }
                    else{
                        if($request->jenisjam = 'DEPOSITO'){
                            // return redirect()->route('kredit.jaminantabdep', compact($jaminan));
                        }
                    }
                }
            }
        }
        return redirect('kredit.daftarjaminan',[$request->idkredit]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jamkendaraan = Agunan_kend::where('kredit_id','=',$id)
            ->get();
           Log::info($jamkendaraan); 
            return view('kredit.daftar_jaminan',compact('jamkendaraan','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
