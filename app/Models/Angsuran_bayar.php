<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Angsuran_bayar extends Model
{
    use HasFactory;
    protected $fillable = ['kredit_id','nobukti','kantorkas','tanggal','ongkos','potongan',
        'pokok','bunga','denda','denda_kena','denda_lalu','denda_utang','cara','selisih','opr'];
}
