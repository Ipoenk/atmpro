<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agunan_sertnuk extends Model
{
    use HasFactory;
    protected $fillable = ['kreditnuk_id','no_agunan','no_mohon','nosertif','jenis','sertstatus',
        'pemilik','alamat','kodya','lokkodya','luastanah','luasbangunan','lokasi','taksasi','nilpasar',
        'niltaksasi','nilai','ikatanpk','nilnjop','nilhaktg','perjanjian','ikatan','status','terkait',
        'no_kait','opr'];
}
