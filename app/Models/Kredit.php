<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kredit extends Model
{
    use HasFactory;
    protected $fillable = ['no_kredit','no_ref','kode_kantor','no_mohon','tgl_kredit','pinj_pokok',
        'no_kredit','no_ref','kode_kantor','no_mohon','tgl_kredit','pinj_pokok','plafon','pinj_prsbunga',
        'sistem','lama','tgl_mulai','tgl_akhir','advance','nbulan','angsur_pokok','angsur_bunga',
        'pinj_nombunga','bbt','npokok','prs_provisi','prs_adm','nom_provisi','nom_notaris','nom_adm',
        'nom_meterai','nom_asuransi','assjiwa','sim_ang','bakidebet','saldo_bbt','saldo_piutang',
        'jatuhtempo','status','opr','plafon','pinj_prsbunga','sistem','lama','tgl_mulai','tgl_akhir',
        'advance','nbulan','angsur_pokok','angsur_bunga','pinj_nombunga','bbt','npokok','prs_provisi',
        'prs_adm','nom_provisi','nom_notaris','nom_adm','nom_meterai','nom_asuransi','assjiwa','sim_ang',
        'bakidebet','saldo_bbt','saldo_piutang','jatuhtempo','jml_jaminan','tgl_lunas','status','totangspokok',
        'totangsbunga','norektab','nobg','kode_pk','tt_nama','tt_kerja','tt_alamat','tt_noktp','penggunaan',
        'golongan','organisasi','sektor','goljamin','programkredit','namaao','perantara','tgl_kurang',
        'tgl_ragu','tgl_macet','tgl_cadang','tgl_hapusint','no_hapusint','tgl_hapusbi','no_hapusbi',
        'tgl_lunaslalu','cek','sts_kirim','panjang','mnl_angs','mnl_tabl','upload','bagjamin','fasilitas',
        'sifatkrd','golkredit','cdg_nosrt','cdg_ok','jnsbunga','usaha','jnsusaha','nom_trans','prdbayar',
        'smbdana','terkait','dampak','tt_skdir','tt_tglsk','opr','restrukturisasi','tgl_restruk',
        'tgl_restruk_awal','tgl_restruk_akhir','restruk_ke','kode_restruk','ket_restruk'];


        public static function jumlahfasilitas()
    {
        $jumlahfasilitas = DB::table('kredits')
        ->where('tgl_lunas','=','1900-01-01')
        ->count();
         return Kredit::count();
        
    }
}
