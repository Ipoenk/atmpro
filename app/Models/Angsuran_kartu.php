<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Angsuran_kartu extends Model
{
    use HasFactory;
    protected $fillable = ['kredit_id','angs_nobukti','denda_simp','kantorkas','angs_tgl','bunga_sldbbt',
        'plafon','bakidebet','potongan','ongkos','bunga_hasil','angs_titippokok','angs_titipbunga',
        'denda','sld_piutang','angs_pokok','angs_bunga','angs_ke','cara','selisih','opr'];
}
