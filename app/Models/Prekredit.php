<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Prekredit extends Model
{
    use HasFactory;
    protected $fillable = ['nasabah_id', 
    'kredit_id','no_mohon','niknsb','namansb','tmplahirnsb','tgllahirnsb','kelaminnsb','agamansb',
    'alamatnsbktp','propinsinsbktp','kotansbktp','kecamatannsbktp','desansbktp','kodeposnsbktp','alamatdomisili',
    'propinsidomisili','kotadomisili','kecamatandomisili','desadomisili','notelp','statusperkawinan',
    'namaibukandung','nikps','namaps','alamatps','propinsips','kotaps','kecamatanps','desaps',
    'kodeposps','notelpps','kelaminps','tmplahirps','tgllahirps','agamaps','jenispsh','namapsh',
    'alamatpsh','propinsipsh','kotapsh','kecamatanpsh','desapsh','kodepospsh','notelppsh','Bidangusaha',
    'sumberdana','pendapatan','pengeluaran','tanggungan','ststempattinggal','jenisnsb','tgl_mohon',
    'catatan','hsltani','hslpegawai','hsljasa','kegunaan','usaha','hsldagang','hubdagang','kreditlalu',
    'andalan','pjm_nama','pjm_alamat','keperluan','status','pengajuan','survey','analisa','no_srtsetuju',
    'tgl_srtsetuju','tujuan','tglubah','email','terkait','langgarbmpk','lampauibmpk','gopublic',
    'peringkat','lembaga_peringkat','tglperingkat','namagrup','kodekantor','golpihak3'];

    public static function getId(){
        $blt = date('m/Y');
        return $getId = DB::table('prekredits')->where('no_mohon','like',$blt)->orderBy('Id','DESC')->take(1)->get();
    }
}
