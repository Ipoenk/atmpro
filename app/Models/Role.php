<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Log;

class Role extends Model
{
    use HasFactory;

    public function users(){
    	return $this->hasMany('App\Models\User','role_id','id');
    }
    public function permissions(){
    	return $this->belongsToMany('App\Models\Permission')->withTimestamps();
    }
    public function hasPermission($kode){
    	
    	foreach($this->permissions as $permission){
    		Log::info($kode);
    		Log::info($permission->kode);
    		if($permission->hasAccess($kode)){
    			return true;
    		}
    	}
    	return false;
    }
}
