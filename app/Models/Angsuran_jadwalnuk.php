<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Angsuran_jadwalnuk extends Model
{
    use HasFactory;
    protected $fillable = ['kredit_id','tgl_angsur','bayar_pokok','bayar_bunga','angs_pokok',
        'angs_bunga','bayar_ke','provisi','bitrans','akhbulan','ab_pdbunga','ab_provisi','ab_bitrans',
        'jta_pdbunga','jta_provisi','jta_bitrans','jta_adm','ab_adm','adm','opr'];
}
