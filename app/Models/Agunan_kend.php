<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agunan_kend extends Model
{
    use HasFactory;
    protected $fillable = ['kredit_id','no_agunan','no_mohon','merktype','plat','nopolisi','norangka',
    'nomesin','nobpkb','kodya','tahun','warna','jenis','pemilik','alamat','niltaksasi','nilai',
    'taksasi','nilpasar','ikatan','ikatanpk','terkait','status','no_kait','perjanjian','opr'];

}
