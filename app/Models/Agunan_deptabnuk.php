<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agunan_deptabnuk extends Model
{
    use HasFactory;
    protected $fillable = ['kredit_id','no_agunan','no_mohon','terkait','kodya','nodep','namabank',
        'jenis','pemilik','alamat','tempo','nilpasar','niltaksasi','nominal','nilai','taksasi','jangka',
        'ikatanpk','perjanjian','ikatan','status','no_kait','opr'];
}
