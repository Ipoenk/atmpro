<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Nasabah extends Model
{
    use HasFactory;
    protected $fillable = ['niknsb','namansb','alamatnsbktp','propinsinsbktp','kotansbktp','kecamatannsbktp',
    'desansbktp','kodeposnsbktp','notelpnsb','email','jenisnsb','alamatdomisili','propinsidomisili','kotadomisili',
    'kecamatandomisili','desadomisili','kelaminnsb','tmplahirnsb','tgllahirnsb','agamansb','statusperkawinan',
    'namaibukandung','jenispsh','namapsh','alamatpsh','propinsipsh','kotapsh','kecamatanpsh','desapsh',
    'kodepospsh','notelppsh','Bidangusaha','sumberdana','pendapatan','pengeluaran','tanggungan','ststempattinggal']; 

    public static function totalNasabah() {
        return Nasabah::count();
    }
}
