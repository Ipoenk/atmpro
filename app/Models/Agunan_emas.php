<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agunan_emas extends Model
{
    use HasFactory;
    protected $fillable = ['kredit_id','no_agunan','no_mohon','pemilik','kodya','nosurat','namatoko',
    'jenis','alamat','nilpasar','niltaksasi','berat','nilai','taksasi','karat','ikatanpk','perjanjian',
    'terkait','ikatan','status','no_kait','opr'];

}
