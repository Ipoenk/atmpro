<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kreditnuk extends Model
{
    use HasFactory;
    protected $fillable = ['no_kredit','no_ref','kode_kantor','no_mohon','tgl_kredit','pinj_pokok',
        'plafon','pinj_prsbunga','sistem','lama','tgl_mulai','tgl_akhir','advance','nbulan','angsur_pokok',
        'angsur_bunga','pinj_nombunga','bbt','npokok','prs_provisi','prs_adm','nom_provisi','nom_notaris',
        'nom_adm','nom_meterai','nom_asuransi','assjiwa','sim_ang','bakidebet','saldo_bbt','saldo_piutang',
        'jatuhtempo','status','opr'];
}
