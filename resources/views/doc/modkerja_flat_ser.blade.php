<?php
include 'fungsi_pk.php'
?>
<html>
	<head>
		<title></title>
		<style>
		body{width:690px;}
			td{border:solid 0px #000;}
			tr{border:solid 0px red;}
			 @media print
			{
				.nprintable { display: none; }
				.printable { display: block;  border: solid 1px #fff;}
				.halaman{border-top:solid 1px #000; height:15px; text-align:right; font-size:10px;};
			}
			@media screen
			{
				.nprintable { display: block; }
				.printable { display: block; border: solid 0px #000; height:1030px;}
				.halaman{display: none;};
			}
		</style>
	</head>
	<body style="text-align:justify; margin-left:20px;">
		<p align="center" style="margin-left:18.0pt;">
			PERJANJIAN PEMBIAYAAN MULTIGUNA</p>
		<p align="center">
			<strong>No : <?php echo trim($row[npp]);?></strong></p>
		<p>
			&nbsp;</p>
		<p>
			Perjanjian ini dibuat dan ditandatangani pada hari ini, <?php echo getday($row['tgl_pk']).','.dmy($row['tgl_pk']);?>, antara:</p>
		<ol>		
		<table border=0>
			<tr>
				<td width=30 valign=top>1.</td>
				<td width=80 colspan=4>KOPERASI SIMPANPINJAM. MITRA USAHA atau disingkat KSP. MITRA USAHA yang beralamat di <?php echo setbpr('PKALAMAT1');?> <?php echo setbpr('KOTABPR');?> dalam hal ini diwakili oleh <?php echo setbpr('NAMADIRUT');?> selaku <?php echo setbpr('PKKERJA1')?></td>				
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan=3>- Selanjutnya dalam Perjanjian ini disebut sebagai KREDITUR </td>
			</tr>
			<tr>
				<td width=30></td>
				<td width=80></td>
				<td></td>
				<td><b></b></td>
			</tr>
			<tr>
				<td width=30>2.</td>
				<td width=80>N a m a</td>
				<td>:</td>
				<td><b><?php echo $row['nama']; ?></b></td>
			</tr>
			<!--<tr>
				<td></td>
				<td></td>
				<td>Pekerjaan</td>
				<td>:</td>
				<td><b><?php echo trim($row['usaha']); ?></b></td>
			</tr>-->
			<tr>
				<td></td>
				<td valign=top>Alamat</td>
				<td valign=top>:</td>
				<td valign=top><b><?php echo trim($row['alamat']).', RT/RW '.$row['rtrw'].', DS. '.trim($row['desa']).', KEC. '.trim($row['camat']).' - '.$row['kodya']; ?></b></td>
			</tr>

			<tr>
				<td>&nbsp;</td><td colspan=3>-untuk melaksanakan tindakan hukum dalam Surat Perjanjian ini telah mendapat persetujuan dari istri/suami yang sah yang turut pula menandatangani surat ini, yaitu :</td>
			</tr>
			<tr>
				<td></td>
				<td>N a m a</td>
				<td>:</td>
				<td><b><?php echo $row['ps_nama']; ?></b></td>
			</tr>
			<!--<tr>
				<td></td>
				<td>Pekerjaan</td>
				<td>:</td>
				<td></td>
			</tr>-->
			<tr>
				<td></td>
				<td valign=top>Alamat</td>
				<td valign=top>:</td>
				<td valign=top><b><?php echo trim($row['ps_alamat']).', RT/RW '.$row['rtrw'].', DS. '.trim($row['desa']).', KEC. '.trim($row['camat']).' - '.$row['kodya']; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan=3>- Selanjutnya dalam Perjanjian ini disebut sebagai DEBITUR </td>
			</tr>			
		</table>	
		<p>
			-KREDITUR dan DEBITUR,&nbsp; keduanya disebut juga Para Pihak dengan ini menerangkan terlebih dahulu :</p>
		<ul>
			<li>
				bahwa Debitur telah mengajukan permohonan kepada KREDITUR untuk mendapatkan fasilitas pembiayaan Modal Kerja dan Kreditur telah setuju memberikan fasilitas pembiayaan kepada Debitur berupa Pembiayaan Modal Kerja;</li>
		</ul>
		<p>
			-Sehubungan dengan hal tersebut para pihak telah sepakat dan setuju mengadakan Perjanjian Pembiayaan, selanjutnya disebut PERJANJIAN, dengan syarat dan ketentuan sebagai berikut :</p>

		<p>
			<strong>PASAL 1. JENIS dan BESARNYA FASILITAS PEMBIAYAAN</strong></p>
		<ol>
			<li>
				KREDITUR telah setuju memberikan fasilitas pembiayaan kepada DEBITUR untuk Modal Kerja berupa <?php echo $row[kred_tujuan]; ?> <em>(pembelian mobil, &nbsp;utk renovasi rumah, utk tambahan beli rumah, untuk modal usaha/kerja).</em></li>
			<li>
				Fasilitas pembiayaan yang diberikan KREDITUR kepada DEBITUR sebesar &nbsp;Rp <?php echo rp($row[pinj_pokok]);?></li>
		</ol>

		<p>
			<strong>PASAL 2. BIAYA-BIAYA</strong></p>
		<ol>
			<li>
				Terhadap fasilitas pembiayaan ini DEBITUR sepakat dan setuju untuk membayar biaya-biaya kepada KREDITUR, yaitu: &nbsp;</li>
		</ol>
		<table border=0 style="margin-left:50px">			
			<tr>
				<td>a.</td>
				<td style="width:215px;">biaya Provisi sebesar</td>
				<td>:Rp <?php echo rp($jml_prov);?></td>
			</tr>
			<tr>
				<td>b.</td>
				<td>biaya Administrasi sebesar</td>
				<td>:Rp <?php echo rp($jml_adm);?></td>
			</tr>
			<tr>
				<td>c.</td>
				<td>biaya Asuransi sebesar</td>
				<td>:Rp <?php echo rp($jml_ass);?></td>
			</tr>
			<tr>
				<td>d.</td>
				<td>biaya  Fiducia /APHT sebesar</td>
				<td>:Rp <?php echo rp($jml_fidusia);?></td>
			</tr>
			<tr>
				<td>e.</td>
				<td>biaya Polis</td>
				<td>:Rp <?php echo rp($jml_polis);?></td>
			</tr>
			<tr>
				<td>e.</td>
				<td>biaya Lainya</td>
				<td>:Rp <?php echo rp($jml_lain);?></td>
			</tr>			
		</table>
	
		<ol>
			<li value="2">
				biaya-biaya tersebut di atas seluruhnya berjumlah Rp <?php echo rp($jml_bi);?> dibayar DEBITUR dengan cara <em>(*dimasukkan pada pokok pembiayaan / dibayar tunai kepada KREDITUR / dipotong saat pencairan)</em></li>
			<li value="3">
				Selain biaya-biaya tersebut di atas DEBITUR juga wajib membayar segala biaya yang mungkin timbul karena penagihan atas kelalaian DEBITUR termasuk tetapi tidak terbatas pada biaya berperkara, pengacara, biaya eksekusi Agunan, biaya pengurusan dokumen/surat pendukung untuk klaim asuransi karena kehilangan atau kerusakan Agunan dan biaya lainnya.</li>
		</ol>
		<p>
			<strong>PASAL 3. POKOK HUTANG, BUNGA </strong></p>
		<ol>
			<li>
				Pokok Hutang Pembiayaan seluruhnya sebesar Rp <?php echo rp($row[pinj_pokok]);?></li>
			<li>
				Terhadap pokok hutang tersebut dikenakan bunga sebesar Rp <?php echo rp($row[bbt]);?></li>
		</ol>
		<p>
			<strong>PASAL 4. JANGKA WAKTU, PEMBAYARAN KEMBALI dan DENDA </strong></p>
		<ol>
			<li>
				Jangka waktu pegembalian fasilitas Pembiayaan berlangsung selama <?php echo ($row[lama]);?> bulan.</li>
			<li>
				Pengembalian fasilitas pembiayaan dilakukan DEBITUR dengan cara :
				<ul>
					<li>
						Diangsur setiap bulan sebesar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Rp. <?php echo rp($angs_bln);?></li>
					<li>
						Banyaknya angsuran&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $row[lama];?> kali angsuran</li>
					<li>
						Angsuran dibayar setiap tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo substr($row['tgl_pk'],8,2);?> setiap bulan</li>
					<li>
						Tanggal Angsuran Pertama&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo dmy(adddate2($row[tgl_pk],"+1 month"));?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
					<li>
						Tanggal Angsuran Terakhir&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo dmy(adddate2($row[tgl_pk],"".$row[lama]." month"));?></li>
				</ul>
			</li>
			<li>
				Jika pada tanggal pembayaran angsuran sebagaimana ayat 2 Pasal ini, DEBITUR tidak dapat membayar tepat pada waktunya maka DEBITUR sepakat, setuju dan dengan suka rela untuk mengganti kerugian kepada KREDITUR sebesar <b><?php echo $bpr_denda;?></b>% perhari dari pembayaran tertunggak.</li>
		</ol>
		<p>
			<strong>PASAL 5.&nbsp; PENGAKUAN BERHUTANG</strong></p>
		<p>
			Sehubungan dengan segala sesuatu yang diuraikan pada pasal-pasal dalam perjanjian ini maka DEBITUR, sekarang dan untuk dikemudian hari, mengakui secara sah telah berhutang pada KREDITUR sejumlah Hutang yang dari waktu ke waktu terhutang oleh DEBITUR kepada KREDITUR dan cukup dibuktikan dengan suatu pernyataan tertulis dari KREDITUR yang menyebutkan jumlah hutang yang didasarkan pada catatan-catatan pembukuan KREDITUR sendiri, pernyataan mana menjadi bukti yang sah dan mengikat kepada DEBITUR dan merupakan satu kesatuan yang tidak terpisahkan dari Perjanjian ini <strong>tanpa mengurangi hak </strong>DEBITUR untuk membuktikan sebaliknya, dan apabila ada catatan yang keliru, maka KREDITUR akan melakukan pembetulan.</p>
		<p>
			<strong>PASAL 6. A G U N A N</strong></p>
		<ol>
			<li>
				Untuk menjamin lebih lanjut pembayaran kembali secara tertib dan sebagaimana mestinya dari utang yang harus dibayarkan oleh DEBITUR kepada KREDITUR berdasarkan Perjanjian ini maupun perubahan, tambahan, novasi dan / atau perjanjian yang akan dibuat kemudian hari, maka dengan ini DEBITUR menyerahkan jaminan kepada KREDITUR berupa :</li>
		</ol>
		<?php
		echo '<table style="margin-left:30pt;">';
		//echo '<b>1 (Satu)</b> sertifikat dengan identitas sebagai berikut :</br>';
		echo '
		<tr>
			<td></td>
			<td>Atas Nama</td>
			<td>:</td>
			<td>'.trim($row[ser_pemilik]).'</td>
		</tr>
		<tr>
			<td></td>
			<td>Luas Tanah</td>
			<td>:</td>
			<td>'.trim($row[luastanah]).' M2</td>
		</tr>
		<tr>
			<td></td>
			<td>Nomor Sertifikat</td>
			<td>:</td>
			<td>'.trim($row[ser_nomor]).'</td>
		</tr>
		<tr>
			<td></td>
			<td>Luas Bangunan</td>
			<td>:</td>
			<td>'.trim($row[luasbangunan]).' M2</td>
		</tr>
		<tr>
			<td></td>
			<td>Jenis</td>
			<td>:</td>
			<td>'.trim($row[sertstatus]).'</td>
		</tr>
		<tr>
			<td></td>
			<td>Taksasi</td>
			<td>:</td>
			<td>Rp '.rp($row[ser_taksasi]).'</td>
		</tr>
		<tr>
			<td></td>
			<td>Nilai NJOP</td>
			<td>:</td>
			<td>Rp '.rp($row[nilnjop]).'</td>
		</tr>		
		';
		echo '</table>';
		?>
		<!--
		<p style="margin-left:13.5pt;">
			&nbsp;</p>
		<ol style="list-style-type:lower-roman;">
			<li value="5">
				1. &hellip;. (&hellip;..) bidang tanah Sertifikat Hak &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;(&hellip;&hellip;&hellip;), yaitu :</li>
		</ol>
		<p style="margin-left:36.0pt;">
			&nbsp;</p>
		<ul>
			<li>
				Nomor &hellip;. / seluas &hellip;.M2, terletak di &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...</li>
		</ul>
		<p style="margin-left:54.0pt;">
			&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
		<p style="margin-left:54.0pt;">
			-nama Pemegang Hak :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
		<p style="margin-left:27.0pt;">
			&nbsp;</p>
		<p style="margin-left:54.0pt;">
			-demikian berikut segala sesuatu yang berada, ditanam dan didirikan di atas tanah tersebut yang karena sifat, peruntukannya dan menurut Undang-Undang termasuk barang tak bergerak yang keadaannya telah diketahui oleh KREDITUR;</p>
		<p style="margin-left:27.0pt;">
			&nbsp;</p>
		<p style="margin-left:27.0pt;">
			<em>atau</em></p>
		<p style="margin-left:27.0pt;">
			&nbsp;</p>
		<ol style="list-style-type:lower-roman;">
			<li value="5">
				..... unit kendaraan bermotor roda....dengan identitas sebagai berikut :</li>
		</ol>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
		<p style="margin-left:18.0pt;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Merk/type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>
		<p style="margin-left:18.0pt;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tahun &ndash; Warna&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>
		<p style="margin-left:18.0pt;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nomor Rangka&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>
		<p style="margin-left:18.0pt;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nomor Mesin&nbsp;&nbsp; :</p>
		<p style="margin-left:18.0pt;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No. Polisi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>
		<p>
			&nbsp;</p>
		<p style="margin-left:18.0pt;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; satu dan lain hal sebagaimana tercantum dalam Sertifikat Nomor <?php echo trim($row[ser_nomor]);?>, dan apabila terjadi perubahan yang disebabkan &nbsp;&nbsp;&nbsp; Ketentuan dalam Pasal 64 dan 65 UU No.22 tahun 2009, tentang Registrasi dan Indentitas Kendaraan Bermotor, maka perubahan yang dimaksud kata demi kata &nbsp; dianggap sebagaimana telah termaktub dalam jaminan yang telah diserahkan maupun &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; surat-surat, keterangan-keterangan, pernyataan-pernyataan dan perjanjian-perjanjian &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; yang telah dan akan dibuat.</p>
		<p style="margin-left:18.0pt;">
			&nbsp;</p>
			-->
		<ol>
			<li value="2">
				Pemberian jaminan tersebut diikat dalam suatu Akta Pembebanan Agunan sesuai dengan ketentuan hukum yang berlaku di Indonesia dan karenanya DEBITUR dan/atau Pemegang Hak mengikatkan diri untuk menandatangani seluruh surat-surat atau akta-akta yang berkaitan dengan pengikatan jaminan yang merupakan satu kesatuan yang tidak terpisahkan dan tidak terlepas dari perjanjian ini.</li>
		</ol>
		<p>
			<strong>PASAL 7. PELUNASAN LEBIH AWAL</strong></p>
		<ol>
			<li>
				DEBITUR dapat mempercepat pelunasan utang sebelum batas waktu yang telah ditetapkan dan untuk maksud tersebut DEBITUR sepakat dan setuju untuk memberitahukan kepada KREDITUR secara tertulis selambat-lambatnya 7 (tujuh) hari sebelum tanggal percepatan pelunasan.</li>
			<li>
				Untuk percepatan pelunasan angsuran sebagaimana dimaksud, DEBITUR sepakat dan setuju mengganti kerugian kepada KREDITUR sebesar 2% dari sisa pokok.</li>
		</ol>

		<p>
			<strong>PASAl 8. PERISTIWA CIDERA JANJI (WANPRERSTASI)</strong></p>
		<ol>
			<li>
				Peristiwa Cidera Janji timbul apabila telah terjadi salah satu atau lebih dari kejadian-kejadian sebagai&nbsp; berikut :</li>
		</ol>
		<ol style="list-style-type:lower-alpha;">
			<li>
				DEBITUR lalai dan tidak membayar angsuran dan/atau bunga, denda dan jumlah lainnya yang terhutang yang wajib dibayar DEBITUR kepada KREDITUR setelah lebih dari 5 hari dari waktu yang telah ditentukan menurut perjanjian ini;</li>
			<li>
				DEBITUR lalai atau tidak memenuhi syarat-syarat lain dalam perjanjian ini dan atau sesuatu perpanjangan, penambahan, perubahan atau penggantiannya serta terjadi pelanggaran terhadap syarat-syarat yang tertera dalam perjanjian jaminan yang dibuat berkenaan dengan perjanjian ini;</li>
			<li>
				Surat keterangan dan dokumen-dokumen yang diberikan DEBITUR berhubungan dengan perjanjian ini dan / atau tambahan daripadanya ternyata palsu atau tidak mengandung kebenaran baik seluruh maupun sebagian;</li>
			<li>
				Barang agunan atau barang yang dijaminkan itu hilang, dijual, disewakan, dipindahtangankan, dialihkan atau dijaminkan kepada pihak lain atau terjadi kerusakan atau kehancuran baik sebagian maupun seluruhnya. Dalam hal barang jaminan itu hilang atau musnah, maka KREDITUR dan DEBITUR sepakat untuk melepaskan ketentuan dalam Pasal 1444 dan 1445 Kitab Undang-undang Hukum Perdata.</li>
			<li>
				DEBITUR telah dinyatakan tidak mampu membayar utang-utangnya atau&nbsp; dinyatakan pailit, atau berada di bawah pengampuan, atau DEBITUR telah mengajukan permohonan penundaan pembayaran utang (surseance van betalling) atau sebab apapun DEBITUR dinyatakan tidak berhak lagi mengurus dan menguasai harta kekayaannya, baik seluruh maupun sebagian;</li>
			<li>
				Jika sebagian maupun seluruh harta kekayaan DEBITUR telah disita oleh Pengadilan maupun pihak lainnya;</li>
			<li>
				DEBITUR meninggal dunia,&nbsp; sedangkan ahli waris tidak bersedia meneruskan kewajiban DEBITUR terhadap KREDITUR menurut Perjanjian ini;</li>
		</ol>
		<ol>
			<li value="2">
				Dalam hal terjadi peristiwa cidera janji (Wanprestasi) sebagaimana ayat 1 pasal ini, maka seluruh hutang serta kewajiban DEBITUR berdasarkan perjanjian ini menjadi jatuh tempo, oleh sebab itu DEBITUR diwajibkan dan bersepakat untuk membayar seketika dan sekaligus lunas seluruh jumlah hutang yang tersisa berikut bunga dan biaya-biaya yang masih terhutang, yang harus dibayar DEBITUR&nbsp; pada waktu yang akan ditentukan oleh KREDITUR</li>
			<li value="3">
				Jika telah melebihi 30 hari dari lewatnya waktu yang ditentukan oleh KREDITUR, DEBITUR lalai dan tidak membayar lunas seluruh hutang yang jatuh tempo sebagaimana dimaksud ayat 2 pasal ini maka&nbsp; KREDITUR akan melaksanakan hak-haknya terhadap agunan dengan menggunakan Parate Eksekutorial.</li>
			<li value="4">
				Dalam hal pelaksanaan ayat 3 pasal ini, maka DEBITUR menyatakan telah sepakat dan / atau setuju secara sukarela untuk mengijinkan KREDITUR melakukan tindakan dimaksud guna menyelesaikan dan / atau melunasi hutang DEBITUR.</li>
			<li value="5">
				dalam hal berakhirnya perjanjian ini sebagaimana dimaksud pada ayat 2 pasal ini, para pihak sepakat untuk melepaskan ketentuan Pasal 1266 dan 1267 KUH Perdata.</li>
		</ol>
		<p>
			<strong>PASAL 9. PERUBAHAN</strong></p>
		<p>
			Perjanjian dapat diubah hanya dengan persetujuan tertulis dari DEBITUR dan KREDITUR.&nbsp; Perubahan tersebut akan diatur dalam suatu perjanjian yang&nbsp; merupakan bagian dan menjadi kesatuan yang tidak dapat terpisahkan dari Perjanjian ini, dan karenanya seluruh ketentuan dalam Perjanjian ini tetap berlaku pada perjanjian perubahan tersebut kecuali untuk hal- hal yang disepakati untuk diubah.</p>
		<p>
			<strong>PASAL 10.&nbsp; KETENTUAN BERLAKUNYA KUASA-KUASA</strong></p>
		<p>
			Semua dan setiap kuasa yang diberikan oleh debitur kepada KREDITUR di dalam dan/atau berdasarkan perjanjian ini merupakan bagian-bagian yang terpenting dan tidak dapat dipisahkan dari perjanjian ini dan dengan demikian kuasa-kuasa tersebut tidak dapat ditarik kembali maupun dibatalkan oleh sebab-sebab yang tercantum dalam pasal 1813, 1814 dan 1816 Kitab Undang-undang Hukum Perdata.</p>
		<p>
			<strong>PASAL 11.&nbsp; LAIN-LAIN</strong></p>
		<ol>
			<li>
				Segala sesuatu yang belum cukup di atur dalam Perjanjian ini akan diatur kemudian, baik dalam surat-surat, maupun dalam perjanjian-perjanjian tambahan dan merupakan satu kesatuan yang tidak dapat dipisahkan dengan Perjanjian ini.</li>
			<li>
				Judul-judul dalam setiap pasal Perjanjian ini hanya untuk memudahkan membaca Perjanjian dan tidak dapat dianggap sebagai bagian dari Perjanjian serta tidak memberikan penafsiran apapun atas isi Perjanjian.</li>
		</ol>
		<p>
			<strong>PASAL 12. PENUTUP </strong></p>
		<ol>
			<li>
				Perjanjian ini telah disesuaikan dengan ketentuan Peraturan Perundang-undangan termasuk ketentuan Peraturan Otoritas Jasa Keuangan.</li>
			<li>
				Jika terjadi perselisihan atas penafsiran dan / atau pelaksanaan Perjanjian ini akan diselesaikan dengan cara musyawarah untuk mencapai mufakat.</li>
			<li>
				Dalam hal musyawarah tidak tercapai mufakat maka kedua belah pihak sepakat untuk memilih tempat kedudukan hukum yang tetap dan tidak berubah di kantor Pengadilan Negeri Malang dengan tidak mengurangi hak dari KREDITUR untuk memohon pelaksanaan eksekusi atau mengajukan tuntutan hukum terhadap DEBITUR berdasarkan perjanjian ini dihadapan Pengadilan Negeri lainnya dimanapun dalam wilayah Republik Indenesia.</li>
		</ol>
		<p>
			&nbsp;</p>
		<p>
			Demikian perjanjian ini di buat dan ditandatangani di <?PHP echo $kota_kantor; ?> oleh para pihak, pada hari dan tanggal sebagaimana tersebut diatas oleh Para Pihak.</p>
		<p>
			&nbsp;</p>
		<table>
		<tr>
		<td></td>
		<td width=200 style="text-align:left">KREDITUR</td>
		<td width=230 style="text-align:left">&nbsp;</td>
		<td width=200 style="text-align:right">DEBITUR</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr><tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td></td>
		<td <p style="font-size:14;font-weight:bold;text-align:left; width:200px;"><?php echo trim(setbpr('NAMADIR'));?></p></td>
		<td width=200px;>&nbsp;</td>
		<td <p style="font-size:14;font-weight:bold;text-align:right; width:200px;"><?php echo trim($row['nama']);?></p></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<td></td>
		<td width=150 style="text-align:left">SAKSI-SAKSI</td>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr><tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>1.</td>
		<td style="text-align:left">(............................)</td>
		<td>2. (............................)</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr><tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	
	<tr>
		<td>&nbsp;
		</td>
	</tr>
		
	<tr>
		<td>&nbsp;
		</td>
	</tr>
		
	<tr>
		<td>&nbsp;
		</td>
	</tr>
		
	<tr>
		<td>&nbsp;
		</td>
	</tr>
		
	<tr>
		<td>&nbsp;
		</td>
	</tr>
		
	<tr>
		<td>&nbsp;
		</td>
	</tr>	
	<tr>
		<td>&nbsp;
		</td>
	</tr>	
	<tr>
		<td>&nbsp;
		</td>
	</tr>	
	<tr>
		<td>&nbsp;
		</td>
	</tr>	
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	
	</table>
	</body>
</html>
<script>
	window.print();
	//setTimeout("window.close()",100);
</script>
