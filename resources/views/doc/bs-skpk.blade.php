<?php
	//$dbname = 'ksp_wum5';
	//$svname = 'localhost';
	
	include "koneksi.php";
	include "fungsi.php";
	include 'terbilang.fnc.php';
	$bpr_nama = 'KSP WIRA USAHA MANDIRI';
	$bpr_alamat = 'Jl. Panglima Sudirman No. 29 Kepanjen-Malang';
	//$nk = trim($_GET[nk]);
	$nk = "WUM1211000027";
	//$setbpr=array('namapimp'=>'CINDY MARGARETH','jabatan'=>'PIMPINAN','alamat'=>'JL. PANGLIMA SUDIRMAN NO 29 KEPANJEN MALANG');

	$sql = "SELECT KREDIT.NO_KREDIT, KREDIT.no_ref, KREDIT.Tgl_Kredit, KREDIT.smbdana,PREKREDIT.NAMA, PREKREDIT.PEKERJAAN, 
		PREKREDIT.ALAMAT, PREKREDIT.RTRW, PREKREDIT.DESA, PREKREDIT.CAMAT, PREKREDIT.KODYA, prekredit.usaha,nasabah.kodepos, nasabah.no_cif, 
		PREKREDIT.NOKTP, PREKREDIT.pasangan,prekredit.ps_nmkecil as ps_noktp,prekredit.ps_tmplahir,prekredit.ps_tgllahir,prekredit.ps_pekerjaan, 
		 prekredit.no_nsb, nasabah.tglktp,prekredit.kelamin,
		prekredit.tmplahir,prekredit.tgllahir,nasabah.notelp,nasabah.nohp,nasabah.namaibu,nasabah.ps_tmplahir,nasabah.ps_tgllahir,
		KREDIT.TT_NAMA, KREDIT.TT_KERJA, KREDIT.TT_ALAMAT, KREDIT.TT_NOKTP, KREDIT.PINJ_POKOK, KREDIT.BBT, kredit.perantara, 
		KREDIT.sistem, KREDIT.nbulan, KREDIT.tgl_mulai, KREDIT.tgl_akhir, KREDIT.PINJ_PRSBUNGA, KREDIT.goljamin, KREDIT.bagjamin,
		KREDIT.LAMA, KREDIT.angsur_pokok, KREDIT.angsur_bunga, KREDIT.prs_provisi, KREDIT.NOM_PROVISI, KREDIT.prs_adm, KREDIT.NOM_ADM, 
		 KREDIT.NOM_NOTARIS,KREDIT.NOM_meterai,kredit.sim_ang,kredit.advance, 
		KREDIT.jatuhtempo,KREDIT.KODE_PK,KREDIT.PENGGUNAAN,KREDIT.golongan,KREDIT.sektor,KREDIT.PINJ_NOMBUNGA,ao.namaao 
		FROM AKRD KREDIT 
		INNER JOIN aprekrd PREKREDIT ON KREDIT.NO_KREDIT = PREKREDIT.NO_KREDIT 
		 inner join ao on ao.kodeao = kredit.namaao 
		 inner join nasabah on nasabah.no_nsb = prekredit.no_nsb 
		where kredit.no_ref = '".$nk."' order by KREDIT.Tgl_Kredit";
	$result = pg_query($sql);
	$row = pg_fetch_array($result);
	$date="2012/11/26";
	$namahari = date('l', strtotime($date));
?>

<body>
<center>                                                                      
<h3>SURAT KUASA PENGAMBILAN KENDARAAN</h3>                        
=====================================</br>
</center> 
Yang bertanda tangan dibawah ini:</br>                      
 <table border=0>  
	<tr>
		<td width=30></td>
		<td width=80>N a m a</td>
		<td>:</td>
		<td><b><?php echo $row ['nama'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><b><?php echo $row ['pekerjaan'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><b><?php echo $row['alamat'].' '.$row['rtrw'].' '.$row['desa'].', '.$row['camat'].' - '.$row['kodya']; ?></b></td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>
</table>	  
Dengan  ini memberi kuasa penuh dan tidak dapat ditarik kembali serta tidak akan berakhir karena sebab-sebab apapun kepada :</br>  
 <table border=0>  
	<tr>
		<td></td>
		<td>N a m a</td>
		<td>:</td>
		<td><p style="font-weight:bold"><?php echo setbpr('NAMADIRUT');?></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><p style="font-weight:bold"><?php echo setbpr('PKKERJA1')?></p></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><p style="font-weight:bold"><?php echo setbpr('PKALAMAT1');?></p></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>

</table>	  
                                                                                
<center><h4>K H U S U S</h4></center>    
                                                                                
Untuk  dan  atas  nama  dan  demikian  mewakili  pemberi kuasa untuk pengambilan kendaraan  yang  telah  pemberi  kuasa  jaminkan secara fiducia kepada <b><?php echo $bpr_nama;?> <?php echo $bpr_alamat;?></b>,  sesuai  dengan  akta Pengakuan  Hutang  No.<b><?php echo $nk;?></b> tanggal <b><?php echo normdate($row['tgl_kredit'],0,',','.');?></b> data-data sebagai berikut :</br>
<b>1</b> unit kendaraan dengan identitas sebagai berikut :</br>
<table border=0>
	<tr>
		<td></td>
		<td>Merk/Type</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Tahun/Warna</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.Rangka</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.Mesin</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Nomor Polisi</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.BPKB</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Atas Nama</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
</table>                                                                                                         
                                                                                
Dari tangan siapa saja yang memegang kendaraan tersebut.Untuk itu segala akibat dan resiko yang timbul dari  diterbitkannya kuasa  ini secara penuh menjadi tanggung jawab Pemberi Kuasa.Demikian kuasa ini saya buat dengan hak untuk dapat dipindahkan kepada pihak lain(Substitutie)dan untuk dapat dipergunakan bilamana perlu (Wanprestasi).</br>
<table>
	<tr>
		<td></td>
		<td width=600 style="text-align:right"><?php echo setbpr('KOTABPR');?></td>
		<td style="text-align:left">,</td>
		<td style="text-align:left"><?php echo normdate($row['tgl_kredit'],0,',','.');?></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	
</table>
<table>
<tr>
		<td></td>
		<td width=150 style="text-align:left">PEMBERI KUASA</td>
		<td width=500 style="text-align:right">PENERIMA KUASA</td>
</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>            
<tr>
		<td></td>
		<td style="text-align:center"><?php echo $row ['nama'];?></td>
		<td style="text-align:right"><?php echo setbpr('NAMADIRUT');?></td>
</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<td></td>
</table>	
</body>