<?php
	//$dbname = 'ksp_wum5';
	//$svname = 'localhost';
	
	include "koneksi.php";
	include "fungsi.php";
	include 'terbilang.fnc.php';
	$bpr_nama = 'KSP WIRA USAHA MANDIRI';
	$bpr_alamat = 'Jl. Panglima Sudirman No. 29 Kepanjen-Malang';
	//$nk = trim($_GET[nk]);
	$nk = "WUM1211000027";
	//$setbpr=array('namapimp'=>'CINDY MARGARETH','jabatan'=>'PIMPINAN','alamat'=>'JL. PANGLIMA SUDIRMAN NO 29 KEPANJEN MALANG');

	$sql = "SELECT KREDIT.NO_KREDIT, KREDIT.no_ref, KREDIT.Tgl_Kredit, KREDIT.smbdana,PREKREDIT.NAMA, PREKREDIT.PEKERJAAN, 
		PREKREDIT.ALAMAT, PREKREDIT.RTRW, PREKREDIT.DESA, PREKREDIT.CAMAT, PREKREDIT.KODYA, prekredit.usaha,nasabah.kodepos, nasabah.no_cif, 
		PREKREDIT.NOKTP, PREKREDIT.pasangan,prekredit.ps_nmkecil as ps_noktp,prekredit.ps_tmplahir,prekredit.ps_tgllahir,prekredit.ps_pekerjaan, 
		 prekredit.no_nsb, nasabah.tglktp,prekredit.kelamin,
		prekredit.tmplahir,prekredit.tgllahir,nasabah.notelp,nasabah.nohp,nasabah.namaibu,nasabah.ps_tmplahir,nasabah.ps_tgllahir,
		KREDIT.TT_NAMA, KREDIT.TT_KERJA, KREDIT.TT_ALAMAT, KREDIT.TT_NOKTP, KREDIT.PINJ_POKOK, KREDIT.BBT, kredit.perantara, 
		KREDIT.sistem, KREDIT.nbulan, KREDIT.tgl_mulai, KREDIT.tgl_akhir, KREDIT.PINJ_PRSBUNGA, KREDIT.goljamin, KREDIT.bagjamin,
		KREDIT.LAMA, KREDIT.angsur_pokok, KREDIT.angsur_bunga, KREDIT.prs_provisi, KREDIT.NOM_PROVISI, KREDIT.prs_adm, KREDIT.NOM_ADM, 
		 KREDIT.NOM_NOTARIS,KREDIT.NOM_meterai,kredit.sim_ang,kredit.advance, 
		KREDIT.jatuhtempo,KREDIT.KODE_PK,KREDIT.PENGGUNAAN,KREDIT.golongan,KREDIT.sektor,KREDIT.PINJ_NOMBUNGA,ao.namaao 
		FROM AKRD KREDIT 
		INNER JOIN aprekrd PREKREDIT ON KREDIT.NO_KREDIT = PREKREDIT.NO_KREDIT 
		 inner join ao on ao.kodeao = kredit.namaao 
		 inner join nasabah on nasabah.no_nsb = prekredit.no_nsb 
		where kredit.no_ref = '".$nk."' order by KREDIT.Tgl_Kredit";
	$result = pg_query($sql);
	$row = pg_fetch_array($result);
	$date="2012/11/26";
	$namahari = date('l', strtotime($date));
?>

<body>
<center>
<h2>SURAT PERNYATAAN PEMINJAMAN<h2></center>                           
                                                                                
Yang bertanda tangan dibawah ini :</br>
<table border=0>  
	<tr>
		<td width=30></td>
		<td width=80>No Kredit</td>
		<td>:</td>
		<td><b><?php echo $nk;?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>N a m a</td>
		<td>:</td>
		<td><b><?php echo $row ['nama'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><b><?php echo $row ['pekerjaan'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><b><?php echo $row['alamat'].' '.$row['rtrw'].' '.$row['desa'].', '.$row['camat'].' - '.$row['kodya']; ?></b></td>
	</tr>
	<tr>
		<td colspan=4>Selanjutnya disebut PEMBERI KUASA </td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>
</table>	 
                                                                                
Menyatakan dengan sebenarnya dan berani mengangkat sumpah.Bahwa benar pada tanggal  <b><?php echo normdate($row['tgl_kredit'],0,',','.');?></b> bertempat di  <b><?php echo $bpr_nama;?> <?php echo $bpr_alamat;?> </b> Badan Hukum <b>No. <?php echo setbpr('PKIJIN');?></b> Saya  telah menerima kredit / pinjaman uang dari Pimpinan <b><?php echo $bpr_nama;?></b>,dengan jaminan sebagai salah satu syarat yang lazim untuk pinjaman sebagai berikut :</br>
                                                                                
<b>1 (Satu)</b> unit kendaraan roda 2 / roda 4 dengan identitas sebagai berikut :</br>
                                                                                
<table border=0>
	<tr>
		<td></td>
		<td>Merk/Type</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Tahun/Warna</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.Rangka</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.Mesin</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Nomor Polisi</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.BPKB</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Atas Nama</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
</table>                                                         
                                                                                
dimana  surat-surat  hak  milik,telah  diserahkan  pada <b><?php echo $bpr_nama;?></b>,selanjutnya kami menerangkan dengan  sesungguhnya,mulai hari <b><?php echo hari($namahari);?></b> tanggal <b><?php echo normdate($row['tgl_kredit'],0,',','.');?></b>  seterusnya  kami  memegang  kendaraan tersebut diatas dalam kedudukan sebagai PEMINJAM dengan syarat-syarat sebagai berikut :</br>

<table>
<tr>
<td>a.</td>
<td style="text-align:justify"> Kendaraan  bermotor  tersebut  akan dirawat/dipelihara dengan sebaik-baiknya selama  pinjaman  ini masih berlangsung dan dilarang pula kendaraan tersebut dirombak,dibongkar sedemikian rupa sehingga  bentuk  asal  dari  kendaraan tersebut tidak sesuai lagi dengan apa yang diuraikan didalam surat-surat hak milik  yang  telah  diserahkan pada Koperasi. Koperasi setiap waktu berhak untuk memeriksa kendaraan tersebut jika dipandang perlu.</td>
</tr>
<tr>
<td>b.</td>
<td style="text-align:justify"> Apabila  terjadi  sesuatu tabrakan / hilang atau kejadian-kejadian lain baik disengaja  maupun  tidak  yang  mengakibatkan  kendaraan  tersebut rusak hal ini sepenuhnya menjadi tanggung  jawab  peminjam  dan  kewajiban  mengangsur tetap berlaku  hingga  lunas,  sesuai  ketentuan Perjanjian Kredit Nomer <b><?php echo $nk;?></b> tertanggal <b><?php echo normdate($row['tgl_kredit'],0,',','.');?></b> yang telah disepakati dan ditandatangani bersama.</td>
</tr>
<tr>
<td>c.</td>                                                                                
<td style="text-align:justify"> Kendaraan  tersebut  tidak  pernah  disita atau menjadi sengketa dalam suatu perkara baik Perdata maupun Pidana pada Pengadilan Negeri setempat.</td>
</tr>
<tr>
<td>d.</td>
<td style="text-align:justify">Dalam   hal  benar  atau  kelalaian  oleh  peminjam  atau  untuk  memenuhi kewajibannya  menurut  ketentuan-ketentuan tersebut diatas, maka Koperasi diberi kuasa/hak yang  tidak dapat ditarik kembali baik oleh undang - undang manapun karena  apapun  juga untuk mengambil kendaraan tersebut dari peminjam atau orang lain yang memegangnya,untuk dijual dan diperhitungkan dengan seluruh pinjaman.</td>
</tr>
<tr>
<td>e.</td>
<td style="text-align:justify">Bahwa  untuk  menghindari  hal-hal yang mungkin akan berakibat merugikan Koperasi  maka  angsuran sisa kredit / pinjaman tersebut, saya setorkan langsung ke  kantor<b> <?php echo $bpr_nama;?> <?php echo $bpr_alamat;?> </b>untuk  mendapatkan bukti kwitansi yang sah, dan tidak akan saya titipkan melalui siapapun  juga  baik  itu  karyawan  dari<b> <?php echo $bpr_nama;?> <?php echo $bpr_alamat;?></b>,  jika  angsuran  sisa kredit/ pinjaman saya titipkan kepada orang lain maka segala resiko/akibat yang timbul adalah menjadi tanggung jawab saya. </td>                                                                                
Dengan  pernyataan  ini  saya  buat dengan sebenarnya atas kesadaran sendiri dan dengan-  penuh  rasa  tanggung  jawab, tanpa ada tekanan atau paksaan dari pihak manapun juga untuk dipergunakan sebagai bukti bilamana diperlukan.</br>
                                                                                
<table>
	<tr>
		<td></td>
		<td width=700 style="text-align:right"><?php echo setbpr('KOTABPR');?></td>
		<td style="text-align:left">,</td>
		<td style="text-align:left"><?php echo normdate($row['tgl_kredit']);?></td>
	</tr>
	<tr>
		<td></td>
		<td width=880 style="text-align:right">Yang menyatakan</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	
	
</table>
<table>
	<tr>
		<td></td>
		<td width=850 style="font-size:14;font-weight:bold;text-align:right"><?php echo $row['nama'];?></td>
		
	</tr>
	
	
</table>                                                                                
 </body>