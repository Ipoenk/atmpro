<?php
	session_start();
	$dbname = $_SESSION["dbname"];
	$svname = $_SESSION["svname"];
	echo '= Database : '.$_SESSION["dbname"];
	
	include "../koneksi.php";
	include "../fungsi.php";
	require_once '../terbilang.fnc.php';
	
	$nk = "12/RAN.00/0121483-002";
	
	$sql = "SELECT KREDIT.NO_KREDIT, KREDIT.no_ref, KREDIT.Tgl_Kredit, KREDIT.smbdana,PREKREDIT.NAMA, PREKREDIT.PEKERJAAN, 
		PREKREDIT.ALAMAT, PREKREDIT.RTRW, PREKREDIT.DESA, PREKREDIT.CAMAT, PREKREDIT.KODYA, prekredit.usaha,nasabah.kodepos, nasabah.no_cif, 
		PREKREDIT.NOKTP, PREKREDIT.pasangan,prekredit.ps_nmkecil as ps_noktp,prekredit.ps_tmplahir,prekredit.ps_tgllahir,prekredit.ps_pekerjaan, 
		 prekredit.no_nsb, nasabah.tglktp,prekredit.kelamin,
		prekredit.tmplahir,prekredit.tgllahir,nasabah.notelp,nasabah.nohp,nasabah.namaibu,nasabah.ps_tmplahir,nasabah.ps_tgllahir,
		KREDIT.TT_NAMA, KREDIT.TT_KERJA, KREDIT.TT_ALAMAT, KREDIT.TT_NOKTP, KREDIT.PINJ_POKOK, KREDIT.BBT, kredit.perantara, 
		KREDIT.sistem, KREDIT.nbulan, KREDIT.tgl_mulai, KREDIT.tgl_akhir, KREDIT.PINJ_PRSBUNGA, KREDIT.goljamin, KREDIT.bagjamin,
		KREDIT.LAMA, KREDIT.angsur_pokok, KREDIT.angsur_bunga, KREDIT.prs_provisi, KREDIT.NOM_PROVISI, KREDIT.prs_adm, KREDIT.NOM_ADM, 
		 KREDIT.NOM_NOTARIS,KREDIT.NOM_meterai,kredit.sim_ang,kredit.advance, 
		KREDIT.jatuhtempo,KREDIT.KODE_PK,KREDIT.PENGGUNAAN,KREDIT.golongan,KREDIT.sektor,KREDIT.PINJ_NOMBUNGA,ao.namaao 
		FROM AKRD KREDIT 
		INNER JOIN aprekrd PREKREDIT ON KREDIT.NO_KREDIT = PREKREDIT.NO_KREDIT 
		 inner join ao on ao.kodeao = kredit.namaao 
		 inner join nasabah on nasabah.no_nsb = prekredit.no_nsb 
		where kredit.no_kredit = '".$nk."' order by KREDIT.Tgl_Kredit";
	
	$result = pg_query($sql);
	$row = pg_fetch_array($result);
	
	$a=2
?>

<body style="width:800px;text-align:justify">
	<center>
		PT. BPR. ROGOJAMPI ARTHA NIAGA</br>
		Badan Hukum No.</br>

		<h3>PERJANJIAN  KREDIT</h3>
		No. : <?php echo $nk;?></br></br>
	</center>
	Yang bertanda tangan dibawah ini :</br>
	<table border=0>
		<tr>
			<td width=20>1.</td>
			<td width=200>N a m a</td>
			<td>:</td>
			<td><?php echo '<b>'.$row['nama'].'</b>'; ?></td>
		</tr>
		<tr>
			<td></td>
			<td>Pekerjaan</td>
			<td>:</td>
			<td><?php echo '<b>'.$row['pekerjaan'].'</b>'; ?></td>
		</tr>
		<tr>
			<td></td>
			<td>Alamat</td>
			<td>:</td>
			<td><?php echo '<b>'.trim($row['alamat']).' '.trim($row['rtrw']).' '.trim($row['desa']).', '.trim($row['camat']).' - '.trim($row['kodya']).'</b>'; ?></td>
		</tr>
		<tr>
			<td></td>
			<td>No.ANG/ANG KEL/AKL</td>
			<td>:</td>
			<td><?php echo '<b>'.$row['no_kredit'].'</b>'; ?></td>
		</tr>
		<tr>
			<td colspan=4>Selanjutnya disebut PIHAK KESATU</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		
		<tr>
			<td>2.</td>
			<td>N a m a</td>
			<td>:</td>
			<td><?php echo '<b>'.$row['pimpinan'].'</b>'; ?></td>
		</tr>
		<tr>
			<td></td>
			<td>Pekerjaan</td>
			<td>:</td>
			<td>Direktur Utama</td>
		</tr>
		<tr>
			<td></td>
			<td>Alamat</td>
			<td>:</td>
			<td>Dsn. Maron Rt.01/01 Genteng Kulon-Bwi BANYUWANGI</td>
		</tr>
		<tr>
			<td colspan=4>Dalam    kedudukan    tersebut   diatas bertindak untuk dan atas nama <b>PT. BPR. ROGOJAMPI  ARTHA  NIAGA</b> berkedudukan di Jl. Raya No. 200 Rogojampi - BANYUWANGI.</td>
		</tr>
		<tr>
			<td colspan=4>Demikian  berdasarkan  surat keputusan pengangkatan Pimpinan  PT. BPR. ROGOJAMPI ARTHA NIAGA nomor :   Tgl. .</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan=4>Selanjutnya disebut PIHAK KEDUA.</td>
		</tr>
	</table>
</br>
PIHAK KESATU mengakui benar-benar dan dengan sah telah menerima pinjaman uang tunai  sebesar  <b>Rp.  <?php echo number_format($row['pinj_pokok'],'0','.',',');?>  ( <?php echo terbilang($row['pinj_pokok'],0);?> rupiah ) </b>dari PIHAK KEDUA yang telah diterima oleh PIHAK KESATU pada hari ini dari PIHAK KEDUA di Kantor PIHAK KEDUA  Jl. Raya  No. 200 Rogojampi - Banyuwangi BANYUWANGI dan untuk penerimaan uang tersebut, akta ini dinyatakan  pula  sebagai  tanda penerimaan yang sah (Kuitansinya), dan PIHAK KESATU dengan ini menerima baik pinjamandari PIHAK KEDUA tersebut.</br></br>

Sehubungan  dengan  pinjaman / hutang tersebut maka PIHAK KESATU dan PIHAK KEDUA telah  bersepakat bahwa perjanjian kredit tersebut dilakukan dan diterima dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut :</br></br>

<center><h3>Pasal 1</h3></center>
Hutang  sebesar <b>Rp <?php echo number_format($row['pinj_pokok'],'0','.',',');?> ( <?php echo terbilang($row['pinj_pokok'],0);?> rupiah )</b> ditambah dengan bunga yang telah ditentukan  sebesar  <b><?php echo number_format($row['pinj_prsbunga'],'2','.',',');?>%</b>  per  bulan selama <b><?php echo number_format($row['lama'],'0','.',',');?> ( <?php echo terbilang($row['lama'],0);?> ) bulan</b>, menjadi sebesar  <b>Rp <?php echo number_format($row['pinj_pokok']+$row['bbt'],'0','.',',');?> ( <?php echo terbilang($row['pinj_pokok']+$row['bbt']);?> rupiah )</b> harus dilunasi  dalam jangka waktu <b><?php echo number_format($row['lama'],'0','.',',');?> ( <?php echo terbilang($row['lama'],0);?> ) bulan</b> dengan ketentuan angsuran dibayar  setiap  bulan  sebesar Rp. 352,777(Tiga ratus lima puluh dua ribu tujuh ratus  tujuh  puluh delapan rupiah) dibulatkan menjadi sebesar Rp. 353,000 (Tiga ratus lima puluh tiga ribu rupiah ). </br>

Angsuran  harus  dilakukan  paling  lambat  pada tanggal 10 (Sepuluh ) tiap-tiap bulan  dan  untuk  pertama  kalinya dimulai pada tanggal 10-11-2012 dan demikian seterusnya hingga berakhir paling lambat pada tanggal 10-04-2014.</br></br>
	

	

</body>
