<?php
	//$dbname = 'ksp_wum5';
	//$svname = 'localhost';
	
	include "koneksi.php";
	include "fungsi.php";
	include 'terbilang.fnc.php';
	$bpr_nama = 'KSP WIRA USAHA MANDIRI';
	$bpr_alamat = 'Jl. Panglima Sudirman No. 29 Kepanjen-Malang';
	//$nk = trim($_GET[nk]);
	$nk = "WUM1211000027";
	//$setbpr=array('namapimp'=>'CINDY MARGARETH','jabatan'=>'PIMPINAN','alamat'=>'JL. PANGLIMA SUDIRMAN NO 29 KEPANJEN MALANG');

	$sql = "SELECT KREDIT.NO_KREDIT, KREDIT.no_ref, KREDIT.Tgl_Kredit, KREDIT.smbdana,PREKREDIT.NAMA, PREKREDIT.PEKERJAAN, 
		PREKREDIT.ALAMAT, PREKREDIT.RTRW, PREKREDIT.DESA, PREKREDIT.CAMAT, PREKREDIT.KODYA, prekredit.usaha,nasabah.kodepos, nasabah.no_cif, 
		PREKREDIT.NOKTP, PREKREDIT.pasangan,prekredit.ps_nmkecil as ps_noktp,prekredit.ps_tmplahir,prekredit.ps_tgllahir,prekredit.ps_pekerjaan, 
		 prekredit.no_nsb, nasabah.tglktp,prekredit.kelamin,
		prekredit.tmplahir,prekredit.tgllahir,nasabah.notelp,nasabah.nohp,nasabah.namaibu,nasabah.ps_tmplahir,nasabah.ps_tgllahir,
		KREDIT.TT_NAMA, KREDIT.TT_KERJA, KREDIT.TT_ALAMAT, KREDIT.TT_NOKTP, KREDIT.PINJ_POKOK, KREDIT.BBT, kredit.perantara, 
		KREDIT.sistem, KREDIT.nbulan, KREDIT.tgl_mulai, KREDIT.tgl_akhir, KREDIT.PINJ_PRSBUNGA, KREDIT.goljamin, KREDIT.bagjamin,
		KREDIT.LAMA, KREDIT.angsur_pokok, KREDIT.angsur_bunga, KREDIT.prs_provisi, KREDIT.NOM_PROVISI, KREDIT.prs_adm, KREDIT.NOM_ADM, 
		 KREDIT.NOM_NOTARIS,KREDIT.NOM_meterai,kredit.sim_ang,kredit.advance, 
		KREDIT.jatuhtempo,KREDIT.KODE_PK,KREDIT.PENGGUNAAN,KREDIT.golongan,KREDIT.sektor,KREDIT.PINJ_NOMBUNGA,ao.namaao 
		FROM AKRD KREDIT 
		INNER JOIN aprekrd PREKREDIT ON KREDIT.NO_KREDIT = PREKREDIT.NO_KREDIT 
		 inner join ao on ao.kodeao = kredit.namaao 
		 inner join nasabah on nasabah.no_nsb = prekredit.no_nsb 
		where kredit.no_ref = '".$nk."' order by KREDIT.Tgl_Kredit";
	$result = pg_query($sql);
	$row = pg_fetch_array($result);
?>
<body>
<center>                                                                     
<h2>SURAT KUASA MENJUAL</h2>
================================================================================</br></center>                                                                                     
Yang bertanda tangan dibawah ini :</br>
<table border=0>  
	<tr>
		<td width=30></td>
		<td width=80>No Kredit</td>
		<td>:</td>
		<td><b><?php echo $nk;?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>N a m a</td>
		<td>:</td>
		<td><b><?php echo $row ['nama'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><b><?php echo $row ['pekerjaan'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><b><?php echo $row['alamat'].' '.$row['rtrw'].' '.$row['desa'].', '.$row['camat'].' - '.$row['kodya']; ?></b></td>
	</tr>
	<tr>
		<td colspan=4>Selanjutnya disebut PEMBERI KUASA </td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>	            
                                                                                     
Dengan ini memberikan KUASA kepada :</br>
<tr>
		<td></td>
		<td>N a m a</td>
		<td>:</td>
		<td><p style="font-weight:bold"><?php echo setbpr('NAMADIRUT');?></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><p style="font-weight:bold"><?php echo setbpr('PKKERJA1')?></p></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><p style="font-weight:bold"><?php echo setbpr('PKALAMAT1');?></p></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
    <tr>
		<td colspan=4>Selanjutnya disebut PENERIMA KUASA</td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>	            
</table>
<center><h3>K H U S U S</h3></center>
                                                                                     
Untuk menawarkan,menjual dan menerima hasil penjualan tersebut,untuk pembayaran seluruh Pinjaman(baik Angsuran Pokok, Bunga maupun penggantian kerugian)dengan Nomor Kredit : <b><?php echo $nk;?></b> kepada <b><?php echo $bpr_nama;?></b>,<b><?php echo $bpr_alamat;?></b> dengan data sebagai berikut :</br>
                                                                                     
<b>1 (Satu)</b> unit kendaraan roda 2 / roda 4 dengan identitas sebagai berikut :</br>
<table border=0>
	<tr>
		<td></td>
		<td>Merk/Type</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Tahun/Warna</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.Rangka</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.Mesin</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Nomor Polisi</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>No.BPKB</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Atas Nama</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
</table>                                                                                     
                                                                                     
Demikian  Surat  Kuasa  ini  dibuat  untuk  dipergunakan  sebagaimana mestinya dengan segala,akibat hukum yang timbul dikemudian hari sehubungan dengan pemberian Surat Kuasa ini sepenuhnya menjadi tanggung jawab Pemberi Kuasa.</br>
<table>
	<tr>
		<td></td>
		<td width=800 style="text-align:right"><?php echo setbpr('KOTABPR');?></td>
		<td style="text-align:left">,</td>
		<td style="text-align:left"><?php echo normdate($row['tgl_kredit']);?></td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
</table>
<table>
<tr>
		<td></td>
		<td width=150 style="text-align:left">PEMBERI KUASA</td>
		<td width=700 style="text-align:right">PENERIMA KUASA</td>
</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>            
	<tr>
		<td></td>
		<td <p style="font-size:14;font-weight:bold;text-align:center"><?php echo $row['nama'];?></p></td>
		<td <p style="font-size:14;font-weight:bold;text-align:right"><?php echo setbpr('NAMADIRUT');?></p></td>
		</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<td></td>
</table>	
</body>