<html>
	<head>
		<title></title>
	</head>
	<body style="width:700px;text-align:justify">
		<P align="center">
				KOPERASI SIMPAN PINJAM (KSP) {{ $kantor->namakantor }}
		</P>
		<P align="center">
			{{ $kantor->alamat }} - {{ $kantor->kota }}
		</P>
		<P align="center">
			=============================================================================
		</P>
		{{-- <p>
			&nbsp;
		</p> --}}
		<p align="center" style="margin-left:24.0pt;">
			PERJANJIAN KEDIT</p>
		<p align="center">
			{{-- <strong>No : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</strong></p> --}}
		<strong>Nomor Kredit : {{$datakredit->no_kredit}}</strong></p>
		<p align="center">
			&nbsp;</p>
		<p>
			Perjanjian ini dibuat dan ditandatangani pada hari ini, <strong>{{ namahari(now()) }}</strong>  tanggal <strong>{{ tanggal_local(now()) }}</strong>, antara:</p>
		{{-- <p>
			&nbsp;</p> --}}
		<ol>
			<li>
				KOPERASI SIMPAN PINJAM. {{ $kantor->namakantor }} atau disingkat KSP. {{ $kantor->namakantor }} yang beralamat di {{$kantor->alamat}}, {{$kantor->kota}}, dalam hal ini diwakili oleh&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</li>
		</ol>
		<table>
			<tr>
				<th width="50px"></th>	
				<th width="200px"></th>
				<th width="300px"></th>
			</tr>
			<tr>
				<td></td>
				<td>N a m a</td>
				<td>: {{ $kantor->dirut }}</td>
			</tr>
			<tr>
				<td></td>
				<td>Tempat / Tgl Lahir</td>
				<td>: {{ $datakredit->tmplahirnsb }} / {{ date('d-m-Y',strtotime($datakredit->tgllahirnsb)) }}</td>
			</tr>
			<tr>
				<td></td>
				<td>Alamat</td>
				<td>: {{ $kantor->alamat }}</td>
			</tr>
			<tr>
				<td></td>
				<td>Status Perkawinan</td>
				<td>: </td>
			</tr>
			<tr>
				<td></td>
				<td>No.KTP</td>
				<td>: </td>
			</tr>
		</table>
		<p style="margin-left:54.0pt;">
			-selanjutnya dalam Perjanjian ini disebut sebagai <strong>KREDITUR.</strong></p>

		<p style="margin-left:14.2pt;">
			-untuk melaksanakan tindakan hukum dalam Surat Perjanjian ini telah mendapat persetujuan dari istri/suami*) yang sah yang turut pula menandatangani surat ini, yaitu :</p>
			<table>
				<tr>
					<th width="50px"></th>	
					<th width="200px"></th>
					<th width="300px"></th>
				</tr>
				<tr>
					<td></td>
					<td>N a m a</td>
					<td>: {{ $datakredit->namansb }}</td>
				</tr>
				<tr>
					<td></td>
					<td>Tempat / Tgl Lahir</td>
					<td>: {{ $datakredit->tmplahirnsb }} / {{ date('d-m-Y',strtotime($datakredit->tgllahirnsb)) }}</td>
				</tr>
				<tr>
					<td></td>
					<td>Alamat</td>
					<td>: {{ $datakredit->alamatnsbktp }}</td>
				</tr>
				<tr>
					<td></td>
					<td>Status Perkawinan</td>
					<td>: {{ $datakredit->statusperkawinan }}</td>
				</tr>
				<tr>
					<td></td>
					<td>No.KTP</td>
					<td>: {{ $datakredit->niknsb }}</td>
				</tr>
			</table>
	
		<p>
			-selanjutnya dalam Perjanjian ini disebut sebagai <strong>DEBITUR.</strong></p>
		{{-- <p>
			&nbsp;</p> --}}
		<p>
			-KREDITUR dan DEBITUR,&nbsp; keduanya disebut juga Para Pihak dengan ini menerangkan terlebih dahulu :</p>
		<ul>
			<li>
				bahwa Debitur telah mengajukan permohonan kepada KREDITUR untuk mendapatkan fasilitas kredit Modal Kerja dan Kreditur telah setuju memberikan fasilitas kredit kepada Debitur berupa kredit Modal Kerja;</li>
		</ul>
		{{-- <p>
			&nbsp;</p> --}}
		<p>
			-Sehubungan dengan hal tersebut para pihak telah sepakat dan setuju mengadakan Perjanjian Kredit, selanjutnya disebut PERJANJIAN, dengan syarat dan ketentuan sebagai berikut :</p>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 1. JENIS dan BESARNYA KREDIT</strong></p>
		<ol>
			<li>
				KREDITUR telah setuju memberikan fasilitas kredit kepada DEBITUR untuk Modal Kerja berupa .......... <em>(sebutkan keperluan DEBITUR, misalnya :pembelian mobil, &nbsp;utk renovasi rumah, utk tambahan beli rumah di.....untuk modal usaha/kerja..... dsb).</em></li>
			<li>
			Fasilitas kredit yang diberikan KREDITUR kepada DEBITUR sebesar &nbsp;Rp. {{ number_format($datakredit->pinj_pokok)}} ({{ terbilang($datakredit->pinj_pokok) }} rupiah).</li>
		</ol>
		<p style="margin-left:13.5pt;">
			&nbsp;</p>
		<p align="center" style="margin-left:35.45pt;">
			<strong>PASAL </strong><strong>2</strong><strong>. BIAYA-BIAYA</strong></p>
		<ol>
			<li>
				Terhadap fasilitas kredit ini DEBITUR sepakat dan setuju untuk membayar biaya-biaya kepada KREDITUR, yaitu: &nbsp;</li>
		</ol>
		<table>
			<tr>
				<th width="50px"></th>	
				<th width="250px"></th>
				<th width="25px"></th>
				<th width="100px"></th>
			</tr>
			<tr>
				<td></td>
				<td>Biaya Provisi sebesar</td>
				<td>Rp.</td>
				<td align="right">{{ number_format($datakredit->nom_provisi)}}</td>
			</tr>	
			<tr>
				<td></td>
				<td>Biaya Administrasi sebesar</td>
				<td>Rp.</td>
				<td  align="right">{{ number_format($datakredit->nom_adm)}}</td>
			</tr>	
			<tr>
				<td></td>
				<td>Biaya Asuransi sebesar</td>
				<td>Rp.</td>
				<td align="right">{{ number_format($datakredit->assjiwa)}}</td>
			</tr>	
			<tr>
				<td></td>
				<td>Biaya Fiducia /APHT sebesar</td>
				<td>Rp.</td>
				<td align="right">{{ number_format($datakredit->notaris)}}</td>
			</tr>	
			<tr>
				<td></td>
				<td>Biaya lainnya</td>
				<td>Rp.</td>
				<td align="right">{{ number_format($datakredit->lainnya)}}</td>
			</tr>	
		</table>
		<ol>
			<li value="2">
				biaya-biaya tersebut di atas seluruhnya berjumlah Rp. {{ number_format($datakredit->nom_provisi+$datakredit->nom_adm+$datakredit->assjiwa+$datakredit->notaris+$datakredit->lainnya)}} ({{ terbilang($datakredit->nom_provisi+$datakredit->nom_adm+$datakredit->assjiwa+$datakredit->notaris+$datakredit->lainnya)}} rupiah ), dibayar DEBITUR dengan cara <em>(*dimasukkan pada pokok kredit / dibayar tunai kepada KREDITUR / dipotong saat pencairan&hellip;PILIH SALAH SATU)</em></li>
			<li value="3">
				Selain biaya-biaya tersebut di atas DEBITUR juga wajib membayar segala biaya yang mungkin timbul karena penagihan atas kelalaian DEBITUR termasuk tetapi tidak terbatas pada biaya berperkara, pengacara, biaya eksekusi Agunan, biaya pengurusan dokumen/surat pendukung untuk klaim asuransi karena kehilangan atau kerusakan Agunan dan biaya lainnya.</li>
		</ol>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 3. POKOK HUTANG, BUNGA </strong></p>
		<ol>
			<li>
			Pokok Hutang kredit seluruhnya sebesar Rp. {{number_format($datakredit->pinj_pokok)}},- ( {{ terbilang($datakredit->pinj_pokok) }} rupiah. )</li>
			<li>
			Terhadap pokok hutang tersebut dikenakan bunga sebesar Rp. {{ number_format($datakredit->bbt)}},- ( {{ terbilang($datakredit->bbt) }} rupiah. )</li>
		</ol>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 4. JANGKA WAKTU, PEMBAYARAN KEMBALI dan DENDA </strong></p>
		<ol>
			<li>
				Jangka waktu pegembalian fasilitas kredit berlangsung selama {{$datakredit->lama}} bulan.</li>
			<li>
				Pengembalian fasilitas kredit dilakukan DEBITUR dengan cara :
				<table>
					<tr>
						<th width="20px"></th>	
						<th width="200px"></th>
						<th width="300px"></th>
					</tr>
					<tr>
						<td></td>
						<td>Diangsur setiap bulan sebesar</td>
						<td>: Rp. {{number_format($datakredit->angsur_pokok+$datakredit->angsur_bunga)}} ({{ terbilang($datakredit->angsur_pokok+$datakredit->angsur_bunga) }})</td>
					</tr>
					<tr>
						<td></td>
						<td>Banyaknya angsuran</td>
						<td>: {{$datakredit->lama}} kali angsuran</td>
					</tr>
					<tr>
						<td></td>
						<td>Angsuran dibayar setiap tanggal</td>
						<td>: {{ date('d',strtotime($datakredit->tgl_kredit)) }} setiap bulan</td>
					</tr>
					<tr>
						<td></td>
						<td>Tanggal Angsuran Pertama</td>
						<td>: {{ date('d-m-Y',strtotime($datakredit->tgl_mulai)) }}</td>
					</tr>
					<tr>
						<td></td>
						<td>Tanggal Angsuran Terakhir</td>
						<td>: {{ date('d-m-Y',strtotime($datakredit->tgl_akhir)) }}</td>
					</tr>
				</table>
			</li>
			<li>
				Jika pada tanggal pembayaran angsuran sebagaimana ayat 2 Pasal ini, DEBITUR tidak dapat membayar tepat pada waktunya maka DEBITUR sepakat, setuju dan dengan suka rela untuk mengganti kerugian kepada KREDITUR sebesar 0,5% perhari dari pembayaran tertunggak.</li>
		</ol>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 5.&nbsp; PENGAKUAN BERHUTANG</strong></p>
		<p>
			Sehubungan dengan segala sesuatu yang diuraikan pada pasal-pasal dalam perjanjian ini maka DEBITUR, sekarang dan untuk dikemudian hari, mengakui secara sah telah berhutang pada KREDITUR sejumlah Hutang yang dari waktu ke waktu terhutang oleh DEBITUR kepada KREDITUR dan cukup dibuktikan dengan suatu pernyataan tertulis dari KREDITUR yang menyebutkan jumlah hutang yang didasarkan pada catatan-catatan pembukuan KREDITUR sendiri, pernyataan mana menjadi bukti yang sah dan mengikat kepada DEBITUR dan merupakan satu kesatuan yang tidak terpisahkan dari Perjanjian ini <strong>tanpa mengurangi hak </strong>DEBITUR untuk membuktikan sebaliknya, dan apabila ada catatan yang keliru, maka KREDITUR akan melakukan pembetulan.</p>
		<p style="margin-left:14.2pt;">
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 6. A G U N A N</strong></p>
		<ol>
			<li>
				Untuk menjamin lebih lanjut pembayaran kembali secara tertib dan sebagaimana mestinya dari utang yang harus dibayarkan oleh DEBITUR kepada KREDITUR berdasarkan Perjanjian ini maupun perubahan, tambahan, novasi dan / atau perjanjian yang akan dibuat kemudian hari, maka dengan ini DEBITUR menyerahkan jaminan kepada KREDITUR berupa :</li>
		</ol>
		<div id = "sertifikat" name = "sertifikat" hidden>
		<p style="margin-left:13.5pt;">
			&nbsp;</p>
		<ol style="list-style-type:lower-roman;">
			<li value="5">
				1. &hellip;. (&hellip;..) bidang tanah Sertifikat Hak &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;(&hellip;&hellip;&hellip;), yaitu :</li>
		</ol>
		<p style="margin-left:36.0pt;">
			&nbsp;</p>
		<ul>
			<li>
				Nomor &hellip;. / seluas &hellip;.M2, terletak di &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...</li>
		</ul>
		<p style="margin-left:54.0pt;">
			&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
		<p style="margin-left:54.0pt;">
			-nama Pemegang Hak :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
		<p style="margin-left:27.0pt;">
			&nbsp;</p>
		<p style="margin-left:54.0pt;">
			-demikian berikut segala sesuatu yang berada, ditanam dan didirikan di atas tanah tersebut yang karena sifat, peruntukannya dan menurut Undang-Undang termasuk barang tak bergerak yang keadaannya telah diketahui oleh KREDITUR;</p>
		<p style="margin-left:27.0pt;">
			&nbsp;</p>
		<p style="margin-left:27.0pt;">
			<em>atau</em></p>
		<p style="margin-left:27.0pt;">
			&nbsp;</p>
		</div>
		<div id = "kendaraan" name = "kendaraan">
		<ol style="list-style-type:lower-roman;">
			<!-- <li value=""> -->
				{{ count($agunan_kend) }} unit kendaraan bermotor roda....dengan identitas sebagai berikut :
				<!-- </li> -->
		</ol>
		<?php foreach ($agunan_kend as $agunan_kend): ?>	
		<table >
			<tr>
				<th width="50"></th>
				<th width="200px"></th>
				<th width="100px"></th>	
			</tr>
			<tr>
				<td></td>
				<td>Merk/Type </td>
				<td>:  <?php echo $agunan_kend->merktype; ?></td>
			</tr>
			<tr>	
				<td></td>
				<td>Tahun / Warna </td>
				<td>:  <?php echo $agunan_kend->tahun; ?> / <?php echo $agunan_kend->warna; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>Nomor Rangka </td>
				<td>:  <?php echo $agunan_kend->norangka; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>Nomor Mesin </td>
				<td>:  <?php echo $agunan_kend->nomesin; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>No. Polisi </td>
				<td>:  <?php echo $agunan_kend->nopolisi; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>No. BPKB </td>
				<td>:  <?php echo $agunan_kend->nobpkb; ?></td> 
			</tr>		
			<tr>
				<td>

				</td>
			</tr>	
		</table>		
		<?php endforeach; ?>

			<p style="margin-left:18.0pt;">
			satu dan lain hal sebagaimana tercantum dalam Buku Pemilik Kendaraan Bermotor (BPKB), dan apabila terjadi perubahan yang disebabkan Ketentuan dalam Pasal 64 dan 65 UU No.22 tahun 2009, tentang Registrasi dan Indentitas Kendaraan Bermotor, maka perubahan yang dimaksud kata demi kata dianggap sebagaimana telah termaktub dalam jaminan yang telah diserahkan maupun surat-surat, keterangan-keterangan, pernyataan-pernyataan dan perjanjian-perjanjian yang telah dan akan dibuat.</p>
		{{-- <p style="margin-left:18.0pt;">
			&nbsp;</p> --}}
		</div>	
		<ol>
			<li value="2">
				Pemberian jaminan tersebut diikat dalam suatu Akta Pembebanan Agunan sesuai dengan ketentuan hukum yang berlaku di Indonesia dan karenanya DEBITUR dan/atau Pemegang Hak mengikatkan diri untuk menandatangani seluruh surat-surat atau akta-akta yang berkaitan dengan pengikatan jaminan yang merupakan satu kesatuan yang tidak terpisahkan dan tidak terlepas dari perjanjian ini.</li>
		</ol>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 7. PELUNASAN LEBIH AWAL</strong></p>
		<ol>
			<li>
				DEBITUR dapat mempercepat pelunasan utang sebelum batas waktu yang telah ditetapkan dan untuk maksud tersebut DEBITUR sepakat dan setuju untuk memberitahukan kepada KREDITUR secara tertulis selambat-lambatnya 7 (tujuh) hari sebelum tanggal percepatan pelunasan.</li>
			<li>
				Untuk percepatan pelunasan angsuran sebagaimana dimaksud, DEBITUR sepakat dan setuju mengganti kerugian kepada KREDITUR sebesar 2% dari sisa pokok.</li>
		</ol>
		<p style="margin-left:35.45pt;">
			&nbsp;</p>
		<p align="center" >
			<strong>PASAL </strong><strong>8</strong><strong>. PERISTIWA CIDERA JANJI (WANPRERSTASI)</strong></p>

		<ol>
			<li>
				Peristiwa Cidera Janji timbul apabila telah terjadi salah satu atau lebih dari kejadian-kejadian sebagai berikut :</li>
		</ol>
		<ol style="list-style-type:lower-alpha;">
			<li>
				DEBITUR lalai dan tidak membayar angsuran dan/atau bunga, denda dan jumlah lainnya yang terhutang yang wajib dibayar DEBITUR kepada KREDITUR setelah lebih dari 5 hari dari waktu yang telah ditentukan menurut perjanjian ini;</li>
			<li>
				DEBITUR lalai atau tidak memenuhi syarat-syarat lain dalam perjanjian ini dan atau sesuatu perpanjangan, penambahan, perubahan atau penggantiannya serta terjadi pelanggaran terhadap syarat-syarat yang tertera dalam perjanjian jaminan yang dibuat berkenaan dengan perjanjian ini;</li>
			<li>
				Surat keterangan dan dokumen-dokumen yang diberikan DEBITUR berhubungan dengan perjanjian ini dan / atau tambahan daripadanya ternyata palsu atau tidak mengandung kebenaran baik seluruh maupun sebagian;</li>
			<li>
				Barang agunan atau barang yang dijaminkan itu hilang, dijual, disewakan, dipindahtangankan, dialihkan atau dijaminkan kepada pihak lain atau terjadi kerusakan atau kehancuran baik sebagian maupun seluruhnya. Dalam hal barang jaminan itu hilang atau musnah, maka KREDITUR dan DEBITUR sepakat untuk melepaskan ketentuan dalam Pasal 1444 dan 1445 Kitab Undang-undang Hukum Perdata.</li>
			<li>
				DEBITUR telah dinyatakan tidak mampu membayar utang-utangnya atau dinyatakan pailit, atau berada di bawah pengampuan, atau DEBITUR telah mengajukan permohonan penundaan pembayaran utang (surseance van betalling) atau sebab apapun DEBITUR dinyatakan tidak berhak lagi mengurus dan menguasai harta kekayaannya, baik seluruh maupun sebagian;</li>
			<li>
				Jika sebagian maupun seluruh harta kekayaan DEBITUR telah disita oleh Pengadilan maupun pihak lainnya;</li>
			<li>
				DEBITUR meninggal dunia, sedangkan ahli waris tidak bersedia meneruskan kewajiban DEBITUR terhadap KREDITUR menurut Perjanjian ini;</li>
		</ol>
		<ol>
			<li value="2">
				Dalam hal terjadi peristiwa cidera janji (Wanprestasi) sebagaimana ayat 1 pasal ini, maka seluruh hutang serta kewajiban DEBITUR berdasarkan perjanjian ini menjadi jatuh tempo, oleh sebab itu DEBITUR diwajibkan dan bersepakat untuk membayar seketika dan sekaligus lunas seluruh jumlah hutang yang tersisa berikut bunga dan biaya-biaya yang masih terhutang, yang harus dibayar DEBITUR pada waktu yang akan ditentukan oleh KREDITUR</li>
			<li value="3">
				Jika telah melebihi 30 hari dari lewatnya waktu yang ditentukan oleh KREDITUR, DEBITUR lalai dan tidak membayar lunas seluruh hutang yang jatuh tempo sebagaimana dimaksud ayat 2 pasal ini maka&nbsp; KREDITUR akan melaksanakan hak-haknya terhadap agunan dengan menggunakan Parate Eksekutorial.</li>
			<li value="4">
				Dalam hal pelaksanaan ayat 3 pasal ini, maka DEBITUR menyatakan telah sepakat dan / atau setuju secara sukarela untuk mengijinkan KREDITUR melakukan tindakan dimaksud guna menyelesaikan dan / atau melunasi hutang DEBITUR.</li>
			<li value="5">
				dalam hal berakhirnya perjanjian ini sebagaimana dimaksud pada ayat 2 pasal ini, para pihak sepakat untuk melepaskan ketentuan Pasal 1266 dan 1267 KUH Perdata.</li>
		</ol>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 9. PERUBAHAN</strong></p>
		<p>
			Perjanjian dapat diubah hanya dengan persetujuan tertulis dari DEBITUR dan KREDITUR.&nbsp; Perubahan tersebut akan diatur dalam suatu perjanjian yang&nbsp; merupakan bagian dan menjadi kesatuan yang tidak dapat terpisahkan dari Perjanjian ini, dan karenanya seluruh ketentuan dalam Perjanjian ini tetap berlaku pada perjanjian perubahan tersebut kecuali untuk hal- hal yang disepakati untuk diubah.</p>
		<p style="margin-left:18.0pt;">
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 10.&nbsp; KETENTUAN BERLAKUNYA KUASA-KUASA</strong></p>
		<p>
			Semua dan setiap kuasa yang diberikan oleh debitur kepada KREDITUR di dalam dan/atau berdasarkan perjanjian ini merupakan bagian-bagian yang terpenting dan tidak dapat dipisahkan dari perjanjian ini dan dengan demikian kuasa-kuasa tersebut tidak dapat ditarik kembali maupun dibatalkan oleh sebab-sebab yang tercantum dalam pasal 1813, 1814 dan 1816 Kitab Undang-undang Hukum Perdata.</p>
		<p>
			&nbsp;</p>
		<p align="center">
			<strong>PASAL 11.&nbsp; LAIN-LAIN</strong></p>
		<ol>
			<li>
				Segala sesuatu yang belum cukup di atur dalam Perjanjian ini akan diatur kemudian, baik dalam surat-surat, maupun dalam perjanjian-perjanjian tambahan dan merupakan satu kesatuan yang tidak dapat dipisahkan dengan Perjanjian ini.</li>
			<li>
				Judul-judul dalam setiap pasal Perjanjian ini hanya untuk memudahkan membaca Perjanjian dan tidak dapat dianggap sebagai bagian dari Perjanjian serta tidak memberikan penafsiran apapun atas isi Perjanjian.</li>
		</ol>
		<p align="center">
			&nbsp;</p>
		<p>
			<strong>PASAL 12. PENUTUP </strong></p>
		<ol>
			<li>
				Perjanjian ini telah disesuaikan dengan ketentuan Peraturan Perundang-undangan termasuk ketentuan Peraturan Otoritas Jasa Keuangan.</li>
			<li>
				Jika terjadi perselisihan atas penafsiran dan / atau pelaksanaan Perjanjian ini akan diselesaikan dengan cara musyawarah untuk mencapai mufakat.</li>
			<li>
				Dalam hal musyawarah tidak tercapai mufakat maka kedua belah pihak sepakat untuk memilih tempat kedudukan hukum yang tetap dan tidak berubah di kantor Pengadilan Negeri Malang dengan tidak mengurangi hak dari KREDITUR untuk memohon pelaksanaan eksekusi atau mengajukan tuntutan hukum terhadap DEBITUR berdasarkan perjanjian ini dihadapan Pengadilan Negeri lainnya dimanapun dalam wilayah Republik Indenesia.</li>
		</ol>
		<p>
			&nbsp;</p>
		<p>
			Demikian perjanjian ini di buat dan ditandatangani di &hellip;&hellip;&hellip;&hellip;&hellip;.. oleh para pihak, pada hari dan tanggal sebagaimana tersebut diatas oleh Para Pihak.</p>
		<p>
			&nbsp;</p>

<table>
	<tr>
		<th width="350"></th>
		<th width="350"></th>
	</tr>
	<tr>
		<td align="center">KREDITUR</td>
		<td align="center">DEBITUR</td>
	</tr>
	<tr><td>&nbsp;</td> </tr>
	<tr><td>&nbsp;</td> </tr>
	<tr><td>&nbsp;</td> </tr>
	<tr><td>&nbsp;</td> </tr>
	<tr>
		<td align="center">{{$kantor->dirut}}</td>
		<td align="center">{{ $datakredit->namansb }}</td>
	</tr>
</table>
	</body>
</html>
