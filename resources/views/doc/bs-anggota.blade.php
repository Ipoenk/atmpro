<?php
	//$dbname = 'ksp_wum5';
	//$svname = 'localhost';
	
	include "koneksi.php";
	include "fungsi.php";
	include 'terbilang.fnc.php';
	$bpr_nama = 'KSP WIRA USAHA MANDIRI';
	$bpr_alamat = 'Jl. Panglima Sudirman No. 29 Kepanjen-Malang';
	//$nk = trim($_GET[nk]);
	$nk = "WUM1211000027";
	//$setbpr=array('namapimp'=>'CINDY MARGARETH','jabatan'=>'PIMPINAN','alamat'=>'JL. PANGLIMA SUDIRMAN NO 29 KEPANJEN MALANG');

	$sql = "SELECT KREDIT.NO_KREDIT, KREDIT.no_ref, KREDIT.Tgl_Kredit, KREDIT.smbdana,PREKREDIT.NAMA, PREKREDIT.PEKERJAAN, 
		PREKREDIT.ALAMAT, PREKREDIT.RTRW, PREKREDIT.DESA, PREKREDIT.CAMAT, PREKREDIT.KODYA, prekredit.usaha,nasabah.kodepos, nasabah.no_cif, 
		PREKREDIT.NOKTP, PREKREDIT.pasangan,prekredit.ps_nmkecil as ps_noktp,prekredit.ps_tmplahir,prekredit.ps_tgllahir,prekredit.ps_pekerjaan, 
		 prekredit.no_nsb, nasabah.tglktp,prekredit.kelamin,
		prekredit.tmplahir,prekredit.tgllahir,nasabah.notelp,nasabah.nohp,nasabah.namaibu,nasabah.ps_tmplahir,nasabah.ps_tgllahir,
		KREDIT.TT_NAMA, KREDIT.TT_KERJA, KREDIT.TT_ALAMAT, KREDIT.TT_NOKTP, KREDIT.PINJ_POKOK, KREDIT.BBT, kredit.perantara, 
		KREDIT.sistem, KREDIT.nbulan, KREDIT.tgl_mulai, KREDIT.tgl_akhir, KREDIT.PINJ_PRSBUNGA, KREDIT.goljamin, KREDIT.bagjamin,
		KREDIT.LAMA, KREDIT.angsur_pokok, KREDIT.angsur_bunga, KREDIT.prs_provisi, KREDIT.NOM_PROVISI, KREDIT.prs_adm, KREDIT.NOM_ADM, 
		 KREDIT.NOM_NOTARIS,KREDIT.NOM_meterai,kredit.sim_ang,kredit.advance, 
		KREDIT.jatuhtempo,KREDIT.KODE_PK,KREDIT.PENGGUNAAN,KREDIT.golongan,KREDIT.sektor,KREDIT.PINJ_NOMBUNGA,ao.namaao 
		FROM AKRD KREDIT 
		INNER JOIN aprekrd PREKREDIT ON KREDIT.NO_KREDIT = PREKREDIT.NO_KREDIT 
		 inner join ao on ao.kodeao = kredit.namaao 
		 inner join nasabah on nasabah.no_nsb = prekredit.no_nsb 
		where kredit.no_ref = '".$nk."' order by KREDIT.Tgl_Kredit";
	$result = pg_query($sql);
	$row = pg_fetch_array($result);
	$date="2012/11/26";
	$namahari = date('l', strtotime($date));
?>
<body>
<table>
	<tr>
		<td></td>
		<td width=700 style="text-align:right">Kepada Yth : Bapak Ketua Kelompok</td>
		<td></td>
	</tr>
</table>
<table>	
	<tr>
		<td></td>
		<td width=180 style="text-align:left">Hal : pernyataan/permohonan</td>
		<td width=500 style="text-align:right"><?php echo $bpr_nama;?></td>
	</tr>
</table>
<table>	
	<tr>
		<td></td>
		<td width=180 style="text-align:left">pengunduran diri dari anggota</td>
		<td width=525 style="text-align:right"><?php echo $bpr_alamat;?></td>
		
	</tr>
	<tr>
		<td></td>
		<td width=180 style="text-align:left">Kelompok Koperasi</td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>
</table>	 
                                                                                          
Yang bertanda tangan dibawah ini :</br>
 <table border=0>  
	<tr>
		<td width=30></td>
		<td width=80>N a m a</td>
		<td>:</td>
		<td><b><?php echo $row ['nama'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><b><?php echo $row ['pekerjaan'];?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><b><?php echo $row['alamat'].' '.$row['rtrw'].' '.$row['desa'].', '.$row['camat'].' - '.$row['kodya']; ?></b></td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>
</table>	  
                                                                                          
Dengan  ini  saya  mengajukan  permohonan  pengunduran  diri  dari <b><?php echo $bpr_nama;?> <?php echo setbpr('PROP');?></br>
                                                                                          
Demikian  surat  pernyataan  ini saya  buat dengan sebenarnya tanpa ada unsur paksaan dari phak manapun juga serta dapat dijadikan bukti bilamana diperlukan.</br>
<table>
	<tr>
		<td></td>
		<td width=300 style="text-align:right">........</td>
		<td style="text-align:left">,</td>
		<td style="text-align:left">.........</td>
	</tr>
	<tr>
		<td></td>
		<td width=880 style="text-align:right">Yang mengajukan permohonan</td>
	</tr>
	<tr>
		<td></td>
		<td width=880 style="text-align:right">pengunduran diri</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
</table>
<table>
	<tr>
		<td></td>
		<td width=850 style="font-size:14;font-weight:bold;text-align:right"><?php echo $row['nama'];?></td>
	</tr>
</table>                                                                                                                                      <table>
	<tr>
		<td></td>
		<td width=700 style="text-align:right">Kepada Yth : Bapak Ketua Kelompok</td>
		<td></td>
	</tr>
</table>
<table>	
	<tr>
		<td></td>
		<td width=180 style="text-align:left">Hal : Pengajuan untuk</td>
		<td width=500 style="text-align:right"><?php echo $bpr_nama;?></td>
	</tr>
</table>
<table>	
	<tr>
		<td></td>
		<td width=180 style="text-align:left">menjadi Anggota</td>
		<td width=525 style="text-align:right"><?php echo $bpr_alamat;?></td>
		
	</tr>
	<tr>
		<td></td>
		<td width=180 style="text-align:left">Kelompok Koperasi</td>
		<td width=180 style="text-align:right">KEPANJEN MALANG</td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>
</table>


	 
<table border=0>  
	<tr>
		<td width=30></td>
		<td width=80>N a m a</td>
		<td>:</td>
		<td><b>MARTIN KIDING</b></td>
	</tr>
	<tr>
		<td></td>
		<td>Pekerjaan </td>
		<td>:</td>
		<td><b>SWASTA</b></td>
	</tr>
	<tr>
		<td></td>
		<td>Alamat  </td>
		<td>:</td>
		<td><b>CURUNG BARAT 04/03 CURUNGREJO KEPANJEN Malang, Kab.</b></td>
	</tr>
	<tr>
		<td>&nbsp; </td>
	</tr>
</table>	      
Mengajukan Permohonan untuk menjadi anggota,dan saya sanggup mentaati persyaratan-persyaratan yang telah ditetapkan.</br>
                                                                                          
Demikian  permohonan  pengajuan saya untuk menjadi anggota dan atas terkabulnya permohonan saya ini, saya sampaikan terima kasih.</br>
 <table>
	<tr>
		<td></td>
		<td width=300 style="text-align:right">........</td>
		<td style="text-align:left">,</td>
		<td style="text-align:left">.........</td>
	</tr>
	<tr>
		<td></td>
		<td width=880 style="text-align:right">PEMOHON</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
</table>
<table>
	<tr>
		<td></td>
		<td width=880 style="text-align:right">(      MARTIN KIDING       )</td>
	</tr>                                                                                         
</table>                                                                                          
                                                               
</body>