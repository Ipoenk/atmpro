<?php
include 'fungsi_pk.php'	
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
<link rel=File-List href="fidusia_files/filelist.xml">
<title>Fidusia</title>
	<style>	
body{width:690px; font: 12px "Arial", sans-serif;}
td,th{
	border:solid 0px #000;
	color:#000;
	margin:1px;
	padding:2px;
	border-collapse: collapse;
	border-spacing: 0;
	font-size:12.5px;
}
p{
	font-size:10.5px;
}			
</style>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>marketing </o:Author>
  <o:Template>Normal</o:Template>
  <o:LastAuthor>Sapari Cak</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2014-09-24T02:55:00Z</o:Created>
  <o:LastSaved>2014-09-24T02:55:00Z</o:LastSaved>
  <o:Pages>2</o:Pages>
  <o:Words>410</o:Words>
  <o:Characters>2343</o:Characters>
  <o:Company>AR</o:Company>
  <o:Lines>19</o:Lines>
  <o:Paragraphs>5</o:Paragraphs>
  <o:CharactersWithSpaces>2748</o:CharactersWithSpaces>
  <o:Version>12.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<link rel=themeData href="fidusia_files/themedata.thmx">
<link rel=colorSchemeMapping href="fidusia_files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:DefaultTableStyle Number="0">Normal</w:DefaultTableStyle>
  <w:DrawingGridHorizontalSpacing>0 pt</w:DrawingGridHorizontalSpacing>
  <w:DrawingGridVerticalSpacing>0 pt</w:DrawingGridVerticalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
  <w:DisplayVerticalDrawingGridEvery>0</w:DisplayVerticalDrawingGridEvery>
  <w:UseMarginsForDrawingGridOrigin/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:DrawingGridHorizontalOrigin>0 pt</w:DrawingGridHorizontalOrigin>
  <w:DrawingGridVerticalOrigin>0 pt</w:DrawingGridVerticalOrigin>
  <w:Compatibility>
   <w:SpaceForUL/>
   <w:BalanceSingleByteDoubleByteWidth/>
   <w:DoNotLeaveBackslashAlone/>
   <w:ULTrailSpace/>
   <w:AdjustLineHeightInTable/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:UseWord2002TableStyleRules/>
   <w:DontUseIndentAsNumberingTabStop/>
   <w:FELineBreak11/>
   <w:WW11IndentRules/>
   <w:DontAutofitConstrainedTables/>
   <w:AutofitLikeWW11/>
   <w:HangulWidthLikeWW11/>
   <w:UseNormalStyleForList/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="--"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="0" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611985 1107304683 0 0 159 0;}
@font-face
	{font-family:"Liberation Serif";
	mso-font-alt:"MS Mincho";
	mso-font-charset:128;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"DejaVu Sans";
	mso-font-alt:"MS Mincho";
	mso-font-charset:128;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:OpenSymbol;
	mso-font-alt:"Arial Unicode MS";
	mso-font-charset:128;
	mso-generic-font-family:auto;
	mso-font-pitch:auto;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"Liberation Sans";
	mso-font-alt:Arial;
	mso-font-charset:128;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"\@DejaVu Sans";
	mso-font-charset:128;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"\@Liberation Serif";
	mso-font-charset:128;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"\@Liberation Sans";
	mso-font-charset:128;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"\@OpenSymbol";
	mso-font-charset:128;
	mso-generic-font-family:auto;
	mso-font-pitch:auto;
	mso-font-signature:0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:none;
	mso-hyphenate:none;
	font-size:12.0pt;
	font-family:"Liberation Serif","serif";
	mso-fareast-font-family:"DejaVu Sans";
	mso-hansi-font-family:"Liberation Serif";
	mso-bidi-font-family:"DejaVu Sans";
	mso-font-kerning:.5pt;
	mso-fareast-language:HI;
	mso-bidi-language:HI;}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	mso-pagination:no-line-numbers;
	mso-hyphenate:none;
	font-size:12.0pt;
	font-family:"Liberation Serif","serif";
	mso-fareast-font-family:"DejaVu Sans";
	mso-hansi-font-family:"Liberation Serif";
	mso-bidi-font-family:"DejaVu Sans";
	mso-font-kerning:.5pt;
	mso-fareast-language:HI;
	mso-bidi-language:HI;
	font-style:italic;}
p.MsoList, li.MsoList, div.MsoList
	{mso-style-unhide:no;
	mso-style-parent:"Body Text";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	mso-pagination:none;
	mso-hyphenate:none;
	font-size:12.0pt;
	font-family:"Liberation Serif","serif";
	mso-fareast-font-family:"DejaVu Sans";
	mso-hansi-font-family:"Liberation Serif";
	mso-bidi-font-family:"DejaVu Sans";
	mso-font-kerning:.5pt;
	mso-fareast-language:HI;
	mso-bidi-language:HI;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-unhide:no;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	mso-pagination:none;
	mso-hyphenate:none;
	font-size:12.0pt;
	font-family:"Liberation Serif","serif";
	mso-fareast-font-family:"DejaVu Sans";
	mso-hansi-font-family:"Liberation Serif";
	mso-bidi-font-family:"DejaVu Sans";
	mso-font-kerning:.5pt;
	mso-fareast-language:HI;
	mso-bidi-language:HI;}
span.NumberingSymbols
	{mso-style-name:"Numbering Symbols";
	mso-style-unhide:no;
	mso-style-parent:"";}
span.Bullets
	{mso-style-name:Bullets;
	mso-style-unhide:no;
	mso-style-parent:"";
	font-family:OpenSymbol;
	mso-ascii-font-family:OpenSymbol;
	mso-fareast-font-family:OpenSymbol;
	mso-hansi-font-family:OpenSymbol;
	mso-bidi-font-family:OpenSymbol;}
p.Heading, li.Heading, div.Heading
	{mso-style-name:Heading;
	mso-style-unhide:no;
	mso-style-next:"Body Text";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	mso-pagination:none;
	page-break-after:avoid;
	mso-hyphenate:none;
	font-size:14.0pt;
	font-family:"Liberation Sans","sans-serif";
	mso-fareast-font-family:"DejaVu Sans";
	mso-hansi-font-family:"Liberation Sans";
	mso-bidi-font-family:"DejaVu Sans";
	mso-font-kerning:.5pt;
	mso-fareast-language:HI;
	mso-bidi-language:HI;}
p.Index, li.Index, div.Index
	{mso-style-name:Index;
	mso-style-unhide:no;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:no-line-numbers;
	mso-hyphenate:none;
	font-size:12.0pt;
	font-family:"Liberation Serif","serif";
	mso-fareast-font-family:"DejaVu Sans";
	mso-hansi-font-family:"Liberation Serif";
	mso-bidi-font-family:"DejaVu Sans";
	mso-font-kerning:.5pt;
	mso-fareast-language:HI;
	mso-bidi-language:HI;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
@page Section1
	{size:612.0pt 792.0pt;
	margin:2.0cm 2.0cm 2.0cm 2.0cm;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 @list l0
	{mso-list-id:1;
	mso-list-template-ids:1;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level5
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:126.0pt;
	mso-level-number-position:left;
	margin-left:126.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	margin-left:144.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level8
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:162.0pt;
	mso-level-number-position:left;
	margin-left:162.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l0:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	margin-left:180.0pt;
	text-indent:-18.0pt;
	mso-ascii-font-family:Symbol;
	mso-hansi-font-family:Symbol;
	mso-bidi-font-family:OpenSymbol;}
@list l1
	{mso-list-id:2;
	mso-list-template-ids:2;}
@list l1:level1
	{mso-level-tab-stop:18.7pt;
	mso-level-number-position:left;
	margin-left:36.75pt;
	text-indent:-18.0pt;}
@list l1:level2
	{mso-level-tab-stop:54.75pt;
	mso-level-number-position:left;
	margin-left:54.75pt;
	text-indent:-18.0pt;}
@list l1:level3
	{mso-level-tab-stop:72.75pt;
	mso-level-number-position:left;
	margin-left:72.75pt;
	text-indent:-18.0pt;}
@list l1:level4
	{mso-level-tab-stop:90.75pt;
	mso-level-number-position:left;
	margin-left:90.75pt;
	text-indent:-18.0pt;}
@list l1:level5
	{mso-level-tab-stop:108.75pt;
	mso-level-number-position:left;
	margin-left:108.75pt;
	text-indent:-18.0pt;}
@list l1:level6
	{mso-level-tab-stop:126.75pt;
	mso-level-number-position:left;
	margin-left:126.75pt;
	text-indent:-18.0pt;}
@list l1:level7
	{mso-level-tab-stop:144.75pt;
	mso-level-number-position:left;
	margin-left:144.75pt;
	text-indent:-18.0pt;}
@list l1:level8
	{mso-level-tab-stop:162.75pt;
	mso-level-number-position:left;
	margin-left:162.75pt;
	text-indent:-18.0pt;}
@list l1:level9
	{mso-level-tab-stop:180.75pt;
	mso-level-number-position:left;
	margin-left:180.75pt;
	text-indent:-18.0pt;}
@list l2
	{mso-list-id:3;
	mso-list-template-ids:3;}
@list l2:level1
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:21.6pt;
	mso-level-number-position:left;
	margin-left:21.6pt;
	text-indent:-21.6pt;}
@list l2:level2
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:28.8pt;
	mso-level-number-position:left;
	margin-left:28.8pt;
	text-indent:-28.8pt;}
@list l2:level3
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l2:level4
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:43.2pt;
	mso-level-number-position:left;
	margin-left:43.2pt;
	text-indent:-43.2pt;}
@list l2:level5
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:50.4pt;
	mso-level-number-position:left;
	margin-left:50.4pt;
	text-indent:-50.4pt;}
@list l2:level6
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:57.6pt;
	mso-level-number-position:left;
	margin-left:57.6pt;
	text-indent:-57.6pt;}
@list l2:level7
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:64.8pt;
	mso-level-number-position:left;
	margin-left:64.8pt;
	text-indent:-64.8pt;}
@list l2:level8
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l2:level9
	{mso-level-number-format:none;
	mso-level-suffix:none;
	mso-level-text:"";
	mso-level-tab-stop:79.2pt;
	mso-level-number-position:left;
	margin-left:79.2pt;
	text-indent:-79.2pt;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2050"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US style='tab-interval:35.45pt;line-break:strict'>

<div class=Section1>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Times New Roman","serif"'>SURAT KUASA MEMBEBANKAN JAMINAN
SECARA FIDUCIA<o:p></o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'>Yang
<span class=SpellE>bertanda</span> <span class=SpellE>tangan</span> <span
class=SpellE>di</span> <span class=SpellE>bawah</span> <span class=SpellE>ini</span>
:<o:p></o:p></span></p>
		<table border=0>
			<tr>
				<td width=30>1.</td>
				<td width=80>N a m a</td>
				<td>:</td>
				<td><b><?php echo $row['nama']; ?></b></td>
			</tr>
			<!--<tr>
				<td></td>
				<td>Pekerjaan</td>
				<td>:</td>
				<td><b><?php echo trim($row['usaha']); ?></b></td>
			</tr>-->
			<tr>
				<td></td>
				<td valign=top>Alamat</td>
				<td valign=top>:</td>
				<td valign=top><b><?php echo trim($row['alamat']).', RT/RW '.$row['rtrw'].', DS. '.trim($row['desa']).', KEC. '.trim($row['camat']).' - '.$row['kodya']; ?></b></td>
			</tr>

			
			<tr>
				<td>2.</td>
				<td>N a m a</td>
				<td>:</td>
				<td><b><?php echo $row['ps_nama']; ?></b></td>
			</tr>
			<!--<tr>
				<td></td>
				<td>Pekerjaan</td>
				<td>:</td>
				<td></td>
			</tr>-->
			<tr>
				<td></td>
				<td valign=top>Alamat</td>
				<td valign=top>:</td>
				<td valign=top><b><?php echo trim($row['ps_alamat']).', RT/RW '.$row['rtrw'].', DS. '.trim($row['desa']).', KEC. '.trim($row['camat']).' - '.$row['kodya']; ?></b></td>
			</tr>
		</table>
<!--
<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'>1.
<span class=SpellE>Nama</span><span style='mso-tab-count:3'>                                  </span>:<?php echo $row[nama];?></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span><span class=SpellE>Tempat-tgl</span> <span
class=SpellE>lahir</span><span style='mso-tab-count:2'>                  </span>:<?php echo $row[tmplahir].','.dmy($row['tgllahir']); ?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span><span class=SpellE>Alamat</span><span
style='mso-tab-count:3'>                                </span>:<?php echo $row[alamat].','.$row[rtrw].','.$row[desa].','.$row[camat].','.$row[kodya];?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify; display:none;'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span>Status<span style='mso-tab-count:3'>                                 </span>:<?php echo $row[status];?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span>No. KTP<span style='mso-tab-count:3'>                             </span>:<?php echo $row[noktp];?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'>2.
<span class=SpellE>Nama</span><span style='mso-tab-count:3'>                                  </span>:<?php echo $row[ps_nama];?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span><span class=SpellE>Tempat-tgl</span> <span
class=SpellE>lahir</span><span style='mso-tab-count:2'>                  </span>:<?php echo $row[ps_tmplahir].','.dmy($row[ps_tgllahir]);?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span><span class=SpellE>Alamat</span><span
style='mso-tab-count:3'>                                </span>:<?php echo $row[ps_alamat].','.$row[ps_rtrw].','.$row[ps_desa].','.$row[ps_camat].','.$row[ps_kodya];?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;display:none; ' ><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span>Status<span style='mso-tab-count:3'>                                 </span>:<?php echo $row[status];?><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><span
style='mso-spacerun:yes'>    </span>No. KTP<span style='mso-tab-count:3'>                             </span>:<?php echo $row[ps_noktp];?><o:p></o:p></span></p>
-->
<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-18.0pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span class=SpellE><span style='font-family:
"Times New Roman","serif"'>keduanya</span></span><span style='font-family:"Times New Roman","serif"'>
<span class=SpellE>untuk</span> <span class=SpellE>selanjutnya</span> <span
class=SpellE>disebut</span> PEMBERI KUASA<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'>-<span
class=SpellE>Dengan</span> <span class=SpellE>ini</span> <span class=SpellE>memberi</span>
<span class=SpellE>kuasa</span> <span class=SpellE>dengan</span> <span
class=SpellE>hak</span> <span class=SpellE>subtitusi</span> <span class=SpellE>kepada</span>
:<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:48.75pt;text-align:justify;text-indent:
-11.25pt'><span style='font-family:"Times New Roman","serif"'>- <span
class=SpellE>Direktur</span> KOPERASI SIMPANPINJAM. MITRA USAHA, <span
class=SpellE>disingkat</span> KSP. MITRA USAHA, <span class=SpellE>beralamat</span>
<span class=SpellE>di</span> Jl. R. <span class=SpellE>Tumenggung</span> <span
class=SpellE>Suryo</span> No. 34 Malang;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:39.0pt;text-align:justify'><span
style='font-family:"Times New Roman","serif"'>- <span class=SpellE>Selanjutnya</span>
<span class=SpellE>disebut</span> PENERIMA KUASA<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><span
style='font-family:"Times New Roman","serif"'>-------------------------------------------------
KHUSUS -------------------------------------------------<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'>-<span
class=SpellE>untuk</span> <span class=SpellE>dan</span> <span class=SpellE>atas</span>
<span class=SpellE>nama</span> <span class=SpellE>serta</span> <span
class=SpellE>mewakili</span> <span class=SpellE>Pemberi</span> <span
class=SpellE>Kuasa</span> <span class=SpellE>dalam</span> <span class=SpellE>hal</span>
:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;mso-list:l1 level1 lfo2;tab-stops:list 18.7pt'><![if !supportLists]><span
style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span class=SpellE><span style='font-family:
"Times New Roman","serif"'>Menghadap</span></span><span style='font-family:
"Times New Roman","serif"'> <span class=SpellE>kepada</span> <span
class=SpellE>Notaris</span> <span class=SpellE>dan</span> <span class=SpellE>menandatangani</span>
<span class=SpellE>Akta</span> <span class=SpellE>Jaminan</span> <span
class=SpellE>Fiducia</span> <span class=SpellE>atas</span> <span class=SpellE>nama</span>
<span class=SpellE>Pemberi</span> <span class=SpellE>Kuasa</span> <span
class=SpellE>untuk</span><span style='mso-spacerun:yes'> </span>(1) unit <span class=SpellE>Kendaraan</span>
<span class=SpellE>Bermotor</span> <span class=SpellE>dengan</span> data-data <span
class=SpellE>sebagai</span> <span class=SpellE>berikut</span> :<o:p></o:p></span></p>
<table border=0 style="margin-left:32px">
	<?php
		echo '<tr>
				<td></td>
				<td>Merk/Type</td>
				<td>:</td>
				<td>'.trim($row[merktype]).'</td>
			</tr>
			<tr>
				<td></td>
				<td>Tahun/Warna</td>
				<td>:</td>
				<td>'.$row[tahun].'/'.$row[warna].'</td>
			</tr>
			<tr>
				<td></td>
				<td>No.Rangka</td>
				<td>:</td>
				<td>'.$row[norangka].'</td>
			</tr>
			<tr>
				<td></td>
				<td>No.Mesin</td>
				<td>:</td>
				<td>'.$row[nomesin].'</td>
			</tr>
			<tr>
				<td></td>
				<td>Nomor Polisi</td>
				<td>:</td>
				<td>'.$row[nopolisi].'</td>
			</tr>
			<tr>
				<td></td>
				<td>No.BPKB</td>
				<td>:</td>
				<td>'.$row[nobpkb].'</td>
			</tr>
			<tr>
				<td></td>
				<td>Atas Nama</td>
				<td>:</td>
				<td>'.$row[pemilik].'</td>
			</tr>
			<tr>
				<td></td>
				<td>Alamat</td>
				<td>:</td>
				<td>'.$row[al_kend].'</td>
			</tr>';
	?>
</table>
<!--
<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span class=SpellE><span style='font-family:"Times New Roman","serif"'>Jenis</span></span><span
style='font-family:"Times New Roman","serif"'><span style='mso-tab-count:1'>                                       </span>:<?php echo jenisPgn(trim($row[jenis]));?><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span class=SpellE><span style='font-family:"Times New Roman","serif"'>Merk</span></span><span
style='font-family:"Times New Roman","serif"'>/Type<span style='mso-tab-count:
1'>                              </span>:<?php echo $row[merktype];?><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span class=SpellE><span style='font-family:"Times New Roman","serif"'>Tahun</span></span><span
style='font-family:"Times New Roman","serif"'>/<span class=SpellE>Warna</span><span
style='mso-tab-count:1'>                          </span>:<?php echo $row[tahun].'/'.$row[warna];?><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'>No.
<span class=SpellE>Mesin</span><span style='mso-tab-count:1'>                              </span>:<?php echo $row[nomesin]?><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'>No.
<span class=SpellE>Rangka</span><span style='mso-tab-count:1'>                            </span>:<?php echo $row[norangka];?><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'><span
style='mso-tab-count:1'>      </span><o:p></o:p></span></p>
-->

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'><span
style='mso-tab-count:1'>      </span><span class=SpellE>Satu</span> <span
class=SpellE>dan</span> lain <span class=SpellE>hal</span> <span class=SpellE>sebagaimana</span>
<span class=SpellE>tercantum</span> <span class=SpellE>dalam</span>, <span
class=SpellE>dan</span> <span class=SpellE>apabila</span> <span class=SpellE>terjadi</span>
<span class=SpellE>perubahan</span> yang <span class=SpellE>disebabkan</span> <span
class=SpellE>Ketentuan</span> <span class=SpellE>dalam</span> <span
class=SpellE>Pasal</span> 64 <span class=SpellE>dan</span> 65 UU No. 22 <span
class=SpellE>tahun</span> 2009, <span class=SpellE>tentang</span> <span
class=SpellE>Registrasi</span> <span class=SpellE>dan</span> <span
class=SpellE>Identitas</span> <span class=SpellE>Kendaraan</span> <span
class=SpellE>Bermotor</span>, <span class=SpellE>maka</span> <span
class=SpellE>perubahan</span> yang <span class=SpellE>dimaksud</span> <span
class=SpellE>kata</span> <span class=SpellE>demi</span> <span class=SpellE>kata</span>
<span class=SpellE>dianggap</span> <span class=SpellE>sebagaimana</span> <span
class=SpellE>telah</span> <span class=SpellE>termaktub</span> <span
class=SpellE>dalam</span> <span class=SpellE>jaminan</span> yang <span
class=SpellE>telah</span> <span class=SpellE>diserahkan</span> <span
class=SpellE>maupun</span> <span class=SpellE>surat-surat</span>, <span
class=SpellE>keterangan-keterangan</span>, <span class=SpellE>pernyataan-pernyataan</span>
<span class=SpellE>dan</span> <span class=SpellE>perjanjian-perjanjian</span>
yang <span class=SpellE>telah</span> <span class=SpellE>dan</span> <span
class=SpellE>akan</span> <span class=SpellE>dibuat</span>. - <span
class=SpellE>selanjutnya</span> <span class=SpellE>disebut</span> AGUNAN.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'><span
style='mso-tab-count:1'>      </span>- <span class=SpellE>Sehubungan</span> <span
class=SpellE>dengan</span> <span class=SpellE>Fasilitas</span> <span
class=SpellE>Pembiayaan</span> yang <span class=SpellE>diterima</span> PEMBERI
KUASA <span class=SpellE>berdasarkan</span> <span class=SpellE>Perjanjian</span>
<span class=SpellE>Pembiayaan</span> <span class=SpellE>Konsumen</span>
No.<span style='mso-spacerun:yes'> <?php echo trim($row[npk]);?> </span><span
style='mso-spacerun:yes'></span><span class=SpellE>tanggal</span><span
style='mso-spacerun:yes'> <?php echo dmy(trim($row[tgl_pk]));?> </span>yang <span
class=SpellE>ditandatangani</span> PEMBERI KUASA <span class=SpellE>dengan</span>
KSP. MITRA USAHA.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:140.25pt'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;mso-list:l1 level1 lfo2;tab-stops:list 18.7pt'><![if !supportLists]><span
style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span class=SpellE><span style='font-family:
"Times New Roman","serif"'>Memberikan</span></span><span style='font-family:
"Times New Roman","serif"'> <span class=SpellE>keterangan-keterangan</span> <span
class=SpellE>dan</span> <span class=SpellE>dokumen</span> <span class=SpellE>seperlunya</span>
<span class=SpellE>kepada</span> <span class=SpellE>notaris</span> <span
class=SpellE>atas</span> <span class=SpellE>semua</span> <span class=SpellE>hal</span>
yang <span class=SpellE>berkaitan</span> <span class=SpellE>dengan</span> <span
class=SpellE>pembuatan</span> <span class=SpellE>akta</span> <span
class=SpellE>Jaminan</span> <span class=SpellE>Fiducia</span> <span
class=SpellE>serta</span> <span class=SpellE>pendaftarannya</span> <span
class=SpellE>di</span> <span class=SpellE>instansi</span> yang <span
class=SpellE>berwenang</span>;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;mso-list:l1 level1 lfo2;tab-stops:list 18.7pt'><![if !supportLists]><span
style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span class=SpellE><span style='font-family:
"Times New Roman","serif"'>Menerima</span></span><span style='font-family:"Times New Roman","serif"'>
<span class=SpellE>Sertifikat</span> <span class=SpellE>Fiducia</span> <span
class=SpellE>dari</span> <span class=SpellE>instansi</span> yang <span
class=SpellE>berwenang</span>;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:.75pt;text-align:justify;text-indent:
-.75pt'><span style='font-family:"Times New Roman","serif"'>-<span
class=SpellE>Sehubungan</span> <span class=SpellE>dengan</span> <span
class=SpellE>hal</span> <span class=SpellE>tersebut</span> <span class=SpellE>Penerima</span>
<span class=SpellE>Kuasa</span> <span class=SpellE>atau</span> <span
class=SpellE>kuasa</span> <span class=SpellE>subtitusinya</span> <span
class=SpellE>dikuasakan</span> <span class=SpellE>untuk</span> <span
class=SpellE>mengajukan</span> <span class=SpellE>dan</span> <span
class=SpellE>menandatangani</span> <span class=SpellE>surat-surat</span> <span
class=SpellE>permohonan</span> <span class=SpellE>dan</span> <span
class=SpellE>surat-surat</span> <span class=SpellE>lainnya</span>, <span
class=SpellE>termasuk</span> <span class=SpellE>mengajukan</span> <span
class=SpellE>surat</span> <span class=SpellE>permohonan</span> <span
class=SpellE>pendaftaran</span> <span class=SpellE>perubahan</span> <span
class=SpellE>jika</span> <span class=SpellE>diperlukan</span>, <span
class=SpellE>dan</span> / <span class=SpellE>atau</span> <span class=SpellE>mengambil</span>
<span class=SpellE>segala</span> <span class=SpellE>langkah</span>/<span
class=SpellE>tindakan</span> <span class=SpellE>serta</span> <span
class=SpellE>upaya</span> <span class=SpellE>lainnya</span> yang <span
class=SpellE>dianggap</span> <span class=SpellE>perlu</span> <span
class=SpellE>oleh</span> <span class=SpellE>penerima</span> <span class=SpellE>kuasa</span>
<span class=SpellE>untuk</span> <span class=SpellE>mencapai</span> <span
class=SpellE>tujuan</span> <span class=SpellE>tersebut</span> <span
class=SpellE>di</span> <span class=SpellE>atas</span> <span class=SpellE>serta</span>
<span class=SpellE>tidak</span> <span class=SpellE>bertentangan</span> <span
class=SpellE>dengan</span> <span class=SpellE>peraturan</span> <span
class=SpellE>hukum</span> yang <span class=SpellE>berlaku</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.75pt;text-align:justify;text-indent:
-.75pt'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:.75pt;text-align:justify;text-indent:
-.75pt'><span class=SpellE><span style='font-family:"Times New Roman","serif"'>Kuasa</span></span><span
style='font-family:"Times New Roman","serif"'> <span class=SpellE>ini</span> <span
class=SpellE>tidak</span> <span class=SpellE>akan</span> <span class=SpellE>berakhir</span>
<span class=SpellE>karena</span> <span class=SpellE>sebab</span> <span
class=SpellE>apapun</span> <span class=SpellE>juga</span> <span class=SpellE>termasuk</span>
<span class=SpellE>sebab-sebab</span> yang <span class=SpellE>ada</span> <span
class=SpellE>dalam</span> <span class=SpellE>Pasal</span> 1813 KUH <span
class=SpellE>Perdata</span> Indonesia, <span class=SpellE>kecuali</span> <span
class=SpellE>seluruh</span> <span class=SpellE>Hutang</span> <span
class=SpellE>Pemberi</span> <span class=SpellE>Kuasa</span> <span class=SpellE>telah</span>
<span class=SpellE>dibayar</span> <span class=SpellE>lunas</span> <span
class=SpellE>seluruhnya</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span class=SpellE><span
style='font-family:"Times New Roman","serif"'>Demikian</span></span><span
style='font-family:"Times New Roman","serif"'> <span class=SpellE>Surat</span> <span
class=SpellE>Kuasa</span> <span class=SpellE>ini</span> <span class=SpellE>dibuat</span>
agar <span class=SpellE>dapat</span> <span class=SpellE>dipergunakan</span> <span
class=SpellE>sebagaimana</span> <span class=SpellE>mestinya</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Times New Roman","serif"'><?php echo setbpr('KOTABPR');?>, <?php echo dmy($row[tgl_pk]);?></o:p></span></p>

<table>
	<tr>
		<td style="width:310px;">Pemberi Kuasa</td>
		<td style="width:310px;text-align:right;"> Penerima Kuasa</td>
	</tr>
	<tr><td>&nbsp;</td><td style="text-align:right;"><?php echo $bpr_nama;?></td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr>
		<td style="width:310px;"><?php echo $row[nama];?></td>
		<td style="width:310px;text-align:right;"><?php echo trim(setbpr('NAMADIR'));?></td>
	</tr>
</table>
<!--
<div style="border:solid 0px #000; width:90%; height:200px; display:none;">
	<div style="width:50%; float:left;">Pemberi Kuasa</div><div style="width:50%; float:left;">Penerima Kuasa</div>
	<div style="width:50%; float:left;">&nbsp;</div><div style="width:50%; float:left;">KSP. MITRA USAHA</div>
	<div style="width:50%; float:left;">&nbsp;</div><div style="width:50%; float:left;">&nbsp;</div>
	<div style="width:50%; float:left;">&nbsp;</div><div style="width:50%; float:left;">&nbsp;</div>
	<div style="width:50%; float:left;">&nbsp;</div><div style="width:50%; float:left;">&nbsp;</div>
	<div style="width:50%; float:left;"><?php echo $row[nama];?></div><div style="width:50%; float:left;"><?php echo setbpr('NAMADIR');?></div>
</div-->
</div>

</body>

</html>
<script>
	window.print();
	//setTimeout("window.close()",100);
</script>
