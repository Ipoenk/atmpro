@extends('layouts.master')
@section('content')
<section class="content">
    <div>
        <h2 align="center">DAFTAR SUB KANTOR</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Kantor</th>
                    <th>Cabang</th>
                    <th>Nama Pimpinan</th>
                    <th>Nama Wakil Pimpinan</th>
                    <th>Nama Komisaris</th>
                    <th>Pengelola</th>
                    <th>Status</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>No Telp</th>
                    <th>No Fax</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
 
                <?php
                Log::info($kantors); 
                ?>
                @foreach ($kantors as $kantors)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{ strtoupper($kantors->namakantor) }}</td>
                    <td>{{strtoupper($kantors->cabang)}}</td>
                    <td>{{strtoupper($kantors->dirut)}}</td>
                    <td>{{strtoupper($kantors->direktur)}}</td>
                    <td>{{strtoupper($kantors->koma)}}</td>
                    <td>{{strtoupper($kantors->koordinator)}}</td>
                    <td>{{strtoupper($kantors->status)}}</td>
                    <td>{{strtoupper($kantors->alamat)}}</td>
                    <td>{{strtoupper($kantors->kota)}}</td>
                    <td>{{strtoupper($kantors->telp)}}</td>
                    <td>{{strtoupper($kantors->fax)}}</td>
                    <td>
                        <div class="timeline-footer">
                            <a href="/kantorsub.show/{{ $kantors->id }}" class="btn btn-info btn-sm">Lihat Detail</a>
                            {{-- <a href="#" class="btn btn-success btn-sm">Sub Kantor</a>
                        </div> --}}
                    </td>
                </tr>                               
                @endforeach 

            </tbody>
        </table>
    <a type="button" href="/kantorsub.create/{{$id}}" class="btn btn-primary col-1">+ Add</a>
    </div>	
</section>
@endsection    