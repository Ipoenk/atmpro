@extends('layouts.master')
@section('content')
<section class="content">

    
    <form action="/kantorsub.update/{{$kantor->kantor_id}}" method="post" class="form-horizontal">
      @csrf
        <div class="container">
          <h1>Setting Sub Kantor</h1>
          <p>Please fill in this form to create an office.</p>
          <hr>

          <input class="form-control col-5" type="hidden"  name="kantor_id" id="kantor_id" value="{{ $kantor->kantor_id }}" required>

          <label for="kodekantor"><b>Kode Kantor</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Kode Kantor" name="kodekantor" id="kodekantor" value="{{ $kantor->kodekantor }}" required>

          <label for="koordinator"><b>Nama Koordinator</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Koordinator" name="koordinator" id="koordinator" value="{{ $kantor->koordinator }}" required>

          <label for="cabang"><b>Cabang</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Cabang" name="cabang" id="cabang" value="{{ $kantor->cabang }}" required>
          
          <label for="status"><b>Status Kantor</b></label>
          <div>
              <select name="status" id="status" class="form-control  col-5 @error('status') is-invalid @enderror">
                  <option value ="{{ $kantor->status }}">{{ $kantor->status }}</option>
                  <option value ="CABANG">CABANG</option>
                  <option value ="KAS">KAS</option>
              </select>
              @error('status')
                  <div class="invalid-feedback">{{ $message }}</div>
              @enderror
          </div>

          <label for="alamatktr"><b>Alamat</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Alamat" name="alamatktr" id="alamatktr" value="{{ $kantor->alamat }}" required>

          <label for="kotaktr"><b>Kota</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Kota" name="kotaktr" id="kotaktr" value="{{ $kantor->kota }}" required>

          <label for="notelp"><b>No Telp</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nomor Telephon" name="notelp" id="notelp" value="{{ $kantor->telp }}" required>

          <label for="nofax"><b>No. Fax</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan No. Faximile" name="nofax" id="nofax" value="{{ $kantor->fax }}" required>

        </div>
 
     
          {{-- <p>By creating an office you agree to our <a href="#">Terms & Privacy</a>.</p> --}}
          <div>
             <p>
                <button type="submit" onClick="return confirm('Apakah data sudah benar?')" class="registerbtn">Simpan</button>
            </p>   
          </div>
      
        {{-- <div class="container signin">
          <p>Already have an account? <a href="{{ route('login') }}">Sign in</a>.</p>
        </div> --}}
      </form>    
</section>
@endsection