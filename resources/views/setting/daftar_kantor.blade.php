@extends('layouts.master')
@section('content')
<section class="content">
    <div>
        <h2 align="center">DAFTAR KANTOR</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Kantor</th>
                    <th>Nama Pimpinan</th>
                    <th>Nama Wakil Pimpinan</th>
                    <th>Nama Komisaris</th>
                    <th>No Telp</th>
                    <th>Email</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
 
                @foreach ($kantors as $kantors)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{ strtoupper($kantors->namakantor) }}</td>
                    <td>{{strtoupper($kantors->dirut)}}</td>
                    <td>{{strtoupper($kantors->direktur)}}</td>
                    <td>{{strtoupper($kantors->koma)}}</td>
                    <td>{{strtoupper($kantors->kontak)}}</td>
                    <td>{{strtoupper($kantors->email)}}</td>
                    <td>
                        <div class="timeline-footer">
                            <a href="/kantor.detail/{{$kantors->id}}" class="btn btn-info btn-sm">Lihat Detail</a>
                        <a href="/kantor.sub/{{$kantors->id}}" class="btn btn-success btn-sm">Sub Kantor</a>
                        </div>
                    </td>
                </tr>                               
                @endforeach 

            </tbody>
        </table>
		<a type="button" href="{{route('kantor.create')}}" class="btn btn-primary col-1">+ Add</a>
    </div>	
</section>
@endsection    