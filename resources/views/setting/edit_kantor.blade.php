@extends('layouts.master')
@section('content')
<section class="content">
   
    <form action="/kantor.update/{{ $kantor->id }}" method="post" class="form-horizontal">
      @csrf
        <div class="container">
          <h1>Edit Kantor</h1>
          <p>Please fill in this form to create an office.</p>
          <hr>

          <label for="kodekantor"><b>Kode Kantor</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Kode Kantor" name="kodekantor" id="kodekantor" value="{{ $kantor->idkantor }}" required>

          <label for="namakantor"><b>Nama Kantor</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Kantor" name="namakantor" id="namakantor" value="{{ $kantor->namakantor }}" required>

          <label for="namadirut"><b>Pimpinan</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Pimpinan" name="namadirut" id="namadirut" value="{{ $kantor->dirut }}" required>

          <label for="namadirektur"><b>Wakil Pimpinan</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Wakil Pimpinan" name="namadirektur" id="namadirektur" value="{{ $kantor->direktur }}" required>

          <label for="namakomisaris"><b>Komisaris</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nama Komisaris Aktif" name="namakomisaris" id="namakomisaris" value="{{ $kantor->koma }}" required>

          <label for="notelp"><b>No Telp</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Nomor Telephon" name="notelp" id="notelp" value="{{ $kantor->kontak }}" required>

          <label for="email"><b>Email</b></label>
          <input class="form-control col-5" type="text" placeholder="Isikan Email" name="email" id="email" value="{{ $kantor->email }}" required>
    
          {{-- <p>By creating an office you agree to our <a href="#">Terms & Privacy</a>.</p> --}}
          <button type="submit" onClick="return confirm('Apakah data sudah benar?')" class="registerbtn">Simpan</button>
        </div>
      
        {{-- <div class="container signin">
          <p>Already have an account? <a href="{{ route('login') }}">Sign in</a>.</p>
        </div> --}}
      </form>    
</section>
@endsection