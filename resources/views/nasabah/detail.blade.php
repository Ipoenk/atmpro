@extends('layouts.master')
@section('content')    
  
<!-- Content Wrapper. Contains page content -->
{{-- <div class="content-wrapper" style="min-height: 1416.81px;"> --}}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Nasabah</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            {{-- <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active "><a href="#"onclick="hidup()"> Nasabah Edit</a></li> --}}
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
  <form action="/nasabah.edit/{{ $nasabahs->id }}" method="POST" class="form-horizontal">
    @csrf
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Pribadi</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="niknsb">Nik NSB</label>
              <input type="text" name ="niknsb" id="niknsb" class="form-control" value="{{ $nasabahs->niknsb }}" disabled>
            </div>
            <div class="form-group">
              <label for="namansb">Nama Nasabah</label>
              <input type="text" name ="namansb" id="namansb" class="form-control" value="{{ strtoupper($nasabahs->namansb) }}" disabled>
            </div>
            <div class="form-group">
              <label for="tmplahirnsb">Tempat Lahir</label>
              <input type="text" name ="tmplahirnsb" id="tmplahirnsb" class="form-control" value="{{ strtoupper($nasabahs->tmplahirnsb) }}" disabled>
            </div>
            {{-- <div class="form-group">
              <label for="tgllahirsb">Tanggal Lahir</label>
              <input type="date" id="tgllahirsb" class="form-control" value="{{ date('d-m-Y',strtotime($nasabahs->tgllahirnsb)) }}" disabled>
            </div> --}}
            <div class="form-group">
              <label class="col-sm-6 control-label" for="tgllahirnsb">Tanggal Lahir</label>
              <div>
                  <input type="date" class="form-control @error('tgllahirnsb') is-invalid @enderror" name = "tgllahirnsb" id="tgllahirnsb" placeholder="Isikan Tanggal Lahir" value="{{date('Y-m-d',strtotime($nasabahs->tgllahirnsb)) }}" disabled/>
                  {{-- <input type="date" id="tgllahirnsb" class="form-control" value="{{ date('d-m-Y',strtotime($nasabahs->tgllahirnsb)) }}" disabled> --}}
                  @error('tgllahirnsb')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
  
            <div class="form-group">
              <label class="col-sm-6 control-label" for="kelaminnsb">Jenis Kelamin</label>
              <div>
                  <select class="form-control @error('kelaminnsb') is-invalid @enderror" name = "kelaminnsb" = id="kelaminnsb" disabled>
                      <option value="{{ strtoupper($nasabahs->kelaminnsb) }}">{{ strtoupper($nasabahs->kelaminnsb) }}</option>
                      <option value="LAKI-LAKI">LAKI-LAKI</option>
                      <option value="PEREMPUAN">PEREMPUAN</option>
                  </select>
                  @error('kelaminnsb')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-6 control-label" for="agamansb">Agama</label>
              <div>
              <select class="form-control @error('agamansb') is-invalid @enderror" name = "agamansb" id="agamansb" disabled>
                  <option value ="{{ strtoupper($nasabahs->agamansb) }}">{{ strtoupper($nasabahs->agamansb) }}</option>
                  <option value ="ISLAM">ISLAM</option>
                  <option value ="KATOLIK">KATOLIK</option>
                  <option value ="PROTESTAN">PROTESTAN</option>
                  <option value ="HINDU">HINDU</option>
                  <option value ="BUDHA">BUDHA</option>
                  <option value ="KONGHUCU">KONGHUCU</option>
              </select>
              @error('agamansb')
                  <div class="invalid-feedback">{{ $message }}</div>
              @enderror
              </div>
          </div>
  
            <div class="form-group">
              <label for="alamatnsbktp">Alamat Sesuai KTP</label>
              <input type="text" name ="alamatnsbktp" id="alamatnsbktp" class="form-control" value="{{ strtoupper($nasabahs->alamatnsbktp) }}" disabled>
            </div>
            <div class="form-group ">
              <label class="col-sm-4 control-label" for="propinsinsbktp">Propinsi Sesuai KTP</label>
              <div >
                  <select name="propinsinsbktp" id="propinsinsbktp" class="form-control @error('propinsinsbktp') is-invalid @enderror" disabled>
                  <option value="{{ strtoupper($nasabahs->propinsinsbktp) }}">{{ strtoupper($nasabahs->namaprop) }}</option>
                  @foreach ($allProvinsi as $provinsis => $value)
                      <option value="{{ $provinsis }}"> {{ $value }} </option>
                  @endforeach 
                  </select>
                  @error('propinsinsbktp')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
          <div class="form-group ">
            <label class="col-sm-6 control-label" for="kotansbktp">Kota/Kabupaten Sesuai KTP</label>
            <div >
                <select name="kotansbktp" id="kotansbktp" class="form-control @error('kotansbktp') is-invalid @enderror" disabled>
                <option value="{{ strtoupper($nasabahs->kotansbktp) }}">{{ strtoupper($nasabahs->kotanama) }}</option>
                </select>
                @error('kotansbktp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group ">
          <label class="col-sm-6 control-label" for="kecamatannsbktp">Kecamatan Sesuai KTP</label>
          <div >
              <select name="kecamatannsbktp" id="kecamatannsbktp" class="form-control @error('kecamatannsbktp') is-invalid @enderror" disabled>
              <option value="{{ strtoupper($nasabahs->kecamatannsbktp) }}">{{ strtoupper($nasabahs->camatnama) }}</option>
              </select>
              @error('kecamatannsbktp')
                  <div class="invalid-feedback">{{ $message }}</div>
              @enderror
          </div>
      </div>            
      <div class="form-group ">
        <label class="col-sm-6 control-label" for="desansbktp">Desa/Kelurahan Sesuai KTP</label>
        <div >
            <select name="desansbktp" id="desansbktp" class="form-control @error('desansbktp') is-invalid @enderror" disabled>
            <option value="{{ strtoupper($nasabahs->desansbktp) }}">{{ strtoupper($nasabahs->desanama) }}</option>
            </select>
            @error('desansbktp')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
      </div>  

      <div class="form-group">
          <label for="deposnsbktp">Kodepos Sesuai KTP</label>
          <input type="text" name ="kodeposnsbktp"  id="kodeposnsbktp" class="form-control" value="{{ strtoupper($nasabahs->kodeposnsbktp) }}" disabled>
      </div>
      <div class="form-group">
        <label for="notelpnsb">No Telp</label>
        <input type="text" name ="notelpnsb" id="notelpnsb" class="form-control" value="{{ strtoupper($nasabahs->notelpnsb) }}" disabled>
      </div>
      <div class="form-group">
        <label for="emailnsb">Alamat Email</label>
        <input type="text" name ="emailnsb"  id="emailnsb" class="form-control" value="{{ strtoupper($nasabahs->email) }}" disabled>
      </div>
      <div class="form-group">
        <label for="alamatnsbdom">Alamat Domisili</label>
        <input type="text" name ="alamatnsbdom" id="alamatnsbdom" class="form-control" value="{{ strtoupper($nasabahs->alamatdomisili) }}" disabled>
      </div>
      <div class="form-group ">
        <label class="col-sm-6 control-label" for="propinsidomisili">Propinsi Domisili</label>
        <div >
            <select name="propinsidomisili" id="propinsidomisili" class="form-control @error('propinsidomisili') is-invalid @enderror" disabled>
            <option value="{{ strtoupper($nasabahs->propinsidomisili) }}">{{ strtoupper($nasabahs->propdom) }}</option>
            @foreach ($allProvinsi as $provinsis => $value)
                <option value="{{ $provinsis }}"> {{ $value }} </option>
            @endforeach 
            </select>
            @error('propinsidomisili')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group ">
      <label class="col-sm-6 control-label" for="kotadomisili">Kota/Kabupaten Domisili</label>
      <div>
          <select name="kotadomisili" id="kotadomisili" class="form-control @error('kotadomisili') is-invalid @enderror" disabled>
          <option value="{{ strtoupper($nasabahs->kotadomisili) }}">{{ strtoupper($nasabahs->kotadom) }}</option>
          </select>
      </div>
        @error('kotadomisili')
          <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group ">
      <label class="col-sm-6 control-label" for="kecamatandomisili">Kecamatan Domisili</label>
      <div>
          <select name="kecamatandomisili" id="kecamatandomisili" class="form-control @error('kecamatandomisili') is-invalid @enderror" disabled>
          <option value="{{ strtoupper($nasabahs->kecamatandomisili) }}">{{ strtoupper($nasabahs->camatdom) }}</option>
          </select>
          @error('kecamatandomisili')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
    </div>
    <div class="form-group ">
      <label class="col-sm-6 control-label" for="desadomisili">Desa/Kelurahan Domisili</label>
      <div>
          <select name="desadomisili" id="desadomisili" class="form-control @error('desadomisili') is-invalid @enderror" disabled>
          <option value="{{ strtoupper($nasabahs->desadomisili) }}">{{ strtoupper($nasabahs->desadom) }}</option>
          </select>
          @error('desadomisili')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
    </div>  
      <div class="form-group">
        <label for="notelp">No HP</label>
        <input type="text" name ="notelp" id="notelp" class="form-control" value="{{ strtoupper($nasabahs->notelpnsb) }}" disabled>
      </div>
      <div class="form-group">
        <label class="col-sm-6 control-label" for="statusperkawinan">Status Perkawinan</label>
        <div>
            <select class="form-control @error('statusperkawinan') is-invalid @enderror" name = "statusperkawinan" id="statusperkawinan" onchange="showDiv('datapasangan', this)" disabled>
                <option value ="{{ strtoupper($nasabahs->statusperkawinan) }}">{{ strtoupper($nasabahs->statusperkawinan) }}</option>
                <option value ="BELUM MENIKAH">BELUM MENIKAH</option>
                <option value ="MENIKAH">MENIKAH</option>
                <option value ="CERAI HIDUP">CERAI HIDUP</option>
                <option value ="CERAI MATI">CERAI MATI</option>
            </select>
            @error('statusperkawinan')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-6 control-label" for="pendidikan">Status Pendidikan</label>
        <div>
            <select class="form-control @error('pendidikan') is-invalid @enderror" name = "pendidikan" id="pendidikan" disabled>
                <option value ="{{ strtoupper($nasabahs->pendidikan) }}">{{ strtoupper($nasabahs->pendidikan) }}</option>
                <option value ="TIDAK SEKOLAH">TIDAK SEKOLAH</option>
                <option value ="SD/SEDERAJAT">SD/SEDERAJAT</option>
                <option value ="SMP/SEDERAJAT">SMP/SEDERAJAT</option>
                <option value ="SMA/SEDERAJAT">SMA/SEDERAJAT</option>
                <option value ="DIPLOMA 1">DIPLOMA 1</option>
                <option value ="DIPLOMA 2">DIPLOMA 2</option>
                <option value ="DIPLOMA 3">DIPLOMA 3</option>
                <option value ="SARJANA S1">SARJANA S1</option>
                <option value ="SARJANA S2">SARJANA S2</option>
                <option value ="SARJANA S3">SARJANA S3</option>
        </select>
            @error('pendidikan')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
      </div>

      <div class="form-group">
        <label for="namaibukandung">Nama Ibu Kandung</label>
        <input type="text" name ="namaibukandung" id="namaibukandung" class="form-control" value="{{ strtoupper($nasabahs->namaibukandung) }}" disabled>
      </div>
      
    </div>
    <!-- /.card-body -->
  </div>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Penghasilan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="sumberdana">Sumber Dana</label>
              <input type="text" name ="sumberdana" id="sumberdana" class="form-control" value="{{ strtoupper($nasabahs->sumberdana)}}" disabled>
            </div>
            <div class="form-group">
              <label for="pendapatan">Pendapatan</label>
              <input type="text"style="text-align: right"  name ="pendapatan" id="pendapatan" class="form-control" value="{{ $nasabahs->pendapatan }}" disabled>
            </div>
            <div class="form-group">
              <label for="pengeluaran">Pengeluaran</label>
              <input type="text" style="text-align: right" name ="pengeluaran" id="pengeluaran" class="form-control" value="{{ $nasabahs->pengeluaran }}" disabled>
            </div>
            <div class="form-group">
              <label for="tanggungan">Tanggungan</label>
              <input type="text" style="text-align: right" name ="tanggungan" id="tanggungan" class="form-control" value="{{ $nasabahs->tanggungan }}" disabled>
            </div>
            <div class="form-group">
              <label class="col-sm-6 control-label" for="ststempattinggal">Status Tempat Tinggal</label>
              <div>
                  <select class="form-control @error('ststempattinggal') is-invalid @enderror" name = "ststempattinggal" id="ststempattinggal" disabled>
                      <option value = "{{ strtoupper($nasabahs->ststempattinggal) }}">{{ strtoupper($nasabahs->ststempattinggal) }}</option>
                      <option value = "MILIK SENDIRI">MILIK SENDIRI</option>
                      <option value = "MILIK ORANG TUA">MILIK ORANG TUA</option>
                      <option value = "RUMAH DINAS">RUMAH DINAS</option>
                      <option value = "SEWA">SEWA</option>
                      <option value = "LAINNYA">LAINNYA</option>
                  </select>
                  @error('ststempattinggal')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
  
          </div>
        </div>
        <!-- /.card -->
      </div>
      
      <div class = "col-md-6" >
        <div class="datapasangan name = "datapasangan" id = "datapasangan" style="display: block">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Data Pasangan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="nikps">NIK Pasangan</label>
              <input type="text" name ="nikps"  id="nikps" class="form-control" value="{{ strtoupper($nasabahs->nikps) }}" disabled>
            </div>
            <div class="form-group">
              <label for="namaps">Nama Pasangan</label>
              <input type="text" name ="namaps" id="namaps" class="form-control" value="{{ strtoupper($nasabahs->namaps) }}" disabled>
            </div>
            <div class="form-group">
              <label for="tmplahirps">Tempat Lahir Pasangan</label>
              <input type="text" name ="tmplahirps" id="tmplahirps" class="form-control" value="{{ strtoupper($nasabahs->tmplahirps) }}" disabled>
            </div>
            <div class="form-group">
              <label for="tgllahirps">Tanggal Lahir Pasangan</label>
              <input type="date" name ="tgllahirps" id="tgllahirps" class="form-control" value="{{ date('Y-m-d',strtotime($nasabahs->tgllahirps)) }}" disabled>
            </div>
            {{-- <div class="form-group">
              <label for="agamaps">Agama Pasangan</label>
              <input type="text" id="agamaps" class="form-control" value="{{ strtoupper($nasabahs->agamaps) }}" disabled>
            </div> --}}
            <div class="form-group">
              <label class="col-sm-6 control-label" for="agamaps">Agama</label>
              <div>
              <select class="form-control @error('agamaps') is-invalid @enderror" name = "agamaps" id="agamaps" disabled>
                  <option value ="{{ strtoupper($nasabahs->agamaps) }}">{{ strtoupper($nasabahs->agamaps) }}</option>
                  <option value ="ISLAM">ISLAM</option>
                  <option value ="KATOLIK">KATOLIK</option>
                  <option value ="PROTESTAN">PROTESTAN</option>
                  <option value ="HINDU">HINDU</option>
                  <option value ="BUDHA">BUDHA</option>
                  <option value ="KONGHUCU">KONGHUCU</option>
              </select>
              @error('agamaps')
                  <div class="invalid-feedback">{{ $message }}</div>
              @enderror
          </div>
        </div>            
            <div class="form-group">
              <label for="alamatps">Alamat Pasangan</label>
              <input type="text" name ="alamatps" id="alamatps" class="form-control" value="{{ strtoupper($nasabahs->alamatps) }}" disabled>
            </div>
            <div class="form-group ">
              <label class="col-sm-6 control-label" for="propinsips">Propinsi</label>
              <div>
                  <select name="propinsips" id="propinsips" class="form-control @error('propinsips') is-invalid @enderror" disabled>
                  <option value="{{ strtoupper($nasabahs->propinsips) }}">{{ strtoupper($nasabahs->propps) }}</option>
                  @foreach ($allProvinsi as $provinsis => $value)
                      <option value="{{ $provinsis }}"> {{ $value }} </option>
                  @endforeach 
                  </select>
                  @error('propinsips')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
  
            <div class="form-group ">
              <label class="col-sm-6 control-label" for="kotaps">Kota/Kabupaten</label>
              <div>
                  <select name="kotaps" id="kotaps" class="form-control @error('kotaps') is-invalid @enderror" disabled>
                  <option value="{{ strtoupper($nasabahs->kotaps) }}">{{ strtoupper($nasabahs->namakotaps) }}</option>
                  </select>
                  @error('kotaps')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
  
          <div class="form-group ">
              <label class="col-sm-6 control-label" for="kecamatanps">Kecamatan</label>
              <div>
                  <select name="kecamatanps" id="kecamatanps" class="form-control @error('kecamatanps') is-invalid @enderror" disabled>
                  <option value="{{ strtoupper($nasabahs->kecamatanps) }}">{{ strtoupper($nasabahs->namacamatps) }}</option>
                  </select>
                  @error('kecamatanps')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
  
            <div class="form-group ">
              <label class="col-sm-6 control-label" for="desaps">Desa/Kelurahan</label>
              <div>
                  <select name="desaps" id="desaps" class="form-control @error('desaps') is-invalid @enderror" disabled>
                  <option value="{{ strtoupper($nasabahs->desaps) }}">{{ strtoupper($nasabahs->namadesaps) }}</option>
                  </select>
                  @error('desaps')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
            </div>

            <div class="form-group">
              <label for="kodeposps">Kodepos </label>
              <input type="text" name ="kodeposps"  id="kodeposps" class="form-control @error('kodeposps') is-invalid @enderror" value="{{ strtoupper($nasabahs->kodeposps) }}" disabled>
              @error('kodeposps')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
    
            <div class="form-group">
              <label for="notelpps">No HP Pasangan</label>
              <input type="text" name ="notelpps"  id="notelpps" class="form-control" value="{{ strtoupper($nasabahs->notelpps) }}" disabled>
            </div>
            <div class="form-group">
              <label class="col-sm-6 control-label" for="kelaminps">Jenis Kelamin</label>
              <div>
                  <select name="kelaminps" id="kelaminps" class="form-control  @error('kelaminps') is-invalid @enderror" disabled>
                      <option value ="{{ strtoupper($nasabahs->kelaminps) }}">{{ strtoupper($nasabahs->kelaminps) }}</option>
                      <option value ="LAKI-LAKI">LAKI-LAKI</option>
                      <option value ="PEREMPUAN">PEREMPUAN</option>
                  </select>
                  @error('kelaminps')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
          </div>
  
        </div>
              <!-- /.card-body -->
  </div>
</div>
        <!-- /.card -->
        
        <!-- /.card -->
      {{-- </div>
      <div class="col-md-6"> --}}
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Data Pekerjaan</h3>
    
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                {{-- <div class="form-group">
                  <label for="jenispsh">Jenis Usaha</label>
                  <input type="text" id="jenispsh" class="form-control" value="{{ strtoupper($nasabahs->jenispsh) }}" disabled>
                </div> --}}
                <div class="form-group">
                  <label class="col-sm-6 control-label" for="jenispsh">Jenis Perusahaan</label>
                  <div>
                      <select class="form-control @error('jenispsh') is-invalid @enderror" name = "jenispsh" id="jenispsh" disabled>
                          <option value ="{{ strtoupper($nasabahs->jenispsh) }}">{{ strtoupper($nasabahs->jenispsh) }}</option>
                          <option value ="PNS">PNS</option>
                          <option value ="TNI-POLRI">TNI-POLRI</option>
                          <option value ="PERSERO">PERSERO</option>
                          <option value ="PERUSAHAAN DAERAH">PERUSAHAAN DAERAH</option>
                          <option value ="PT">PT</option>
                          <option value ="CV">CV</option>
                          <option value ="FIRMA">FIRMA</option>
                          <option value ="KOPERASI">KOPERASI</option>
                          <option value ="YAYASAN">YAYASAN</option>
                          <option value ="LAIN-LAIN">LAIN-LAIN</option>
                      </select>
                      @error('jenispsh')
                      <div class="invalid-feedback">{{ $message }}</div>
                       @enderror
                  </div>
              </div>
      
                <div class="form-group">
                  <label for="namapsh">Nama Perusahaan</label>
                  <input type="text" name ="namapsh" id="namapsh" class="form-control" value="{{ strtoupper($nasabahs->namapsh) }}" disabled>
                </div>
                <div class="form-group">
                  <label for="alamatpsh">Alamat Perusahaan</label>
                  <input type="text" name ="alamatpsh" id="alamatpsh" class="form-control" value="{{ strtoupper($nasabahs->alamatpsh) }}" disabled>
                </div>
                {{-- <div class="form-group">
                  <label for="provinsipsh">Provinsi Perusahaan</label>
                  <input type="text" id="provinsipsh" class="form-control" value="{{ strtoupper($nasabahs->proppsh) }}" disabled>
                </div> --}}
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="propinsipsh">Propinsi</label>
                  <div>
                      <select name="propinsipsh" id="propinsipsh" class="form-control @error('propinsipsh') is-invalid @enderror" disabled>
                      <option value="{{ strtoupper($nasabahs->propinsipsh) }}">{{ strtoupper($nasabahs->proppsh) }}</option>
                      @foreach ($allProvinsi as $provinsis => $value)
                          <option value="{{ $provinsis }}"> {{ $value }} </option>
                      @endforeach 
                      </select>
                      @error('propinsipsh')
                      <div class="invalid-feedback">{{ $message }}</div>
                       @enderror
                  </div>
              </div>
      
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="kotapsh">Kota/Kabupaten</label>
                  <div>
                      <select name="kotapsh" id="kotapsh" class="form-control @error('kotapsh') is-invalid @enderror" disabled>
                      <option value="{{ strtoupper($nasabahs->kotapsh) }}">{{ strtoupper($nasabahs->namakotapsh) }}</option>
                      </select>
                      @error('kotapsh')
                      <div class="invalid-feedback">{{ $message }}</div>
                       @enderror
                  </div>
              </div>
      
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="kecamatanpsh">Kecamatan</label>
                  <div>
                      <select name="kecamatanpsh" id="kecamatanpsh" class="form-control @error('kecamatanpsh') is-invalid @enderror" disabled>
                      <option value="{{ strtoupper($nasabahs->kecamatanpsh) }}">{{ strtoupper($nasabahs->namacamatpsh) }}</option>
                      </select>
                      @error('kecamatanpsh')
                      <div class="invalid-feedback">{{ $message }}</div>
                       @enderror
                  </div>
              </div>
      
                <div class="form-group ">
                  <label class="col-sm-6 control-label" for="desapsh">Desa/Kelurahan</label>
                  <div>
                      <select name="desapsh" id="desapsh" class="form-control @error('desapsh') is-invalid @enderror" disabled>
                      <option value="{{ strtoupper($nasabahs->desapsh) }}">{{ strtoupper($nasabahs->namadesapsh) }}</option>
                      </select>
                      @error('desapsh')
                      <div class="invalid-feedback">{{ $message }}</div>
                       @enderror
                  </div>
              </div>
      
                <div class="form-group">
                  <label for="kodepospsh">Kodepos Perusahaan</label>
                  <input type="text" name ="kodepospsh" id="kodepospsh" class="form-control" value="{{ strtoupper($nasabahs->kodepospsh) }}" disabled>
                </div>
                <div class="form-group">
                  <label for="notelppsh">No Telp Perusahaan</label>
                  <input type="text" name ="notelppsh" id="notelppsh" class="form-control" value="{{ strtoupper($nasabahs->notelppsh) }}" disabled>
                </div>
                <div class="form-group">
                  <label for="bidangusaha">Bidang Usaha</label>
                  <input type="text" name ="bidangusaha" id="bidangusaha" class="form-control" value="{{ strtoupper($nasabahs->bidangusaha) }}" disabled>
                </div>
              </div>
    
          <!-- /.card-body -->
            </div>
        </div>
      </div>   
    <div class="row">
      <div class="col-12">
        <a type="button" href="{{route('nasabah') }}" name = "kembali" id ="kembali" onClick="return confirm('Apakah Anda benar-benar akan kembali?')" class="btn btn-primary col-1">Kembali</a>
        <a type="button" name = "edit" id ="edit" class="btn btn-secondary col-1" onclick="hidup()">Edit</a>
        <button type="submit" name = "update" id ="update" onClick="return confirm('Apakah Anda data Nasabah sudah benar?')" class="btn btn-success col-1" hidden>Simpan</button>
        <a type="button" name = "batal" id ="batal" class="btn btn-warning col-1" onclick="mati()" hidden>Batal</a>

      </div>
    </div>
  </form>
  </section>
  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <script>
    function hidup(){
      $('#niknsb').attr('disabled',false);
      $('#namansb').attr('disabled',false);
      $('#tmplahirnsb').attr('disabled',false);
      $('#tgllahirnsb').attr('disabled',false);
      $('#kelaminnsb').attr('disabled',false);
      $('#agamansb').attr('disabled',false);
      $('#alamatnsbktp').attr('disabled',false);
      $('#propinsinsbktp').attr('disabled',false);
      $('#kotansbktp').attr('disabled',false);
      $('#kecamatannsbktp').attr('disabled',false);
      $('#desansbktp').attr('disabled',false);
      $('#kodeposnsbktp').attr('disabled',false);
      $('#notelpnsb').attr('disabled',false);
      $('#emailnsb').attr('disabled',false);
      $('#alamatnsbdom').attr('disabled',false);
      $('#propinsidomisili').attr('disabled',false);
      $('#kotadomisili').attr('disabled',false);
      $('#kecamatandomisili').attr('disabled',false);
      $('#desadomisili').attr('disabled',false);
      $('#notelp').attr('disabled',false);
      $('#email').attr('disabled',false);
      $('#statusperkawinan').attr('disabled',false);
      $('#pendidikan').attr('disabled',false);
      $('#namaibukandung').attr('disabled',false);
      $('#sumberdana').attr('disabled',false);
      $('#pendapatan').attr('disabled',false);
      $('#pengeluaran').attr('disabled',false);
      $('#tanggungan').attr('disabled',false);
      $('#ststempattinggal').attr('disabled',false);
      $('#nikps').attr('disabled',false);
      $('#namaps').attr('disabled',false);
      $('#tmplahirps').attr('disabled',false);
      $('#tgllahirps').attr('disabled',false);
      $('#agamaps').attr('disabled',false);
      $('#alamatps').attr('disabled',false);
      $('#propinsips').attr('disabled',false);
      $('#kotaps').attr('disabled',false);
      $('#kecamatanps').attr('disabled',false);
      $('#desaps').attr('disabled',false);
      $('#kodeposps').attr('disabled',false);
      $('#notelpps').attr('disabled',false);
      $('#kelaminps').attr('disabled',false);
      $('#jenispsh').attr('disabled',false);
      $('#namapsh').attr('disabled',false);
      $('#alamatpsh').attr('disabled',false);
      $('#propinsipsh').attr('disabled',false);
      $('#kotapsh').attr('disabled',false);
      $('#kecamatanpsh').attr('disabled',false);
      $('#desapsh').attr('disabled',false);
      $('#kodepospsh').attr('disabled',false);
      $('#notelppsh').attr('disabled',false);
      $('#bidangusaha').attr('disabled',false);
      $('#update').attr('hidden',false);
      $('#batal').attr('hidden',false);
      $('#edit').attr('hidden',true);
    }
    function mati(){
      $('#namansb').attr('disabled',true);
      $('#tmplahirnsb').attr('disabled',true);
      $('#tgllahirnsb').attr('disabled',true);
      $('#kelaminnsb').attr('disabled',true);
      $('#agamansb').attr('disabled',true);
      $('#alamatnsbktp').attr('disabled',true);
      $('#propinsinsbktp').attr('disabled',true);
      $('#kotansbktp').attr('disabled',true);
      $('#kecamatannsbktp').attr('disabled',true);
      $('#desansbktp').attr('disabled',true);
      $('#kodeposnsbktp').attr('disabled',true);
      $('#notelpnsb').attr('disabled',true);
      $('#emailnsb').attr('disabled',true);
      $('#alamatnsbdom').attr('disabled',true);
      $('#propinsidomisili').attr('disabled',true);
      $('#kotadomisili').attr('disabled',true);
      $('#kecamatandomisili').attr('disabled',true);
      $('#desadomisili').attr('disabled',true);
      $('#notelp').attr('disabled',true);
      $('#email').attr('disabled',true);
      $('#statusperkawinan').attr('disabled',true);
      $('#pendidikan').attr('disabled',true);
      $('#namaibukandung').attr('disabled',true);
      $('#sumberdana').attr('disabled',true);
      $('#pendapatan').attr('disabled',true);
      $('#pengeluaran').attr('disabled',true);
      $('#tanggungan').attr('disabled',true);
      $('#ststempattinggal').attr('disabled',true);
      $('#nikps').attr('disabled',true);
      $('#namaps').attr('disabled',true);
      $('#tmplahirps').attr('disabled',true);
      $('#tgllahirps').attr('disabled',true);
      $('#agamaps').attr('disabled',true);
      $('#alamatps').attr('disabled',true);
      $('#propinsips').attr('disabled',true);
      $('#kotaps').attr('disabled',true);
      $('#kecamatanps').attr('disabled',true);
      $('#desaps').attr('disabled',true);
      $('#notelpps').attr('disabled',true);
      $('#kelaminps').attr('disabled',true);
      $('#jenispsh').attr('disabled',true);
      $('#namapsh').attr('disabled',true);
      $('#alamatpsh').attr('disabled',true);
      $('#propinsipsh').attr('disabled',true);
      $('#kotapsh').attr('disabled',true);
      $('#kecamatanpsh').attr('disabled',true);
      $('#desapsh').attr('disabled',true);
      $('#kodepospsh').attr('disabled',true);
      $('#notelppsh').attr('disabled',true);
      $('#bidangusaha').attr('disabled',true);
      $('#update').attr('hidden',true);
      $('#edit').attr('hidden',false);
      $('#batal').attr('hidden',true);
    }

    $("#propinsinsbktp").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotansbktp').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotansbktp').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kotansbktp").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatannsbktp').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatannsbktp').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatannsbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatannsbktp").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    console.log(data);
                    $('#desansbktp').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desansbktp').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#propinsidomisili").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotadomisili').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotadomisili').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kotadomisili").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatandomisili').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatandomisili').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatandomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatandomisili").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desadomisili').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desadomisili').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#propinsips").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotaps').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotaps').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotaps').attr('disabled',false);
                }
            });
        });
        
        $("#kotaps").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanps').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanps').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanps').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanps").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desaps').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desaps').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desaps').attr('disabled',false);
                }
            });
        });
        
        $("#propinsipsh").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotapsh').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotapsh').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotapsh').attr('disabled',false);
                }
            });
        });
        
        $("#kotapsh").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanpsh').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanpsh').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanpsh').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanpsh").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desapsh').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desapsh').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desapsh').attr('disabled',false);
                }
            });
        });
        function showDiv(divId, element)
            {
                document.getElementById(divId).style.display = element.value == 'MENIKAH' ? 'block' : 'none';
            }
        function sexps(divId, element)
            {
                document.getElementById(divId).style.display = element.value == 'LAKI-LAKI' ? 'value' : 'PEREMPUAN';
            }              


  </script>
  @endsection