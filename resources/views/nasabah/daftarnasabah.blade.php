@extends('layouts.master')
@section('content')  
<div class="container">
   
{{-- <body class="wide comments example dt-example-bootstrap4"> --}}
	<a name="top" id="top"></a>
	<div class="fw-background">
		<div cal>
			<h2 align="center">DAFTAR NASABAH</h2>
		</div>
	</div>
		{{-- <div>
			<h2>Daftar Nasabah</h2>
			<p>
			  <a class="btn btn-primary" href="{{route('nasabah.create')}}">ADD Nasabah</a>
			</p>    
		</div> --}}
		
		<table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%"> 
			<thead>
				<tr>
					<th>No</th>
					<th>No. KTP</th>
					<th>Nama</th>
					<th>Alamat</th>
					<th>Tmp.Lahir</th>
					<th>Tgl.Lahir</th>
					<th>Pekerjaan</th>
					<th>Pendidikan</th>
					<th>Nama Ibu</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($datanasabahs as $datanasabahs)
				 

				<tr>
					<td style="width:10%" name={{$datanasabahs->id}} id="nomor">{{$loop->iteration}}</td>
					<td style="width:10%">{{$datanasabahs->niknsb}}</td>
					<td style="width:15%">{{strtoupper($datanasabahs->namansb)}}</td>
					<td >{{strtoupper($datanasabahs->alamatnsbktp)}}</td>
					<td style="width:8%">{{strtoupper($datanasabahs->tmplahirnsb)}}</td>
					<td style="width:8%">{{date('d-m-Y',strtotime($datanasabahs->tgllahirnsb))}}</td>
					<td style="width:8%">{{strtoupper($datanasabahs->jenispsh)}}</td>
					<td style="width:8%">{{strtoupper($datanasabahs->pendidikan)}}</td>
					<td style="width:8%">{{strtoupper($datanasabahs->namaibukandung)}}</td>
					<td style="width:11%">
						<div class="timeline-footer">
							<a href="/nasabah.detail/{{$datanasabahs->id}}" class="btn btn-info btn-sm">Detail</a>
							<a href="/kredit.create/{{$datanasabahs->id}}" class="btn btn-danger btn-sm">Kredit</a>
							<a class="btn btn-warning btn-sm">Saving</a>
							<a class="btn btn-success btn-sm">Depo</a>
							</div>
					</td>
				</tr>                               
				@endforeach 

			</tbody>
			{{-- <tfoot>
				<th>No</th>
				<th>No. KTP</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tmp.Lahir</th>
				<th>Tgl.Lahir</th>
				<th>Pekerjaan</th>
				<th>Pendidikan</th>
				<th>Nama Ibu</th>
				<th>Akti</th>
			</tfoot> --}}
		</table>
		<a href=""></a>
		<a type="button" href="{{route('nasabah.create')}}" class="btn btn-primary col-1">+ Add</a>
		{{-- <a type="button" href="/nasabah.detail/{{ $_POST['name'] }}" class="btn btn-secondary col-1">Detail</a>
		<a type="button" href="/kredit.create/{{$datanasabahs->id}}" class="btn btn-success col-1">Kredit</a>
		<a type="button" class="btn btn-danger col-1">Tabungan</a>
		<a type="button" class="btn btn-warning col-1">Deposito</a> --}}
		
</div>	


@endsection
{{-- <script type="text/javascript">
				  var _gaq = _gaq || [];
				  _gaq.push(['_setAccount', 'UA-365466-5']);
				  _gaq.push(['_trackPageview']);

				  (function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				  })();

	function nomor(){
		$('#name').attr('value');	
	} 
    	  
    </script> --}}



