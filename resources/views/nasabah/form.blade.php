@extends('layouts.master')

@section('title', 'Entri Data') 
@section('subtitle', 'Page Title')
@section('content')
<div class="container">
    <div>
    <form action="{{ route('nasabah.store') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        {{-- <div >
            <h3>DATA PRIBADI</h3>
        </div> --}}
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PRIBADI</b>
            </h4>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="jenisnsb">Kelompok Anggota</label>
            </div>
            <label>:</label>  
            <div class="col-25">
            <select class="form-control @error('jenisnsb') is-invalid @enderror" name = "jenisnsb" id="jenisnsb" >
                <option value = "PERORANGAN">PERORANGAN</option>
                <option value = "KELOMPOK">KELOMPOK</option>
            </select>
            @error('jenisnsb')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
        </div>
         

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="niknsb">Kode ID Pengenal</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="niknsb" id="niknsb" minlength="1" maxlength="16" class="form-control numberonly @error('niknsb') is-invalid @enderror " value="{{ old('niknsb') }}"  />
                @error('niknsb')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                <small id="niknsb" class="form-text text-muted">Maksimum 16 karakter</small>
            </div>
        </div>

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="namansb">Nama</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase"  name="namansb" id="namansb" class="form-control @error('niknsb') is-invalid @enderror" value="{{ old('namansb') }}"  />
                @error('namansb')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
               <small id="namansb" class="form-text text-muted">Maksimum 100 karakter</small>                
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="alamatnsbktp">Alamat Sesuai KTP</label>
            </div>
            <label>:</label>  
            <div class="col-50">
                <textarea  name="alamatnsbktp" style="text-transform:uppercase"  id="alamatnsbktp" class="form-control @error('alamatnsbktp') is-invalid @enderror" placeholder="Alamat KTP">{{ old('alamatnsbktp') }}</textarea>
                @error('alamatnsbktp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="propinsinsbktp">Propinsi Sesuai KTP</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="propinsinsbktp" id="propinsinsbktp" class="form-control @error('propinsinsbktp') is-invalid @enderror" >
                <option value="">Pilih Propinsi</option>
                @foreach ($allProvinsi as $provinsis => $value)
                    <option value="{{ $provinsis }}"> {{ $value }} </option>
                @endforeach 
                </select>
                @error('propinsinsbktp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kotansbktp">Kota/Kabupaten sesuai KTP</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kotansbktp" id="kotansbktp" class="form-control @error('kotansbktp') is-invalid @enderror" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
                @error('kotansbktp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kotansbktp">Kecamatan Sesuai KTP</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kecamatannsbktp" id="kecamatannsbktp" class="form-control @error('kecamatannsbktp') is-invalid @enderror" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                @error('kecamatannsbktp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="desansbktp">Desa/Kelurahan Sesuai KTP</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="desansbktp" id="desansbktp" class="form-control @error('desansbktp') is-invalid @enderror" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                @error('desansbktp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kodeposnsbktp">KodePos Sesuai KTP</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="kodeposnsbktp" id="kodeposnsbktp" minlength="1" maxlength="5" class="form-control @error('kodeposnsbktp') is-invalid @enderror" value="{{ old('kodeposnsbktp') }}"  />
                    @error('kodeposnsbktp')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="notelpnsb">Telepon</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="notelpnsb" id="notelpnsb" class="form-control numbersOnly @error('kodeposnsbktp') is-invalid @enderror" placeholder="+62" value="{{ old('notelpnsb') }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="emailnsb">Email</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="emailnsb" id="emailnsb" class="form-control @error('emailnsb') is-invalid @enderror" placeholder="atm@gmail.com" value="{{ old('emailnsb') }}" />
                @error('emailnsb')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="alamatdomisili">Alamat Domisili</label>
            </div>
            <label>:</label>  
            <div class="col-50">
                <textarea  name="alamatdomisili" style="text-transform:uppercase"  id="alamatdomisili" class="form-control @error('emailnsb') is-invalid @enderror" placeholder="Alamat Domisili">{{ old('alamatdomisili') }}</textarea>
                @error('alamatdomisili')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="propinsidomisili">Propinsi Domisili</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="propinsidomisili" id="propinsidomisili" class="form-control @error('propinsidomisili') is-invalid @enderror" >
                <option value="">Pilih Propinsi</option>
                @foreach ($allProvinsi as $provinsis => $value)
                    <option value="{{ $provinsis }}"> {{ $value }} </option>
                @endforeach 
                </select>
                @error('propinsidomisili')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kotadomisili">Kota/Kabupaten Domisili</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kotadomisili" id="kotadomisili" class="form-control @error('kotadomisili') is-invalid @enderror" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
            </div>
            @error('kotadomisili')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kecamatandomisili">Kecamatan Domisili</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kecamatandomisili" id="kecamatandomisili" class="form-control @error('kecamatandomisili') is-invalid @enderror" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                @error('kecamatandomisili')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="desadomisili">Desa/Kelurahan Domisili</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="desadomisili" id="desadomisili" class="form-control @error('desadomisili') is-invalid @enderror" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                @error('desadomisili')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kelaminnsb">Jenis Kelamin</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select class="form-control @error('kelaminnsb') is-invalid @enderror" name = "kelaminnsb" = id="kelaminnsb" >
                    <option value="LAKI-LAKI">LAKI-LAKI</option>
                    <option value="PEREMPUAN">PEREMPUAN</option>
                </select>
                @error('kelaminnsb')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="tmplahirnsb">Tempat Lahir</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase"  name="tmplahirnsb" id="tmplahirnsb" class="form-control @error('tmplahirnsb') is-invalid @enderror" value="{{ old('tmplahirnsb') }}"  />
                @error('tmplahirnsb')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="tgllahirnsb">Tanggal Lahir</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="date" timezone="[[timezone]]" class="form-control @error('tgllahirnsb') is-invalid @enderror" name = "tgllahirnsb" id="tgllahirnsb" placeholder="Isikan Tanggal Lahir" value="{{ old('tgllahirnsb') }}" >
                @error('tgllahirnsb')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="agamansb">Agama</label>
            </div>
            <label>:</label>  
            <div class="col-25">
            <select class="form-control @error('agamansb') is-invalid @enderror" name = "agamansb" id="agamansb" >
                <option value ="ISLAM">ISLAM</option>
                <option value ="KATOLIK">KATOLIK</option>
                <option value ="PROTESTAN">PROTESTAN</option>
                <option value ="HINDU">HINDU</option>
                <option value ="BUDHA">BUDHA</option>
                <option value ="KONGHUCU">KONGHUCU</option>
            </select>
            @error('agamansb')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="statusperkawinan">Status Perkawinan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select class="form-control @error('statusperkawinan') is-invalid @enderror" name = "statusperkawinan" id="statusperkawinan" onchange="showDiv('datapasangan', this)">
                    <option value ="BELUM MENIKAH">BELUM MENIKAH</option>
                    <option value ="MENIKAH">MENIKAH</option>
                    <option value ="CERAI HIDUP">CERAI HIDUP</option>
                    <option value ="CERAI MATI">CERAI MATI</option>
                </select>
                @error('statusperkawinan')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="pendidikan">Status Pendidikan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select class="form-control @error('pendidikan') is-invalid @enderror" name = "pendidikan" id="pendidikan">
                    <option value ="TIDAK SEKOLAH">TIDAK SEKOLAH</option>
                    <option value ="SD/SEDERAJAT">SD/SEDERAJAT</option>
                    <option value ="SMP/SEDERAJAT">SMP/SEDERAJAT</option>
                    <option value ="SMA/SEDERAJAT">SMA/SEDERAJAT</option>
                    <option value ="DIPLOMA 1">DIPLOMA 1</option>
                    <option value ="DIPLOMA 2">DIPLOMA 2</option>
                    <option value ="DIPLOMA 3">DIPLOMA 3</option>
                    <option value ="SARJANA S1">SARJANA S1</option>
                    <option value ="SARJANA S2">SARJANA S2</option>
                    <option value ="SARJANA S3">SARJANA S3</option>
                </select>
                @error('pendidikan')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="namaibukandung">Nama Ibu Kandung</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase"  name="namaibukandung" id="namaibukandung" class="form-control @error('namaibukandung') is-invalid @enderror" value="{{ old('namaibukandung') }}"  />
                @error('namaibukandung')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="datapasangan" name = "datapasangan" id = "datapasangan" style="display: none">
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PASANGAN</b>
            </h4>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="nikps">Kode ID Pengenal</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="nikps" id="nikps" minlength="1" maxlength="16" class="form-control @error('nikps') is-invalid @enderror" value="{{ old('Nikps') }}"  />
                @error('nikps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror

            </div>
        </div>

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="namaps">Nama</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase"  name="namaps" id="namaps" class="form-control @error('namaps') is-invalid @enderror" value="{{ old('namaps') }}"  />
                @error('namaps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="alamatps">Alamat</label>
            </div>
            <label>:</label>  
            <div class="col-50">
                <textarea  name="alamatps" style="text-transform:uppercase"  id="alamatps" class="form-control @error('alamatps') is-invalid @enderror" placeholder="Alamat Pasangan">{{ old('alamatps') }}</textarea>
                @error('alamatps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="propinsips">Propinsi</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="propinsips" id="propinsips" class="form-control @error('propinsips') is-invalid @enderror">
                <option value="">Pilih Propinsi</option>
                @foreach ($allProvinsi as $provinsis => $value)
                    <option value="{{ $provinsis }}"> {{ $value }} </option>
                @endforeach 
                </select>
                @error('propinsips')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kotaps">Kota/Kabupaten</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kotaps" id="kotaps" class="form-control @error('kotaps') is-invalid @enderror" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
                @error('kotaps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kecamatanps">Kecamatan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kecamatanps" id="kecamatanps" class="form-control @error('kecamatanps') is-invalid @enderror" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                @error('kecamatanps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="desaps">Desa/Kelurahan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="desaps" id="desaps" class="form-control @error('desaps') is-invalid @enderror" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                @error('desaps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kodeposps">KodePos</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="kodeposps" id="kodeposps" minlength="1" maxlength="5" class="form-control @error('kodeposps') is-invalid @enderror" value="{{ old('kodeposps') }}" />
                @error('kodeposps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror

            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="notelpps">Telepon</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="notelpps" id="notelpps" class="form-control @error('notelpps') is-invalid @enderror numbersOnly" placeholder="" value="{{ old('notelpps') }}" />
                @error('notelpps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kelaminps">Jenis Kelamin</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kelaminps" id="kelaminps" class="form-control  @error('kelaminps') is-invalid @enderror" value="{{ old('kelaminps') }}">
                    <option value ="LAKI-LAKI">LAKI-LAKI</option>
                    <option value ="PEREMPUAN">PEREMPUAN</option>
                </select>
                @error('kelaminps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="tmplahirps">Tempat Lahir</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase" name="tmplahirps" id="tmplahirps" class="form-control @error('tmplahirps') is-invalid @enderror" value="{{ old('tmplahirps') }}" />
                @error('tmplahirps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="tgllahirps">Tgl Lahir</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="date" class="form-control @error('tgllahirps') is-invalid @enderror" name = "tgllahirps" id="tgllahirps" placeholder="Isikan Tanggal Lahir" value="{{ old('tgllahirps') }}" />
                @error('tgllahirps')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="agamaps">Agama</label>
            </div>
            <label>:</label>  
            <div class="col-25">
            <select class="form-control @error('agamaps') is-invalid @enderror" name = "agamaps" id="agamaps" value="{{ old('agamaps') }}">
                <option value ="ISLAM">ISLAM</option>
                <option value ="KATOLIK">KATOLIK</option>
                <option value ="PROTESTAN">PROTESTAN</option>
                <option value ="HINDU">HINDU</option>
                <option value ="BUDHA">BUDHA</option>
                <option value ="KONGHUCU">KONGHUCU</option>
            </select>
            @error('agamaps')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        </div>
        </div>    
                <div class="alert alert-info">
                <h4 class="align-center">
                    <b>DATA PEKERJAAN</b>
                </h4>
                </div>
            <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="jenispsh">Jenis Perusahaan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select class="form-control @error('jenispsh') is-invalid @enderror" name = "jenispsh" id="jenispsh" value="{{ old('jenispsh') }}">
                    <option value ="PNS">PNS</option>
                    <option value ="TNI-POLRI">TNI-POLRI</option>
                    <option value ="PERSERO">PERSERO</option>
                    <option value ="PERUSAHAAN DAERAH">PERUSAHAAN DAERAH</option>
                    <option value ="PT">PT</option>
                    <option value ="CV">CV</option>
                    <option value ="FIRMA">FIRMA</option>
                    <option value ="KOPERASI">KOPERASI</option>
                    <option value ="YAYASAN">YAYASAN</option>
                    <option value ="LAIN-LAIN">LAIN-LAIN</option>
                </select>
                @error('jenispsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="namapsh">Nama Perusahaan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase" name="namapsh" id="namapsh" class="form-control @error('namapsh') is-invalid @enderror" value="{{ old('namapsh') }}" />
                @error('namapsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="alamatpsh">Alamat Perusahaan</label>
            </div>
            <label>:</label>  
            <div class="col-50">
                <textarea  name="alamatpsh" style="text-transform:uppercase"  id="alamatpsh" class="form-control @error('alamatpsh') is-invalid @enderror" placeholder="Alamat Perusahaan" >{{ old('alamatpsh') }}</textarea>
                @error('alamatpsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="propinsipsh">Propinsi Perusahaan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="propinsipsh" id="propinsipsh" class="form-control @error('propinsipsh') is-invalid @enderror">
                <option value="">Pilih Propinsi</option>
                @foreach ($allProvinsi as $provinsis => $value)
                    <option value="{{ $provinsis }}"> {{ $value }} </option>
                @endforeach 
                </select>
                @error('propinsipsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kotapsh">Kota/Kabupaten</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kotapsh" id="kotapsh" class="form-control @error('kotapsh') is-invalid @enderror" disabled>
                <option value="">Pilih Kota/Kabupaten</option>
                </select>
                @error('kotapsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kecamatanpsh">Kecamatan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="kecamatanpsh" id="kecamatanpsh" class="form-control @error('kecamatanpsh') is-invalid @enderror" disabled>
                <option value="">Pilih Kecamatan</option>
                </select>
                @error('kecamatanpsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="desapsh">Desa/Kelurahan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select name="desapsh" id="desapsh" class="form-control @error('desapsh') is-invalid @enderror" disabled>
                <option value="">Pilih Desa/Kelurahan</option>
                </select>
                @error('desapsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="kodepospsh">KodePos</label>
            </div>
            <label>:</label>  
            <div class="col-25">

                <input type="text" name="kodepospsh" id="kodepospsh" minlength = "1" maxlength = "5" class="form-control @error('kodepospsh') is-invalid @enderror" value="{{ old('kodepospsh') }}" />
                @error('kodepospsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror

            </div>
        </div>        
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="notelppsh">Telepon</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" name="notelppsh" id="notelppsh" class="form-control @error('notelppsh') is-invalid @enderror" value="{{ old('notelppsh') }}" />
                @error('notelppsh')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="bidangusaha">Bidang Usaha</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-transform:uppercase" name="bidangusaha" id="bidangusaha" class="form-control @error('bidangusaha') is-invalid @enderror" value="{{ old('Bidangusaha') }}" />
                @error('bidangusaha')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PENDAPATAN</b>
            </h4>
            </div>
            <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="sumberdana">Sumber Dana</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select class="form-control @error('sumberdana') is-invalid @enderror" name = "sumberdana" id="sumberdana" value="{{ old('sumberdana') }}">
                    <option value = "GAJI" >GAJI</option>
                    <option value = "USAHA">USAHA</option>
                    <option value = "LAINNYA">LAINNYA</option>
                </select>
                @error('sumberdana')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="pendapatan">Pendapatan Kotor</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-align: right" min="0" name = "pendapatan" id="pendapatan" class="form-control @error('pendapatan') is-invalid @enderror" value="{{ number_format(old('pendapatan')) }}" >
                <small  class="text-muted">
                    Pertahun
                </small>
                @error('pendapatan')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>   
        </div>        
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="pengeluaran">Pengeluaran Kotor</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="text" style="text-align: right" min="0" name = "pengeluaran" id="pengeluaran" class=" form-control @error('pengeluaran') is-invalid @enderror" value="{{ number_format(old('pengeluaran')) }}" />
                <small  class="text-muted">
                    Pertahun
                </small>
                @error('pengeluaran')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>        
        </div>   
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="tanggungan">Jumlah Tanggungan</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <input type="number" style="text-align: right" min="0" name = "tanggungan" id="tanggungan" class="form-control @error('tanggungan') is-invalid @enderror" value="{{ number_format(old('tanggungan')) }}" />
                <small class="text-muted">
                    Jiwa (Anak + Istri + Famili)
                </small>
                @error('tanggungan')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>        
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="ststempattinggal">Status Tempat Tinggal</label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select class="form-control @error('ststempattinggal') is-invalid @enderror" name = "ststempattinggal" id="ststempattinggal" value="{{ old('ststempattinggal') }}">
                    <option value = "MILIK SENDIRI">MILIK SENDIRI</option>
                    <option value = "MILIK ORANG TUA">MILIK ORANG TUA</option>
                    <option value = "RUMAH DINAS">RUMAH DINAS</option>
                    <option value = "SEWA">SEWA</option>
                    <option value = "LAINNYA">LAINNYA</option>
                </select>
                @error('ststempattinggal')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="alert alert-info">
            <h4 class="align-center">
                <b>DATA PENGAJUAN</b>
            </h4>
            </div>
        <div class="row">
            <div class="col-25 col-sm-3 control-label">
              <label class="serif" for="pengajuan">Jenis Pengajuan</label>
            </div>
            <label>:</label>  
            <div class="col-25"> 
            <select class="form-control @error('pengajuan') is-invalid @enderror" name = "pengajuan" id="pengajuan" value="{{ old('pengajuan') }}">
                <option value = "KREDIT">PENGAJUAN KREDIT</option>
                <option value = "TABUNGAN">BUKA REKENING TABUNGAN</option>
                <option value = "DEPOSITO">BUKA DEPOSITO</option>
            </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button class="btn btn-primary mr-1 col-1" type="submit">Next</button>
                {{-- <input type="submit" value="Simpan" class="btn btn-default"> --}}
                <a href="{{ Route('nasabah') }}" onClick="return confirm('Apakah Anda benar-benar keluar?')" class="btn btn-danger col-1">Kembali</a>
            </div>
        </div>
    </form>
    </div>
</div>
@endsection

<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin-lte.js') }}"></script>
@push('scripts')
    <script>
        $("#propinsinsbktp").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotansbktp').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotansbktp').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kotansbktp").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatannsbktp').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatannsbktp').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatannsbktp').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatannsbktp").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    console.log(data);
                    $('#desansbktp').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desansbktp').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desansbktp').attr('disabled',false);
                }
            });
        });
        
        $("#propinsidomisili").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotadomisili').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotadomisili').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kotadomisili").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatandomisili').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatandomisili').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatandomisili').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatandomisili").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desadomisili').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desadomisili').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desadomisili').attr('disabled',false);
                }
            });
        });
        
        $("#propinsips").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotaps').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotaps').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotaps').attr('disabled',false);
                }
            });
        });
        
        $("#kotaps").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanps').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanps').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanps').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanps").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desaps').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desaps').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desaps').attr('disabled',false);
                }
            });
        });
        
        $("#propinsipsh").change(function(){
            $.ajax({
                url: "../getKabupaten/provinsi/"+ $(this).val(),
                method: 'GET',
                success: function(data){
                // console.log(data);
                    $('#kotapsh').empty();
                    $.each(data.kabupatens,function(i,kabupatens){
                        $('#kotapsh').append(new Option(kabupatens.nama,kabupatens.id));
                    });
                    $('#kotapsh').attr('disabled',false);
                }
            });
        });
        
        $("#kotapsh").change(function(){
            $.ajax({
                url: "../getKecamatan/kabupaten/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#kecamatanpsh').empty();
                    $.each(data.kecamatans,function(i,kecamatans){
                        $('#kecamatanpsh').append(new Option(kecamatans.nama,kecamatans.id));
                    });
                    $('#kecamatanpsh').attr('disabled',false);
                }
            });
        });
        
        $("#kecamatanpsh").change(function(){
            $.ajax({
                url: "../getkelurahan/kecamatan/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('#desapsh').empty();
                    $.each(data.kelurahans,function(i,kelurahans){
                        $('#desapsh').append(new Option(kelurahans.nama,kelurahans.id));
                    });
                    $('#desapsh').attr('disabled',false);
                }
            });
        });


        function showDiv(divId, element)
            {
                document.getElementById(divId).style.display = element.value == 'MENIKAH' ? 'block' : 'none';
            }
        function sexps(divId, element)
            {
                document.getElementById(divId).style.display = element.value == 'LAKI-LAKI' ? 'value' : 'PEREMPUAN';
            }


        // $("#statusperkawinan").change(function(){
        //     $('#datapasangan').attr('hidden',true);
        //     var kawin = $("#statusperkawinan").val() 
        //     console.log(kawin)
        //     if(kawin = 'MENIKAH'){
        //         // $('#datapasangan').attr('hidden',false);
        //     }
        //     else {
        //         // console.log(kawin)

        //         // $('#datapasangan').attr('hidden',true);

        //     }
        // });
               
    </script>
    <script type="text/javascript">
    var pendapatan = document.getElementById('pendapatan');
    var pengeluaran = document.getElementById('pengeluaran');
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendapatan.addEventListener('keyup', function(e){
      pendapatan.value = formatRupiah(this.value);
    });
    pengeluaran.addEventListener('keyup', function(e){
      pengeluaran.value = formatRupiah(this.value);
    }); 

    /* Fungsi formatRupiah */
    
    
    function formatRupiah(angka, prefix){
      var number_string = angka.replace(/[^.\d]/g, '').toString(),
      split             = number_string.split('.'),
      sisa              = split[0].length % 3, 
      rupiah            = split[0].substr(0, sisa),
      ribuan            = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
      }

    rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }

    </script>
    @stack('scripts')
    @endpush    
