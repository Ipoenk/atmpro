   <ul class="brand-link navbar-purple">
    <img src="/adminlte/img/atm4.png"
         class="brand-image img-circle elevation-4"
         style="opacity: .8">
    <span class="brand-text font-weight-dark"><b>SIM - KOP</b></span>
  </ul>

  <!-- Sidebar --> 
  <div class="sidebar sidebar-dark-primary">
    <!-- Sidebar user (optional) -->
    <br/>
    <br/>
    
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
         
        <li class="nav-item"> 
          <a href="{{route('home') }}" class="nav-link"> 
            <i class="nav-icon fas fa-home"></i>
            <p>
              Home
               
            </p>
          </a>
           
        </li>

        <li class="nav-item"> 
          <a href="{{route('nasabah') }}" class="nav-link">
            <i class="nav-icon fas fa-portrait"></i>
            <p>
              Nasabah
              <span class="badge badge-info right">{{ \App\Models\Nasabah::totalNasabah() }}</span>
            </p> 
          </a>
        </li>
        <br/>
        
              
        @if (Auth()->user()->role_id == 10 || Auth()->user()->role_id == 4 || Auth()->user()->role_id == 5 || Auth()->user()->role_id == 2)
        <li class="nav-item">
          <a href="{{route('simpanan') }}" class="nav-link">
            <i class="nav-icon fas fa-file-invoice-dollar"></i>
            <p>
              Simpanan
              <span class="badge badge-info right"></span>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{route('deposito') }}" class="nav-link">
            <i class="nav-icon fas fa-money-check-alt"></i>
            <p>
              Deposito
              <span class="badge badge-info right"></span> 
            </p>
          </a>

        </li>
        @endif

       @if (Auth()->user()->role_id == 10 || Auth()->user()->role_id == 2 || Auth()->user()->role_id == 3 || Auth()->user()->role_id == 1) 
        <li class="nav-item">
          <a href="{{route('kredit.index') }}" class="nav-link">
            <i class="nav-icon fas fa-hand-holding-usd"></i>
            <p>
              Pinjaman
              <span class="badge badge-info right">{{ \App\Models\Kredit::jumlahfasilitas() }} 
                
              </span>
              
               
            </p>
          </a>
          
        </li>
        <br/>
        @endif
        @if (Auth()->user()->role_id == 10 || Auth()->user()->role_id == 2)
        <li class="nav-item">
          <a href="{{route('kasir') }}" class="nav-link">
            <i class="nav-icon fas fa-cash-register"></i>
            <p>
              Kasir
              
            </p>
          </a>

        </li>
      

        <li class="nav-item">
          <a href="{{route('accounting') }}" class="nav-link">
            <i class="nav-icon fas fa-file-invoice"></i>
            <p>
              Accounting

            </p>
          </a>

        </li>

        <li class="nav-item">
          <a href="{{route('setting') }}" class="nav-link">
            <i class="nav-icon fas fa-users-cog"></i>
            <p>
              SETTING

            </p>
          </a>

        </li>
        @endif
        
        <!-- {{-- <li class="nav-header">
        <i class="nav-icon fas fa-users-cog"></i>
        &nbsp;&nbsp;&nbsp;SETTING
        </li>
        
            <li class="nav-item">
              <a href="../examples/recover-password.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Recover Password</p>
              </a>
            </li> --}} -->
      </ul>
    </nav>
    <!-- /.sidebar-menu -->

  </div> 