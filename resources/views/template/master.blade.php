<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIM | KOP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{ URL::asset('/adminlte/img/atm3.png') }}" type="image/png" sizes="96x96">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/adminlte')}}/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons --> 
  <link rel="stylesheet" href="{{asset('/adminlte')}}/css/adminlte.min.css">  
  <style>

.table-condensed{
  font-size: 10px;
}

.body2, .html2 {
  height: 90%;
  margin: 0;
}

#bg-size {
  /* The image used */
  background-image: url("{{ asset('/adminlte/img/koperasiatm.png') }}");

  /* Full height */
  height: 100%; 
  border: 1px solid black; 
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: 550px 450px;
} 
div.transbox {
  width: 550px;
  height: 480px;
  margin: 26px; 
  margin-left: auto;
  margin-right: auto;   
  background-color: #ffffff;
  opacity: 0.6;
  text-align: center;
}
div.transbox p {
  margin: 5%;
  font-weight: bold;
  color: #000000;
}
 
</style> 
</head>

<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  {{-- <nav class="navbar navbar-light" style="background-color: #e3f2fd;"> --}}
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
       
       
    </ul>

     

     
    <ul class="navbar-nav ml-auto">
      <a><strong>{{ Auth::user()->name }}</strong></a>&emsp;
      <!-- Messages Dropdown Menu -->
       
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown" > 
        <div>
          <a href="{{route('logout')}}">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
              <img src="{{asset('/adminlte')}}/img/off.png" class="img-circle img-size-32" alt="User Image">
            </button>

          </a>
        </div>
         
      </li>
       
    </ul> 
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
   @include('template.sidebar')
    
    <!-- /.sidebar -->
    
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="bg-size">
    
    <!-- Content Header (Page header) -->
    @yield('content') 
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer"> 
    <marquee behavior="scroll" direction="left" scrollamount="7">
      <strong>Kepada semua karyawan karyawati agar selalu menjaga disiplin dalam bekerja dan taat pada aturan yang berlaku, Trimakasih.</strong> 
      {{-- <div class="float-right d-none d-sm-block"> --}}
      <b><a href="http://www.atm28.net">&copy; 2020 Team - Andalan Tata Manajemen</a></b>
    {{-- </div> --}}
    </marquee>
  </footer>

    
</div>
<!-- ./wrapper -->
<script>
  $.simpleTicker($("#breakingNewsTicker"), {
        speed: 1250,
        delay: 3500,
        easing: 'swing',
        effectType: 'roll'
    });
</script>
<!-- jQuery --> 
<script src="{{asset('/adminlte')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/adminlte')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('/adminlte')}}/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('/adminlte')}}/js/demo.js"></script>  
@include('sweetalert::alert')


</body>
</html>
