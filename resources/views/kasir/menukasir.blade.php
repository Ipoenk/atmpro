@extends('layouts.master')
@section('content')
<section class="content">
    <!-- Info boxes -->
<div>
    </br> </br> 
    <div class="row">
          
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>ENTRY DATA</h3>

                </div>
                <div class="icon"> 
                    <i class="fas fa-keyboard"></i>
                </div> 
                <div class="card card-primary collapsed-card">
                  <div class="card-header"> 
                    <div class="card-tools"> 
                        <button type="button" class="small-box-footer btn btn-tool" data-card-widget="collapse">  
                            <b>Menu Entry <i class="fas fa-arrow-circle-down"></i></b>
                        </button> 
                    </div>
                </div>
                <div class="card-body small-box">
                    <a href="#">
                        <span class="glyphicon fas fa-arrow-circle-right"> 
                            <b style="color:black">Buka Kas</b>
                        </span>
                    </a>

                </div>
                <div class="card-body small-box">
                    <a href="#">
                        <span class="glyphicon fas fa-arrow-circle-right"> 
                            <b style="color:black">Tutup Kas</b>
                        </span>
                    </a>
                </div>
                <div class="card-body small-box">
                    <a href="#">
                        <span class="glyphicon fas fa-arrow-circle-right"> 
                            <b style="color:black">Realisasi</b>
                        </span>
                    </a>
                </div>
                <div class="card-body small-box">
                    <a href="{{ route('kasir.transangsuran')}}">
                        <span class="glyphicon fas fa-arrow-circle-right"> 
                            <b style="color:black">Angsuran</b>
                        </span>
                    </a>
                </div>
            </div>
        </div>   
           
        
    </div>
    <div class="col-lg-3 col-6 centered">
            <!-- small card -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>LAPORAN</h3>

                </div>
                <div class="icon">
                    <i class="fas fa-book-reader"></i>
                </div>
                <div class="card card-primary collapsed-card">
                  <div class="card-header">
                    <div class="card-tools">
                        <button type="button" class="small-box-footer btn btn-tool" data-card-widget="collapse">  
                            <b>Menu Laporan <i class="fas fa-arrow-circle-down"></i></b>
                        </button>
                    </div>
                </div>
                <div class="card-body small-box">
                    <a href="{{ route('kredit.realisasi') }}">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan LKH</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                    <a href="{{ route('kredit.lapangsuran') }}">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan LBH</b>
                    </span></a>
                
                </div>
                <div class="card-body small-box">
                <a href="{{ route('kredit.nominatif') }}">
                    <span class="glyphicon fas fa-arrow-circle-right"> 
                    <b style="color:black">Laporan Transaksi</b>
                    </span></a>
                
                </div>
            </div>
    </div> 
     
</div>


     
  
  </section>
@endsection