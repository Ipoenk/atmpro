@extends('layouts.master')
@section('content')
<section class="content">
    <!-- Info boxes -->
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    ABC FINANCE
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                                            <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">KAS<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                                                            <li><a href="http://192.168.2.3:90/kas/buka">Buka Kas</a></li>
                                                                                        <li><a href="http://192.168.2.3:90/kas/brankas">Brankas</a></li>
                                                                                        <li><a href="http://192.168.2.3:90/kas/tutup">Tutup Kas</a></li>
                                                      </ul>
                        </li>
                                                                <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">KREDIT<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                                                    <li><a href="http://192.168.2.3:90/kredit/daftarnasabah/realisasi">Realisasi</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/kredit/daftarnasabah/angsur">Angsuran</a></li>
                                                                                                                                                                </ul>
                        </li>
                                                                <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LAINNYA<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                                                    <li><a href="http://192.168.2.3:90/lain/bank" target="_blank">Bank</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/lain/translain" target="_blank">Lain-Lain</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/lain/bongantung">Bon Gantung</a></li>
                                                            </ul>
                        </li>
                                                                                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LAPORAN<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                                                    <li><a href="http://192.168.2.3:90/laporan/lkh">LKH</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/kasharian">Laporan Harian</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/bankharian">Laporan Bank Harian</a></li>
                                    <li><a href="http://192.168.2.3:90/laporan/detbankharian">Laporan Detail Bank Harian</a></li>
                                                                                                    <!-- <li><a href="http://192.168.2.3:90/laporan/kashariangab">Laporan Harian Konsol</a></li> -->
                                                                <!-- <li><a href="#">Tutup Kas</a></li> -->
                                <li role="separator" class="divider"></li>
                                                                    <li><a href="http://192.168.2.3:90/laporan/realisasi">Laporan Realisasi</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/angsuran">Laporan Angsuran</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/lainlain">Laporan Lain-Lain</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/pelunasan">Laporan Pelunasan</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/bank">Laporan Bank</a></li>
                                                                                                    <li><a href="http://192.168.2.3:90/laporan/bongantung">Laporan Bon Gantung</a></li>
                                                                <li role="separator" class="divider"></li>
                                                            </ul>
                        </li>
                                                                                                                        
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Fretsy Meibawati <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="http://192.168.2.3:90/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                                    </ul>
            </div>
        </div>
    </nav>

    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary" id="panelangsuran">
                <div class="panel-heading"><h4 align="center">PEMBAYARAN ANGSURAN</h4></div>
                    <div class="panel-body">
                      <form class="form-horizontal" id="bayarangsuranform" role="form" method="POST" action="http://192.168.2.3:90/kredit/angsuran/bayar/save/20/ABC.00/2000720-007    " >
                        <input type="hidden" name="_token" value="DfkpNq24CJ9Hrnyc1tDxvriBOJ9vzFzKrQOP0Q0c" />
                        <input type="hidden" name="input_tunggak_ke" value="6" />
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Tanggal</label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" name="input_tanggal" value="03-11-2020" readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="input_nama_nasabah" autocomplete="off" value="UMANG GIANTO                                      " readonly />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">NPP</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="input_npp_nasabah" autocomplete="off" value="ADD-2-33/61-18/V/2020         " readonly />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="input_keterangan_nasabah" autocomplete="off" value="ANGSURAN UMANG GIANTO                                      " />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Sistem</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="input_sistem_angsuran" autocomplete="off" value="BUNGA     " readonly />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Tunggak / Jatuh Tempo</label>
                                    <div class="col-sm-4">
                                                                                <input type="text" class="form-control" name="input_tunggakan" autocomplete="off" value="0 angsuran" readonly />
                                    </div>
                                    <div class="col-sm-4">
                                                                                    <input type="text" class="form-control" name="input_jatuh_tempo" autocomplete="off" value="07-11-2020" readonly />
                                                                            </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Bayar Pokok</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" class="form-control" name="input_jumlah_pokok" autocomplete="off" value="0" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Bayar Bunga</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" class="form-control" name="input_jumlah_bunga" autocomplete="off" value="0" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Total Bayar</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" class="form-control" name="input_jumlah_total" autocomplete="off" value="0" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Bayar Lunas</label>
                                    <div class="col-sm-3">
                                        <input id="inputBayarLunas" name="input_bayar_lunas" type="checkbox" value="LUNAS">
                                        <label for="inputBayarLunas">LUNAS</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="inputBayarDialihkan" name="input_bayar_lunas" type="checkbox" value="DIALIHKAN">
                                        <label for="inputBayarDialihkan">DIALIHKAN</label>
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <input type="radio" id="inputBayarLunas" name="input_bayar_lunas" value="LUNAS" checked />
                                        <label for="inputBayarLunas">LUNAS</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="radio" id="inputBayarDialihkan" name="input_bayar_lunas" value="DIALIHKAN" />
                                        <label for="inputBayarDialihkan">DIALIHKAN</label>
                                    </div> -->
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Jumlah Bayar</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                                                                            <input type="text" class="form-control" name="input_jumlah_bayar" autocomplete="off" value="115.000.000" />
                                                                                    </div>
                                    </div>
                                </div>
                                                                    <div class="row form-group">
                                        <label class="col-sm-4 control-label">Bayar Pokok</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp.</span>
                                                <input type="text" class="form-control" name="input_jumlah_bayar_pokok" autocomplete="off" value=0 />
                                            </div>
                                        </div>
                                    </div>
                                                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Potongan Pokok</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" class="form-control" name="input_jumlah_potongan_pokok" autocomplete="off" value=0 />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Potongan Bunga</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                                <input type="text" class="form-control" name="input_jumlah_potongan_bunga" autocomplete="off" value=0 />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Denda Belum Bayar</label>
                                    <div class="col-sm-8">
                                            <span>Rp. 0,00</span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Denda Angsuran</label>
                                    <div class="col-sm-8">
                                                                                    <span>Rp. 0,00</span>
                                                                            </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Denda</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" class="form-control" name="input_jumlah_denda" autocomplete="off" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Potongan Denda</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" class="form-control" name="input_jumlah_potongan_denda" autocomplete="off" value=0 />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group" style="background-color:#F7819F">
                                    <label class="col-sm-4 control-label">Total Bayar</label>
                                    <div class="col-sm-8">
                                        <label name="total_bayar">Rp. 0,00</label>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Cara Pembayaran</label>
                                    <div class="col-sm-3">
                                        <input type="radio" id="inputJenisTunai" name="input_cara_pembayaran" value="TUNAI" checked />
                                        <label for="inputJenisTunai">Tunai</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="radio" id="inputJenisNonTunai" name="input_cara_pembayaran" value="NON TUNAI" />
                                        <label for="inputJenisNonTunai">Non Tunai</label>
                                    </div>
                                                                    </div>
                                <div class="row form-group fieldnontunai" hidden>
                                    <label class="col-sm-4 control-label">Rek. ABC / Tanggal</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" name="input_transfer_ke">
                                                                                            <option value="1">BCA MALANG</option>
                                                                                            <option value="2">BCA VIRTUAL ACCOUNT</option>
                                                                                            <option value="4">MANDIRI (144.00.1019025.1)</option>
                                                                                            <option value="6">MAYAPADA</option>
                                                                                            <option value="7">BUKOPIN MALANG</option>
                                                                                            <option value="16">BCA KEDIRI (0333197887)</option>
                                                                                            <option value="20">BPR KERTA ARTHA MANDIRI</option>
                                                                                    </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                              <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                            <input type="text" class="form-control" name="input_tanggal_transfer" value="03-11-2020">
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group fieldnontunai" hidden>
                                    <label class="col-sm-4 control-label">Rek. Asal / Atas Nama</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" name="input_transfer_dari">
                                                                                            <option value="BCA">BCA</option>
                                                                                            <option value="MANDIRI">MANDIRI</option>
                                                                                            <option value="BNI">BNI</option>
                                                                                            <option value="BRI">BRI</option>
                                                                                            <option value="MEGA">MEGA</option>
                                                                                            <option value="PERMATA">PERMATA</option>
                                                                                            <option value="BUKOPIN">BUKOPIN</option>
                                                                                            <option value="BTN">BTN</option>
                                                                                            <option value="JATIM">JATIM</option>
                                                                                            <option value="SULUT">SULUT</option>
                                                                                            <option value="MAYAPADA">MAYAPADA</option>
                                                                                            <option value="WOORI SAUDARA">WOORI SAUDARA</option>
                                                                                            <option value="KAM">KAM</option>
                                                                                            <option value="BUKOPIN (1003179067)">BUKOPIN (1003179067)</option>
                                                                                    </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="input_atas_nama_dari" autocomplete="off" value="UMANG GIANTO                                      " style="text-transform:uppercase" />
                                    </div>
                                </div>
                                                                    <div class="row form-group">
                                        <label class="col-sm-2 control-label">&nbsp;</label>
                                        <div class="col-sm-4">
                                                                                                                                                                            </div>
                                        <div class="col-sm-4">
                                            <button type="submit" class="btn btn-primary" name="simpanbayarangsuranbutton">SIMPAN</button>
                                        </div>
                                    </div>
                                                            </div>
                        </div>
                      </form>
                        <hr />
                        <div class="row">
                            <div class="col-sm-12">
                                                            </div>
                        </div>
                        <div class="row" style="overflow: auto;max-height:500px">
                            <div class="col-sm-12">
                                <table class="table" name="datanasabahtable">
                                    <thead>
                                        <col>
                                        <colgroup span="3"></colgroup>
                                        <colgroup span="3"></colgroup>
                                        <tr>
                                            <th colspan="3" scope="colgroup" align="center">Data Debitur</th>
                                            <th colspan="3" scope="colgroup" align="center">Piutang Kredit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>No Kredit</td>
                                            <td>:</td>
                                            <td>ADD-2-33/61-18/V/2020         </td>
                                            <td>Plafon</td>
                                            <td>:</td>
                                            <td>Rp. 11.500.000.000</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>UMANG GIANTO                                      </td>
                                            <td>Bunga</td>
                                            <td>:</td>
                                            <td>1%</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td>JL. CILIWUNG NO 33                                                                                  </td>
                                            <td>Tgl Angsuran</td>
                                            <td>:</td>
                                            <td>07-06-2020 s/d 07-05-2021</td>
                                        </tr>
                                        <tr>
                                            <td>Telepon</td>
                                            <td>:</td>
                                            <td>08113662205    </td>
                                            <td>Jangka Waktu</td>
                                            <td>:</td>
                                            <td>12 bulan</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="overflow: auto;max-height:500px">
                            <div class="col-sm-12">
                                <table class="table table-bordered" name="histpaymenttable">
                                    <thead>
                                        <col>
                                        <colgroup span="4"></colgroup>
                                        <colgroup span="4"></colgroup>
                                        <colgroup span="1"></colgroup>
                                        <tr>
                                            <th colspan="4" scope="colgroup" align="center">Jadwal</th>
                                            <th colspan="4" scope="colgroup" align="center">Pembayaran</th>
                                            <th colspan="1" scope="colgroup" align="center">Saldo</th>
                                            <th colspan="1" rowspan="3" scope="colgroup" align="center">Denda</th>
                                            <th colspan="1" rowspan="3" scope="colgroup" align="center">Bayar Denda</th>
                                            <th colspan="1" rowspan="3" scope="colgroup" align="center">Catatan</th>
                                        </tr>
                                        <tr>
                                            <th scope="col" align="center">Tanggal</th>
                                            <th scope="col" align="center">Ke</th>
                                            <th scope="col" align="center">Pokok</th>
                                            <th scope="col" align="center">Bunga</th>
                                            <th scope="col" align="center">No Bukti</th>
                                            <th scope="col" align="center">Tgl.Bayar</th>
                                            <th scope="col" align="center">Pokok</th>
                                            <th scope="col" align="center">Bunga</th>
                                            <th scope="col" align="center">Saldo Piutang</th>
                                        </tr>
                                        <tr>
                                            <th colspan="8" scope="col" align="center"></th>
                                            <th scope="col" align="center">12.880.000.000</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td class="angsuran1tgljadwal">07-06-2020</td><td class="angsuran1bayarke">1</td><td style="display:none;" class="angsuran1angsuranjadwal">115.000.000</td><td class="angsuran1pokokjadwal">0</td><td class="angsuran1bungajadwal">115.000.000</td><td class="angsuran1nobukti">0620000046</td><td class="angsuran1tglangsuran">08-06-2020</td><td style="display:none;" class="angsuran1angsurandibayar">115000000</td><td class="angsuran1angsurpokok">0</td><td class="angsuran1angsurbunga">115.000.000</td><td class="angsuran1saldopiutang">12.765.000.000</td><td class="angsuran1dendakena">0</td><td class="angsuran1denda">0</td><td class="angsuran1catatan">Terlambat <span class="text-danger">1</span> hari, Penuh</td></tr><tr><td class="angsuran2tgljadwal">07-07-2020</td><td class="angsuran2bayarke">2</td><td style="display:none;" class="angsuran2angsuranjadwal">115.000.000</td><td class="angsuran2pokokjadwal">0</td><td class="angsuran2bungajadwal">115.000.000</td><td class="angsuran2nobukti">0720000048</td><td class="angsuran2tglangsuran">09-07-2020</td><td style="display:none;" class="angsuran2angsurandibayar">115000000</td><td class="angsuran2angsurpokok">0</td><td class="angsuran2angsurbunga">115.000.000</td><td class="angsuran2saldopiutang">12.650.000.000</td><td class="angsuran2dendakena">0</td><td class="angsuran2denda">0</td><td class="angsuran2catatan">Terlambat <span class="text-danger">2</span> hari, Penuh</td></tr><tr><td class="angsuran3tgljadwal">07-08-2020</td><td class="angsuran3bayarke">3</td><td style="display:none;" class="angsuran3angsuranjadwal">115.000.000</td><td class="angsuran3pokokjadwal">0</td><td class="angsuran3bungajadwal">115.000.000</td><td class="angsuran3nobukti">0820000048</td><td class="angsuran3tglangsuran">07-08-2020</td><td style="display:none;" class="angsuran3angsurandibayar">115000000</td><td class="angsuran3angsurpokok">0</td><td class="angsuran3angsurbunga">115.000.000</td><td class="angsuran3saldopiutang">12.535.000.000</td><td class="angsuran3dendakena">0</td><td class="angsuran3denda">0</td><td class="angsuran3catatan">Tepat waktu, Penuh</td></tr><tr><td class="angsuran4tgljadwal">07-09-2020</td><td class="angsuran4bayarke">4</td><td style="display:none;" class="angsuran4angsuranjadwal">115.000.000</td><td class="angsuran4pokokjadwal">0</td><td class="angsuran4bungajadwal">115.000.000</td><td class="angsuran4nobukti">0920000049</td><td class="angsuran4tglangsuran">07-09-2020</td><td style="display:none;" class="angsuran4angsurandibayar">115000000</td><td class="angsuran4angsurpokok">0</td><td class="angsuran4angsurbunga">115.000.000</td><td class="angsuran4saldopiutang">12.420.000.000</td><td class="angsuran4dendakena">0</td><td class="angsuran4denda">0</td><td class="angsuran4catatan">Tepat waktu, Penuh</td></tr><tr><td class="angsuran5tgljadwal">07-10-2020</td><td class="angsuran5bayarke">5</td><td style="display:none;" class="angsuran5angsuranjadwal">115.000.000</td><td class="angsuran5pokokjadwal">0</td><td class="angsuran5bungajadwal">115.000.000</td><td class="angsuran5nobukti">1020000034</td><td class="angsuran5tglangsuran">07-10-2020</td><td style="display:none;" class="angsuran5angsurandibayar">115000000</td><td class="angsuran5angsurpokok">0</td><td class="angsuran5angsurbunga">115.000.000</td><td class="angsuran5saldopiutang">12.305.000.000</td><td class="angsuran5dendakena">0</td><td class="angsuran5denda">0</td><td class="angsuran5catatan">Tepat waktu, Penuh</td></tr><tr><td class="angsuran6tgljadwal">07-11-2020</td><td class="angsuran6bayarke">6</td><td style="display:none;" class="angsuran6angsuranjadwal">115.000.000</td><td class="angsuran6pokokjadwal">0</td><td class="angsuran6bungajadwal">115.000.000</td><td class="angsuran6nobukti">-</td><td class="angsuran6tglangsuran">-</td><td style="display:none;" class="angsuran6angsurandibayar">0</td><td class="angsuran6angsurpokok">0</td><td class="angsuran6angsurbunga">0</td><td class="angsuran6saldopiutang">-</td><td class="angsuran6dendakena">0</td><td class="angsuran6denda">-</td><td class="angsuran6catatan">-</td></tr><tr><td class="angsuran7tgljadwal">07-12-2020</td><td class="angsuran7bayarke">7</td><td style="display:none;" class="angsuran7angsuranjadwal">115.000.000</td><td class="angsuran7pokokjadwal">0</td><td class="angsuran7bungajadwal">115.000.000</td><td class="angsuran7nobukti">-</td><td class="angsuran7tglangsuran">-</td><td style="display:none;" class="angsuran7angsurandibayar">0</td><td class="angsuran7angsurpokok">0</td><td class="angsuran7angsurbunga">0</td><td class="angsuran7saldopiutang">-</td><td class="angsuran7dendakena">0</td><td class="angsuran7denda">-</td><td class="angsuran7catatan">-</td></tr><tr><td class="angsuran8tgljadwal">07-01-2021</td><td class="angsuran8bayarke">8</td><td style="display:none;" class="angsuran8angsuranjadwal">115.000.000</td><td class="angsuran8pokokjadwal">0</td><td class="angsuran8bungajadwal">115.000.000</td><td class="angsuran8nobukti">-</td><td class="angsuran8tglangsuran">-</td><td style="display:none;" class="angsuran8angsurandibayar">0</td><td class="angsuran8angsurpokok">0</td><td class="angsuran8angsurbunga">0</td><td class="angsuran8saldopiutang">-</td><td class="angsuran8dendakena">0</td><td class="angsuran8denda">-</td><td class="angsuran8catatan">-</td></tr><tr><td class="angsuran9tgljadwal">07-02-2021</td><td class="angsuran9bayarke">9</td><td style="display:none;" class="angsuran9angsuranjadwal">115.000.000</td><td class="angsuran9pokokjadwal">0</td><td class="angsuran9bungajadwal">115.000.000</td><td class="angsuran9nobukti">-</td><td class="angsuran9tglangsuran">-</td><td style="display:none;" class="angsuran9angsurandibayar">0</td><td class="angsuran9angsurpokok">0</td><td class="angsuran9angsurbunga">0</td><td class="angsuran9saldopiutang">-</td><td class="angsuran9dendakena">0</td><td class="angsuran9denda">-</td><td class="angsuran9catatan">-</td></tr><tr><td class="angsuran10tgljadwal">07-03-2021</td><td class="angsuran10bayarke">10</td><td style="display:none;" class="angsuran10angsuranjadwal">115.000.000</td><td class="angsuran10pokokjadwal">0</td><td class="angsuran10bungajadwal">115.000.000</td><td class="angsuran10nobukti">-</td><td class="angsuran10tglangsuran">-</td><td style="display:none;" class="angsuran10angsurandibayar">0</td><td class="angsuran10angsurpokok">0</td><td class="angsuran10angsurbunga">0</td><td class="angsuran10saldopiutang">-</td><td class="angsuran10dendakena">0</td><td class="angsuran10denda">-</td><td class="angsuran10catatan">-</td></tr><tr><td class="angsuran11tgljadwal">07-04-2021</td><td class="angsuran11bayarke">11</td><td style="display:none;" class="angsuran11angsuranjadwal">115.000.000</td><td class="angsuran11pokokjadwal">0</td><td class="angsuran11bungajadwal">115.000.000</td><td class="angsuran11nobukti">-</td><td class="angsuran11tglangsuran">-</td><td style="display:none;" class="angsuran11angsurandibayar">0</td><td class="angsuran11angsurpokok">0</td><td class="angsuran11angsurbunga">0</td><td class="angsuran11saldopiutang">-</td><td class="angsuran11dendakena">0</td><td class="angsuran11denda">-</td><td class="angsuran11catatan">-</td></tr><tr><td class="angsuran12tgljadwal">07-05-2021</td><td class="angsuran12bayarke">12</td><td style="display:none;" class="angsuran12angsuranjadwal">11.615.000.000</td><td class="angsuran12pokokjadwal">11.500.000.000</td><td class="angsuran12bungajadwal">115.000.000</td><td class="angsuran12nobukti">-</td><td class="angsuran12tglangsuran">-</td><td style="display:none;" class="angsuran12angsurandibayar">0</td><td class="angsuran12angsurpokok">0</td><td class="angsuran12angsurbunga">0</td><td class="angsuran12saldopiutang">-</td><td class="angsuran12dendakena">0</td><td class="angsuran12denda">-</td><td class="angsuran12catatan">-</td></tr>                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- JavaScripts -->
    <script src="http://192.168.2.3:90/jquery.js"></script>
    <script src="http://192.168.2.3:90/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://192.168.2.3:90/jquery_mask/jquery.mask.min.js"></script>
    <script src="http://192.168.2.3:90/fittext/jquery.fittext.js"></script>
    <script src="http://192.168.2.3:90/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="http://192.168.2.3:90/select2/dist/js/select2.full.min.js"></script>
    <script src="http://192.168.2.3:90/summernote/summernote.min.js"></script>
                        
    
<script type="text/javascript">
    var CONTENT;
    var SUBM = 0;
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
    function number_format (number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
    function parseDate(str) {
        var mdy = str.split('-')
        return new Date(mdy[2], mdy[1]-1, mdy[0]);
    }

    function daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }
    function generateNote(i,dd,mm,yyyy)
    {
        var diff = daydiff(parseDate(dd+'-'+mm+'-'+yyyy),parseDate($('.angsuran'+i+'tgljadwal').text()));
                    //console.log(diff);
                    var cat = '';
                    var angsurjadwal = parseFloat($('.angsuran'+i+'pokokjadwal').text().split('.').join(''))+parseFloat($('.angsuran'+i+'bungajadwal').text().split('.').join(''));
                    var angsurkartu = parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))+parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''));
                    if(diff < 0){
                        cat += 'Terlambat '+Math.abs(diff)+' hari, ';
                    } else if(diff > 0) {
                        cat += 'Cepat '+Math.abs(diff)+' hari, ';
                    } else if(diff == 0) {
                        cat += 'Tepat Waktu, ';
                    }
                    if(angsurkartu < angsurjadwal){
                        cat += 'Belum Penuh';
                    } else {
                        cat += 'Penuh';
                    }
                    $('.angsuran'+i+'catatan').text(cat);
                    /*if((i-1) == 0){
                        $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                    } else {
                        $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                    }*/
    }

    $(document).ready(function() {
        CONTENT = $('[name="histpaymenttable"]').html();
        //console.log(CONTENT.toString());
        jQuery('[name="histpaymenttable"]').fitText(1, { minFontSize: '10px', maxFontSize: '13px' });

        $('[name="total_bayar"]').text("Rp. 115.000.000,00");

        $('[name="input_tanggal_transfer"]').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        }).on('changeDate', function(e) {
            // Revalidate the date field
            //alert('sukses');
        });

        $('[name="input_transfer_ke"]').select2({ width: '100%' });
        $('[name="input_transfer_dari"]').select2({ width: '100%' });

        $('[name="printsample"]').click(function(){
            var base_url = $(this).attr('data-url');
            var no_kredit = "20/ABC.00/2000720-007";
            window.open(base_url+'/'+no_kredit.toString()+'/'+$('[name="input_jumlah_bayar"]').val().split('.').join(''));
        });
        //$('[name="input_cara_pembayaran"]:checked').val();
        $('[name="input_cara_pembayaran"]').click(function(){
            //alert($('[name="input_cara_pembayaran"]:checked').val());
            //if($('[name="input_cara_pembayaran"]:checked').val() === 'TRANSFER'){
            if(($('[name="input_cara_pembayaran"]:checked').val() === 'NON TUNAI') || ($('[name="input_cara_pembayaran"]:checked').val() === 'TITIPAN')){
                $('.fieldnontunai').show();
            } else {
                $('.fieldnontunai').hide();
            }
        });

        $('[name="input_jumlah_bayar"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});
        $('[name="input_jumlah_bayar_pokok"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});
        $('[name="input_jumlah_denda"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});
        //$('[name="input_jumlah_potongan"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});
        $('[name="input_jumlah_potongan_pokok"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});
        $('[name="input_jumlah_potongan_bunga"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});
        $('[name="input_jumlah_potongan_denda"]').mask('000.000.000.000.000', {reverse: true,selectOnFocus: true});

        /*$('[name="input_angsuran_ke_sampai"]').change(function(e){
            if(!isNumber($('[name="input_angsuran_ke_sampai"]').val())){
                $('[name="input_angsuran_ke_sampai"]').val($('[name="input_angsuran_ke"]').val());
            } else {
                if($('[name="input_angsuran_ke_sampai"]').val() > parseInt("12")){
                    $('[name="input_angsuran_ke_sampai"]').val($('[name="input_angsuran_ke"]').val());
                }
            }
            var jmlangs = 0;
            for(var x=$('[name="input_angsuran_ke"]').val();x<=$('[name="input_angsuran_ke_sampai"]').val();x++){
                var a = $('[name="input_angsuran_ke"]').val();
                jmlangs += parseFloat($('.angsuran'+x+'angsuranjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+x+'angsurandibayar').text());
            }
            $('#bayar_angsuran').text('Rp. '+number_format(jmlangs,0,'','.')+',00');
            $('[name="input_jumlah_bayar"]').val(number_format(jmlangs,0,'','.'));
        });*/

        $('#bayarangsuranform').on('keyup keypress', function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) { 
                e.preventDefault();
                return false;
            }
        });  
        $('#bayarangsuranform').submit(function(e){
            var checked = $('[name="input_bayar_lunas"]:checked').length;
            if(checked > 1){
                alert("Bayar lunas hanya boleh dipilih salah satu");
                return false;
            } else {
                if(SUBM == 0){
                    if(confirm('Apakah anda yakin semua entry sudah benar ?')){
                        SUBM = 1;
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        });
        $('#inputBayarLunas').click(function(){
            var checked = $('[name="input_bayar_lunas"]:checked').length;
            if(checked > 1){
                $(this).prop('checked', false);
            }
        });
        $('#inputBayarDialihkan').click(function(){
            var checked = $('[name="input_bayar_lunas"]:checked').length;
            if(checked > 1){
                $(this).prop('checked', false);
            }
        });
        $('[name="input_bayar_lunas"]').click(function(){
            var checked = $('[name="input_bayar_lunas"]:checked').length;
            //alert(checked);
            if(checked > 0){
                $('[name="input_jumlah_pokok"]').val("11.500.000.000");
                $('[name="input_jumlah_bunga"]').val("805.000.000");
                $('[name="input_jumlah_total"]').val("12.305.000.000");
                var totaldenda = '0';
                var bayardenda = parseFloat($('[name="input_jumlah_denda"]').val().split('.').join(''));
                $('[name="input_jumlah_potongan_denda"]').val(number_format((totaldenda-bayardenda),0,'','.'));
                //$('#bayarbiayaform').attr("action","http://192.168.2.3:90/kredit/angsuran/lunas/save/20/ABC.00/2000720-007    ");
            } else {
                $('[name="input_jumlah_pokok"]').val("0");
                $('[name="input_jumlah_bunga"]').val("0");
                $('[name="input_jumlah_total"]').val("0");
                $('[name="input_jumlah_potongan_denda"]').val(0);
            }
        });
        $('[name="input_jumlah_bayar"]').on('change textInput input',function(){
            if($('[name="input_bayar_lunas"]').is(':checked')){
                var saldopokok = parseFloat($('[name="input_jumlah_pokok"]').val().split('.').join(''));
                var saldobunga = parseFloat($('[name="input_jumlah_bunga"]').val().split('.').join(''));
                var jmlbayar = parseFloat($('[name="input_jumlah_bayar"]').val().split('.').join(''));
                if(jmlbayar >= saldopokok){
                    $('[name="input_jumlah_potongan_pokok"]').val(0);
                    $('[name="input_jumlah_potongan_bunga"]').val(number_format((saldobunga-(jmlbayar-saldopokok)),0,'','.'));
                } else {
                    $('[name="input_jumlah_potongan_pokok"]').val(number_format(saldopokok-jmlbayar,0,'','.'));
                    $('[name="input_jumlah_potongan_bunga"]').val(number_format(saldobunga,0,'','.'));
                }
            }
            $('[name="total_bayar"]').text("Rp. "+number_format((parseFloat($('[name="input_jumlah_bayar"]').val().split('.').join(''))+parseFloat($('[name="input_jumlah_denda"]').val().split('.').join(''))),0,'','.')+",00");
        });
        $('[name="input_jumlah_denda"]').on('change textInput input',function(){
            if($('[name="input_bayar_lunas"]').is(':checked')){
                var totaldenda = '0';
                var bayardenda = parseFloat($('[name="input_jumlah_denda"]').val().split('.').join(''));
                $('[name="input_jumlah_potongan_denda"]').val(number_format((totaldenda-bayardenda),0,'','.'));
            }
            $('[name="total_bayar"]').text("Rp. "+number_format((parseFloat($('[name="input_jumlah_bayar"]').val().split('.').join(''))+parseFloat($('[name="input_jumlah_denda"]').val().split('.').join(''))),0,'','.')+",00");
        });

        $('[name="generatesample"]').click(function(){
            $('[name="histpaymenttable"]').empty();
            $('[name="histpaymenttable"]').append(CONTENT);

            //var jmlbayar = parseFloat($('[name="input_jumlah_bayar"]').val().split('.').join(''))+parseFloat($('[name="input_jumlah_potongan"]').val().split('.').join(''));
            var jmlbayar = parseFloat($('[name="input_jumlah_bayar"]').val().split('.').join(''));
            var potpokok = parseFloat($('[name="input_jumlah_potongan_pokok"]').val().split('.').join(''));
            var potbunga = parseFloat($('[name="input_jumlah_potongan_bunga"]').val().split('.').join(''));
            var denda = parseFloat($('[name="input_jumlah_denda"]').val().split('.').join(''))+parseFloat($('[name="input_jumlah_potongan_denda"]').val().split('.').join(''));
            var tunggakke = parseInt("6");
            var bakidebet = parseFloat("11500000000");

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            } 

            if(mm<10) {
                mm='0'+mm
            } 

            var i = parseInt("6");
            var j = parseInt("6");
            /*while (daydiff(parseDate(dd+'-'+mm+'-'+yyyy),parseDate($('.angsuran'+i+'tgljadwal').text())) <= 0) {
                console.log('The number is '+i);
                console.log('Today date '+parseDate(dd+'-'+mm+'-'+yyyy));
                console.log('Angsuran date '+parseDate($('.angsuran'+i+'tgljadwal').text()));
                console.log('Tunggakan '+daydiff(parseDate(dd+'-'+mm+'-'+yyyy),parseDate($('.angsuran'+i+'tgljadwal').text())));
                var angsuranjadwal = $('.angsuran'+i+'angsuranjadwal').text().split('.').join('');
                var angsurandibayar = $('.angsuran'+i+'angsurandibayar').text().split('.').join('');
                jmlbayar -= angsuranjadwal-angsurandibayar;
                console.log('Yang harus dibayar '+angsuranjadwal);
                console.log('Yang dibayar dulu '+angsurandibayar);
                console.log('Sisa belum terbayar '+(angsuranjadwal-angsurandibayar));
                console.log('Sisa yang dibayar nasabah '+jmlbayar);
                if(jmlbayar === 0){
                    break;
                }
                i++;
            }*/
            var x = 1;
            while (denda > 0){
                if(($('.angsuran'+x+'dendakena').text().split('.').join('') != 0) && ($('.angsuran'+x+'dendakena').text().split('.').join('') != '-')){
                    if($('.angsuran'+x+'denda').text().split('.').join('') == '-'){
                        var dendabayar = 0;
                    } else {
                        var dendabayar = parseFloat($('.angsuran'+x+'denda').text().split('.').join(''));
                    }
                    var sisadenda = parseFloat($('.angsuran'+x+'dendakena').text().split('.').join('')) - dendabayar;
                    if(denda < sisadenda){
                        $('.angsuran'+x+'denda').text(number_format((dendabayar+denda),0,'','.'));
                        denda = 0;
                    } else {
                        denda -= sisadenda;
                        $('.angsuran'+x+'denda').text($('.angsuran'+x+'dendakena').text());

                    }
                }
                x++;
            }
            while (jmlbayar > 0) {
                /*if($('[name="input_cara_pembayaran"]:checked').val() == "TUNAI"){
                    var tanggal = parseDate(dd+'-'+mm+'-'+yyyy);
                    console.log('TUNAI '+parseDate(dd+'-'+mm+'-'+yyyy));
                } else {
                    var tanggal = parseDate($('[name="input_tanggal_transfer"]').val());
                    console.log('NON TUNAI '+parseDate($('[name="input_tanggal_transfer"]').val()));
                }
                console.log(tanggal);
                console.log(daydiff(tanggal,parseDate($('.angsuran'+i+'tgljadwal').text())));*/
                if(!$('[name="input_bayar_lunas"]').is(':checked')){
                    if(daydiff(parseDate(dd+'-'+mm+'-'+yyyy),parseDate($('.angsuran'+i+'tgljadwal').text())) <= 0){
                        if(parseInt("0") < 3){
                            var sisabunga = parseFloat($('.angsuran'+i+'bungajadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''));
                            if(jmlbayar < sisabunga){
                                $('.angsuran'+i+'angsurbunga').text(number_format((parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))+jmlbayar),0,'','.'));
                                jmlbayar = 0;
                            } else {
                                jmlbayar -= sisabunga;
                                $('.angsuran'+i+'angsurbunga').text($('.angsuran'+i+'bungajadwal').text());
                            }
                        } else if(parseInt("0") >= 3){
                            var sisapokok = parseFloat($('.angsuran'+i+'pokokjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''));
                            if(jmlbayar < sisapokok){
                                $('.angsuran'+i+'angsurpokok').text(number_format((parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))+jmlbayar),0,'','.'));
                                jmlbayar = 0;
                            } else {
                                jmlbayar -= sisapokok;
                                $('.angsuran'+i+'angsurpokok').text($('.angsuran'+i+'pokokjadwal').text());
                            }
                            //$('.angsuran'+i+'angsurpokok').text(i);
                        }
                        generateNote(i,dd,mm,yyyy);
                        if((i-1) == 0){
                            $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        } else {
                            $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        }
                        
                        i++;    
                    } else if(daydiff(parseDate(dd+'-'+mm+'-'+yyyy),parseDate($('.angsuran'+j+'tgljadwal').text())) <= 0){
                        if(parseInt("0") < 3){
                            var sisapokok = parseFloat($('.angsuran'+j+'pokokjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+j+'angsurpokok').text().split('.').join(''));

                            if(jmlbayar < sisapokok){
                                $('.angsuran'+j+'angsurpokok').text(number_format((parseFloat($('.angsuran'+j+'angsurpokok').text().split('.').join(''))+jmlbayar),0,'','.'));
                                jmlbayar = 0;
                            } else {
                                jmlbayar -= sisapokok;
                                $('.angsuran'+j+'angsurpokok').text($('.angsuran'+j+'pokokjadwal').text());
                            }
                        } else if(parseInt("0") >= 3){
                            var sisabunga = parseFloat($('.angsuran'+j+'bungajadwal').text().split('.').join(''))-parseFloat($('.angsuran'+j+'angsurbunga').text().split('.').join(''));
                            
                            if(jmlbayar < sisabunga){
                                $('.angsuran'+j+'angsurbunga').text(number_format((parseFloat($('.angsuran'+j+'angsurbunga').text().split('.').join(''))+jmlbayar),0,'','.'));
                                jmlbayar = 0;
                            } else {
                                jmlbayar -= sisabunga;
                                $('.angsuran'+j+'angsurbunga').text($('.angsuran'+j+'bungajadwal').text());
                            }
                        }
                        generateNote(j,dd,mm,yyyy);
                        if((j-1) == 0){
                            $('.angsuran'+j+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+j+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        } else {
                            $('.angsuran'+j+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(j-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+j+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+j+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        }
                        j++;    
                    } else {
                        var sisabunga = parseFloat($('.angsuran'+i+'bungajadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''));
                        if(jmlbayar < sisabunga){
                            $('.angsuran'+i+'angsurbunga').text(number_format((jmlbayar+parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                            jmlbayar = 0;
                        } else {
                            $('.angsuran'+i+'angsurbunga').text($('.angsuran'+i+'bungajadwal').text());
                            jmlbayar -= sisabunga;
                            var sisapokok = parseFloat($('.angsuran'+i+'pokokjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''));
                            if(jmlbayar < sisapokok){
                                $('.angsuran'+i+'angsurpokok').text(number_format((jmlbayar+parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))),0,'','.'));
                                jmlbayar = 0;
                            } else {
                                $('.angsuran'+i+'angsurpokok').text($('.angsuran'+i+'pokokjadwal').text());
                                jmlbayar -= sisapokok;
                            }
                        }
                        generateNote(i,dd,mm,yyyy);
                        if((i-1) == 0){
                            $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        } else {
                            $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        }
                        //$('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        i++;
                    }
                } else {
                    var sisapokok = parseFloat($('.angsuran'+i+'pokokjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''));
                    /*if(sisapokok != 0){
                        if(jmlbayar < sisapokok){
                            $('.angsuran'+i+'angsurpokok').text(number_format((jmlbayar+parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))),0,'','.'));
                            jmlbayar = 0;
                        } else {
                            $('.angsuran'+i+'angsurpokok').text($('.angsuran'+i+'pokokjadwal').text());
                            jmlbayar -= sisapokok;
                        }
                    } else {
                        var sisabunga = parseFloat($('.angsuran'+i+'bungajadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''));
                        if(jmlbayar < sisabunga){
                            $('.angsuran'+i+'angsurbunga').text(number_format((parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))+jmlbayar),0,'','.'));
                            jmlbayar = 0;
                        } else {
                            jmlbayar -= sisabunga;
                            $('.angsuran'+i+'angsurbunga').text($('.angsuran'+i+'bungajadwal').text());
                        }
                    }*/
                    if(bakidebet > 0){
                        var sisapokok = parseFloat($('.angsuran'+i+'pokokjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''));
                        if(jmlbayar < sisapokok){
                            $('.angsuran'+i+'angsurpokok').text(number_format((jmlbayar+parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))),0,'','.'));
                            jmlbayar = 0;
                            bakidebet -= jmlbayar;
                        } else {
                            $('.angsuran'+i+'angsurpokok').text($('.angsuran'+i+'pokokjadwal').text());
                            jmlbayar -= sisapokok;
                            bakidebet -= sisapokok;
                        }
                        generateNote(i,dd,mm,yyyy);
                        if((i-1) == 0){
                            $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        } else {
                            $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        }
                        if(bakidebet == 0){
                            i = parseInt("6");
                        } else {
                            i++;
                        }
                    } else {
                        var sisabunga = parseFloat($('.angsuran'+i+'bungajadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''));
                        if(jmlbayar < sisabunga){
                            $('.angsuran'+i+'angsurbunga').text(number_format((parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))+jmlbayar),0,'','.'));
                            jmlbayar = 0;
                        } else {
                            jmlbayar -= sisabunga;
                            $('.angsuran'+i+'angsurbunga').text($('.angsuran'+i+'bungajadwal').text());
                        }
                        generateNote(i,dd,mm,yyyy);
                        if((i-1) == 0){
                            $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        } else {
                            $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                        }
                        i++;
                    }
                }
            }
            //console.log(daydiff(parseDate('01-03-2016'),parseDate('21-01-2016')));
            i = parseInt("6");
            while(potpokok > 0){
                var sisapokok = parseFloat($('.angsuran'+i+'pokokjadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''));
                if(sisapokok == 0){
                    i++;
                } else {
                    if(potpokok < sisapokok){
                        $('.angsuran'+i+'angsurpokok').text(number_format((parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))+potpokok),0,'','.'));
                        potpokok = 0;
                    } else {
                        potpokok -= sisapokok;
                        $('.angsuran'+i+'angsurpokok').text($('.angsuran'+i+'pokokjadwal').text());
                    }
                    generateNote(i,dd,mm,yyyy);
                    if((i-1) == 0){
                        $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                    } else {
                        $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                    }
                    i++;
                }
            }
            i = parseInt("6");
            while(potbunga > 0){
                var sisabunga = parseFloat($('.angsuran'+i+'bungajadwal').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''));
                if(sisabunga == 0){
                    i++;
                } else {
                    if(potbunga < sisabunga){
                        $('.angsuran'+i+'angsurbunga').text(number_format((parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))+potbunga),0,'','.'));
                        potbunga = 0;
                    } else {
                        potbunga -= sisabunga;
                        $('.angsuran'+i+'angsurbunga').text($('.angsuran'+i+'bungajadwal').text());
                    }
                    generateNote(i,dd,mm,yyyy);
                    if((i-1) == 0){
                        $('.angsuran'+i+'saldopiutang').text(number_format(("12880000000"-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                    } else {
                        $('.angsuran'+i+'saldopiutang').text(number_format((parseFloat($('.angsuran'+(i-1)+'saldopiutang').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurpokok').text().split('.').join(''))-parseFloat($('.angsuran'+i+'angsurbunga').text().split('.').join(''))),0,'','.'));
                    }
                    i++;
                }
            }
        }); 
    });
</script>
  
  </section>
@endsection