@extends('layouts.master')
@section('content')    
<section class="content">
    <div>
        <h2 align="center">DAFTAR JAMINAN</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis Agunan</th>
                    <th>Atas Nama</th>
                    <th>Alamat</th>
                    <th>Keterangan Jaminan</th>
                    <th>Nilai Pasar</th>
                    <th>Nilai Taksasi</th>
                    <th>Ikatan</th>
                    <th>Perjanjian</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($jamkendaraan as $jamkendaraan)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{strtoupper($jamkendaraan->jenis)}}</td>
                    <td>{{strtoupper($jamkendaraan->pemilik)}}</td>
                    <td>{{strtoupper($jamkendaraan->alamat)}}</td>
                    <td>Merk/Type : {{strtoupper($jamkendaraan->merktype)}} 
                        Nopol : {{strtoupper($jamkendaraan->nopolisi)}} 
                        Tahun : {{strtoupper($jamkendaraan->tahun)}} 
                        Warna : {{strtoupper($jamkendaraan->warna)}} </td>
                    <td>{{number_format($jamkendaraan->nilpasar)}}</td>
                    <td>{{number_format($jamkendaraan->niltaksasi)}}</td>
                    <td>{{strtoupper($jamkendaraan->ikatan)}}</td>
                    <td>{{strtoupper($jamkendaraan->perjanjian)}}</td>
                    <td>
                        <div class="timeline-footer">
                            <a href="/kredit.viewangsuran/{{$jamkendaraan->kredit_id}}" class="btn btn-info btn-sm">Lihat Kartu</a>
                            </div>
                    </td>
                </tr>                               
                @endforeach 

            </tbody>
            {{-- <tfoot>
                <th>No. KTP</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Pekerjaan</th>
                <th>Status</th>
            </tfoot> --}}
        </table>
        <a type="button" href="/kredit.jaminan/{{$id}}" class="btn btn-primary col-1">+ Add</a>
    </div>	
</section>
@endsection  