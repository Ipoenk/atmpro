@extends('layouts.master')
@section('content')    
  
<!-- Content Wrapper. Contains page content -->
{{-- <div class="content-wrapper" style="min-height: 1416.81px;"> --}}
  <!-- Content Header (Page header) -->

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Nasabah</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Nasabah Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form action="{{ route('kredit.save') }}" method="post" class="form-horizontal">
      {{ csrf_field() }}
        <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Pribadi</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="niknsb">Nik NSB</label>
              <input type="text" name ="niknsb" id="niknsb" class="form-control" value="{{ $nasabahs->niknsb }}" >
            </div>
            <div class="form-group">
              <label for="namansb">Nama Nasabah</label>
              <input type="text" name ="namansb" id="namansb" class="form-control" value="{{ strtoupper($nasabahs->namansb) }}" disabled>
            </div>
            <div class="form-group">
              <label for="tmplahirnsb">Tempat Lahir</label>
              <input type="text" name="tmplahirnsb" id="tmplahirnsb" class="form-control" value="{{ strtoupper($nasabahs->tmplahirnsb) }}" disabled>
            </div>
            <div class="form-group">
              <label for="tgllahirnsb">Tanggal Lahir</label>
              <input type="text" name ="tgllahirnsb" id="tgllahirnsb" class="form-control" value="{{ date('d-m-Y',strtotime($nasabahs->tgllahirnsb)) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kelaminnsb">Kelamin</label>
              <input type="text" name ="kelaminnsb" id="kelaminnsb" class="form-control" value="{{ strtoupper($nasabahs->kelaminnsb) }}" disabled>
            </div>
            <div class="form-group">
              <label for="agamansb">Agama</label>
              <input type="text" name ="agamansb" id="agamansb" class="form-control" value="{{ strtoupper($nasabahs->agamansb) }}" disabled>
            </div>
            <div class="form-group">
              <label for="alamatnsbktp">Alamat Sesuai KTP</label>
              <input type="text" name ="alamatnsbktp" id="alamatnsbktp" class="form-control" value="{{ strtoupper($nasabahs->alamatnsbktp) }}" disabled>
            </div>
            <div class="form-group">
              <label for="provinsinsbktp">Provinsi Sesuai KTP</label>
              <input type="text" name ="provinsinsbktp" id="provinsinsbktp" class="form-control" value="{{ strtoupper($nasabahs->namaprop) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kabupatennsbktp">Kabupaten Sesuai KTP</label>
              <input type="text" name ="kabupatennsbktp" id="kabupatennsbktp" class="form-control" value="{{ strtoupper($nasabahs->kotanama) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kecamatannsbktp">Kecamatan Sesuai KTP</label>
              <input type="text" name ="kecamatannsbktp" id="kecamatannsbktp" class="form-control" value="{{ strtoupper($nasabahs->camatnama) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kelurahannsbktp">Kelurahan Sesuai KTP</label>
              <input type="text" name ="kelurahannsbktp" id="kelurahannsbktp" class="form-control" value="{{ strtoupper($nasabahs->desanama) }}" disabled>
            </div>
            <div class="form-group">
              <label for="deposnsbktp">Kodepos Sesuai KTP</label>
              <input type="text" name ="kodeposnsbktp" id="kodeposnsbktp" class="form-control" value="{{ strtoupper($nasabahs->kodeposnsbktp) }}" disabled>
            </div>
            <div class="form-group">
              <label for="alamatnsbdom">Alamat Domisili</label>
              <input type="text" name ="alamatnsbdom" id="alamatnsbdom" class="form-control" value="{{ strtoupper($nasabahs->alamatdomisili) }}" disabled>
            </div>
            <div class="form-group">
              <label for="provinsidomisili">Provinsi Domisili</label>
              <input type="text" name ="provinsidomisili" id="provinsidomisili" class="form-control" value="{{ strtoupper($nasabahs->propdom) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kabupatendomisili">Kabupaten Domisili</label>
              <input type="text" name ="kabupatendomisili" id="kabupatendomisili" class="form-control" value="{{ strtoupper($nasabahs->kotadom) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kecamatandomisili">Kecamatan Domisili</label>
              <input type="text" name ="kecamatandomisili" id="kecamatandomisili" class="form-control" value="{{ strtoupper($nasabahs->camatdom) }}" disabled>
            </div>
            <div class="form-group">
              <label for="kelurahandomisili">Kelurahan Domisili</label>
              <input type="text" name ="kelurahandomisili" id="kelurahandomisili" class="form-control" value="{{ strtoupper($nasabahs->desadom) }}" disabled>
            </div>
            <div class="form-group">
              <label for="notelp">No HP</label>
              <input type="text" name ="notelp" id="notelp" class="form-control" value="{{ strtoupper($nasabahs->notelpnsb) }}" disabled>
            </div>
            <div class="form-group">
              <label for="email">Alamat Email</label>
              <input type="text" name ="email" id="email" class="form-control" value="{{ strtoupper($nasabahs->email) }}" disabled>
            </div>
            <div class="form-group">
              <label for="statusperkawinan">Status Pernikahan</label>
              <input type="text" name ="statusperkawinan" id="statusperkawinan" class="form-control" value="{{ strtoupper($nasabahs->statusperkawinan) }}" disabled>
            </div>
            <div class="form-group">
              <label for="namaibukandung">Nama Ibu Kandung</label>
              <input type="text" name = "namaibukandung" id="namaibukandung" class="form-control" value="{{ strtoupper($nasabahs->namaibukandung) }}" disabled>
            </div>
            
          </div>
          <!-- /.card-body -->
        </div>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Penghasilan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="sumberdana">Sumber Dana</label>
              <input type="text" name ="sumberdana" id="sumberdana" class="form-control" value="{{ strtoupper($nasabahs->sumberdana)}}" disabled>
            </div>
            <div class="form-group">
              <label for="pendapatan">Pendapatan</label>
              <input type="text" name = "pendapatan" id="pendapatan" class="form-control" value="{{ number_format($nasabahs->pendapatan) }}" disabled>
            </div>
            <div class="form-group">
              <label for="pengeluaran">Pengeluaran</label>
              <input type="text" name ="pengeluaran" id="pengeluaran" class="form-control" value="{{ number_format($nasabahs->pengeluaran) }}" disabled>
            </div>
            <div class="form-group">
              <label for="tanggungan">Tanggungan</label>
              <input type="text" name ="tanggungan" id="tanggungan" class="form-control" value="{{ number_format($nasabahs->tanggungan) }}" disabled>
            </div>
            <div class="form-group">
              <label for="stsrumah">Status Tempat Tinggal</label>
              <input type="text" name ="stsrumah" id="stsrumah" class="form-control" value="{{ strtoupper($nasabahs->ststempattinggal) }}" disabled>
            </div>

          </div>
        </div>
        <!-- /.card -->
      </div>
      <div class="form-group col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Data Pengajuan Kredit</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group col-sm-8 control-label">
              <label for="no_kredit">Nomor Kredit</label>
              <input type="text" name = "no_kredit" id="no_kredit" style="text-transform:uppercase" class="form-control @error('no_kredit') is-invalid @enderror" value="{{ $no_kredit }}" >
              @error('no_kredit')
              <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
             <small id="no_kredit" class="form-text text-muted">Maksimum 16 karakter</small>

            </div>
            <div class="form-group col-sm-6 control-label">
              <label for="tglkredit">Tanggal Pengajuan</label>
              <input type="date" name = "tglkredit" id="tglkredit"  value="<?php echo date('Y-m-d'); ?>" class="form-control"  >
            </div>
            <div class="form-group col-sm-5 control-label">
              <label for="pinj_pokok">Pokok Pengajuan</label>
              <input type="text" style = "text-align: right" name ="pinj_pokok" id="pinj_pokok" class="form-control" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type = "currency" value="{{ old('pinj_pokok') }}">
            </div>
            <div class="form-group col-sm-5">
              <label for="skbunga">Suku Bunga</label>
              <input type="text" style = "text-align: right" name = "skbunga" id="skbunga" class="form-control" value="{{ old('skbunga') }}">
            </div>
            {{-- <div class="form-group"> --}}
              <div class="form-group col-sm-4 control-label">
                <label for="sistem">Sistem</label>
                <select class="form-control" name = "sistem" id="sistem" value="{{ old('sistem') }}">
                    <option value = "FLAT">FLAT</option>
                    <option value = "BUNGA-BUNGA">BUNGA-BUNGA</option>
                    <option value = "MUSIMAN">MUSIMAN</option>
                </select>
              </div>
              <div class="form-group col-sm-4 control-label">
                <label for="jnsbayar">Pembayaran Pertama</label>
                <select class="form-control" name = "jnsbayar" id="jnsbayar" value="{{ old('jnsbayar') }}">
                    <option value = "0">ADVANCE</option>
                    <option value = "1">ARREAR</option>
                </select>
              </div>
            {{-- </div> --}}
            <div class="form-group col-sm-5 control-label">
              <label for="lama">Lama Kredit</label>
              <input type="text" style = "text-align: right" name = "lama" id="lama" class="form-control" value="{{ old('lama') }}">
            </div>
            <div class="form-group col-sm-6">
              <label for="jatuhtempo">Jatuh Tempo</label>
              <input type="text" name = "jatuhtempo" id="jatuhtempo" class="form-control" value="{{ old('jatuhtempo') }}" readonly>
            </div>
            <div class="form-group col-sm-6">
              <label for="tglmulai">Angsuran Awal</label>
              <input type="text" name = "tglmulai" id="tglmulai" class="form-control" value="{{ old('tglmulai') }}" readonly>
            </div>
            <div class="form-group col-sm-6">
              <label for="tglakhir">Angsuran Akhir</label>
              <input type="text" name ="tglakhir"  id="tglakhir" class="form-control" value="{{ old('tglakhir') }}" readonly>
            </div>
            {{-- <div class="form-group"> --}}
              <div class="form-group col-sm-4 control-label">
                <label for="namaao">Nama AO</label>
                <select class="form-control" name = "namaao" id="namaao" value="{{ old('namaao') }}">
                    <option value = "KANTOR">KANTOR</option>
                    <option value = "WINDO">WINDO</option>
                    <option value = "ALIP">ALIP</option>
                </select>
              </div>
            {{-- </div> --}}
            <div class="form-group control-label">
              <label for="perantara">Nama Perantara</label>
              <input type="text" name ="perantara"  id="perantara" class="form-control" value="{{ old('perantara') }}">
            </div>

          </div>
                    <!-- /.card-body -->
        </div>
        <!-- /.card -->
        
        <!-- /.card -->
      {{-- </div>
      <div class="col-md-6"> --}}
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Perhitungan Kredit</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group col-sm-5">
              <label for="saldopiutang">Saldo Piutang</label>
              <input type="text" style = "text-align: right" name ="saldopiutang" id="saldopiutang" class="form-control" value="{{ 0 }}" readonly>
            </div>
            <div class="form-group col-sm-5">
              <label for="bbt">Saldo BBT</label>
              <input type="text" style = "text-align: right" name ="bbt" id="bbt" class="form-control" value="{{ 0 }}" readonly>
            </div>
            <div class="form-group col-sm-5">
              <label for="totangsuran">Angsuran</label>
              <input type="text" style = "text-align: right" name = "totangsuran" id="totangsuran" class="form-control" value="{{ 0 }}" readonly>
            </div>
            <div class="form-group col-sm-5">
              <label for="angs_pokok">Angsuran Pokok</label>
              <input type="text" style = "text-align: right" name ="angs_pokok" id="angs_pokok" class="form-control" value="{{ 0 }}" readonly>
            </div>
            <div class="form-group col-sm-5">
              <label for="angs_bunga">Angsuran Bunga</label>
              <input type="text" style = "text-align: right" name ="angs_bunga" id="angs_bunga" class="form-control" value="{{ 0 }}" readonly>
            </div>
          </div>
    
          <!-- /.card-body -->
          </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Biaya-Biaya Kredit</h3>
    
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group col-sm-5">
                  <label for="nom_provisi">Provisi</label>
                  <input type="text" style = "text-align: right" name ="nom_provisi"  id="nom_provisi" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="nom_admin">Administrasi</label>
                  <input type="text" style = "text-align: right" name ="nom_admin" id="nom_admin" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="notaris">Biaya Notaris</label>
                  <input type="text" style = "text-align: right" name =  "notaris" id="notaris" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="materai">Materai</label>
                  <input type="text" style = "text-align: right" name ="materai" id="materai" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="assjiwa">Asuransi Jiwa</label>
                  <input type="text" style = "text-align: right" name ="assjiwa" id="assjiwa" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="asuransi">Asuransi Jaminan</label>
                  <input type="text" style = "text-align: right" name ="asuransi" id="asuransi" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="sim_ang">Simpanan Pokok</label>
                  <input type="text" style = "text-align: right" name ="sim_ang" id="sim_ang" class="form-control" value="{{ 0 }}">
                </div>
                <div class="form-group col-sm-5">
                  <label for="bitrans">Biaya Lain-Lain</label>
                  <input type="text" style = "text-align: right" name ="bitrans" id="bitrans" class="form-control" value="{{ 0 }}">
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            {{-- <button class="btn btn-secondary" onclick="tampilangsuran()">Lihat Angsuran</button> --}}
            <div class="card-body">
 
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Kelengkapan Kredit Tujuan Kredit</h3>
    
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="sifatpk">Sifat Perjanjian</label>
                  <select name="sifatpk" id="sifatpk" class="form-control @error('sifatpk') is-invalid @enderror" >
                    <option value="">Pilih Sifat Perjanjian</option>
                    @foreach ($sifatpks as $sifatpks => $value)
                        <option value="{{ $sifatpks->kodeslik }}"> {{ $value }} </option>
                    @endforeach 
                  </select>
                    @error('sifatpk')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                  <label for="jnsguna">Jenis Penggunaan</label>
                  <select name="jnsguna" id="jnsguna" class="form-control @error('jnsguna') is-invalid @enderror" >
                    <option value="">Pilih Jenis Penggunaan</option>
                    @foreach ($jnsgunas as $jnsgunas => $value)
                        <option value="{{ $jnsgunas->kode }}"> {{ $value }} </option>
                    @endforeach 
                  </select>
                    @error('jnsguna')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="terkait">Keterkaitan</label>
                  <select name="terkait" id="terkait" class="form-control @error('terkait') is-invalid @enderror" >
                    <option value="">Pilih Keterkaitan</option>
                    <option value="0">Tidak Terkait</option>
                    <option value="1">Terlait</option>
                  </select>
                    @error('terkait')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="sumberdana">Sumber Dana Pelunasan</label>
                  <select name="sumberdana" id="sumberdana" class="form-control @error('sumberdana') is-invalid @enderror" >
                    <option value="">Pilih Sumber Dana Pelunasan</option>
                    <option value="10">Gaji/Honor</option>
                    <option value="21">Usaha - Subsidi</option>
                    <option value="22">Usaha - Non Subsidi</option>
                    <option value="31">Lainnya - Subsidi</option>
                    <option value="32">Lainnya - Non Subsidi</option>
                  </select>
                    @error('sumberdana')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="prdbayar">Periode Pembayaran</label>
                  <select name="prdbayar" id="prdbayar" class="form-control @error('prdbayar') is-invalid @enderror" >
                    <option value="">Pilih Periode Pembayaran</option>
                    <option value="1">Harian</option>
                    <option value="2">Mingguan</option>
                    <option value="3">Bulanan</option>
                    <option value="4">Triwulan</option>
                    <option value="5">Semester</option>
                    <option value="6">Tahunan</option>
                    <option value="7">Sekaligus</option>
                    <option value="8">Setiap Saat</option>
                  </select>
                    @error('prdbayar')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="goldeb">Golongan Debitur</label>
                  <select name="goldeb" id="goldeb" class="form-control @error('goldeb') is-invalid @enderror" >
                    <option value="">Pilih Golongan Debitur</option>
                    @foreach ($goldebs as $goldebs => $value)
                        <option value="{{ $goldebs }}"> {{ $value }} </option>
                    @endforeach 
                  </select>
                    @error('goldeb')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="sekekonomi">Sektor Ekonomi</label>
                  <select name="sekekonomi" id="sekekonomi" class="form-control @error('sekekonomi') is-invalid @enderror" >
                    <option value="">Pilih Sektor Ekonomi</option>
                    @foreach ($sekekonomis as $sekekonomis => $value)
                        <option value="{{ $sekekonomis }}"> {{ $value }} </option>
                    @endforeach 
                  </select>
                    @error('sekekonomi')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="jnsusaha">Jenis Usaha</label>
                  <select name="jnsusaha" id="jnsusaha" class="form-control @error('jnsusaha') is-invalid @enderror" >
                    <option value="">Pilih Jenis Usaha</option>
                    @foreach ($jnsusahas as $jnsusahas => $value)
                        <option value="{{ $jnsusahas }}"> {{ $value }} </option>
                    @endforeach 
                  </select>
                    @error('jnsusaha')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="goljamin">Golongan Penjamin</label>
                  <select name="goljamin" id="goljamin" class="form-control @error('goljamin') is-invalid @enderror" >
                    <option value="">Pilih Golongan Penjamin</option>
                    @foreach ($goljamins as $goljamins => $value)
                        <option value="{{ $goljamins }}"> {{ $value }} </option>
                    @endforeach 
                  </select>
                    @error('goljamin')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="bidangusaha">Status Usaha</label>
                  <select name="bidangusaha" id="bidangusaha" class="form-control @error('bidangusaha') is-invalid @enderror" >
                    <option value="">Pilih Status Usaha</option>
                    <option value="1">Mikro</option>
                    <option value="2">Kecil</option>
                    <option value="3">Menengah</option>
                    <option value="4">Selain Usaha Mikro, Kecil dan Menengah </option>
                  </select>
                    @error('bidangusaha')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="tujuan">Tujuan Kredit</label>
                  <textarea type="text"  name ="tujuan"  id="tujuan" class="form-control" value="{{ old('tujuan') }}"></textarea>
                </div>
              </div>
            </div>
          </div>
          </div>
          
      </div>   
      <h2>Tabel Angsuran</h2>
      <table class="table table-stripped" id = "tabelangs" name = "tabelangs" border="2">
        <tr>
          <th style="width:17%">Tanggal Angsur</th>
          <th style="width:7%">Ke</th>
          <th style="width:15%">Bayar Pokok</th>
          <th style="width:15%">Bayar Bunga</th>
          <th style="width:15%">Bakidebet</th>  
          <th style="width:15%">BBT</th>  
          <th style="width:15%">Saldo Piutang</th>  
        </tr> 
        
      </table>
    <div id = "viewangsuran" name = "viewangsuran">
        <table class="table table-stripped" id = "tabelAngsuran" name = "tabelAngsuran" border="2">
          <tr>
            <th style="width:17%">Tanggal Angsur</th>
            <th style="width:7%">Ke</th>
            <th style="width:15%">Bayar Pokok</th>
            <th style="width:15%">Bayar Bunga</th>
            <th style="width:15%">Bakidebet</th>  
            <th style="width:15%">BBT</th>  
            <th style="width:15%">Saldo Piutang</th>  
          </tr> 
          
        </table>
      </div>
    <div class="row">
        <div class="col-12">
          <a href="{{route('nasabah') }}" onClick="return confirm('Apakah Anda benar-benar akan kembali?')"class="btn btn-secondary">Kembali</a>
          <button class="btn btn-success float-right" data-toggle="confirmation" data-placement="right" onClick="return confirm('Apakah data pengajuan sudah benar Kredit?')" type="submit">Simpan</button>
          {{-- <a class="btn" data-toggle="confirmation" data-placement="right" data-original-title="" title="">Confirmation on right</a> --}}
          {{-- <input type="submit" value="Save Changes" class="btn btn-success float-right"> --}}
        </div>
      </div>
    </form>
  </section>
  <!-- jQuery -->
  <script src="{{asset('/adminlte')}}/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="{{asset('/adminlte')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('/adminlte')}}/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{asset('/adminlte')}}/js/demo.js"></script>
   <script src="{{ asset('/js/currency.js') }}"></script> 
  <script scr="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script type="text/javascript">
       $(document).ready(function(){
        $(".form-group").focusout(function(){
        var apinpokok = $("#pinj_pokok").val()
        var abbt = $("#bbt").val()
        var xpinjpokok = parseFloat($("#pinj_pokok").val())
        spinjpokok = parseInt(apinpokok.replaceAll(',',''))
        sbbt = parseInt(abbt.replaceAll(',',''))
        var xprsbunga  = parseFloat($("#skbunga").val())
        var xlama = parseInt($("#lama").val())
        var xangspokok = parseInt(spinjpokok / xlama)
        xangpokokfix = xangspokok.toFixed(0)
        $('#angs_pokok').attr("value",formatNumber(xangpokokfix))

        var xangbunga = parseInt(spinjpokok * (xprsbunga/100))
        xangsbungafix = xangbunga.toFixed(0) 
        $('#angs_bunga').attr("value",formatNumber(xangsbungafix))
        
        var xtotangsur = parseInt(xangpokokfix) + parseInt(xangsbungafix)
        $('#totangsuran').attr("value",formatNumber(xtotangsur))

        var xbbt = (spinjpokok) * (xprsbunga/100) * xlama
        xbbtfix = xbbt.toFixed(0)  
        $('#bbt').attr("value",formatNumber(xbbtfix))
        
        var xsaldopiutang = (spinjpokok) + parseInt(xbbtfix)
        $('#saldopiutang').attr("value",formatNumber(xsaldopiutang) )

        var xtglaju = $("#tglkredit").val()
        Date.prototype.addMonths = function (m) {
            var d = new Date(this);
            var years = Math.floor(m / 12);
            var months = m - (years * 12);
            if (years) d.setFullYear(d.getFullYear() + years);
            if (months) d.setMonth(d.getMonth() + months);
            return d;
            
        }
        vjatuhtempo = new Date(xtglaju).addMonths(xlama).toLocaleDateString()
        $('#jatuhtempo').attr("value",vjatuhtempo)

        var vjnsbayar = $("#jnsbayar").val()
        console.log(vjnsbayar)
        if(vjnsbayar > 0){
          // console.log('Masuk Arrear')
          $('#tglmulai').attr("value",new Date(xtglaju).addMonths(1).toLocaleDateString())
          xtglawal = new Date(xtglaju).addMonths(0)
          $('#tglakhir').attr("value",new Date(xtglaju).addMonths(xlama).toLocaleDateString())
          xtglakhir = new Date(xtglaju).addMonths(xlama)
        }
        else{
          // console.log('Masuk Advance')
          // console.log(new Date(xtglaju).addMonth(1).toLocaleDateString())
          $('#tglmulai').attr("value",new Date(xtglaju).toLocaleDateString())
          xtglawal = new Date(xtglaju).addMonths(-1)
          $('#tglakhir').attr("value",new Date(xtglaju).addMonths(xlama-1).toLocaleDateString())
          xtglakhir = new Date(xtglaju).addMonths(xlama-1)
        }
       

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }        

          $('#tabelAngsuran').empty();
          if(xlama > 0 && spinjpokok > 0 && xprsbunga > 0 && sbbt > 0){
            document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
            <tr>
                  <td style="width:17%">`+ new Date(xtglaju).toLocaleDateString() +`</td>
                  <td style="width:7%"></td>
                  <td style="width:15%"></td>
                  <td style="width:15%"></td>
                  <td style="width:15%" align="right">`+ formatNumber($("#pinj_pokok").val()) +`</td>
                  <td style="width:15%" align="right">`+ formatNumber($("#bbt").val()) +`</td>
                  <td style="width:15%" align="right">`+ formatNumber($("#saldopiutang").val()) +`</td>
            </tr>`
            vbaki = spinjpokok
            vbbt = sbbt
            vsaldopiut = xsaldopiutang
            for( i = 1; i < xlama; i++){
                vbaki = vbaki - xangspokok
                vbbt = vbbt - xangbunga
                vsaldopiut = vsaldopiut - (xangspokok + xangbunga)
                xtglangsur = new Date(xtglawal).addMonths(i).toLocaleDateString()
                console.log(xtglangsur)
                document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
                <tr>
                    <td>`+ xtglangsur +`</td>
                    <td>`+i+`</td>
                    <td align="right">`+ formatNumber($("#angs_pokok").val()) +`</td>
                    <td align="right">`+ formatNumber($("#angs_bunga").val()) +`</td>
                    <td align="right">`+ formatNumber(vbaki) +`</td>
                    <td align="right">`+ formatNumber(vbbt) +`</td>
                    <td align="right">`+ formatNumber(vsaldopiut) +`</td>                
                </tr>`
              }
            xangspokokakhir = spinjpokok -  (xangspokok * (xlama - 1))
            xangsbungaakhir = sbbt - (xangbunga * (xlama - 1))
            document.getElementById('tabelAngsuran').insertRow(-1).innerHTML = `
            <tr>
                  <td>`+ new Date(xtglakhir).toLocaleDateString() +`</td>
                  <td>`+xlama+`</td>
                  <td align="right">`+formatNumber(xangspokokakhir)+`</td>
                  <td align="right">`+formatNumber(xangsbungaakhir)+`</td>
                  <td align="right">`+ 0 +`</td>
                  <td align="right">`+ 0 +`</td>
                  <td align="right">`+ 0 +`</td>
            </tr>`
        };       
      })
    });
    // var pendapatan = document.getElementById('pendapatan');
    // var currentDate = moment('2015-10-30');
    // var futureMonth = moment(currentDate).add(1, 'M');
    // var futureMonthEnd = moment(futureMonth).endOf('month');

    // if(currentDate.date() != futureMonth.date() && futureMonth.isSame(futureMonthEnd.format('YYYY-MM-DD'))) {
    //   futureMonth = futureMonth.add(1, 'd');
    // }
</script>
@endsection