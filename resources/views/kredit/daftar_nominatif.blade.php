@extends('layouts.master')
@section('content')
    <section class="content">
        <form action="{{route('kredit.nominatif')}}" methode ="GET">
         @csrf
        <div>
			<h2 align="center">DAFTAR NOMINATIF</h2>
			{{-- <p>
			  <a class="btn btn-primary" href="{{route('nasabah.create')}}">ADD Nasabah</a>
            </p>     --}}
            <div class="col-6">
                <label for="tgllap">Tanggal :
                <input type="date" name="inputtgllap" id="inputtgllap" value = {{$tanggalLaporan}} " >
                <button class="btn btn-success " type="submit">Proses</button>
            </label>
            </div>
        </div>
        <div class="container">
            <table id="example1" class="table table-bordered table-striped table-condensed" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Kredit</th>
                        <th>Nama Debitur</th>
                        <th>Plafon Akad</th>
                        <th>BBT Awal</th>
                        <th>Suku Bunga</th>
                        <th>Lama</th>
                        <th>Sistem</th>
                        <th>Plafon</th>
                        <th>Bakidebet</th>
                        <th>Saldo BBT</th>
                        <th>Saldo Piutang</th>
                        <th>Tgl Kredit</th>
                        <th>Tgl Awal Angsur</th>
                        <th>Tgl Akhir Angsur</th>
                        <th>Tgl Jatuh Tempo</th>
                        <th>Angsuran Pokok</th>
                        <th>Angsuran Bunga</th>
                        <th>Tgk Pokok</th>
                        <th>Tgk Bunga</th>
                        <th>Lama Tgk Pokok</th>
                        <th>Lama Tgk Bunga</th>
                        <th>Provisi</th>
                        <th>Administrasi</th>
                        <th>Materai</th>
                        <th>Notaris</th>
                        <th>Asuransi Jiwa</th>
                        <th>Asuransi Jaminan</th>
                        <th>Simpanan Anggota</th>
                        <th>Biaya Lain</th>
                        <th>Kolek</th>
                        <th>Nama Ao</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach ($nominatif as $nominatif)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $nominatif->no_kredit }}</td>
                        <td>{{strtoupper($nominatif->namansb)}}</td>
                        <td>{{number_format($nominatif->pinj_pokok)}}</td>
                        <td>{{number_format($nominatif->bbt)}}</td>
                        <td>{{number_format($nominatif->pinj_prsbunga,2)}}</td>
                        <td>{{number_format($nominatif->lama)}}</td>
                        <td>{{strtoupper($nominatif->sistem)}}</td>
                        <td>{{number_format($nominatif->pinj_pokok)}}</td>
                        <td>{{number_format($nominatif->pinj_pokok-$nominatif->pokokdibayar)}}</td>
                        <td>{{number_format($nominatif->bbt-$nominatif->bungadibayar )}}</td>
                        <td>{{number_format(($nominatif->pinj_pokok + $nominatif->bbt))-($nominatif->pokokdibayar+$nominatif->bungadibayar)}}</td>
                        <td>{{date('d-m-Y',strtotime($nominatif->tgl_nominatif))}}</td>
                        <td>{{date('d-m-Y',strtotime($nominatif->tgl_mulai))}}</td>
                        <td>{{date('d-m-Y',strtotime($nominatif->tgl_akhir))}}</td>
                        <td>{{date('d-m-Y',strtotime($nominatif->jatuhtempo))}}</td>
                        <td>{{number_format(0)}}</td>
                        <td>{{number_format(0)}}</td>
                        <td>{{number_format(0)}}</td>
                        <td>{{number_format(0)}}</td>
                        <td>{{number_format(0)}}</td>
                        <td>{{number_format(0)}}</td>
                        <td>{{number_format($nominatif->nom_provisi)}}</td>
                        <td>{{number_format($nominatif->nom_adm)}}</td>
                        <td>{{number_format($nominatif->nom_meterai)}}</td>
                        <td>{{number_format($nominatif->nom_notaris)}}</td>
                        <td>{{number_format($nominatif->assjiwa)}}</td>
                        <td>{{number_format($nominatif->nom_asuransi)}}</td>
                        <td>{{number_format($nominatif->sim_ang)}}</td>
                        <td>{{number_format($nominatif->nom_trans)}}</td>
                        <td>Lancar  </td>
                        <td>{{strtoupper($nominatif->namaao)}}</td>
                        {{-- <td>
                            <div class="timeline-footer">
                                <a href="/nasabah.detail/{{$datanasabahs->id}}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/kredit.create/{{$datanasabahs->id}}" class="btn btn-danger btn-sm">Kredit</a>
                                <a class="btn btn-warning btn-sm">Saving</a>
                                <a class="btn btn-success btn-sm">Depo</a>
                                </div>
                        </td> --}}
                    </tr>                               
                    @endforeach 

                </tbody>
                {{-- <tfoot>
                    <th>No. KTP</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Pekerjaan</th>
                    <th>Status</th>
                </tfoot> --}}
            </table>
        </div>
    </form>	
    </section>
@endsection
{{-- <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin-lte.js') }}"></script> --}}
{{-- @push('scripts')
<script type="text/javascript">
    // $(document).ready(function(){
        $(".inputtgllap").focusout(function(){
            var tgllapor = $("#inputtgllap").val()
            console.log(tgllapor)
        });
            
//  });
 
</script>
@stack('scripts')
@endpush         --}}