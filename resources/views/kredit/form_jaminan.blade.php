@extends('layouts.master')
@section('content')    

<div class="container">
<form action="/kredit.jaminansimpan/{{ $id }}") }}" method="POST" class="form-horizontal" >
  @csrf
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Jaminan</h1>
        </div>
        <input type="hidden" name = "idkredit" id = "idkredit" value = "{{ $id }}">
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            {{-- <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active "><a href="#"onclick="hidup()"> Nasabah Edit</a></li> --}}
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <div class="form-group col-sm-6">
    <label class="col-sm-6 control-label" for="jenisjam">Jenis Jaminan</label>
    <div>
        <select class="form-control @error('jenisjam') is-invalid @enderror" name = "jenisjam" id = "jenisjam" onchange="showDiv('jaminankend','jaminansert','jaminanemas','jaminantab','jaminandep','ikatanpkkend','ikatanpksert','ikatanpkemas','ikatanpktab','ikatanpkdep', this)">
            <option value="">Pilih Jenis Jaminan</option>
            <option value="KENDARAAN">KENDARAAN</option>
            <option value="SERTIFIKAT">SERTIFIKAT</option>
            <option value="EMAS">EMAS</option>
            <option value="TABUNGAN">TABUNGAN</option>
            <option value="DEPOSITO">DEPOSITO</option>
        </select>
        @error('jenisjam')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
  </div>

  <div class="row">
      <div class="col-md-6">
            <div name ="jaminankend" id ="jaminankend" style="display: block">
              <div class="card card-primary">
                  <div class="card-header">
                      <h3 class="card-title">Jaminan Kendaraan</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        </div>
                  </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="anjaminankend">Kendaraan Atas Nama</label>
                            <input type="text" style="text-transform:uppercase" name ="anjaminankend" id="anjaminankend" class="form-control @error('anjaminankend') is-invalid @enderror" value="{{ old(anjaminan) }}">
                            @error('anjaminankend')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="alamatkend">Alamat</label>
                            <input type="text" style="text-transform:uppercase" name ="alamatkend" id="alamatkend" class="form-control  @error('alamatkend') is-invalid @enderror" value="{{ old(alamatkend) }}">
                            @error('alamatkend')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kotajaminankend">Kabupaten/Kodya</label>
                            <input type="text" style="text-transform:uppercase" name ="kotajaminankend" id="kotajaminankend" class="form-control @error('kotajaminankend') is-invalid @enderror" value="{{ old(kotajaminankend) }}">
                            @error('kotajaminankend')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="merktype">Merk/Type</label>
                            <input type="text" style="text-transform:uppercase" name ="merktype" id="merktype" class="form-control @error('merktype') is-invalid @enderror" value="{{ old(merktype) }}">
                            @error('merktype')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="tahun">Tahun Pembuatan</label>
                            <input type="text" name ="tahun" id="tahun" class="form-control @error('tahun') is-invalid @enderror" value="{{ old(tahun) }}">
                            @error('tahun')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="warna">Warna</label>
                            <input type="text" style="text-transform:uppercase" name ="warna" id="warna" class="form-control  @error('warna') is-invalid @enderror" value="{{ old(warna) }}">
                            @error('warna')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nopol">Nomor Polisi</label>
                            <input type="text" style="text-transform:uppercase" name ="nopol" id="nopol" class="form-control @error('nopol') is-invalid @enderror" value="{{ old(nopol) }}">
                            @error('nopol')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nobpkb">Nomor BPKB</label>
                            <input type="text" name ="nobpkb" id="nobpkb" class="form-control @error('nobpkb') is-invalid @enderror" value="{{ old(nobpkb) }}">
                            @error('nobpkb')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="norangka">Nomor Rangka</label>
                            <input type="text" name ="norangka" id="norangka" class="form-control @error('norangka') is-invalid @enderror" value="{{ old(norangka) }}">
                            @error('norangka')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nomesin">Nomor Mesin</label>
                            <input type="text" name ="nomesin" id="nomesin" class="form-control @error('nomesin') is-invalid @enderror" value="{{ old(nomesin) }}">
                            @error('nomesin')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nilpasar">Nilai Pasar</label>
                            <input type="text" name ="nilpasar" id="nilpasar" class="form-control @error('nilpasar') is-invalid @enderror" value="{{ old(nilpasar) }}">
                            @error('nilpasar')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="niltaksasi">Nilai Taksasi</label>
                            <input type="text" name ="niltaksasi" id="niltaksasi" class="form-control @error('nilpasar') is-invalid @enderror" value="{{ old(niltaksasi) }}">
                            @error('niltaksasi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
              </div>
            </div>
            <div name ="jaminansert" id ="jaminansert" style="display: none">
              <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Jaminan Setifikat</h3>
                      <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="anjaminansert"Sertifikat Atas Nama</label>
                          <input type="text" style="text-transform:uppercase" name ="anjaminansert" id="anjaminansert" class="form-control @error('anjaminansert') is-invalid @enderror" value="{{ old(anjaminansert) }}">
                          @error('anjaminansert')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="alamatsert">Alamat</label>
                          <input type="text" style="text-transform:uppercase" name ="alamatsert" id="alamatsert" class="form-control @error('alamatsert') is-invalid @enderror" value="{{ old(alamatsert) }}">
                          @error('alamatsert')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="kotajaminansert">Kabupaten/Kodya</label>
                          <input type="text" style="text-transform:uppercase" name ="kotajaminansert" id="kotajaminansert" class="form-control @error('kotajaminansert') is-invalid @enderror" value="{{ old(kotajaminansert) }}">
                          @error('kotajaminansert')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="alamatlokasisert">Alamat Lokasi</label>
                          <input type="text" style="text-transform:uppercase" name ="alamatlokasisert" id="alamatlokasisert" class="form-control @error('alamatlokasisert') is-invalid @enderror" value="{{ old(alamatlokasisert) }}">
                          @error('alamatlokasisert')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="kotalokasisert">Kabupaten/Kodya Lokasi</label>
                          <input type="text" style="text-transform:uppercase" name ="kotalokasisert" id="kotalokasisert" class="form-control @error('kotalokasisert') is-invalid @enderror" value="{{ old(kotalokasisert) }}">
                          @error('kotalokasisert')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="luastanah">Luas Tanah</label>
                          <input type="text" name ="luastanah" id="luastanah" class="form-control @error('luastanah') is-invalid @enderror" value="{{ old(luastanah) }}">
                          @error('luastanah')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="luasbangunan">Luas Bangunan</label>
                          <input type="text" name ="luasbangunan" id="luasbangunan" class="form-control @error('luasbangunan') is-invalid @enderror" value="{{ old(luasbangunan) }}">
                          @error('luasbangunan')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control @error('status') is-invalid @enderror" name = "status" = id="status" >
                              <option value="">Pilih Status Sertifikat</option>
                              <option value="SHM">SHM</option>
                              <option value="SHG">SHG</option>
                              <option value="SKMHT">SKMHT</option>
                              <option value="PETOK-D">PETOK-D</option>
                              <option value="AJB">AJB</option>
                            </select>
                            @error('status')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nosert">Nomor Sertifikat</label>
                          <input type="text" name ="nosert" id="nosert" class="form-control @error('nosert') is-invalid @enderror" value="{{ old(nosert) }}">
                          @error('nosert')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="gambar">Gambar Situasi</label>
                          <input type="text" name ="gambar" id="gambar" class="form-control @error('gambar') is-invalid @enderror" value="{{ old(gambar) }}">
                          @error('gambar')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nilpasarsert">Nilai Pasar</label>
                          <input type="text" name ="nilpasarsert" id="nilpasarsert" class="form-control @error('nilpasarsert') is-invalid @enderror" value="{{ old(nilpasarsert) }}">
                          @error('nilpasarsert')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="niltaksasisert">Nilai Taksasi</label>
                          <input type="text" name ="niltaksasisert" id="niltaksasisert" class="form-control @error('niltaksasisert') is-invalid @enderror" value="{{ old(niltaksasisert) }}">
                          @error('niltaksasisert')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nilnjop">Nilai NJOP</label>
                          <input type="text" name ="nilnjop" id="nilnjop" class="form-control @error('nilnjop') is-invalid @enderror" value="{{ old(nilnjop) }}">
                          @error('nilnjop')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nilht">Nilai Hak Tanggung</label>
                          <input type="text" name ="nilht" id="nilht" class="form-control @error('nilht') is-invalid @enderror" value="{{ old(nilht) }}">
                          @error('nilht')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
              </div>
            </div>
            <div name ="jaminanemas" id ="jaminanemas" style="display: none">
              <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Jaminan Emas</h3>
                      <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="anjaminanemas">Atas Nama Jaminan</label>
                          <input type="text" style="text-transform:uppercase" name ="anjaminanemas" id="anjaminanemas" class="form-control @error('anjaminanemas') is-invalid @enderror" value="{{ old(anjaminanemas) }}">
                          @error('anjaminanemas')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="alamatemas">Alamat</label>
                          <input type="text" style="text-transform:uppercase" name ="alamatemas" id="alamatemas" class="form-control @error('alamatemas') is-invalid @enderror" value="{{ old(alamatemas) }}">
                          @error('alamatemas')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="kotajaminanemas">Kabupaten/Kodya</label>
                          <input type="text" style="text-transform:uppercase" name ="kotajaminanemas" id="kotajaminanemas" class="form-control @error('kotajaminanemas') is-invalid @enderror" value="{{ old(kotajaminanemas) }}">
                          @error('kotajaminanemas')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nosurat">Nomor Surat</label>
                          <input type="text" name ="nosurat" id="nosurat" class="form-control @error('nosurat') is-invalid @enderror" value="{{ old(nosurat) }}">
                          @error('nosurat')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="namatoko">Nama Toko</label>
                          <input type="text" style="text-transform:uppercase" name ="namatoko" id="namatoko" class="form-control @error('namatoko') is-invalid @enderror" value="{{ old(namatoko) }}">
                          @error('namatoko')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="berat">Berat</label>
                          <input type="text" name ="berat" id="berat" class="form-control @error('berat') is-invalid @enderror" value="{{ old(berat) }}">
                          @error('berat')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="karat">Karat</label>
                          <input type="text" name ="karat" id="karat" class="form-control @error('karat') is-invalid @enderror" value="{{ old(karat) }}">
                          @error('karat')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nilpasaremas">Nilai Pasar</label>
                          <input type="text" name ="nilpasaremas" id="nilpasaremas" class="form-control @error('nilpasaremas') is-invalid @enderror" value="{{ old(nilpasaremas) }}">
                          @error('nilpasaremas')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="niltaksasiemas">Nilai Taksasi</label>
                          <input type="text" name ="niltaksasiemas" id="niltaksasiemas" class="form-control @error('niltaksasiemas') is-invalid @enderror" value="{{ old(niltaksasiemas) }}">
                          @error('niltaksasiemas')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
              </div>
            </div>
            <div name ="jaminantab" id ="jaminantab" style="display: none">
              <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Jaminan Tabungan</h3>
                      <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="anjaminantab">Rekening Atas Nama</label>
                          <input type="text" style="text-transform:uppercase" name ="anjaminantab" id="anjaminantab" class="form-control @error('anjaminantab') is-invalid @enderror" value="{{ old(anjaminantab) }}">
                          @error('anjaminantab')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="alamattab">Alamat</label>
                          <input type="text" style="text-transform:uppercase" name ="alamattab" id="alamattab" class="form-control @error('alamattab') is-invalid @enderror" value="{{ old(alamattab) }}">
                          @error('alamattab')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="kotajaminantab">Kabupaten/Kodya</label>
                          <input type="text" style="text-transform:uppercase" name ="kotajaminantab" id="kotajaminantab" class="form-control @error('kotajaminantab') is-invalid @enderror" value="{{ old(kotajaminantab) }}">
                          @error('kotajaminantab')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="norekening">Nomor Rekening</label>
                          <input type="text" name ="norekening" id="norekening" class="form-control @error('norekening') is-invalid @enderror" value="{{ old(norekening) }}">
                          @error('norekening')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="namabank">Nama Bank</label>
                          <input type="text" style="text-transform:uppercase" name ="namabank" id="namabank" class="form-control @error('namabank') is-invalid @enderror" value="{{ old(namabank) }}">
                          @error('namabank')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nominaltab">Nominal</label>
                          <input type="text" name ="nominaltab" id="nominaltab" class="form-control @error('nominaltab') is-invalid @enderror" value="{{ old(nominaltab) }}">
                          @error('nominaltab')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nilpasartab">Nilai Pasar</label>
                          <input type="text" name ="nilpasartab" id="nilpasartab" class="form-control @error('nilpasartab') is-invalid @enderror" value="{{ old(nilpasartab) }}">
                          @error('nilpasartab')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="niltaksasitab">Nilai Taksasi</label>
                          <input type="text" name ="niltaksasitab" id="niltaksasitab" class="form-control @error('niltaksasitab') is-invalid @enderror" value="{{ old(niltaksasitab) }}">
                          @error('niltaksasitab')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
              </div>
            </div>
            <div name ="jaminandep" id ="jaminandep" style="display: none">
              <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Jaminan Deposito</h3>
                      <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="anjaminandep">Deposito Atas Nama</label>
                          <input type="text" style="text-transform:uppercase" name ="anjaminandep" id="anjaminandep" class="form-control @error('anjaminandep') is-invalid @enderror" value="{{ old(anjaminandep) }}">
                          @error('anjaminandep')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="alamatdep">Alamat</label>
                          <input type="text" style="text-transform:uppercase" name ="alamatdep" id="alamatdep" class="form-control @error('alamatdep') is-invalid @enderror" value="{{ old(alamatdep) }}">
                          @error('alamatdep')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="kotajaminandep">Kabupaten/Kodya</label>
                          <input type="text" style="text-transform:uppercase" name ="kotajaminandep" id="kotajaminandep" class="form-control @error('kotajaminandep') is-invalid @enderror" value="{{ old(kotajaminandep) }}">
                          @error('kotajaminandep')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nobilyet">Nomor Deposito / Bilyet</label>
                          <input type="text" name ="nobilyet" id="nobilyet" class="form-control @error('nobilyet') is-invalid @enderror" value="{{ old(nobilyet) }}">
                          @error('nobilyet')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="namabankdep">Nama Bank</label>
                          <input type="text" style="text-transform:uppercase" name ="namabankdep" id="namabankdep" class="form-control @error('namabankdep') is-invalid @enderror" value="{{ old(namabankdep) }}">
                          @error('namabankdep')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nominaldep">Nominal</label>
                          <input type="text" name ="nominaldep" id="nominaldep" class="form-control @error('nominaldep') is-invalid @enderror" value="{{ old(nominaldep) }}">
                          @error('nominaldep')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="lamadep">Jangka Waktu</label>
                          <input type="text" name ="lamadep" id="lamadep" class="form-control @error('lamadep') is-invalid @enderror" value="{{ old(lamadep) }}">
                          @error('lamadep')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="nilpasardep">Nilai Pasar</label>
                          <input type="text" name ="nilpasardep" id="nilpasardep" class="form-control @error('nilpasardep') is-invalid @enderror" value="{{ old(nilpasardep) }}">
                          @error('nilpasardep')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="form-group">
                          <label for="niltaksasidep">Nilai Taksasi</label>
                          <input type="text" name ="niltaksasidep" id="niltaksasidep" class="form-control @error('niltaksasidep') is-invalid @enderror" value="{{ old(niltaksasidep) }}">
                          @error('niltaksasidep')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                  </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label" for="jenisjam">Jenis Perjanjian</label>
              <div>
                  <select class="form-control @error('jenispk') is-invalid @enderror" name = "jenispk" id = "jenispk" value = "{{ old(jenispk) }}">
                      <option value="">Pilih Jenis Perjanjian Kredit</option>
                      <option value="INTERN">INTERN</option>
                      <option value="NOTARIIL">NOTARIIL</option>
                  </select>
                  @error('jenispk')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
            </div>
            <div name = "ikatanpkkend" id = "ikatanpkkend" style="display: block">
              <div class="form-group">
                <label class="control-label" for="ikatankenppk">Ikatan Hukum</label>
                <div>
                    <select class="form-control @error('ikatankenppk') is-invalid @enderror" name = "ikatankenppk" id = "ikatankenppk" value = "{{ old(ikatankenppk) }}">
                        <option value="">Pilih Jenis Perjanjian Kredit</option>
                        <option value="INTERN">FIDUSIA</option>
                    </select>
                    @error('ikatankenppk')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
              </div>
            </div>
            <div name = "ikatanpksert" id = "ikatanpksert" style="display: none">
              <div class="form-group">
                <label class="control-label" for="ikatansertpk">Ikatan Hukum</label>
                <div>
                    <select class="form-control @error('ikatansertpk') is-invalid @enderror" name = "ikatansertpk" id = "ikatansertpk" value = "{{ old(ikatansertpk) }}">
                        <option value="">Pilih Jenis Perjanjian Kredit</option>
                        <option value="SKMHT">SKMHT</option>
                        <option value="APHT">APHT</option>
                    </select>
                    @error('ikatansertpk')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
              </div>
            </div>
            <div name = "ikatanpkemas" id = "ikatanpkemas" style="display: none">
              <div class="form-group">
                <label class="control-label" for="ikatanemaspk">Ikatan Hukum</label>
                <div>
                    <select class="form-control @error('ikatanemaspk') is-invalid @enderror" name = "ikatanemaspk" id = "ikatanemaspk" value = "{{ old(ikatansertpk) }}">
                        <option value="">Pilih Jenis Perjanjian Kredit</option>
                        <option value="SURAT GADAI">SURAT GADAI</option>
                    </select>
                    @error('ikatanemaspk')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
              </div>
            </div>
            <div name = "ikatanpktab" id = "ikatanpktab" style="display: none">
              <div class="form-group">
                <label class="control-label" for="ikatantabpk">Ikatan Hukum</label>
                <div>
                    <select class="form-control @error('ikatantabpk') is-invalid @enderror" name = "ikatantabpk" id = "ikatantabpk" value = "{{ old(ikatantabpk) }}">
                        <option value="">Pilih Jenis Perjanjian Kredit</option>
                        <option value="SURAT GADAI">SURAT GADAI</option>
                    </select>
                    @error('ikatantabpk')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
              </div>
            </div>
            <div name = "ikatanpkdep" id = "ikatanpkdep" style="display: none">
              <div class="form-group">
                <label class="control-label" for="ikatandeppk">Ikatan Hukum</label>
                <div>
                    <select class="form-control @error('ikatandeppk') is-invalid @enderror" name = "ikatandeppk" id = "ikatandeppk" value = "{{ old(ikatandeppk) }}">
                        <option value="">Pilih Jenis Perjanjian Kredit</option>
                        <option value="SURAT GADAI">SURAT GADAI</option>
                    </select>
                    @error('ikatandeppk')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
              </div>
            </div>
        </div>      
  </div>  
  <div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <button class="btn btn-primary mr-1 col-1" onClick="return confirm('Apakah data Jaminan sudah benar?')"type="submit">Simpan</button>
        {{-- <input type="submit" value="Simpan" class="btn btn-default"> --}}
        <a href="{{ Route('nasabah') }}" onClick="return confirm('Apakah Anda benar-benar keluar?')" class="btn btn-danger col-1">Kembali</a>
    </div>
  </div>
</form>
</div>
<script>
       function showDiv(divId1,divId2,divId3,divId4,divId5,divId6,divId7,divId8,divId9,divId10, element)
            {
                document.getElementById(divId1).style.display = element.value == 'KENDARAAN' ? 'block' : 'none';
                document.getElementById(divId2).style.display = element.value == 'SERTIFIKAT' ? 'block' : 'none';
                document.getElementById(divId3).style.display = element.value == 'EMAS' ? 'block' : 'none';
                document.getElementById(divId4).style.display = element.value == 'TABUNGAN' ? 'block' : 'none';
                document.getElementById(divId5).style.display = element.value == 'DEPOSITO' ? 'block' : 'none';
                document.getElementById(divId6).style.display = element.value == 'KENDARAAN' ? 'block' : 'none';
                document.getElementById(divId7).style.display = element.value == 'SERTIFIKAT' ? 'block' : 'none';
                document.getElementById(divId8).style.display = element.value == 'EMAS' ? 'block' : 'none';
                document.getElementById(divId9).style.display = element.value == 'TABUNGAN' ? 'block' : 'none';
                document.getElementById(divId10).style.display = element.value == 'DEPOSITO' ? 'block' : 'none';

            }
 
</script>
@endsection