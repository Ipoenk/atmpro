@extends('layouts.master')
@section('content')    
<section class="content">
<form action="{{route('kredit.lapangsuran')}}" methode="post">
    @csrf
    <div>
        <h2 align="center">DAFTAR ANGSURAN</h2>
    </div>

    <div class="row">
        <div class="col-15 col-sm-2 control-label">
          <label class="serif" >Periode Laporan </label>
        </div>
        <label>:</label>  
        <div class="col-20" >
            <select name ="metodelaporan"  id="metodelaporan" onchange="showDiv('pertanggal','antartanggal','perbulan', this)"> 
                <option value="{{$model}}">{{$ketmodel}}</option>
                <option value="PERTANGGAL">Pertanggal</option>
                <option value="PERBULAN">Perbulan</option>
                <option value="ANTARTANGGAL">Antar Tanggal</option>
            </select>
        </div>
        <div class="col-15 col-sm-2 control-label">
            <button class="btn btn-success " type="submit">Proses</button>
        </div>

    </div>
    <div name = "pertanggal" id ="pertanggal" style="display: block">
        <div class = "form-group col-8">
            <Label>Tanggal :</Label>
            <input type="date" name = "tgl1" id = "tgl1" value = {{ $stgl1 }}> 
        </div>
    </div>

    <div name = "antartanggal" id ="antartanggal" style="display: none">
        <div class = "form-group col-8">
            <Label>Tanggal :</Label>
            <input type="date" name = "attgl1" id = "tgl1" value = {{ $stgl1 }}> 
            <Label>S/d :</Label>
            <input type="date" name = "attgl2" id = "tgl2" value = {{ $stgl2 }}> 
        </div>
    </div>
    <div name = "perbulan" id ="perbulan" style="display: none">
        <div class="row">
            <div class="col-15 col-sm-2 control-label">
              <label class="serif" for="alamatpsh">Bulan </label>
            </div>
            <label>:</label>  
            <div class="col-25">
                <select id="select" name = "bulanlap" id = "bulanlap"> 
                    <option value="{{ $blnlap }}">{{$namabulan}}</option>
                    <option value="1">JANUARI</option>
                    <option value="2">PEBRUARI</option>
                    <option value="3">MARET</option>
                    <option value="4">APRIL</option>
                    <option value="5">MEI</option>
                    <option value="6">JUNI</option>
                    <option value="7">JULI</option>
                    <option value="8">AGUSTUS</option>
                    <option value="9">SEPTEMBER</option>
                    <option value="10">OKTOBER</option>
                    <option value="11">NOPEMBER</option>
                    <option value="12">DESEMBER</option>
                </select>
                
            </div>
            <div class="col-15 col-sm-2 control-label">
                <input type="text" name ="tahunlap" id = "tahunlap">
            </div>
            
        </div>
        </div>


    <div class="container"> 
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tgl Bayar</th>
                    <th>No Kredit</th>
                    <th>Nama Debitur</th>
                    <th>No. Bukti</th>
                    <th>Bayar Pokok</th>
                    <th>Bayar Bunga</th>
                    <th>Total Angsuran</th>
                    <th>Potongan</th>
                    <th>Denda</th>
                    <th>Ongkos</th>
                    <th>Penerimaan Kas</th>
                    <th>Jenis Pembayaran</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
 
                @foreach ($angsuran as $angsuran)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td style="width:8%">{{date('d-m-Y',strtotime($angsuran->tanggal))}}</td>
                    <td>{{ $angsuran->no_kredit }}</td>
                    <td>{{ $angsuran->namansb }}</td>
                    <td>{{ $angsuran->nobukti }}</td>
                    <td>{{number_format($angsuran->pokok)}}</td>
                    <td>{{number_format($angsuran->bunga)}}</td>
                    <td>{{number_format($angsuran->pokok + $angsuran->bunga)}}</td>
                    <td>{{number_format($angsuran->potongan)}}</td>
                    <td>{{number_format($angsuran->denda)}}</td>
                    <td>{{number_format($angsuran->ongkos)}}</td>
                    <td>{{number_format(($angsuran->pokok + $angsuran->bunga-$angsuran->potongan + $angsuran->denda+$angsuran->ongkos))}}</td>
                    <td>{{ $angsuran->cara}}</td>
                    <td>
                        <div class="timeline-footer">
                            <a href="/kredit.viewangsuran/{{$angsuran->id}}" class="btn btn-info btn-sm">Lihat Kartu</a>
                            </div>
                    </td>
                </tr>                               
                @endforeach 

            </tbody>
            {{-- <tfoot>
                <th>No. KTP</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Pekerjaan</th>
                <th>Status</th>
            </tfoot> --}}
        </table>
    </div>	
</form>   
</section>
<script>

           function showDiv(divId1,divId2,divId3, element)
            {
                document.getElementById(divId1).style.display = element.value == 'PERTANGGAL' ? 'block' : 'none';
                document.getElementById(divId2).style.display = element.value == 'ANTARTANGGAL' ? 'block' : 'none';
                document.getElementById(divId3).style.display = element.value == 'PERBULAN' ? 'block' : 'none';

            }

</script>
@endsection  