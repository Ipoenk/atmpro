@extends('layouts.master')
@section('content') 
<div class = "container">
<div class="panel panel-primary" id="panelangsuran">
    <div class="panel-heading"><h4 align="center">KARTU ANGSURAN</h4></div>
        <div class="panel-body">

          {{-- <form class="form-horizontal ng-pristine ng-valid" id="bayarangsuranform" role="form" method="POST" action=""> --}}
            <div class="row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Tanggal Realisasi</label>
                        <div class="input-group date col-sm-8">
                            <input type="text" id="inputTanggal" name="input_tanggal" class="form-control hasDatepicker" value="{{date('d-m-Y',strtotime($datakredit->tgl_kredit))}} " readonly="">
                            {{-- <div class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="input_nama_nasabah" autocomplete="off" value="{{ $datakredit->namansb}}" readonly="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">No kredit</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="input_npp_nasabah" autocomplete="off" value="{{ $datakredit->no_kredit }}" readonly="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Sistem</label>
                        <div class="col-sm-3">
                        <input type="text" class="form-control" name="input_sistem_angsuran" autocomplete="off" value="{{ $datakredit->sistem }}" readonly="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Jatuh Tempo</label>
                        <div class="col-sm-4">                            
                            <input type="text" class="form-control" name="input_tunggakan" autocomplete="off" value="{{date('d-m-Y',strtotime($datakredit->jatuhtempo))}}" readonly="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Angsuran Pokok</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="input_jumlah_pokok" autocomplete="off" value="{{ number_format($datakredit->angsur_pokok) }}" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Angsur Bunga</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="input_jumlah_bunga" autocomplete="off" value="{{ number_format($datakredit->angsur_bunga) }}" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Total Angsuran</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="input_jumlah_total" autocomplete="off" value="{{ number_format($datakredit->angsur_pokok + $datakredit->angsur_bunga) }}" readonly="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-4">
                        {{-- <input type="button" class="btn btn-primary" name="generatesample" value="GENERATE"> --}}
                    </div>
                <div class="col-sm-4">
                                
            </div>
        </div>
    </div>
    </div>
    {{-- </form> --}}
    <hr>
    <div class="row">
        <div class="col-sm-12 alert alert-danger" name="panel" hidden=""></div>
    </div>
    <div class="row">
        <div class="col-sm-12">
                                        </div>
    </div>
    <div class="row" style="overflow: auto;max-height:500px">
        <div class="col-sm-12">
            <table class="table" name="datanasabahtable">
                <thead>
                    </thead><colgroup><col>
                    </colgroup><colgroup span="3"></colgroup>
                    <colgroup span="3"></colgroup>
                    <tbody><tr>
                        <th colspan="3" scope="colgroup" align="center">Data Debitur</th>
                        <th colspan="3" scope="colgroup" align="center">Piutang Kredit</th>
                    </tr>
                
                </tbody><tbody>
                    <tr>
                        <td>No Kredit</td>
                        <td>:</td>
                        <td>{{ $datakredit->no_kredit }}</td>
                        <td>Plafon</td>
                        <td>:</td>
                        <td>{{ number_format($datakredit->pinj_pokok) }}</td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ $datakredit->namansb}}</td>
                        <td>Bunga</td>
                        <td>:</td>
                        <td>{{$datakredit->pinj_prsbunga}}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $datakredit->alamatnsbktp }}</td>                                                                                 </td>
                        <td>Tgl Angsuran</td>
                        <td>:</td>
                        <td>{{date('d-m-Y',strtotime($datakredit->tgl_mulai ))}} s/d {{date('d-m-Y',strtotime($datakredit->tgl_akhir ))}}</td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td>:</td>
                        <td>{{ $datakredit->notelp }}</td>
                        <td>Jangka Waktu</td>
                        <td>:</td>
                        <td>{{ $datakredit->lama }} bulan</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
            <div class="row" style="overflow: auto;max-height:500px">
                <div class="col-sm-12">
                    <table class="table table-bordered" name="histpaymenttable">
                        <thead>
                            </thead><colgroup><col>
                            </colgroup><colgroup span="4"></colgroup>
                            <colgroup span="4"></colgroup>
                            <colgroup span="1"></colgroup>
                            <tbody>
                                <tr>
                                <th colspan="4" scope="colgroup" align="center">Jadwal</th>
                                <th colspan="4" scope="colgroup" align="center">Pembayaran</th>
                                {{-- <th colspan="1" scope="colgroup" align="center">Saldo</th>
                                <th colspan="1" rowspan="3" scope="colgroup" align="center">Denda</th>
                                <th colspan="1" rowspan="3" scope="colgroup" align="center">Bayar Denda</th>
                                <th colspan="1" rowspan="3" scope="colgroup" align="center">Catatan</th> --}}
                            </tr>
                            <tr>
                                <th scope="col" align="center">Tanggal</th>
                                <th scope="col" align="center">Ke</th>
                                <th scope="col" align="center">Pokok</th>
                                <th scope="col" align="center">Bunga</th>
                                <th scope="col" align="center">No Bukti</th>
                                <th scope="col" align="center">Tgl.Bayar</th>
                                <th scope="col" align="center">Pokok</th>
                                <th scope="col" align="center">Bunga</th>
                                {{-- <th scope="col" align="center">Saldo Piutang</th> --}}
                            </tr>
                            {{-- <tr>
                                <th colspan="8" scope="col" align="center"></th>
                                <th scope="col" align="center">12.880.000.000</th>
                            </tr> --}}
                        
                        </tbody>
                        <tbody>

                            @foreach($tabelangsuran as $angsuran)
                            <tr>
                                <td class="angsuran1tgljadwal">{{date('d-m-Y',strtotime($angsuran->tgl_angsur ))}}</td>
                                <td class="angsuran1bayarke">{{ $angsuran->bayar_ke }}</td>
                                <td style="display:none;" class="angsuran1angsuranjadwal"></td>
                                <td class="angsuran1pokokjadwal">{{number_format($angsuran->angs_pokok)}}</td>
                                <td class="angsuran1bungajadwal">{{number_format($angsuran->angs_bunga)}}</td>
                                <td class="angsuran1nobukti"></td>
                                <td class="angsuran1tglangsuran"></td>
                                <td style="display:none;" class="angsuran1angsurandibayar"></td>
                                <td class="angsuran1angsurpokok">{{$angsuran->angsur_pokok}}</td>
                                <td class="angsuran1angsurbunga">{{$angsuran->angsur_bunga}}</td>
                                {{-- <td class="angsuran1saldopiutang"></td>
                                <td class="angsuran1dendakena">0</td>
                                <td class="angsuran1denda">0</td> --}}
                            </tr>

                            @endforeach 
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection