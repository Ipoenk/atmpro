<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIM | ATM</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">   
  <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap5.min.css')}}">   

  
  <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
  </script>

</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('dashboard')}}" class="nav-link">Home</a>
      </li>
       
    </ul>

     
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown" > 
        <div class="image" data-toggle="dropdown" href="#">
          <img src="{{asset('/adminlte')}}/img/user2-160x160.jpg" class="img-circle img-size-32" alt="User Image">
              <!-- <span class="hidden-xs">{{ Auth::user()->name }}</span> -->
        </div>
        <div class="col-sm-1 dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="media">
              <div class="col-sm-2">
                <img src="{{asset('/adminlte')}}/img/user2-160x160.jpg" class="img-circle img-size-32" alt="User Image">
                <!-- <span class="hidden-xs">{{ Auth::user()->name }}</span> -->
              </div>
              <!-- <div class="media-body">
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                 Logout
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>

              </div> -->
            </div>
        </div>
      </li>
     
    </ul>
  </nav>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
   @include('template.sidebar')
    
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
      @yield('content')
    </div>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2020 <a href="http://www.atm28.net">Team - Andalan Tata Manajemen</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script>

<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
</script>
@stack('scripts')

@include('sweetalert::alert')

</body>
</html>
