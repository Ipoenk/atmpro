<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
  <title>Login | SIM-KOP - SISTEM INFORMASI MANAJEMEN KOPERASI</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- VENDOR CSS -->
  <link rel="stylesheet" href="{{asset('loginadm/vendor/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('loginadm/vendor/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('loginadm/vendor/linearicons/style.css')}}">
  <!-- MAIN CSS -->
  <link rel="stylesheet" href="{{asset('loginadm/css/main.css')}}">
  <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  <link rel="stylesheet" href="{{asset('admin/assets/css/demo.css')}}">
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  <!-- ICONS -->
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('loginadm/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('loginadm/img/favicon.png')}}">
</head>
  <!-- @if(session('fail'))
      <div class="alert alert-danger" role="alert">
        <p>{{ session('fail') }}</p>
      </div>
  @endif  -->
  <!-- @if(session('out'))
  <div class="alert alert-warning alert-block" role="alert">
    <p>{{ session('out') }}</p>
  </div>
  @endif -->
<body>
  <!-- WRAPPER -->
  <div id="wrapper">
    <div class="vertical-align-wrap">
      <div class="vertical-align-middle">
        <div class="auth-box ">
          <div class="left">
            <div class="content">
              <div class="header">
                <div class="logo text-center" ><img src="{{asset('loginadm/img/logo_kop.gif')}} "></div>
                <p class="lead"></p>
              </div>
              <form class="form-auth-small" method="POST" action="{{route('otentikasi')}}">
              @csrf
                <div class="form-group">
                  <label for="signin-email" class="control-label sr-only">Email</label>
                  <input name="post_email" type="email" class="form-control" id="signin-email" placeholder="Email">
                </div>
                <div class="form-group">
                  <label for="signin-password" class="control-label sr-only">Password</label>
                  <input name="post_password" type="password" class="form-control" id="signin-password" placeholder="Password">
                </div>
                <div class="form-group clearfix">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                <div class="bottom">
                </div>
              </form>
            </div>
          </div>
          <div class="right">
            <div class="overlay"></div>
            <div class="content text">
              <h1 class="heading">SISTEM INFORMASI MANAJEMEN KOPERASI</h1>
              <p></p>
             
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>

  <!-- END WRAPPER -->
  @include('sweetalert::alert')
</body>

</html>