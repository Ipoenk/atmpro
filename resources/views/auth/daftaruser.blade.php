@extends('layouts.master')
@section('content')
<section class="content">
    <div>
        <h2 align="center">DAFTAR USER</h2>
    </div>
    <div class="container">
        <table id="example1"  class="table table-bordered table-striped table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $users)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{ strtoupper($users->name) }}</td>
                    <td>{{strtoupper($users->level)}}</td>
                    <td>{{strtoupper($users->email)}}</td>
                    <td>{{($users->password)}}</td>
                    <td>
                        <div class="timeline-footer">
                        <a href="/register.detail/{{$users->id}}" class="btn btn-info btn-sm">Lihat Detail</a>
                    </div>
                    </td>
                </tr>                               
                @endforeach 

            </tbody>
        </table>
		<a type="button" href="{{route('register')}}" class="btn btn-primary col-1">+ Add</a>
    </div>	
</section>
@endsection    