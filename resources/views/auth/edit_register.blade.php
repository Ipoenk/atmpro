@extends('layouts.master')
@section('content')
<section class="content">

    
    <form action="/register.update/{{$user->id}}" method="post" class="form-horizontal">
      @csrf
        <div class="container">
          <h1>Edit Register</h1>
          <p>Please fill in this form to create an account.</p>
          <hr>
      
          <label for="Nama"><b>Nama User</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Nama" name="nama" id="nama" value="{{ $user->name }}" required>

          <label for="email"><b>Email</b></label>
          <input class="form-control col-5" type="text" placeholder="Enter Email" name="email" id="email" value="{{ $user->email }}" required>

          <label for="level">Level User</label>
            <select class="form-control col-5" name = "level" id="level" required>
            <option value="{{ $user->role_id }}">{{ $user->level}}</option>
              @foreach ($roles as $roles => $value)
                  <option value="{{ $roles }}"> {{ $value }} </option>
              @endforeach 
          </select>
  
          <label for="kantor">Kantor</label>
          <select class="form-control col-5" name = "kantor" id="kantor" value="{{ old('kantor') }}" required>
            <option value="">Pilih Kantor</option>
            @foreach ($kantors as $kantors => $kantor)
                <option value="{{ $kantor->id_cabang }}"> {{ $kantor->cabang }} </option>
            @endforeach 
        </select>

          <label for="psw"><b>Password</b></label>
          <input class="form-control col-5" type="password" placeholder="Enter Password" name="psw" id="psw" required>
      
          <label for="psw-repeat"><b>Repeat Password</b></label>
          <input class="form-control col-5" type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat" required>
          <hr>
      
          <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
          <button type="submit" onClick="return confirm('Apakah data sudah benar?')" class="registerbtn">Register</button>
        </div>
      
        <div class="container signin">
          <p>Already have an account? <a href="{{ route('login') }}">Sign in</a>.</p>
        </div>
      </form>    
</section>
@endsection